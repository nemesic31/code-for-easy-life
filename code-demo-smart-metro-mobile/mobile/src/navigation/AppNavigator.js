import * as React from 'react';
import 'react-native-gesture-handler';
import { View, Text } from 'react-native';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import SingInScreen from '../screen/SingInScreen';
import HomeScreen from '../screen/HomeScreen';
import Callback from '../screen/CallbackScreen';

const Stack = createStackNavigator();

export default function AppNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="SignIn" component={SingInScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Callback" component={Callback} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

