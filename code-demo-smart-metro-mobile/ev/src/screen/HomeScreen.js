import React, { Component } from 'react';
import {
  Platform,
  Text,
  Linking,
  View,
  StyleSheet,
  TouchableOpacity,
  Touchable,
  Alert,
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { revoke } from 'react-native-app-auth';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataProfile: {},
    };
  }

  async componentDidMount() {
    await this.getData();
    await this.checkAPIEV();
  }

  getData = async () => {
    const token = await AsyncStorage.getItem('@token_Key');
    axios
      .get('https://api-gateway.mea.app/api/profile/auth/users', {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'x-api-key': 'OoyhddNPWpfBklZcmqpytiZxThWMnmMrZfgNbrXS',
        },
      })
      .then(response => {
        const { data } = response;
        this.setState({ dataProfile: data });
      })
      .catch(error => {
        this.setState({ error: 'Something just went wrong' });
        console.log(error);
      });
  };

  logOut = async () => {
    const { navigate } = this.props.navigation;
    const refresh_token = await AsyncStorage.getItem('@refresh_Key');
    const params = new URLSearchParams();
    params.append('token', `${refresh_token}`);
    params.append('token_hint', 'refresh_token');
    params.append('client_id', 'test-client');
    axios
      .post('https://meaid.mea.app/api/mea-id/oauth/revoke', params, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-api-key': 'OoyhddNPWpfBklZcmqpytiZxThWMnmMrZfgNbrXS',
        },
      })
      .then(function (response) {
        console.log(response.data);
        navigate('SignIn');
      })
      .catch(function (error) {
        console.log(error);
        Alert.alert('Log Out Failed!!');
      });
  };

  render() {
    const { navigate } = this.props.navigation;
    const { dataProfile } = this.state;
    return (
      <View style={styles.container}>
        <View
          style={{
            height: '20%',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={styles.TextHeader}>Hello</Text>
        </View>

        <View
          style={{
            height: '80%',
            width: '100%',
            backgroundColor: '#ffffff',
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View>
            {dataProfile ? (
              <View>
                <Text
                  style={{
                    fontSize: 24,
                    textAlign: 'center',
                    marginVertical: 20,
                  }}>
                  name: {dataProfile && dataProfile.displayName}
                </Text>
                <Text
                  style={{
                    fontSize: 18,
                    textAlign: 'center',
                    marginVertical: 5,
                  }}>
                  email : {dataProfile && dataProfile.email}
                </Text>
                <Text
                  style={{
                    fontSize: 18,
                    textAlign: 'center',
                  }}>
                  gender :{dataProfile && dataProfile.gender}{' '}
                </Text>
              </View>
            ) : (
              <Text
                style={{ fontSize: 18, textAlign: 'center', marginVertical: 50 }}>
                Loading ...
              </Text>
            )}
          </View>
          <TouchableOpacity
            onPress={() => this.logOut()}
            style={{
              backgroundColor: 'black',
              width: '70%',
              alignItems: 'center',
              height: 60,
              justifyContent: 'center',
              borderRadius: 10,
              marginVertical: 1,
              marginTop: 50,
            }}>
            <Text style={{ color: '#ffffff', fontSize: 18, fontWeight: 'bold' }}>
              Log Out
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'orange',
  },
  TextHeader: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 36,
  },
  TextSubHeader: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 24,
  },
});
