import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { authorize } from 'react-native-app-auth';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class SingInScreen extends Component {
  onOpenLinkLogIn = async () => {
    const config = {
      redirectUrl: 'evdemo://net.entercorp.evdemo/auth',
      clientId: 'test-client',
      scopes: ['email'],
      serviceConfiguration: {
        authorizationEndpoint: 'https://meaid.mea.app/oauth/authorize',
        tokenEndpoint: 'https://meaid.mea.app/api/mea-id/oauth/token',
      },
      additionalHeaders: {
        'x-api-key': 'OoyhddNPWpfBklZcmqpytiZxThWMnmMrZfgNbrXS',
      },
      additionalParameters: { 'meaid-idp-action': 'login' },
      customHeaders: {
        token: { 'x-api-key': 'OoyhddNPWpfBklZcmqpytiZxThWMnmMrZfgNbrXS' },
      },
    };
    try {
      const result = await authorize(config);
      const { accessToken, refreshToken } = result;
      console.log(result);
      if (accessToken) {
        await this.handleLogin(accessToken, refreshToken);
      } else {
        Alert.alert('login failed');
      }
    } catch (error) {
      console.log(error);
    }
  };

  onOpenLinkRegister = async () => {
    const config = {
      redirectUrl: 'evdemo://net.entercorp.evdemo/auth',
      clientId: 'test-client',
      scopes: ['email'],
      serviceConfiguration: {
        authorizationEndpoint: 'https://meaid.mea.app/oauth/authorize',
        tokenEndpoint: 'https://meaid.mea.app/api/mea-id/oauth/token',
      },
      additionalHeaders: {
        'x-api-key': 'OoyhddNPWpfBklZcmqpytiZxThWMnmMrZfgNbrXS',
      },
      additionalParameters: { 'meaid-idp-action': 'register' },
      customHeaders: {
        token: { 'x-api-key': 'OoyhddNPWpfBklZcmqpytiZxThWMnmMrZfgNbrXS' },
      },
    };
    try {
      const result = await authorize(config);
      const { accessToken, refreshToken } = result;
      if (accessToken) {
        await this.handleLogin(accessToken, refreshToken);
      } else {
        alert('login failed');
      }
    } catch (error) {
      console.log(error);
    }
  };

  handleLogin = async (accessToken, refreshToken) => {
    const { navigate } = this.props.navigation;
    await AsyncStorage.setItem('@token_Key', accessToken);
    await AsyncStorage.setItem('@refresh_Key', refreshToken);
    navigate('Home');
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <View style={{ marginTop: 200 }}>
          <Text style={styles.TextHeader}>E V C S</Text>
          <Text style={styles.TextSubHeader}> EV Charging Station </Text>
        </View>
        <View style={{ marginBottom: 200, width: '100%', alignItems: 'center' }}>
          <TouchableOpacity
            onPress={() => this.onOpenLinkLogIn()}
            style={{
              backgroundColor: '#000000',
              width: '70%',
              alignItems: 'center',
              height: 60,
              justifyContent: 'center',
              borderRadius: 10,
              marginVertical: 5,
            }}>
            <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>
              LogIn
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.onOpenLinkRegister()}
            style={{
              backgroundColor: '#ffffff',
              width: '70%',
              alignItems: 'center',
              height: 60,
              justifyContent: 'center',
              borderRadius: 10,
              marginVertical: 5,
            }}>
            <Text style={{ color: 'orange', fontSize: 18, fontWeight: 'bold' }}>
              Register
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'orange',
  },
  TextHeader: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 72,
  },
  TextSubHeader: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 24,
  },
});
