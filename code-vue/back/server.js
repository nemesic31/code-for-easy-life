const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');
const app = express();

app.use(cors({ origin: '*' , credentials :  true}));
// app.use(cors())
app.use(bodyParser.json())
app.post('/api/get',async function (req, res, next){
  try{
    const consumerKey = '9060fd05-7f68-4673-9682-7891e5b1baeb'
    const consumerSecret = 'g1owWzgQDh7'
    const agentId = req.body.agentId
    // const agentId = '110140105xxxx'
    console.log('------agentId--------')
    console.log(agentId)
    console.log('------agentId--------')
    const responseToken = await axios.get(
      `https://api.egov.go.th/ws/auth/validate?ConsumerSecret=${consumerSecret}&AgentID=${agentId}`
    ,{
      headers:{
        'Consumer-Key' : consumerKey,
        'Content-Type' : 'application/json'
      }

    })
    const Token = responseToken.data.Result
    console.log('------Token--------')
    console.log(Token)
    console.log('------Token--------')
    const appId = req.body.appId
    // const appId = 'b110e42d-583d-4cf4-b1f2-c1a8d7e25bb1'
    console.log('------appId--------')
    console.log(appId)
    console.log('------appId--------')
    const responseUserId = await axios.get(
      `https://api.egov.go.th/ws/dga/dev/czp/core/notification/subscribe/get?appId=${appId}`
      ,{
        headers: {
          'Content-Type': 'application/json',
          'Consumer-Key': consumerKey,
          Token: Token
        }
      }
    )
    const userId = responseUserId.data.result
    console.log('------userId--------')
    console.log(userId)
    console.log('------userId--------')
    res.json({ userId: userId,Token: Token })
  }
  catch(error){
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.headers);
      res.status(error.response.status).send('something wrong')
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
      res.status(500).send('something wrong')
    }
  }
})

app.post('/api/post',async function (req, res, next) {
  try{
    console.log(req.body)
    const consumerKey = '9060fd05-7f68-4673-9682-7891e5b1baeb'
    const Token = req.body.Token
    const responseNotification = await axios.post(
      `https://api.egov.go.th/ws/dga/dev/czp/core/notification/push`
      ,{
        appId : req.body.appId,
        data: [
          {
            message: req.body.message,
            userId: req.body.userId,
            // userId: '5a46bd19-03a0-4dd7-bc73-1c82992e84b9',
            parameter:req.body.parameter,
            // parameter:[],
            sendDateTime: req.body.sendDateTime,
          }, 
        ]
      }
      ,{
        headers: {
          'Content-Type': 'application/json',
          'Consumer-Key': consumerKey,
          Token: Token
        }
      }
    )
    const notification = responseNotification.data
    console.log('------notification--------')
    console.log(notification)
    console.log('------notification--------')
    res.json({ notification : notification })
  }
  catch(error){
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.headers);
      res.status(error.response.status).send('something wrong')
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
      res.status(500).send('something wrong error')
    }
  }
})

app.listen(3000, function(){
  console.log("RESTFUL API IS RUNNING...")
})