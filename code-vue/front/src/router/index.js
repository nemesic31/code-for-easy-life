import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Home from '../views/Home.vue';
import Verify from '../views/verifyView.vue';
import Score from '../views/scoreView.vue';
import Niets from '../views/NIETSView.vue';
import Landing from '../views/landingView.vue';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const routes = [
  {
    path: '/',
    name: 'Landing',
    component: Landing,
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/verify/:subject',
    name: 'Verify',
    component: Verify,
    params: true,
  },
  {
    path: '/score',
    name: 'Score',
    component: Score,
  },
  {
    path: '/niets',
    name: 'Niets',
    component: Niets,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
