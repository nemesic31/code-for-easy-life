import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('access_token') || null,
    examsYear: {},
    studentInfo: {
      CITIZEN_ID: '', FNAME: '', LNAME: '', SCH_NAME: '', PROV_NAME: '', BUREA_NAME: '',
    },
    isLoading: true,
    fullPage: true,
    isValid: false,
    errors: [],
  },
  mutations: {
    retrieveToken(state, token) {
      state.token = token;
    },
    getExamsYear(state) {
      state.examsYear = {};
    },
  },
  actions: {
    retrieveToken() {
      return new Promise((resolve, reject) => {
        axios
          .get(`${process.env.VUE_APP_NIETS_URL}/Exams/Validate`, {
            params: {
              secret: 'P@ssWord',
            },
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then((response) => {
            this.state.token = response.data.Result;

            resolve(response);
            // For mock-up
            // window.czpSdk.ntResponse('{ "data": { "CitizenId": "1100201407547" },
            // "config": [ { "key": "Notification", "value": false } ],
            // "accessToken":"00000000000000000000000000000000" }');
            this.dispatch('getStudentInfo');
            this.dispatch('getExamsYear');
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
    getExamsYear() {
      return new Promise((resolve, reject) => {
        axios
          .get(`${process.env.VUE_APP_NIETS_URL}/Exams/GetExamsYear`, {
            params: {
              citizenId: window.czpSdk.getData().CitizenId,
            },
            headers: {
              'Content-Type': 'application/json',
              Token: this.state.token,
            },
          })
          .then((response) => {
            this.state.isValid = true;
            this.state.errors = [];
            this.state.examsYear = response.data;
            resolve(response);
          })
          .catch((error) => {
            this.state.isValid = false;
            console.log(error);
            reject(error);
          });
      });
    },
    getStudentInfo() {
      return new Promise((resolve, reject) => {
        axios
          .get(`${process.env.VUE_APP_NIETS_URL}/Exams/GetSTDInfo`, {
            params: {
              citizenId: window.czpSdk.getData().CitizenId,
            },
            headers: {
              'Content-Type': 'application/json',
              Token: this.state.token,
            },
          })
          .then((response) => {
            this.state.studentInfo = response.data;
            resolve(response);
            this.state.isLoading = false;
          })
          .catch((error) => {
            this.state.isValid = false;
            this.state.errors.push('ไม่พบข้อมูลผู้เข้าสอบ');
            reject(error);
            this.state.isLoading = false;
          });
      });
    },
  },
  modules: {
  },
});
