import React, { Component } from 'react';
import { CardWallet, CardWalletInfo, CardHistory } from '../components/CardPocket';

export default class Pocket extends Component {
  render() {
    return (
      <div className="flex h-screen">
        <div className="w-1/3 px-3 pt-3">
          <div>
            <CardWallet />
          </div>
        </div>
        <div className="w-2/3 pr-3 py-3">
          <div>
            <CardWalletInfo />
          </div>
          {/* <div className="py-3">
            <CardHistory />
          </div> */}
        </div>
      </div>
    );
  }
}
