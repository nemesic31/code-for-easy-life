import React, { useEffect, useState } from 'react';
import {
  CardButtonTopBar,
  CardToolInfo,
  CardWallet,
  CardUsageInformation,
  CardBuildingInfo,
} from '../components/CardBuildingUser';
import { CardHistory } from '../components/CardHistory';
import { CardOrderList } from '../components/CardOrderList';
import { CardPowerConsumptionGraphUser } from '../components/Graph';
import * as Inventory from '../api/Inventory.script';

const history = [
  {
    contract: 'อาคาร LED',
    transaction: '0x81657A...',
    date: '03/09/2563',
    time: '9.00 - 12.00',
    order: 'ซื้อ',
    kWh: '300',
    unit: '2.50',
    token: '750',
    vat: '0.75',
    price: '750.75',
  },
];

export const DashboardBuildingUser = () => {
  const [lengthResultInventory, setLengthResultInventory] = useState();
  async function getResponseInventory() {
    try {
      const responseTool = await Inventory.getInventory();
      const result = responseTool;
      setLengthResultInventory(result.data.length);
    } catch (error) {
      console.log(error);
    }
  }
  useEffect(() => {
    getResponseInventory();
  }, []);

  return (
    <>
      <div className="p-2">
        <CardButtonTopBar />
      </div>
      <div className="flex mx-2">
        <div className="w-4/12">
          <div>
            <CardBuildingInfo />
          </div>
          <div className="mt-2">{lengthResultInventory !== 0 && <CardToolInfo />}</div>
          <div className="mt-2">
            <CardWallet />
          </div>
          <div className="my-2">
            <CardUsageInformation />
          </div>
        </div>
        <div className="w-8/12 ml-2">
          <div>
            <CardPowerConsumptionGraphUser />
          </div>
          <div className="mt-2">
            <CardOrderList />
          </div>
          <div className="mt-2">
            <CardHistory history={history} />
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardBuildingUser;
