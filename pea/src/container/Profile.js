import React, { useState, useEffect } from 'react';
import { CardBuildingInfo, CardWallet, CardTool } from '../components/CardProfile';
import * as Inventory from '../api/Inventory.script';

export const Profile = () => {
  const [lengthResultInventory, setLengthResultInventory] = useState();
  async function getResponseInventory() {
    const responseTool = await Inventory.getInventory();
    const result = responseTool;
    setLengthResultInventory(result.data.length);
  }
  useEffect(() => {
    getResponseInventory();
  }, []);

  return (
    <div className="flex h-screen">
      <div className="w-1/3 mx-3 mt-3">
        <div>
          <CardBuildingInfo />
        </div>
        <div className="mt-3">
          <CardWallet />
        </div>
      </div>
      <div className="w-2/3 mr-3 mt-3">{lengthResultInventory !== 0 && <CardTool />}</div>
    </div>
  );
};
export default Profile;
