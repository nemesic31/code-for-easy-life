import React from 'react';
import { CardImage, CardRight } from '../components/CardSignIn';

export const SignInScreen = () => (
  <div className="flex justify-center items-center min-h-screen">
    <div className="xl:w-4/6 md:w-5/6">
      <div className="flex w-full h-full bg-white">
        <div className="w-1/2">
          <CardImage />
        </div>
        <div className="w-1/2 px-10 py-10 bg-white h-full flex items-center">
          <CardRight />
        </div>
      </div>
    </div>
  </div>
);

export default SignInScreen;
