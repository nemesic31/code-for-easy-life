import React from 'react';
import loadingPNG from '../resource/loading.png';

export const LoadingScreen = (props) => {
  const { name, id, idTool, userId } = props.location.state;
  const location = {
    pathname: '/DashboardBuildingAdmin',
    state: {
      id: `${id}`,
      idTool: `${idTool}`,
      name: `${name}`,
      userId: `${userId}`,
    },
  };

  setTimeout(() => {
    if (name === 'DSO') {
      props.history.push('/PEAGRID');
    } else {
      props.history.push(location);
    }
  }, 600);
  return (
    <div>
      <div className="w-screen h-screen flex flex-col justify-center items-center ">
        <img src={loadingPNG} alt="loading" className="animate-spin w-48" />
        <div className="mb-20">กำลังดาวน์โหลด...</div>
      </div>
    </div>
  );
};
export default LoadingScreen;
