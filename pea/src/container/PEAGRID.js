import React from 'react';
import { CardElectricSold } from '../components/CardGraphIncome';
import { CardHistory } from '../components/CardHistory';
import { CardTop } from '../components/CardTower';
import { CardGraphIncome } from '../components/Graph';

const history = [
  {
    contract: 'อาคาร LED',
    transaction: 'รหัสสัญญา 0x81657A...',
    date: '03/09/2563',
    time: '9.00 - 12.00',
    order: 'ซื้อ',
    kWh: '300',
    unit: '2.50',
    token: '750',
    vat: '0.75',
    price: '750.75',
  },
  {
    contract: 'อาคาร 10',
    transaction: 'รหัสสัญญา 0x81657A...',
    date: '03/09/2563',
    time: '9.00 - 12.00',
    order: 'ขาย',
    kWh: '300',
    unit: '2.50',
    token: '750',
    vat: '- 0.75',
    price: '749.25',
  },
];

const PEAGRID = () => (
  <div>
    <div className="h-full">
      <div className="min-w-full p-2">
        <CardGraphIncome />
      </div>
      <div className="flex">
        <div className="w-full p-2">
          <CardElectricSold />
        </div>
      </div>
      <div className="min-w-full p-2">
        <CardTop />
      </div>
      <div className="min-w-full p-2">
        <CardHistory history={history} />
      </div>
    </div>
  </div>
);

export default PEAGRID;
