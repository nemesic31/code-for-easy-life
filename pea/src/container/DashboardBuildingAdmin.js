import React, { useState, useEffect } from 'react';
import {
  CardBuildingInfo,
  CardToolInfo,
  CardWallet,
  CardUsageInformation,
} from '../components/CardBuildingAdmin';
import { CardHistoryAdmin } from '../components/CardHistory';
import { CardOrderListAdmin } from '../components/CardOrderList';
import { CardPowerConsumptionGraph } from '../components/Graph';
import * as Admin from '../api/Admin.script';

export const DashboardBuildingAdmin = (props) => {
  const { id, idTool, name, userId } = props.location.state;
  const [inventoryStatus, setInventoryStatus] = useState();
  const [buildingId, setBuildingId] = useState(false);
  async function getResponseAllInventories() {
    try {
      const responseAllInventory = await Admin.getAllInventories();
      const responseAllUser = await Admin.getAllUsers();
      responseAllUser.data.forEach((resUser) => {
        if (resUser.userProfile.length > 0) {
          if (resUser.userProfile[0].name === `${name}`) {
            setBuildingId(resUser.userProfile[0].userId);
            responseAllInventory.data.forEach((resInventory) => {
              if (resInventory.userId === buildingId) {
                setInventoryStatus(true);
              }
            });
          }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseAllInventories();
  }, []);

  return (
    <div className="flex mx-2 ">
      <div className="w-4/12 ">
        <div>
          <CardBuildingInfo id={id} name={name} />
        </div>

        <div className="mt-2">
          <CardToolInfo idTool={idTool} name={name} />
        </div>

        <div className="mt-2">
          <CardWallet name={name} />
        </div>
        <div className="my-2">
          <CardUsageInformation name={name} />
        </div>
      </div>
      <div className="w-8/12 ml-2">
        <div>
          <CardPowerConsumptionGraph />
        </div>
        <div>
          <CardOrderListAdmin name={name} />
        </div>
        <div className="my-2">
          <CardHistoryAdmin userId={userId} />
        </div>
      </div>
    </div>
  );
};

export default DashboardBuildingAdmin;
