import React from 'react';
import CardManageSystem from '../components/CardSettingAdmin';

export const SettingAdmin = () => (
  <div className="min-h-screen">
    <div className="py-3 px-3">
      <CardManageSystem />
    </div>
  </div>
);

export default SettingAdmin;
