import React from 'react';
import { CardBuySystem } from '../components/CardBuySystem';
import { CardGraphPrice } from '../components/Graph';
import { CardOrderList } from '../components/CardOrderList';
import { CardRecommendPrice } from '../components/CardRecommendPrice';

import moment from 'moment';
import 'moment/locale/th';

const recommend = {
  order: 'ซื้อ',
  price: '2.5 - 3.41',
  unit: '3000',
};

export const TradingBuy = () => {
  const start = moment('06:00', 'HH:mm');
  const end = moment('18:00', 'HH:mm');
  const times = moment().isBetween(start, end);

  return (
    <div>
      <div className="px-4 py-4">
        <CardGraphPrice page="ซื้อ" />
      </div>
      <div className="px-4">
        <CardRecommendPrice recommend={recommend} />
      </div>
      <div className="flex py-4 px-4">
        {times ? (
          <div className="w-1/3 pr-2">
            <CardBuySystem />
          </div>
        ) : (
          ''
        )}

        <div className={times ? 'w-2/3 pl-2' : 'w-full'}>
          <CardOrderList />
        </div>
      </div>

      <div className="px-4"></div>
    </div>
  );
};

export default TradingBuy;
