import React, { useEffect, useState } from 'react';
import { CardTower, CardTop } from '../components/CardTower';
import * as Admin from '../api/Admin.script';
import * as Wallet from '../api/Wallet.script';
import * as DSOReconcile from '../api/DSOReconcile.script';
import * as SmartMeter from '../api/SmartMeter.script';
import * as Contract from '../api/Contract.script';

export const DashboardAdmin = () => {
  const [button, setButton] = useState([]);
  const [balance, setBalance] = useState([]);
  const [totalBuy, setTotalBuy] = useState([]);
  const [contractAll, setContractAll] = useState([]);
  const contracts = [];
  const [limitQuantity, setLimitQuantity] = useState([]);

  async function getResponseAllUsers() {
    try {
      const responseAllUser = await Admin.getAllUsers();
      const responseAllWallet = await Wallet.getWalletAdmin();
      const responseMeter = await SmartMeter.getSmartMeterAdmin();
      const responseQuantity = await DSOReconcile.getReconcileAdmin();
      if (responseAllUser.data) {
        setButton(responseAllUser);
        responseAllUser.data.forEach(async (res) => {
          const { userId } = res;
          const responseAllContract = await Contract.getContractAdmin({ userId });
          contracts.push(responseAllContract);
          // console.log(typeof contracts);
        });
      }
      if (responseAllWallet.data) {
        setBalance(responseAllWallet);
      }
      if (responseMeter.data) {
        setTotalBuy(responseMeter);
      }
      if (responseQuantity.data) {
        setLimitQuantity(responseQuantity);
      }
    } catch (error) {
      console.log(error);
    }
  }
  const nameCard = button.data;
  const wallet = balance.data;
  const meter = totalBuy.data;
  const quantity = limitQuantity.data;
  useEffect(() => {
    getResponseAllUsers();
  }, []);
  return (
    <div className="flex flex-wrap p-2 min-h-screen">
      <div className="min-w-full p-2">
        <CardTop />
      </div>
      <div className="w-full p-2">
        <CardTower
          nameCard={nameCard}
          wallet={wallet}
          meter={meter}
          quantity={quantity}
          contracts={contracts}
        />
      </div>
    </div>
  );
};

export default DashboardAdmin;
