import React from 'react';
import { CardGraphProfit } from '../components/Graph';
import { CardHistory } from '../components/CardHistory';

const history = [
  {
    contract: 'อาคาร LED',
    transaction: '0x81657A...',
    date: '03/09/2563',
    time: '9.00 - 12.00',
    order: 'ซื้อ',
    kWh: '300',
    unit: '2.50',
    token: '750',
    vat: '0.75',
    price: '750.75',
  },
  {
    contract: 'อาคาร 10',
    transaction: '0x81657A...',
    date: '03/09/2563',
    time: '9.00 - 12.00',
    order: 'ขาย',
    kWh: '300',
    unit: '2.50',
    token: '750',
    vat: '- 0.75',
    price: '749.25',
  },
];

export const SummaryCustomer = () => (
  <div>
    <div className="px-3 py-3">
      <CardGraphProfit />
    </div>
    <div className="px-3 pb-3">
      <CardHistory history={history} />
    </div>
  </div>
);

export default SummaryCustomer;
