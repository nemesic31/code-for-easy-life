import React from 'react';
import PropTypes from 'prop-types';

const BlankLayout = ({ children }) => <div className="bg-gray-300">{children}</div>;

BlankLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default BlankLayout;
