import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { TabBarTower } from '../components/TabBarTower';
import { NavbarHeader } from '../components/NavbarHeader';
import * as Admin from '../api/Admin.script';
import { SelectDateProvider } from '../store';

const MainLayoutDashboardAdmin = ({ children }) => {
  const [button, setButton] = useState([]);
  async function getResponseAllUsers() {
    try {
      const responseAllUser = await Admin.getAllUsers();
      setButton(responseAllUser);
    } catch (error) {
      console.log(error);
    }
  }

  const nameButton = button.data;
  useEffect(() => {
    getResponseAllUsers();
  }, []);

  return (
    <SelectDateProvider>
      <div className="bg-gray-300 min-h-full">
        <NavbarHeader />
        <TabBarTower path={children.props.location} nameButton={nameButton} />
        <div>{children}</div>
      </div>
    </SelectDateProvider>
  );
};

MainLayoutDashboardAdmin.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainLayoutDashboardAdmin;
