import React from 'react';
import PropTypes from 'prop-types';
import NavbarHeaderUser from '../components/NavbarHeaderUser';

const MainLayoutUser = ({ children }) => (
  <div className="bg-gray-300 min-h-full">
    <NavbarHeaderUser />
    <div>{children}</div>
  </div>
);

MainLayoutUser.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainLayoutUser;
