import React from 'react';
import PropTypes from 'prop-types';
import NavbarHeader from '../components/NavbarHeader';

const MainLayout = ({ children }) => (
  <div className="bg-gray-300 min-h-full">
    <NavbarHeader />
    <div>{children}</div>
  </div>
);

MainLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainLayout;
