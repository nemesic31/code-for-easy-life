import React from 'react';
import swal from 'sweetalert';
import ReactDOM from 'react-dom';
import * as Order from '../api/Order.script';
import * as Admin from '../api/Admin.script';

import { ContentAlertSetting, ContentAlertBuy, ContentAlertSell } from './ContentAlert';

export const alertSettingAdmin = (value) => {
  const wrapper = document.createElement('div');
  ReactDOM.render(<ContentAlertSetting value={value} />, wrapper);
  const el = wrapper.firstChild;

  swal({
    title: 'จัดการระบบ',
    content: el,
    buttons: {
      cancel: {
        text: 'ยกเลิก',
        visible: true,
      },
      confirm: {
        text: 'ยืนยัน',
        visible: true,
      },
    },
  }).then(async (willDelete) => {
    if (willDelete) {
      try {
        const responsePutBuy = await Admin.putConfigsBuy(value);
        const responsePutSell = await Admin.putConfigsSell(value);
        const responsePutFee = await Admin.putConfigsFee(value);
        if (
          responsePutBuy.data !== undefined &&
          responsePutSell.data !== undefined &&
          responsePutFee.data !== undefined
        ) {
          swal('การแก้ไขสำเร็จ', {
            icon: 'success',
            buttons: {
              confirm: {
                text: 'ยืนยัน',
                visible: true,
              },
            },
          }).then(() => window.location.reload());
        } else {
          swal('การแก้ไขล้มเหลว', {
            icon: 'warning',
            buttons: {
              confirm: {
                text: 'ยืนยัน',
                visible: true,
              },
            },
          });
        }
      } catch (error) {
        console.log(error);
      }
    }
  });
};

export const confirmBuy = (value) => {
  const wrapper = document.createElement('div');
  ReactDOM.render(<ContentAlertBuy value={value} />, wrapper);
  const el = wrapper.firstChild;

  swal({
    title: 'คุณต้องการซื้อไฟฟ้า',
    content: el,
    buttons: {
      cancel: {
        text: 'ยกเลิก',
        visible: true,
      },
      confirm: {
        text: 'ยืนยัน',
        visible: true,
      },
    },
  }).then(async (willDelete) => {
    if (willDelete) {
      try {
        const response = await Order.postOrderBuyUser(value);
        if (response.data !== undefined) {
          swal('การซื้อสำเร็จ', {
            icon: 'success',
            buttons: {
              confirm: {
                text: 'ยืนยัน',
                visible: true,
              },
            },
          }).then(() => window.location.reload());
        } else {
          swal('ช่วงเวลาตั้งซื้อซ้ำ', {
            icon: 'warning',
            buttons: {
              confirm: {
                text: 'ยืนยัน',
                visible: true,
              },
            },
          });
        }
      } catch (error) {
        console.log(error);
      }
    }
  });
};

export const confirmSell = (value) => {
  const wrapper = document.createElement('div');
  ReactDOM.render(<ContentAlertSell value={value} />, wrapper);
  const el = wrapper.firstChild;

  swal({
    title: 'คุณต้องการขายไฟฟ้า',
    content: el,
    buttons: {
      cancel: {
        text: 'ยกเลิก',
        visible: true,
      },
      confirm: {
        text: 'ยืนยัน',
        visible: true,
      },
    },
  }).then(async (willDelete) => {
    if (willDelete) {
      const response = await Order.postOrderSellUser(value);
      if (response.data !== undefined) {
        swal('การขายสำเร็จ', {
          icon: 'success',
          buttons: {
            confirm: {
              text: 'ยืนยัน',
              visible: true,
            },
          },
        }).then(() => window.location.reload());
      } else {
        swal('ช่วงเวลาตั้งขายซ้ำ', {
          icon: 'warning',
          buttons: {
            confirm: {
              text: 'ยืนยัน',
              visible: true,
            },
          },
        });
      }
    }
  });
};

export const alertComingSoon = () => {
  swal({
    text: 'เนื้อหานี้ยังไม่พร้อมให้บริการ',
    buttons: false,
    icon: 'error',
  });
};

export default {
  alertSettingAdmin,
  confirmBuy,
  confirmSell,
  alertComingSoon,
};
