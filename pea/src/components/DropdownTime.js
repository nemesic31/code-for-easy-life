import React from 'react';

const DropdownTime = ({ options, selected, onSelectedChange }) => {
  const renderOptions = options.map((option) => {
    if (option.value === selected.value) {
      return null;
    }

    return (
      <option key={option.value} role="menuitem" onClick={() => onSelectedChange(option)}>
        {option.label}
      </option>
    );
  });
  return (
    <div className="xl:w-2/5 md:w-5/12">
      <div>
        <span className="rounded-md">
          <select
            id="startTimes"
            name="startTimes"
            className="flex justify-between w-full rounded-md border border-color-pea-primary px-4 py-2 bg-white xl:text-base md:text-sm leading-5 font-normal text-pea-primary  focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150"
          >
            {renderOptions}
          </select>
        </span>
      </div>
    </div>
  );
};

export default DropdownTime;
