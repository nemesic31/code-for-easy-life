import React, { useState } from 'react';
import { Bar } from 'react-chartjs-2';
import OutsideClickHandler from 'react-outside-click-handler';
import moment from 'moment';
import 'moment/locale/th';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { alertComingSoon } from './SweetAlert';
import { SelectDateConsumer } from '../store/index';

const data = {
  labels: [
    '6.00 - 7.00',
    '7.00 - 8.00',
    '8.00 - 9.00',
    '9.00 - 10.00',
    '10.00 - 11.00',
    '11.00 - 12.00',
    '12.00 - 13.00',
    '13.00 - 14.00',
    '14.00 - 15.00',
    '15.00 - 16.00',
    '16.00 - 17.00',
    '17.00 - 18.00',
  ],
  datasets: [
    {
      label: 'ซื้อจาก DSO',
      data: [12, 19, 0, 5, 2, 0, 0, 6, 0, 0, 10, 0],
      backgroundColor: '#6A1B9A',
    },
    {
      label: 'ซื้อพลังงานกันเอง',
      data: [3, 1, 0, 5, 2, 0, 0, 6, 12, 0, 10, 0],
      backgroundColor: '#D53F8C',
    },
    {
      label: 'ขาย',
      data: [0, 0, -20, 0, 0, -4, -12, 0, 0, -17, 0, -5],
      backgroundColor: '#D69E2E',
    },
  ],
};

const dataIncome = {
  labels: [
    '6.00 - 7.00',
    '7.00 - 8.00',
    '8.00 - 9.00',
    '9.00 - 10.00',
    '10.00 - 11.00',
    '11.00 - 12.00',
    '12.00 - 13.00',
    '13.00 - 14.00',
    '14.00 - 15.00',
    '15.00 - 16.00',
    '16.00 - 17.00',
    '17.00 - 18.00',
  ],
  datasets: [
    {
      label: 'พลังงานที่อาคารซื้อขายกันเอง',
      data: [12, 19, 3, 5, 2, 3, 5, 6, 12, 19, 10, 2],
      backgroundColor: '#D69E2E',
    },
    {
      label: 'พลังงานที่ DSO ขาย',
      data: [2, 3, 20, 5, 1, 4, 12, 7, 1, 17, 11, 5],
      backgroundColor: '#6A1B9A',
    },
  ],
};

const dataProfile = {
  labels: [
    '6.00 - 7.00',
    '7.00 - 8.00',
    '8.00 - 9.00',
    '9.00 - 10.00',
    '10.00 - 11.00',
    '11.00 - 12.00',
    '12.00 - 13.00',
    '13.00 - 14.00',
    '14.00 - 15.00',
    '15.00 - 16.00',
    '16.00 - 17.00',
    '17.00 - 18.00',
  ],
  datasets: [
    {
      label: 'ซื้อ GRID',
      data: [12, 19, 3, 5, 2, 3, 5, 6, 12, 19, 10, 2],
      backgroundColor: '#6A1B9A',
    },
    {
      label: 'ซื้อจาก Prosumer',
      data: [2, 3, 1, 5, 1, 4, 12, 7, 1, 0, 0, 5],
      backgroundColor: '#D53F8C',
    },
  ],
};

const dataPriceWorkday = {
  labels: [
    '6:00',
    '',
    '',
    '',
    '7:00',
    '',
    '',
    '',
    '8:00',
    '',
    '',
    '',
    '9:00',
    '',
    '',
    '',
    '10:00',
    '',
    '',
    '',
    '11:00',
    '',
    '',
    '',
    '12:00',
    '',
    '',
    '',
    '13:00',
    '',
    '',
    '',
    '14:00',
    '',
    '',
    '',
    '15:00',
    '',
    '',
    '',
    '16:00',
    '',
    '',
    '',
    '17:00',
    '',
    '',
    '',
    '18:00',
  ],
  datasets: [
    {
      label: 'ปริมาณการใช้ไฟฟ้า',
      data: [
        '601.90',
        '1046.10',
        '1057.00',
        '1147.80',
        '1119.10',
        '1209.90',
        '1252.10',
        '1472.00',
        '1596.50',
        '1650.50',
        '1644.70',
        '1692.60',
        '1713.00',
        '1746.40',
        '1698.00',
        '1729.00',
        '1701.80',
        '1716.60',
        '1687.30',
        '1670.40',
        '1659.40',
        '1625.50',
        '1649.20',
        '1664.90',
        '1620.10',
        '1640.20',
        '1317.40',
        '1315.90',
        '1339.60',
        '1383.40',
        '1391.70',
        '1379.60',
        '1383.60',
        '1423.00',
        '1361.80',
        '1395.90',
        '1405.70',
        '1329.10',
        '1374.10',
        '1339.80',
        '1352.60',
        '1028.00',
        '1042.30',
        '983.80',
        '956.20',
        '802.80',
        '766.30',
        '777.80',
        '775.60',
      ],
      backgroundColor: 'rgba(127, 63, 152, 0.5)',
      categoryPercentage: 1.0,
      barPercentage: 1.0,
    },
    {
      label: 'ปริมาณการจำหน่ายไฟฟ้า',
      data: [
        '0.64',
        '0.72',
        '1.22',
        '0.59',
        '1.19',
        '1.81',
        '2.42',
        '4.48',
        '5.07',
        '3.44',
        '4.96',
        '8.67',
        '9.95',
        '7.26',
        '7.31',
        '5.14',
        '5.40',
        '3.65',
        '3.37',
        '7.95',
        '5.75',
        '8.81',
        '11.48',
        '16.00',
        '12.95',
        '12.47',
        '18.99',
        '16.30',
        '19.55',
        '15.20',
        '15.93',
        '14.78',
        '13.47',
        '12.58',
        '10.69',
        '8.09',
        '3.47',
        '0',
        '0',
        '0',
        '0',
        '1.40',
        '1.33',
        '1.30',
        '0',
        '0',
        '1.34',
        '0.87',
        '0.39',
      ],
      backgroundColor: 'rgba(217, 119, 6)',
      categoryPercentage: 1.0,
      barPercentage: 1.0,
    },
  ],
};

const dataPriceWeekend = {
  labels: [
    '6:00',
    '',
    '',
    '',
    '7:00',
    '',
    '',
    '',
    '8:00',
    '',
    '',
    '',
    '9:00',
    '',
    '',
    '',
    '10:00',
    '',
    '',
    '',
    '11:00',
    '',
    '',
    '',
    '12:00',
    '',
    '',
    '',
    '13:00',
    '',
    '',
    '',
    '14:00',
    '',
    '',
    '',
    '15:00',
    '',
    '',
    '',
    '16:00',
    '',
    '',
    '',
    '17:00',
    '',
    '',
    '',
    '18:00',
  ],
  datasets: [
    {
      label: 'ปริมาณการใช้ไฟฟ้า',
      data: [
        '405.30',
        '406.40',
        '404.90',
        '412.20',
        '393.50',
        '409.30',
        '401.50',
        '420.20',
        '422.70',
        '416.30',
        '419.70',
        '449.80',
        '454.70',
        '445.10',
        '450.40',
        '462.80',
        '469.60',
        '473.00',
        '462.70',
        '462.00',
        '464.20',
        '481.40',
        '487.00',
        '484.20',
        '487.30',
        '482.10',
        '488.40',
        '470.40',
        '481.30',
        '481.00',
        '481.40',
        '486.70',
        '487.60',
        '453.00',
        '496.90',
        '485.00',
        '488.40',
        '483.10',
        '489.80',
        '483.60',
        '474.80',
        '454.10',
        '477.40',
        '470.60',
        '466.30',
        '459.70',
        '457.50',
        '458.30',
        '437.90',
      ],
      backgroundColor: 'rgba(127, 63, 152, 0.5)',
      categoryPercentage: 1.0,
      barPercentage: 1.0,
    },
    {
      label: 'ปริมาณการจำหน่ายไฟฟ้า',
      data: [
        '0.64',
        '0.72',
        '1.22',
        '0.59',
        '1.19',
        '1.81',
        '2.42',
        '4.48',
        '5.07',
        '3.44',
        '4.96',
        '8.67',
        '9.95',
        '7.26',
        '7.31',
        '5.14',
        '5.40',
        '3.65',
        '3.37',
        '7.95',
        '5.75',
        '8.81',
        '11.48',
        '16.00',
        '12.95',
        '12.47',
        '18.99',
        '16.30',
        '19.55',
        '15.20',
        '15.93',
        '14.78',
        '13.47',
        '12.58',
        '10.69',
        '8.09',
        '3.47',
        '0',
        '0',
        '0',
        '0',
        '1.40',
        '1.33',
        '1.30',
        '0',
        '0',
        '1.34',
        '0.87',
        '0.39',
      ],
      backgroundColor: 'rgba(217, 119, 6)',
      categoryPercentage: 1.0,
      barPercentage: 1.0,
    },
  ],
};

const options1 = {
  tooltips: {
    mode: 'index',
    intersect: false,
  },
  responsive: true,
  legend: {
    position: 'bottom',
    align: 'start',
    onHover: (e) => {
      e.target.style.cursor = 'pointer';
    },
    labels: {
      fontSize: 14,
      padding: 20,
      boxWidth: 20,
    },
  },
  scales: {
    yAxes: [
      {
        stacked: true,
        ticks: {
          beginAtZero: true,
          callback: (v) => (v < 0 ? -v : v),
        },
        scaleLabel: {
          display: true,
          labelString: 'ปริมาณการใช้ไฟ',
          fontStyle: 'bold',
        },
      },
    ],
    xAxes: [
      {
        stacked: true,
      },
    ],
  },
};

const options = {
  tooltips: {
    mode: 'index',
    intersect: false,
  },
  responsive: true,
  legend: {
    position: 'bottom',
    align: 'start',
    onHover: (e) => {
      e.target.style.cursor = 'pointer';
    },
    labels: {
      fontSize: 14,
      padding: 20,
      boxWidth: 20,
    },
  },
  scales: {
    yAxes: [
      {
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        scaleLabel: {
          display: true,
          labelString: 'ปริมาณการซื้อขายไฟฟ้า',
          fontStyle: 'bold',
          fontSize: 15,
        },
      },
    ],
    xAxes: [
      {
        stacked: true,
      },
    ],
  },
};

const option48Hour = {
  tooltips: {
    mode: 'index',
    intersect: false,
  },
  responsive: true,
  legend: {
    position: 'bottom',
    align: 'start',
    onHover: (e) => {
      e.target.style.cursor = 'pointer';
    },
    labels: {
      fontSize: 14,
      padding: 20,
      boxWidth: 20,
    },
  },
  scales: {
    yAxes: [
      {
        stacked: false,
        ticks: {
          beginAtZero: true,
          callback: (v) => (v < 0 ? -v : v),
        },
        scaleLabel: {
          display: true,
          labelString: 'kWh',
          fontStyle: 'bold',
          fontSize: 15,
        },
      },
    ],
    xAxes: [
      {
        stacked: true,
      },
    ],
  },
};

export const CardPowerConsumptionGraph = () => (
  <div className="w-full rounded overflow-hidden shadow bg-white">
    <div className="flex justify-between">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-l-4 border-yellow-600">
        กราฟแสดงปริมาณการใช้ไฟฟ้า
      </div>
      <div className="px-3 py-2">
        <div className="flex">
          <SelectDateConsumer>
            {(store) => (
              <p className="m-4 text-purple-800 font-bold text-xs">
                {moment(store.date).format('Do MMMM YYYY')}
              </p>
            )}
          </SelectDateConsumer>
        </div>
      </div>
    </div>

    <div className="p-3">
      <Bar data={data} options={options1} />
    </div>
  </div>
);

export const CardPowerConsumptionGraphUser = () => (
  <div className="w-full rounded overflow-hidden shadow bg-white">
    <div className="flex justify-between">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-l-4 border-yellow-600">
        กราฟแสดงปริมาณการใช้ไฟฟ้า
      </div>
      <div className="px-3 py-2">
        <div className="flex">
          <p className="m-4 text-purple-800 font-bold text-xs">{moment().format('Do MMMM YYYY')}</p>
        </div>
      </div>
    </div>

    <div className="p-3">
      <Bar data={data} options={options1} />
    </div>
  </div>
);

export const CardGraphIncome = () => (
  <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
    <div>
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start mt-6 mb-2 text-header-pea-gray border-l-4 border-yellow-600">
        กราฟแสดงรายได้
      </div>
      <div className="px-6 mb-4">
        <Bar height={90} data={dataIncome} options={options} />
      </div>
    </div>
  </div>
);

export const CardGraphProfit = () => (
  <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
    <div className="flex justify-between">
      <div className="flex items-center px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        กราฟแสดงรายได้
      </div>
      <div className="px-3 py-2">
        <div className="flex">
          <p className="m-4 text-purple-800 font-bold text-xs">2 ตุลาคม 2563</p>
          <button
            type="button"
            onClick={() => alertComingSoon()}
            className="flex rounded-md justify-between m-2  py-2 px-2 border border-purple-800 text-xs leading-4 font-normal text-purple-800  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-purple-800 focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
          >
            <svg
              className="w-4 mr-2"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
            </svg>
            เลือกวันที่
          </button>
          <button
            type="button"
            onClick={() => alertComingSoon()}
            className="rounded-md m-2 py-2 px-2 border border-purple-800 text-xs leading-4 font-normal text-purple-800  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-purple-800 focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
          >
            7 วันย้อนหลัง
          </button>
        </div>
      </div>
    </div>
    <div className="p-3">
      <Bar height={90} data={dataProfile} options={options} />
    </div>
  </div>
);

export const CardGraphPrice = () => {
  const [value, setDate] = useState('workday');
  const [hide, setHide] = useState(true);

  const barA = () => {
    if (value === 'workday') {
      return <Bar height={90} data={dataPriceWorkday} options={option48Hour} redraw />;
    }
    return <Bar height={90} data={dataPriceWeekend} options={option48Hour} redraw />;
  };

  return (
    <div className="w-full rounded overflow-hidden shadow bg-white">
      <div className="flex justify-between">
        <div className="px-6 flex items-center font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-l-4 border-yellow-600">
          กราฟคาดการการใช้ / ผลิตไฟฟ้า
        </div>
        <div className="px-3 py-2">
          <div className="flex">
            <select
              onChange={(e) => setDate(e.target.value)}
              className="flex rounded-md justify-between items-center m-2  py-2 px-2 border border-color-pea-primary text-xs leading-4 font-normal text-pea-primary  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-color-pea-primary focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
            >
              <option value="workday">วันจันทร์ - วันศุกร์</option>
              <option value="weekend">วันเสาร์ - วันอาทิตย์</option>
            </select>
            <div />
          </div>
        </div>
      </div>
      <div className="p-3">{barA()}</div>
      <div className="">
        <div className="text-right text-red-600 -mt-10 pb-3 mr-3">
          <p>* ข้อมูลเป็นการคาดการเท่านั้น</p>
        </div>
      </div>
    </div>
  );
};

export default {
  CardPowerConsumptionGraph,
  CardGraphIncome,
  CardGraphProfit,
  CardGraphPrice,
  CardPowerConsumptionGraphUser,
};
