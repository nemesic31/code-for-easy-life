import React from 'react';
import PropTypes from 'prop-types';

export const ContentAlertSetting = ({ value }) => (
  <div>
    <div className="text-center mb-3 -mt-3">
      <p className="text-pea-gray">คุณต้องการปรับ</p>
    </div>
    <div className="mx-20">
      <div className="flex mb-2">
        <p className="w-2/4 text-left text-pea-gray">ราคาซื้อ DSO</p>
        <p className="w-1/4 text-right text-pea-primary font-bold">{value.buy}</p>
        <p className="w-1/4 text-right text-pea-gray">Token</p>
      </div>
      <div className="flex mb-2">
        <p className="w-2/4 text-left text-pea-gray">ราคาขาย DSO</p>
        <p className="w-1/4 text-right text-pea-primary font-bold">{value.sell}</p>
        <p className="w-1/4 text-right text-pea-gray">Token</p>
      </div>
      <div className="flex mb-2">
        <p className="w-2/4 text-left text-pea-gray">ค่าธรรมเนียมต่อธุรกรรม</p>
        <p className="w-1/4 text-right text-pea-primary font-bold">{value.fee}</p>
        <p className="w-1/4 text-right text-pea-gray">%</p>
      </div>
      <div className="flex mb-2">
        <p className="w-2/4 text-left text-pea-gray">ค่า Wheeling Charge</p>
        <p className="w-1/4 text-right text-pea-primary font-bold">1</p>
        <p className="w-1/4 text-right text-pea-gray">%</p>
      </div>
    </div>
  </div>
);

ContentAlertSetting.propTypes = {
  value: PropTypes.object,
};

ContentAlertSetting.defaultProps = {
  value: {},
};

export const ContentAlertBuy = ({ value }) => {
  const { price, startTimes, endTimes, estimated } = value;
  return (
    <div>
      <div className="mx-20">
        <div className="flex mb-2">
          <p className="w-2/5 text-left text-pea-gray">ช่วงเวลา</p>
          <p className="w-2/5 text-right text-pea-primary font-bold">
            {startTimes.slice(-8, -3)}น. - {endTimes.slice(-8, -3)}น.
          </p>
          <p className="w-1/5 text-right text-pea-gray">น.</p>
        </div>
        <div className="flex mb-2">
          <p className="w-2/5 text-left text-pea-gray">ราคาซื้อหน่วยละ</p>
          <p className="w-2/5 text-right text-pea-primary font-bold">{price}</p>
          <p className="w-1/5 text-right text-pea-gray">Token</p>
        </div>
        <div className="flex mb-2">
          <p className="w-2/5 text-left text-pea-gray">ปริมาณการซื้อ</p>
          <p className="w-2/5 text-right text-pea-primary font-bold">{estimated}</p>
          <p className="w-1/5 text-right text-pea-gray">หน่วย (kWh)</p>
        </div>
      </div>
    </div>
  );
};
ContentAlertBuy.propTypes = {
  value: PropTypes.object,
};

ContentAlertBuy.defaultProps = {
  value: {},
};

export const ContentAlertSell = ({ value }) => {
  const { price, startTimes, endTimes, estimated } = value;
  return (
    <div>
      <div className="mx-20">
        <div className="flex mb-2">
          <p className="w-2/5 text-left text-pea-gray">ช่วงเวลา</p>
          <p className="w-2/5 text-right text-pea-primary font-bold">
            {startTimes.slice(-8, -3)}น. - {endTimes.slice(-8, -3)}น.
          </p>
          <p className="w-1/5 text-right text-pea-gray">น.</p>
        </div>
        <div className="flex mb-2">
          <p className="w-2/5 text-left text-pea-gray">ราคาขายหน่วยละ</p>
          <p className="w-2/5 text-right text-pea-primary font-bold">{price}</p>
          <p className="w-1/5 text-right text-pea-gray">Token</p>
        </div>
        <div className="flex mb-2">
          <p className="w-2/5 text-left text-pea-gray">ปริมาณการซื้อ</p>
          <p className="w-2/5 text-right text-pea-primary font-bold">{estimated}</p>
          <p className="w-1/5 text-right text-pea-gray">หน่วย (kWh)</p>
        </div>
      </div>
    </div>
  );
};
ContentAlertSell.propTypes = {
  value: PropTypes.object,
};

ContentAlertSell.defaultProps = {
  value: {},
};

export default { ContentAlertSetting, ContentAlertBuy, ContentAlertSell };
