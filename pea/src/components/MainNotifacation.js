import React from 'react';
import PropTypes from 'prop-types';

import SubNotification from './SubNotification';

const data = [
  {
    code: '0x81657A...',
    date: '06/09/2563',
    time: '9:00 - 12:00',
    token: '75075',
  },
  {
    code: '0x81657A...',
    date: '06/09/2563',
    time: '9:00 - 12:00',
    token: '75075',
  },
  {
    code: '0x81657A...',
    date: '06/09/2563',
    time: '9:00 - 12:00',
    token: '75075',
  },
  {
    code: '0x81657A...',
    date: '06/09/2563',
    time: '9:00 - 12:00',
    token: '75055',
  },
  {
    code: '0x81657A...',
    date: '06/09/2563',
    time: '9:00 - 12:00',
    token: '75075',
  },
];

const MainNotification = ({ css }) => (
  <div
    className={`origin-top-right z-50 absolute right-0 -mt-3 w-noti rounded-sm shadow-lg ${css}`}
  >
    <div className="rounded-md bg-white shadow-xs">
      <div className="px-4 py-3">
        <p className="leading-5 text-center text-pea-primary font-bold text-lg">แจ้งเตือน</p>
      </div>
      <div className="border-t border-gray-200" />
      <div className="" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
        {data.map((v, i) => (
          <div key={i}>
            <SubNotification code={v.code} date={v.date} time={v.time} token={v.token} />
            <div className="border-t border-gray-200" />
          </div>
        ))}
      </div>
      <div className="border-t border-gray-200" />
      <div className="px-4 py-3 hover:bg-gray-200">
        <p className="leading-5 text-center text-pea-primary">ดูเพิ่มเติม...</p>
      </div>
    </div>
  </div>
);

MainNotification.propTypes = {
  css: PropTypes.string,
};

MainNotification.defaultProps = {
  css: '',
};

export default MainNotification;
