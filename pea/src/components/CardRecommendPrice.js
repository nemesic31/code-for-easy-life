import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as SmartMeter from '../api/SmartMeter.script';

export const CardRecommendPrice = ({ recommend }) => {
  const [limitQuantityBuy, setLimitQuantityBuy] = useState();
  const [limitQuantitySell, setLimitQuantitySell] = useState();
  const [responseSM, setResponseSM] = useState();
  async function getSmartMeter() {
    try {
      const response = await SmartMeter.getSmartMeterUser();
      setResponseSM(response.data.length);
      const { loadMaxValueKwp, solarMaxValueKw } = response.data[0];
      setLimitQuantityBuy(
        loadMaxValueKwp.toLocaleString('en-US', {
          maximumFractionDigits: 2,
          minimumFractionDigits: 2,
        })
      );
      setLimitQuantitySell(
        solarMaxValueKw.toLocaleString('en-US', {
          maximumFractionDigits: 2,
          minimumFractionDigits: 2,
        })
      );
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getSmartMeter();
  }, []);
  return (
    <div className="w-full rounded overflow-hidden shadow bg-white">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        ราคาแนะนำและจำนวนที่สามารถ{recommend.order}ได้
      </div>
      <div className="px-10 pb-4">
        <div className="flex justify-between mb-2">
          <p className="ml-2 text-pea-gray xl:text-base md:text-sm font-bold">ราคาแนะนำ</p>
          <p className="text-pea-primary xl:text-base md:text-sm font-bold">
            {recommend.price} Token
          </p>
        </div>
        <div className="flex justify-between mb-2">
          <p className="ml-2 text-pea-gray xl:text-base md:text-sm font-bold">
            จำนวนที่{recommend.order}ได้
          </p>
          <p className="text-pea-primary xl:text-base md:text-sm font-bold">
            {responseSM !== 0 &&
              (recommend.order === 'ซื้อ' ? limitQuantityBuy : limitQuantitySell)}{' '}
            kWh
          </p>
        </div>
      </div>
    </div>
  );
};

CardRecommendPrice.propTypes = {
  recommend: PropTypes.object.isRequired,
};

export default CardRecommendPrice;
