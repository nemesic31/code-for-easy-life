import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { alertSettingAdmin } from './SweetAlert';
import * as Admin from '../api/Admin.script';

export const CardManageSystem = () => {
  const [responseConfigs, setResponseConfigs] = useState();
  const [valueBuy, setValueBuy] = useState();
  const [idBuy, setIdBuy] = useState();
  const [valueSell, setValueSell] = useState();
  const [idSell, setIdSell] = useState();
  const [valueFee, setValueFee] = useState();
  const [idFee, setIdFee] = useState();
  const [valueWheeling, setValueWheeling] = useState();
  const [idWheeling, setIdWheeling] = useState();
  const [hide, setHide] = useState(true);
  async function getResponseAllConfigs() {
    try {
      const response = await Admin.getAllConfigs();
      setResponseConfigs(response.data.length);
      if (response.data.length > 0) {
        response.data.forEach((res) => {
          if (res) {
            if (res.name === 'DSO_BUY_PRICE') {
              setValueBuy(res.value);
              setIdBuy(res.configId);
            }
            if (res.name === 'DSO_SELL_PRICE') {
              setValueSell(res.value);
              setIdSell(res.configId);
            }
            if (res.name === 'TRANSACTION_FEE') {
              setValueFee(res.value);
              setIdFee(res.configId);
            }
            if (res.name === 'WHEELING_CHARGE') {
              setValueWheeling(res.value);
              setIdWheeling(res.configId);
            }
          }
        });
      } else {
        console.log('null');
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseAllConfigs();
  }, []);

  const validationSchema = Yup.object({
    buy: Yup.number().typeError('* กรอกตัวเลขเท่านั้น *').moreThan(0.01, '* ขั้นต่ำ 0.01 Token *'),
    sell: Yup.number().typeError('* กรอกตัวเลขเท่านั้น *').moreThan(0.01, '* ขั้นต่ำ 0.01 Token *'),
    fee: Yup.number().typeError('* กรอกตัวเลขเท่านั้น *').moreThan(0.01, '* ขั้นต่ำ 0.01 % *'),
  });
  const { handleSubmit, handleChange, values, errors } = useFormik({
    initialValues: {
      buy: '',
      sell: '',
      fee: '',
      idBuy: '',
      idSell: '',
      idFee: '',
    },
    validationSchema,
    onSubmit: async (value) => {
      if (responseConfigs > 0) {
        if (!values.buy) {
          values.buy = valueBuy;
        }
        if (!values.sell) {
          values.sell = valueSell;
        }
        if (!values.fee) {
          values.fee = valueFee;
        }
        values.idBuy = idBuy;
        values.idSell = idSell;
        values.idFee = idFee;
        await alertSettingAdmin(value);
      }
    },
  });
  return (
    <div className="w-full rounded overflow-hidden shadow bg-white ">
      <form onSubmit={handleSubmit}>
        <div className="font-bold xl:text-xl md:text-lg mt-5 mb-2 pl-6 text-header-pea-gray border-l-4 border-yellow-600">
          จัดการระบบ
        </div>
        <div className="px-3 pb-3">
          <div className="flex justify-between mx-2 my-4">
            <div>
              <p className="text-pea-gray xl:text-xl md:text-lg font-normal">ราคารับซื้อ DSO</p>
            </div>
            <div className="flex items-center">
              {errors.buy && <span className="text-xs text-red-500 mx-2">{errors.buy}</span>}
              <div>
                <input
                  className="flex xl:text-base md:text-sm text-right text-pea-primary font-bold border rounded-sm border-color-pea-primary w-64 justify-end py-2 px-2 placeholder-purple-800 focus:placeholder-transparent"
                  name="buy"
                  value={values.buy}
                  onChange={handleChange}
                  placeholder={valueBuy}
                />
              </div>
              <div className="text-pea-gray xl:text-base md:text-sm font-normal ml-4">Token</div>
            </div>
          </div>
          <div className="flex justify-between mx-2 my-4">
            <div>
              <p className="text-pea-gray xl:text-xl md:text-lg font-normal">ราคาขาย DSO</p>
            </div>
            <div className="flex items-center">
              {errors.sell && <span className="text-xs text-red-500 mx-2">{errors.sell}</span>}
              <div>
                <input
                  className="flex xl:text-base md:text-sm text-right text-pea-primary font-bold border rounded-sm border-color-pea-primary w-64 justify-end py-2 px-2 placeholder-purple-800 focus:placeholder-transparent"
                  name="sell"
                  value={values.sell}
                  onChange={handleChange}
                  placeholder={valueSell}
                />
              </div>
              <div className="text-pea-gray xl:text-base md:text-sm font-normal ml-4">Token</div>
            </div>
          </div>
          <div className="border-black border-b mx-5" />
          <div className="flex justify-between mx-2 my-4">
            <div>
              <p className="text-pea-gray xl:text-xl md:text-lg font-normal">
                ค่าธรรมเนียมต่อธุรกรรม
              </p>
            </div>
            <div className="flex items-center md:mr-6 xl:mr-8">
              {errors.fee && <span className="text-xs text-red-500 mx-2">{errors.fee}</span>}
              <div>
                <input
                  className="flex xl:text-base md:text-sm text-right text-pea-primary font-bold border rounded-sm border-color-pea-primary w-64 justify-end py-2 px-2 placeholder-purple-800 focus:placeholder-transparent"
                  name="fee"
                  value={values.fee}
                  onChange={handleChange}
                  placeholder={valueFee}
                />
              </div>
              <div className="text-pea-gray xl:text-base md:text-sm font-normal ml-4 ">%</div>
            </div>
          </div>
          <div className="flex justify-between mx-2 my-4">
            <div>
              <p className="text-pea-gray xl:text-xl md:text-lg font-normal">
                <div
                  className={`${
                    hide
                      ? 'transition ease-in duration-75 transform opacity-0 scale-95'
                      : 'transition ease-out duration-100 transform opacity-100 scale-100'
                  } px-2 py-1 left-52 rounded-md shadow bg-gray-100 ring-1 ring-black ring-opacity-5 text-sm absolute -mt-9`}
                >
                  <p>
                    อัตราค่าบริการในการใช้หรือการเชื่อมต่อระบบโครงข่ายของ
                    <br />
                    การไฟฟ้า แยกตามระดับแรงดัน มีหน่วยเป็น บาท/kWh
                  </p>
                </div>
                ค่า Wheeling Charge{' '}
                <span
                  onClick={() => setHide(!hide)}
                  className="inline-flex absolute ml-1 items-center px-2 pt-0.5 rounded-full text-xs font-medium bg-gray-600 text-white cursor-pointer"
                >
                  i
                </span>
              </p>
            </div>
            <div className="flex items-center md:mr-6 xl:mr-8">
              {errors.fee && <span className="text-xs text-red-500 mx-2">{errors.fee}</span>}
              <div>
                <input
                  className="flex xl:text-base md:text-sm text-right text-pea-primary font-bold border rounded-sm border-color-pea-primary w-64 justify-end py-2 px-2 placeholder-purple-800 focus:placeholder-transparent"
                  name="fee"
                  value={values.fee}
                  onChange={handleChange}
                  placeholder={valueWheeling}
                />
              </div>
              <div className="text-pea-gray xl:text-base md:text-sm font-normal ml-4 ">%</div>
            </div>
          </div>
          <div className="flex justify-center mt-6">
            <button
              type="submit"
              className="w-1/5 bg-primary hover:bg-purple-400 text-white xl:text-base md:text-sm font-bold py-3 px-4 rounded-full"
            >
              อัพเดท
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default CardManageSystem;
