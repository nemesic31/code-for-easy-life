import React from 'react';
import PropTypes from 'prop-types';

const SubNotification = ({ code, date, time, token }) => (
  <div className="hover:bg-gray-200 py-5">
    <div className="mx-4">
      <p className="text-pea-yellow font-bold mb-1">รหัสสัญญา {code} </p>
      <div className="flex mb-1 text-pea-gray">
        <p className="w-1/6">วันที่</p>
        <p className="w-2/6">{date}</p>
        <p className="w-2/6">{time}</p>
      </div>
      <div className="flex text-pea-gray">
        <p className="w-1/4">คำสั่งซื้อสำเร็จ</p>
        <p className="w-1/4 text-right">เงินสุทธิ</p>
        <p className="w-1/4 text-right">{token}</p>
        <p className="w-1/4 text-right">Token</p>
      </div>
    </div>
  </div>
);

SubNotification.propTypes = {
  code: PropTypes.string,
  date: PropTypes.string,
  time: PropTypes.string,
  token: PropTypes.string,
};

SubNotification.defaultProps = {
  code: 'Loading',
  date: 'Loading',
  time: 'Loading',
  token: 'Loading',
};

export default SubNotification;
