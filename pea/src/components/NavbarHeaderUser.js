import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import OutsideClickHandler from 'react-outside-click-handler';
import MainNotification from './MainNotifacation';
import logo from '../resource/logo-purple.png';
import { DropdownNavUser } from './Dropdown';
import * as User from '../api/User.script';

export const NavbarHeaderUser = () => {
  const [hide, setHide] = useState(true);
  const [nameBuilding, setNameBuilding] = useState();
  const [path, setPath] = useState();

  async function getResponseUser() {
    try {
      const responseUser = await User.getUserBuilding();
      if (responseUser.data.length !== 0) {
        const { name } = responseUser.data[0];
        setNameBuilding(name);
      }
    } catch (error) {
      console.log(error);
    }
  }

  const location = useLocation();

  useEffect(() => {
    const currentPath = location.pathname;
    setPath(currentPath);
    getResponseUser();
  }, [location]);

  return (
    <>
      <div className="flex-row flex justify-between px-5 bg-primary ">
        <div className="flex text-white text-base font-normal ">
          <Link to="/DashboardBuildingUser" className=" bg-white mr-3 px-2">
            <img src={logo} alt="pea-logo" className="w-28 mt-3 bg-white" />
          </Link>
          <Link
            to="/DashboardBuildingUser"
            className={`${
              path === '/DashboardBuildingUser' ? 'border-pink-500' : 'border-color-pea-primary'
            } focus:border-pink-500 border-b-4  py-5 hover:text-purple-400`}
          >
            <div className="mx-3">หน้าหลัก</div>
          </Link>
          <Link
            to="/TradingBuy"
            className={`${
              path === '/TradingBuy' ? 'border-pink-500' : 'border-color-pea-primary'
            } focus:border-pink-500 border-b-4  py-5 hover:text-purple-400`}
          >
            <div className="mx-3">ซื้อไฟฟ้า</div>
          </Link>
          <Link
            to="/TradingSell"
            className={`${
              path === '/TradingSell' ? 'border-pink-500' : 'border-color-pea-primary'
            } focus:border-pink-500 border-b-4  py-5 hover:text-purple-400`}
          >
            <div className="mx-3">ขายไฟฟ้า</div>
          </Link>
          <Link
            to="/SummaryCustomer"
            className={`${
              path === '/SummaryCustomer' ? 'border-pink-500' : 'border-color-pea-primary'
            } focus:border-pink-500 border-b-4  py-5 hover:text-purple-400`}
          >
            <div className="mx-3">ประวัติซื้อขาย</div>
          </Link>
          <Link
            to="/Pocket"
            className={`${
              path === '/Pocket' ? 'border-pink-500' : 'border-color-pea-primary'
            } focus:border-pink-500 border-b-4  py-5 hover:text-purple-400`}
          >
            <div className="mx-3">จัดการกระเป๋าเงิน</div>
          </Link>
        </div>
        <ul className="flex">
          <OutsideClickHandler
            onOutsideClick={() => {
              !hide && this.setState({ hide: true });
            }}
          >
            <li className="mr-6 py-5">
              <p className="text-white hover:text-purple-400" onClick={() => setHide(!hide)}>
                <svg
                  className="w-6 h-6"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                  />
                </svg>
              </p>
            </li>
          </OutsideClickHandler>

          <DropdownNavUser />
        </ul>
      </div>
      {!hide && (
        <MainNotification
          css={
            hide
              ? 'transition ease-out duration-100 transform opacity-0 scale-95'
              : 'transition ease-in duration-75 transform opacity-100 scale-100'
          }
        />
      )}
    </>
  );
};

export default NavbarHeaderUser;
