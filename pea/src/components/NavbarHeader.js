import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import OutsideClickHandler from 'react-outside-click-handler';
import MainNotification from './MainNotifacation';
import { DropDownNavAdmin } from './Dropdown';
import logo from '../resource/logo-purple.png';

export const NavbarHeader = () => {
  const [hide, setHide] = useState(true);

  return (
    <>
      <div className="flex-row flex px-5 pt-4 pb-3 bg-primary justify-between">
        <Link to="/admin" className="-mt-4 pb-3 -mb-3 ml-0 bg-white px-2">
          <img src={logo} alt="pea-logo" className="w-28 mt-2 bg-white" />
        </Link>
        <ul className="flex">
          <li className="mr-6">
            <a className="text-white hover:text-purple-400" href="/admin">
              <svg
                className="w-6 h-6"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                />
              </svg>
            </a>
          </li>
          <DropDownNavAdmin />
        </ul>
      </div>
    </>
  );
};

export default NavbarHeader;
