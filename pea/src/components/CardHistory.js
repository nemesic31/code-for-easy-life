import React, { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import 'moment/locale/th';
import Calendar from 'react-calendar';
import OutsideClickHandler from 'react-outside-click-handler';
import moment from 'moment';
import 'react-calendar/dist/Calendar.css';
import * as Contract from '../api/Contract.script';
import * as Admin from '../api/Admin.script';
import SelectDateContext, { SelectDateConsumer } from '../store';

export const BuyOrder = ({ order }) => {
  const estimatedBuy = order.orderDetails.committedKwh * 1;
  const unitPrice = order.orderDetails.unitPrice * 1;

  return (
    <div className="my-2 mx-2 px-3 py-3 border-2 border-color-pea-primary rounded-md">
      <div className="flex justify-between mb-2">
        <span className="inline-flex items-center px-5 py-2 rounded-full xl:text-base md:text-sm font-bold leading-5 bg-purple-800 text-white">
          รายการซื้อ
        </span>
        <a
          target="_blank"
          href={`https://explorer-etp.rsddev.team/tx/${order.contract.contractHash}/internal_transactions`}
        >
          <p className="font-bold xl:text-base md:text-sm text-pea-yellow mr-3 truncate">
            รหัสสัญญา {order.contract.contractHash.slice(0, 10)}...
          </p>
        </a>
      </div>
      <div className="flex my-1 justify-between">
        <div className="w-1/2 flex">
          <p className="text-pea-gray font-normal xl:text-base md:text-sm">สถานะ</p>
          <p className="ml-6 text-pea-primary font-bold xl:text-base md:text-sm">
            {order.contract.contractStatus === 'pending' ? 'กำลังดำเนินการ' : 'ดำเนินการสำเร็จ'}
          </p>
        </div>
        <div className="w-1/2 flex justify-end">
          <p className="text-pea-gray font-normal xl:text-base md:text-sm">วันที่</p>
          <p className="ml-6 text-pea-primary font-normal xl:text-base md:text-sm">
            {order.orderDetails.startDate.slice(0, 10)}
          </p>
          <p className="ml-6 text-pea-primary font-normal xl:text-base md:text-sm">
            {order.orderDetails.startDate.slice(-8, -3)} -{' '}
            {order.orderDetails.endDate.slice(-8, -3)}
          </p>
        </div>
      </div>
      <div className="flex my-1 justify-between">
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">
            พลังงานที่ขอซื้อ
          </p>
          <p className="w-1/3 text-pea-primary font-bold text-center xl:text-base md:text-sm">
            {' '}
            {estimatedBuy.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">หน่วย(kWh)</p>
        </div>
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            หน่วยละ
          </p>
          <p className="w-1/3 text-pea-primary font-bold text-center xl:text-base md:text-sm">
            {' '}
            {unitPrice.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">Token</p>
        </div>
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">จำนวน</p>
          <p className="w-1/3 text-pea-primary font-bold text-right xl:text-base md:text-sm">
            {order.totalPrice.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            Token
          </p>
        </div>
      </div>
      <div className="flex my-1 justify-end">
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">ค่าธรรมเนียม</p>
          <p className="w-1/3 text-pea-primary font-bold text-right xl:text-base md:text-sm">
            {order.fee.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            Token
          </p>
        </div>
      </div>
      <div className="flex my-1 justify-end">
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">เงินสุทธิ</p>
          <p className="w-1/3 text-pea-primary font-bold text-right xl:text-base md:text-sm">
            {order.netPrice.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            Token
          </p>
        </div>
      </div>
    </div>
  );
};

BuyOrder.propTypes = {
  order: PropTypes.object.isRequired,
};

export const SellOrder = ({ order }) => {
  const estimatedSell = order.orderDetails.committedKwh * 1;
  const unitPrice = order.orderDetails.unitPrice * 1;
  return (
    <div className="my-2 mx-2 px-3 py-3 border-2 border-color-pea-yellow rounded-md">
      <div className="flex justify-between mb-2">
        <span className="inline-flex items-center px-5 py-2 rounded-full xl:text-base md:text-sm font-bold leading-5 bg-pea-yellow text-white">
          รายการขาย
        </span>
        <a
          target="_blank"
          href={`https://explorer-etp.rsddev.team/tx/${order.contract.contractHash}/internal_transactions`}
        >
          <p className="font-bold xl:text-base md:text-sm text-pea-primary mr-3">
            รหัสสัญญา {order.contract.contractHash.slice(0, 10)}...
          </p>
        </a>
      </div>
      <div className="flex my-1 justify-between">
        <div className="w-1/2 flex">
          <p className="text-pea-gray font-normal xl:text-base md:text-sm">สถานะ</p>
          <p className="ml-6 text-pea-yellow font-bold xl:text-base md:text-sm">
            {order.contract.contractStatus === 'pending' ? 'กำลังดำเนินการ' : 'ดำเนินการสำเร็จ'}
          </p>
        </div>
        <div className="w-1/2 flex justify-end">
          <p className="text-pea-gray font-normal xl:text-base md:text-sm">วันที่</p>
          <p className="ml-6 text-pea-yellow font-normal xl:text-base md:text-sm">
            {order.orderDetails.startDate.slice(0, 10)}
          </p>
          <p className="ml-6 text-pea-yellow font-normal xl:text-base md:text-sm">
            {order.orderDetails.startDate.slice(-8, -3)} -{' '}
            {order.orderDetails.endDate.slice(-8, -3)}
          </p>
        </div>
      </div>
      <div className="flex my-1 justify-between">
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">พลังงานที่ขอขาย</p>
          <p className="w-1/3 text-pea-yellow font-bold text-center xl:text-base md:text-sm">
            {' '}
            {estimatedSell.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">หน่วย(kWh)</p>
        </div>
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            หน่วยละ
          </p>
          <p className="w-1/3 text-pea-yellow font-bold text-center xl:text-base md:text-sm">
            {' '}
            {unitPrice.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">Token</p>
        </div>
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">จำนวน</p>
          <p className="w-1/3 text-pea-yellow font-bold text-right xl:text-base md:text-sm">
            {order.totalPrice.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            Token
          </p>
        </div>
      </div>
      <div className="flex my-1 justify-end">
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">ค่าธรรมเนียม</p>
          <p className="w-1/3 text-pea-yellow font-bold text-right xl:text-base md:text-sm">
            {order.fee.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            Token
          </p>
        </div>
      </div>
      <div className="flex my-1 justify-end">
        <div className="flex w-1/3">
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm">เงินสุทธิ</p>
          <p className="w-1/3 text-pea-yellow font-bold text-right xl:text-base md:text-sm">
            {order.netPrice.toLocaleString('en-US', {
              maximumFractionDigits: 2,
              minimumFractionDigits: 2,
            })}
          </p>
          <p className="w-1/3 text-pea-gray font-normal xl:text-base md:text-sm text-right">
            Token
          </p>
        </div>
      </div>
    </div>
  );
};

SellOrder.propTypes = {
  order: PropTypes.object.isRequired,
};

export const CardHistory = () => {
  const [times, setTimes] = useState(new Date());
  const [hide, setHide] = useState(true);
  const [listContract, setListContract] = useState({});

  async function getContractUser() {
    try {
      const response = await Contract.getContractUser();
      if (response.data) {
        setListContract({ ...listContract, response });
      } else {
        setListContract({});
      }
    } catch (error) {
      console.log(error);
    }
  }

  async function selectTime(times) {
    setTimes(times);
    try {
      const newDateTime = moment(times).format('YYYY-MM-DD');
      const response = await Contract.getContractUser(newDateTime);
      if (response.data) {
        setListContract({ ...listContract, response });
      } else {
        setListContract({});
      }
    } catch (error) {
      console.log(error);
    }
  }

  const getDataAll = () => {
    const data = listContract.response.data.reverse();
    return data;
  };

  useEffect(() => {
    getContractUser();
  }, []);

  return (
    <div className="w-full rounded overflow-hidden shadow bg-white ">
      <div className="flex justify-between">
        <div className="font-bold xl:text-lg md:text-base  my-2 pl-6 mt-5 text-header-pea-gray border-l-4 border-yellow-600">
          ประวัติการซื้อขาย
        </div>
        <div className="flex">
          <OutsideClickHandler
            onOutsideClick={() => {
              !hide && setHide(true);
            }}
          >
            <button
              type="button"
              onClick={() => setHide(!hide)}
              aria-hidden="true"
              className="flex rounded-md justify-between items-center mx-5 mt-5  py-2 px-2 border border-color-pea-primary text-xs leading-4 font-normal text-pea-primary  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-color-pea-primary focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
            >
              <svg
                className="w-4 mr-2"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
              </svg>
              เลือกวันที่
            </button>
          </OutsideClickHandler>
        </div>
      </div>
      <div className="flex justify-end">
        <div
          className={`absolute shadow-xl ${
            hide
              ? 'transform opacity-0 ml-64 scale-0'
              : 'transition ease-in duration-75 transform opacity-100 scale-100 '
          } `}
        >
          <Calendar value={times} onChange={selectTime} />
        </div>
      </div>

      <div className="p-3">
        {listContract.response ? (
          getDataAll().map((v) => {
            if (getDataAll) {
              if (v.orderDetails.side === 'buy') {
                return <BuyOrder key={v.contractItemId} order={v} />;
              }
              if (v.orderDetails.side === 'sell') {
                return <SellOrder key={v.contractItemId} order={v} />;
              }
            }
          })
        ) : (
          <div className="flex justify-center py-5 text-xl">ไม่พบการทำรายการ</div>
        )}
      </div>
    </div>
  );
};

export const CardHistoryAdmin = (props) => {
  const [listContract, setListContract] = useState({});
  const [time, setTime] = useState();
  const store = useContext(SelectDateContext);

  async function getContractUser() {
    const times = store.date;
    try {
      const { userId } = props;
      const newDateTime = moment(times).format('YYYY-MM-DD');
      const response = await Contract.getContractAdmin({ newDateTime, userId });
      const { data } = response;
      if (data) {
        setListContract({ ...listContract, data });
      } else {
        setListContract({});
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getContractUser();
  }, [time]);

  return (
    <SelectDateConsumer>
      {(store) => {
        setTime(store.date);
        const { data } = listContract;
        return (
          <div className="w-full rounded overflow-hidden shadow bg-white ">
            <div className="flex justify-between">
              <div className="font-bold xl:text-lg md:text-base  my-2 pl-6 mt-5 text-header-pea-gray border-l-4 border-yellow-600">
                ประวัติการซื้อขาย วัน{moment(store.date).format('ddddที่ Do MMMM YYYY')}
              </div>
            </div>

            <div className="p-3">
              {data !== undefined ? (
                data.map((v) => {
                  if (v.orderDetails.side === 'buy') {
                    return <BuyOrder key={v.contractItemId} order={v} />;
                  }
                  if (v.orderDetails.side === 'sell') {
                    return <SellOrder key={v.contractItemId} order={v} />;
                  }
                })
              ) : (
                <div className="flex justify-center py-5 text-xl">ไม่พบการทำรายการ</div>
              )}
            </div>
          </div>
        );
      }}
    </SelectDateConsumer>
  );
};

export default {
  CardHistory,
  BuyOrder,
  SellOrder,
  CardHistoryAdmin,
};
