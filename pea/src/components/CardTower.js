import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as Admin from '../api/Admin.script';
import logo from '../resource/tower.svg';
import DSO from '../resource/dso.svg';
import * as Wallet from '../api/Wallet.script';
import * as DSOReconcile from '../api/DSOReconcile.script';
import * as SmartMeter from '../api/SmartMeter.script';
import * as Contract from '../api/Contract.script';

export const CardTower = ({ nameCard, wallet, quantity, meter }) => {
  const contracts = [];
  const [resCon, setResCon] = useState();
  async function getResponseAllContract() {
    try {
      const responseAllUser = await Admin.getAllUsers();
      if (responseAllUser.data) {
        responseAllUser.data.forEach(async (res) => {
          const { userId } = res;
          const responseAllContract = await Contract.getContractAdmin({ userId });
          contracts.push(responseAllContract);
        });
        setResCon(contracts);
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseAllContract();
  }, []);

  return (
    <div className="w-full grid grid-cols-3 gap-4 ">
      <div className="w-full h-full">
        <Link to="/PEAGRID">
          <CardDSO />
        </Link>
      </div>

      {nameCard.map((v) => {
        if (v.userProfile.length > 0) {
          return (
            <div className="w-full" key={v.userId}>
              <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
                <div className="px-6 py-4">
                  <Link
                    to={{
                      pathname: '/DashboardBuildingAdmin',
                      state: {
                        name: `${v.userProfile[0].name}`,
                        userId: `${v.userId}`,
                      },
                    }}
                  >
                    <div className="font-bold xl:text-xl md:text-lg text-center my-2 text-header-pea-gray">
                      อาคาร {v.userProfile[0].name}
                    </div>
                    <div className="text-center my-5">
                      <img src={logo} alt="logo" className="img-center" />
                    </div>
                    <div className="mt-5 xl:text-base md:text-sm">
                      <div className="flex">
                        <div className="w-1/2">
                          <p className="text-pea-gray">สถานะ</p>
                        </div>
                        <div className="w-1/2">
                          <p className="text-pea-primary text-right">Prosumer</p>
                        </div>
                      </div>
                      <div className="flex">
                        <div className="w-2/4">
                          <p className="text-pea-gray">พลังงานที่ผลิตได้</p>
                        </div>
                        <div className="w-1/4">
                          <p className="text-pea-primary text-right">
                            {meter.map((resMeter) => {
                              if (v.userId === resMeter.userId) {
                                return (
                                  <span key={resMeter.userId}>
                                    {resMeter.solarMaxValueKw
                                      ? resMeter.solarMaxValueKw.toLocaleString('en-US', {
                                          maximumFractionDigits: 2,
                                          minimumFractionDigits: 2,
                                        })
                                      : '0'}
                                  </span>
                                );
                              }
                            })}
                          </p>
                        </div>
                        <div className="w-1/4">
                          <p className="text-pea-gray text-right">kWh</p>
                        </div>
                      </div>
                      <div className="flex">
                        <div className="w-2/4">
                          <p className="text-pea-gray">พลังงานที่ใช้</p>
                        </div>
                        <div className="w-1/4">
                          <p className="text-pea-primary text-right">
                            {quantity.map((resQuantity) => {
                              if (v.userId === resQuantity.userId) {
                                return (
                                  <span key={resQuantity.userId}>
                                    {resQuantity.totalBuy
                                      ? resQuantity.totalBuy.toLocaleString('en-US', {
                                          maximumFractionDigits: 2,
                                          minimumFractionDigits: 2,
                                        })
                                      : '0'}
                                  </span>
                                );
                              }
                            })}
                          </p>
                        </div>
                        <div className="w-1/4">
                          <p className="text-pea-gray text-right">kWh</p>
                        </div>
                      </div>
                      <div className="flex">
                        <div className="w-2/4">
                          <p className="text-pea-gray">Token</p>
                        </div>
                        <div className="w-1/4">
                          <p className="text-pea-primary text-right">
                            {wallet.map((res) => {
                              if (v.userId === res.userId) {
                                return (
                                  <span key={res.userId}>
                                    {res.token.toLocaleString('en-US', {
                                      maximumFractionDigits: 2,
                                      minimumFractionDigits: 2,
                                    })}
                                  </span>
                                );
                              }
                            })}
                          </p>
                        </div>
                        <div className="w-1/4">
                          <p className="text-pea-gray text-right">Token</p>
                        </div>
                      </div>
                      <hr className="my-2" />
                    </div>
                  </Link>

                  <div className="flex">
                    <div className="w-1/2">
                      <p className="text-pea-gray">สัญญาล่าสุด</p>
                    </div>
                    <div className="w-1/2">
                      <p className="text-pea-primary text-right truncate">
                        {resCon &&
                          resCon.map((resContract) => {
                            if (resContract.data) {
                              if (
                                v.userId ===
                                resContract.data[resContract.data.length - 1].orderDetails.userId
                              ) {
                                return (
                                  <a
                                    href={`https://explorer-etp.rsddev.team/tx/${
                                      resContract.data[resContract.data.length - 1].contract
                                        .contractHash
                                    }/internal_transactions`}
                                  >
                                    {
                                      resContract.data[resContract.data.length - 1].contract
                                        .contractHash
                                    }
                                  </a>
                                );
                              }
                            }
                          })}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        }
      })}
    </div>
  );
};

CardTower.propTypes = {
  nameCard: PropTypes.array,
  wallet: PropTypes.array,
  meter: PropTypes.array,
  quantity: PropTypes.array,
};

CardTower.defaultProps = {
  nameCard: [],
  wallet: [],
  meter: [],
  quantity: [],
};

export const CardDSO = () => {
  const [responseWallet, setResponseWallet] = useState();
  const [totalBuy, setTotalBuy] = useState();
  const [balance, setBalance] = useState();
  const [limitQuantity, setLimitQuantity] = useState();
  const [lastContract, setLastContract] = useState();
  async function getResponseWallet() {
    try {
      const responseMeter = await SmartMeter.getSmartMeterUser();
      const responseQuantity = await DSOReconcile.getReconcileUser();
      const response = await Wallet.getWalletUser();
      const responseContract = await Contract.getContractUser();
      if (responseContract.data) {
        setLastContract(
          responseContract.data[responseContract.data.length - 1].contract.contractHash
        );
      }
      setTotalBuy(responseQuantity.data.totalBuy);
      setResponseWallet(response.data.length);
      if (response.data) {
        const { token } = response.data[0];
        setBalance(
          token.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 })
        );
      }
      const { solarMaxValueKw } = responseMeter.data[0];
      setLimitQuantity(
        solarMaxValueKw.toLocaleString('en-US', {
          maximumFractionDigits: 2,
          minimumFractionDigits: 2,
        })
      );
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseWallet();
  }, []);
  return (
    <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 py-4">
        <div className="font-bold xl:text-xl md:text-lg text-center my-2 text-header-pea-gray">
          DSO
        </div>
        <div className="my-5">
          <img src={DSO} alt="logo" className="img-center" />
        </div>
        <div className="mt-5 xl:text-base md:text-sm">
          <div className="flex">
            <div className="w-1/2">
              <p className="text-pea-gray">สถานะ</p>
            </div>
            <div className="w-1/2">
              <p className="text-pea-primary text-right">DSO</p>
            </div>
          </div>
          <div className="flex">
            <div className="w-2/4">
              <p className="text-pea-gray">พลังงานที่ผลิตได้</p>
            </div>
            <div className="w-1/4">
              <p className="text-pea-primary text-right">{limitQuantity || '0'}</p>
            </div>
            <div className="w-1/4">
              <p className="text-pea-gray text-right">kWh</p>
            </div>
          </div>
          <div className="flex">
            <div className="w-2/4">
              <p className="text-pea-gray">พลังงานที่ใช้</p>
            </div>
            <div className="w-1/4">
              <p className="text-pea-primary text-right">{totalBuy || '0'}</p>
            </div>
            <div className="w-1/4">
              <p className="text-pea-gray text-right">kWh</p>
            </div>
          </div>
          <div className="flex">
            <div className="w-2/4">
              <p className="text-pea-gray">Token</p>
            </div>
            <div className="w-1/4">
              <p className="text-pea-primary text-right">
                {responseWallet !== 0 ? balance : '0.00'}
              </p>
            </div>
            <div className="w-1/4">
              <p className="text-pea-gray text-right">Token</p>
            </div>
          </div>
          <hr className="my-2" />
          <div className="flex">
            <div className="w-1/2">
              <p className="text-pea-gray">สัญญาล่าสุด</p>
            </div>
            <div className="w-1/2">
              <p className="text-pea-primary text-right truncate">
                {lastContract ? lastContract.slice(0, 10) : 'ยังไม่มีการทำสัญญา'}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardTop = () => {
  const [valueBuy, setValueBuy] = useState();
  const [valueSell, setValueSell] = useState();
  const [valueFee, setValueFee] = useState();
  const [valueWheeling, setValueWheeling] = useState();
  async function getResponseAllConfigs() {
    try {
      const response = await Admin.getAllConfigs();
      if (response.data.length > 0) {
        response.data.forEach((res) => {
          if (res) {
            if (res.name === 'DSO_BUY_PRICE') {
              setValueBuy(res.value);
            }
            if (res.name === 'DSO_SELL_PRICE') {
              setValueSell(res.value);
            }
            if (res.name === 'TRANSACTION_FEE') {
              setValueFee(res.value);
            }
            if (res.name === 'WHEELING_CHARGE') {
              setValueWheeling(res.value);
            }
          }
        });
      } else {
        console.log('null');
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseAllConfigs();
  }, []);
  return (
    <div className="w-full rounded overflow-hidden shadow bg-white ">
      <div className="px-5 mt-3 font-bold xl:text-xl md:text-lg text-header-pea-gray border-l-4 border-yellow-600 ">
        จัดการระบบ
      </div>
      <div className="px-5 mb-3 mt-2 flex  xl:text-base md:text-sm">
        <div className="w-1/5 flex p-1">
          <div className="md:w-2/4 xl:w-1/3">
            <p className="text-pea-gray">ราคารับซื้อ DSO</p>
          </div>
          <div className="md:w-1/4 xl:w-1/3 text-center text-pea-primary font-bold">
            {valueBuy && valueBuy.toLocaleString()}
          </div>
          <div className="md:w-1/4 xl:w-1/3 text-pea-gray">Token</div>
        </div>
        <div className="w-1/5 flex p-1">
          <div className="md:w-2/4 xl:w-1/3">
            <p className="text-pea-gray">ราคารขาย DSO</p>
          </div>
          <div className="md:w-1/4 xl:w-1/3 text-center text-pea-primary font-bold">
            {valueSell && valueSell.toLocaleString()}
          </div>
          <div className="md:w-1/4 xl:w-1/3 text-pea-gray">Token</div>
        </div>
        <div className="w-1/5 flex p-1">
          <div className="md:w-2/3 xl:w-1/2">
            <p className="text-pea-gray">ค่าธรรมเนียมต่อธุรกรรม</p>
          </div>
          <div className="md:w-1/3 xl:w-1/2 text-center text-pea-primary font-bold">
            {valueFee && valueFee.toLocaleString()} %
          </div>
        </div>
        <div className="w-1/5 flex p-1">
          <div className="md:w-2/3 xl:w-1/2">
            <p className="text-pea-gray">ค่า Wheeling Charge</p>
          </div>
          <div className="md:w-1/3 xl:w-1/2 text-center text-pea-primary font-bold">
            {valueWheeling && valueWheeling.toLocaleString()} %
          </div>
        </div>
        <div className="w-1/5 text-right">
          <Link to="/SettingAdmin">
            <button
              type="button"
              className="bg-primary hover:bg-purple-900 text-white font-bold py-2 px-4 rounded"
            >
              แก้ไข
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default { CardTower, CardDSO, CardTop };
