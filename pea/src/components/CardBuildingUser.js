import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import solarpanel from '../resource/solar-panel.png';
import tower from '../resource/tower.svg';
import wallet from '../resource/wallet.png';
import bgBuy from '../resource/bg-button-buy.png';
import bgSell from '../resource/bg-button-sell.png';
import * as Inventory from '../api/Inventory.script';
import * as User from '../api/User.script';
import * as Wallet from '../api/Wallet.script';
import * as DSOReconcile from '../api/DSOReconcile.script';
import * as SmartMeter from '../api/SmartMeter.script';

export const CardBuildingInfo = () => {
  const [responseUserProfile, setResponseUserProfile] = useState();
  const [nameBuilding, setNameBuilding] = useState();
  const [nameIntendant, setNameIntendant] = useState();
  const [detailUser, setDetailUser] = useState();
  async function getResponseUser() {
    try {
      const responseUser = await User.getUserBuilding();
      setResponseUserProfile(responseUser.data.length);
      if (responseUser.data.length !== 0) {
        const { name, details, contactName } = responseUser.data[0];
        setNameBuilding(name);
        setNameIntendant(contactName);
        setDetailUser(details);
      }
    } catch (error) {
      console.log(error);
    }
  }
  useEffect(() => {
    getResponseUser();
  }, []);
  return (
    <div className="w-full h-48 rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6  font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        ข้อมูลอาคาร
      </div>
      <div className="flex">
        <div className="w-3/12">
          <img src={tower} alt="logo" className="img-center px-2" />
        </div>
        <div className="w-9/12 p-2">
          <p className="xl:text-base md:text-xs text-purple-800 font-bold">
            อาคาร {responseUserProfile !== 0 && nameBuilding}
          </p>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ชื่ออาคาร</p>
            <p className="mr-2 xl:text-base md:text-xs text-purple-800 font-normal">
              อาคาร {responseUserProfile !== 0 && nameBuilding}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ผู้ดูแลอาคาร</p>
            <p className="mr-2 xl:text-base md:text-xs text-purple-800 font-normal">
              {responseUserProfile !== 0 && nameIntendant}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ข้อมูลทั่วไป</p>
            <p className="mr-2 xl:text-base md:text-xs text-purple-800 font-normal">
              {responseUserProfile !== 0 && detailUser}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardButtonTopBar = () => {
  const [nameBuilding, setNameBuilding] = useState();
  async function getResponseUser() {
    try {
      const responseUser = await User.getUserBuilding();
      if (responseUser.data.length !== 0) {
        const { name } = responseUser.data[0];
        setNameBuilding(name);
      }
    } catch (error) {
      console.log(error);
    }
  }
  useEffect(() => {
    getResponseUser();
  }, []);
  return (
    <div className="w-full rounded overflow-hidden shadow bg-white">
      <div className="flex justify-between">
        <div>
          <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-l-4 border-yellow-600">
            เรามาซื้อขายไฟฟ้ากัน
          </div>
        </div>
        <div className="flex justify-between">
          <Link to="/TradingBuy">
            <div className="my-4 mx-2 rounded-md flex items-center justify-center">
              <img src={bgBuy} alt="bg-buy" />
              <div className="text-3xl font-normal text-white absolute flex justify-between w-56 items-center">
                <div>ซื้อไฟฟ้า</div>
                <svg
                  className="w-16 h-16 text-color-icon"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M11 3a1 1 0 10-2 0v1a1 1 0 102 0V3zM15.657 5.757a1 1 0 00-1.414-1.414l-.707.707a1 1 0 001.414 1.414l.707-.707zM18 10a1 1 0 01-1 1h-1a1 1 0 110-2h1a1 1 0 011 1zM5.05 6.464A1 1 0 106.464 5.05l-.707-.707a1 1 0 00-1.414 1.414l.707.707zM5 10a1 1 0 01-1 1H3a1 1 0 110-2h1a1 1 0 011 1zM8 16v-1h4v1a2 2 0 11-4 0zM12 14c.015-.34.208-.646.477-.859a4 4 0 10-4.954 0c.27.213.462.519.476.859h4.002z" />
                </svg>
              </div>
            </div>
          </Link>
          <Link to="/TradingSell">
            <div className="my-4 mx-2 rounded-md flex items-center justify-center">
              <img src={bgSell} alt="bg-sell" />
              <div className="text-3xl font-normal text-white absolute flex justify-between w-56 items-center">
                <div>ขายไฟฟ้า</div>
                <svg
                  className="w-16 h-16 text-color-icon"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M8.433 7.418c.155-.103.346-.196.567-.267v1.698a2.305 2.305 0 01-.567-.267C8.07 8.34 8 8.114 8 8c0-.114.07-.34.433-.582zM11 12.849v-1.698c.22.071.412.164.567.267.364.243.433.468.433.582 0 .114-.07.34-.433.582a2.305 2.305 0 01-.567.267z" />
                  <path
                    fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-13a1 1 0 10-2 0v.092a4.535 4.535 0 00-1.676.662C6.602 6.234 6 7.009 6 8c0 .99.602 1.765 1.324 2.246.48.32 1.054.545 1.676.662v1.941c-.391-.127-.68-.317-.843-.504a1 1 0 10-1.51 1.31c.562.649 1.413 1.076 2.353 1.253V15a1 1 0 102 0v-.092a4.535 4.535 0 001.676-.662C13.398 13.766 14 12.991 14 12c0-.99-.602-1.765-1.324-2.246A4.535 4.535 0 0011 9.092V7.151c.391.127.68.317.843.504a1 1 0 101.511-1.31c-.563-.649-1.413-1.076-2.354-1.253V5z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export const CardToolInfo = () => {
  const [nameTool, setNameTool] = useState();
  const [capacityTool, setCapacityTool] = useState();
  const [typeTool, setTypeTool] = useState();

  async function getResponseInventory() {
    try {
      const responseTool = await Inventory.getInventory();
      const { name, type, capacity } = responseTool.data[0];
      setNameTool(name);
      setTypeTool(type);
      setCapacityTool(capacity.toLocaleString());
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseInventory();
  }, []);

  return (
    <div className="w-full h-48 rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        ข้อมูลอุปกรณ์
      </div>
      <div className="flex">
        <div className="w-3/12">
          <img src={solarpanel} alt="logo" className="img-center px-2" />
        </div>
        <div className="w-9/12 p-2">
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ชื่ออุปกรณ์</p>
            <p className="mr-2 xl:text-base md:text-sm text-primary font-normal">
              {nameTool && nameTool}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ประเภทอุปกรณ์</p>
            <p className="mr-2 xl:text-base md:text-sm text-primary font-normal">
              {typeTool && typeTool}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">กำลังการผลิต</p>
            <p className="mr-2 xl:text-base md:text-sm text-primary font-normal">
              {capacityTool && capacityTool} kWp
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardWallet = () => {
  const [responseWallet, setResponseWallet] = useState();
  const [balance, setBalance] = useState();
  async function getResponseWallet() {
    try {
      const response = await Wallet.getWalletUser();
      setResponseWallet(response.data.length);
      if (response.data.length !== 0) {
        const { token } = response.data[0];
        setBalance(
          token.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 })
        );
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseWallet();
  }, []);
  return (
    <div className="w-full h-48 rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        กระเป๋าเงิน
      </div>
      <div className="flex">
        <div className="w-3/12 ml-4">
          <img src={wallet} alt="logo" className="img-center" />
        </div>
        <div className="w-9/12 p-2">
          <div className="flex justify-end">
            <p className="mr-2 xl:text-lg md:text-base text-gray-600 font-normal">
              จำนวน Token ที่มี
            </p>
          </div>
          <div className="flex justify-end items-center">
            <p className="mr-2 xl:text-4xl md:text-3xl text-purple-800 font-bold">
              {responseWallet !== 0 && balance}
            </p>
            <p className="mr-2 xl:text-base md:text-sm text-gray-600 font-normal">Token</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardUsageInformation = () => {
  const [totalBuy, setTotalBuy] = useState();
  const [totalSell, setTotalSell] = useState();
  const [limitQuantity, setLimitQuantity] = useState();
  const [saveMoney, setSaveMoney] = useState();
  const [responseSM, setResponseSM] = useState();
  const [mWhBuy, setMWhBuy] = useState('kWh');
  const [mWhSell, setMWhSell] = useState('kWh');
  async function getResponseDSOReconcile() {
    try {
      const responseMeter = await SmartMeter.getSmartMeterUser();
      const response = await DSOReconcile.getReconcileUser();
      if (response.data.totalBuy > 100000) {
        setTotalBuy(response.data.totalBuy / 1000);
        setMWhBuy('mWh');
      } else {
        setTotalBuy(response.data.totalBuy);
      }
      if (response.data.totalSell > 100000) {
        setTotalSell(response.data.totalSell / 1000);
        setMWhSell('mWh');
      } else {
        setTotalSell(response.data.totalSell);
      }
      setResponseSM(response.data.length);
      const { solarMaxValueKw } = responseMeter.data[0];
      setLimitQuantity(
        solarMaxValueKw.toLocaleString('en-US', {
          maximumFractionDigits: 2,
          minimumFractionDigits: 2,
        })
      );
      setSaveMoney(solarMaxValueKw * 4.42);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseDSOReconcile();
  }, []);
  return (
    <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-l-4 border-yellow-600">
        ข้อมูลการใช้ / การผลิต
      </div>
      <div className="flex flex-col m-2">
        <div className="h-6/12 mb-2">
          <div className="flex">
            <div className="flex flex-col items-between text-center xl:w-6/12 md:w-7/12">
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">
                ปริมาณพลังงานไฟฟ้านำเข้า
              </p>
              <p className="mr-2 xl:text-4xl md:text-2xl text-primary font-bold">
                {!totalBuy
                  ? '0'
                  : totalBuy.toLocaleString('en-US', {
                      maximumFractionDigits: 2,
                      minimumFractionDigits: 2,
                    })}
              </p>
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">{mWhBuy}</p>
            </div>
            <div className="flex flex-col items-between text-center xl:w-6/12 md:w-5/12">
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">
                ปริมาณพลังงานไฟฟ้านำออก
              </p>
              <p className="mr-2 xl:text-4xl md:text-2xl text-primary font-bold">
                {responseSM !== 0 ? limitQuantity : '0'}
              </p>
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">kWh</p>
            </div>
          </div>
        </div>
        <div className="h-6/12 mb-2">
          <div className="flex">
            <div className="flex flex-col items-between text-center xl:w-6/12 md:w-7/12 ">
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">
                ปริมาณการใช้ไฟฟ้าที่ผลิตเอง
              </p>
              <p className="mr-2 xl:text-4xl md:text-2xl text-primary font-bold">
                {!totalSell
                  ? '0'
                  : totalSell.toLocaleString('en-US', {
                      maximumFractionDigits: 2,
                      minimumFractionDigits: 2,
                    })}
              </p>
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">kWh/Day</p>
            </div>
            <div className="flex flex-col items-between text-center xl:w-6/12 md:w-5/12">
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">
                จำนวนเงินที่ประหยัดได้
              </p>
              <p className="mr-2 xl:text-4xl md:text-2xl text-primary font-bold">
                {saveMoney
                  ? saveMoney.toLocaleString('en-US', {
                      maximumFractionDigits: 2,
                      minimumFractionDigits: 2,
                    })
                  : '0'}
              </p>
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">Token</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default {
  CardButtonTopBar,
  CardToolInfo,
  CardWallet,
  CardUsageInformation,
  CardBuildingInfo,
};
