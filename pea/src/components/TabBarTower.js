import React, { useState } from 'react';
import PropTypes from 'prop-types';
import OutsideClickHandler from 'react-outside-click-handler';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment/locale/th';
import { SelectDateConsumer } from '../store';

export const TabBarTower = ({ nameButton, path }) => {
  const [hide, setHide] = useState(true);

  return (
    <div className="flex justify-between">
      <div className="flex justify-between">
        <Link
          to={{
            pathname: '/LoadingScreen',
            state: {
              name: 'DSO',
            },
          }}
          className={`${
            path.pathname === '/PEAGRID' ? 'bg-purple-800 text-white' : 'text-pea-primary'
          } focus:text-white focus:bg-purple-800 m-2 rounded-full py-2 px-4 border border-color-pea-primary text-xs leading-4 font-normal hover:text-white hover:bg-purple-800 focus:outline-none focus:border-color-pea-primary focus:shadow-outline-purple transition ease-in-out duration-150`}
        >
          DSO
        </Link>
        {nameButton.map((v) => {
          if (v.userProfile.length > 0) {
            return (
              <Link
                key={v.userProfile[0].name}
                to={{
                  pathname: '/LoadingScreen',
                  state: {
                    name: `${v.userProfile[0].name}`,
                    userId: `${v.userId}`,
                  },
                }}
                className={`${
                  path.state !== undefined && path.state.name === v.userProfile[0].name
                    ? 'bg-purple-800 text-white'
                    : 'text-pea-primary'
                } focus:text-white focus:bg-purple-800 m-2 rounded-full py-2 px-4 border border-color-pea-primary text-xs leading-4 font-normal hover:text-white hover:bg-purple-800 focus:outline-none focus:border-color-pea-primary focus:shadow-outline-purple transition ease-in-out duration-150`}
              >
                <div> อาคาร {v.userProfile[0].name}</div>
              </Link>
            );
          }
        })}
      </div>
      <SelectDateConsumer>
        {(store) => (
          <div className="flex">
            <p className="m-4 text-pea-primary font-bold text-xs">
              {moment(store.date).format('dddd Do MMMM YYYY')}
            </p>
            <OutsideClickHandler
              onOutsideClick={() => {
                !hide && setHide(true);
              }}
            >
              <button
                type="button"
                onClick={() => setHide(!hide)}
                className="rounded-md flex justify-between m-2  py-2 px-4 border border-color-pea-primary text-xs leading-4 font-normal text-pea-primary  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-color-pea-primary focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
              >
                <svg
                  className="w-4 mr-2"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
                </svg>
                เลือกวันที่
              </button>
            </OutsideClickHandler>
            <div
              className={`absolute shadow-xl ${
                hide
                  ? 'transform opacity-0 ml-64 scale-0'
                  : 'transition ease-in duration-75 transform opacity-100 scale-100 mt-10 -ml-28'
              } `}
            >
              <Calendar value={store.date} onChange={(value) => store.submitDate(value)} />
            </div>
          </div>
        )}
      </SelectDateConsumer>
    </div>
  );
};

TabBarTower.propTypes = {
  nameButton: PropTypes.array,
  path: PropTypes.object,
};

TabBarTower.defaultProps = {
  nameButton: [],
  path: {},
};

export default TabBarTower;
