import React, { useState, useEffect } from 'react';
import tower from '../resource/tower.svg';
import solarpanel from '../resource/solar-panel.png';
import wallet from '../resource/wallet.png';
import PowerConsumptionGraph from '../resource/PowerConsumptionGraph.png';

import { getElectricity } from '../api/Electricity.script';
import * as Admin from '../api/Admin.script';
import * as DSOReconcile from '../api/DSOReconcile.script';
import * as Wallet from '../api/Wallet.script';
import * as SmartMeter from '../api/SmartMeter.script';
import { set } from 'lodash';

export const CardBuildingInfo = (props) => {
  const [responseAllUsers, setResponseAllUsers] = useState();
  const [nameBuilding, setNameBuilding] = useState();
  const [nameIntendant, setNameIntendant] = useState();
  const [detailUser, setDetailUser] = useState();
  async function getResponseAllUsers() {
    try {
      const responseAllUser = await Admin.getAllUsers();
      setResponseAllUsers(responseAllUser.data.length);
      responseAllUser.data.forEach((resUser) => {
        if (resUser.userProfile.length > 0) {
          if (resUser.userProfile[0].name === `${props.name}`) {
            setNameBuilding(resUser.userProfile[0].name);
            setNameIntendant(resUser.userProfile[0].contactName);
            setDetailUser(resUser.userProfile[0].details);
          }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseAllUsers();
  }, []);

  return (
    <div className="w-full h-48 rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6  font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        ข้อมูลอาคาร
      </div>
      <div className="flex">
        <div className="w-3/12">
          <img src={tower} alt="logo" className="img-center px-2" />
        </div>
        <div className="w-9/12 p-2">
          <p className="xl:text-base md:text-sm text-purple-800 font-bold">
            อาคาร {responseAllUsers !== 0 && nameBuilding}
          </p>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ชื่ออาคาร</p>
            <p className="mr-2 xl:text-base md:text-xs text-purple-800 font-normal">
              อาคาร {responseAllUsers !== 0 && nameBuilding}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ผู้ดูแลอาคาร</p>
            <p className="mr-2 xl:text-base md:text-xs text-purple-800 font-normal">
              {responseAllUsers !== 0 && nameIntendant}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ข้อมูลทั่วไป</p>
            <p className="mr-2 xl:text-base md:text-xs text-purple-800 font-normal">
              {responseAllUsers !== 0 && detailUser}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardToolInfo = (props) => {
  const [responseAllInventories, setResponseAllInventories] = useState();
  const [nameInventory, setNameInventory] = useState();
  const [typeInventory, setTypeInventory] = useState();
  const [capacityInventory, setCapacityInventory] = useState();
  const [buildingId, setBuildingId] = useState();
  async function getResponseAllInventories() {
    try {
      const responseAllUser = await Admin.getAllUsers();
      const responseAllInventory = await Admin.getAllInventories();
      setResponseAllInventories(responseAllInventory.data.length);
      responseAllUser.data.forEach((resUser) => {
        if (resUser.userProfile.length > 0) {
          if (resUser.userProfile[0].name === `${props.name}`) {
            setBuildingId(resUser.userId);
            responseAllInventory.data.forEach((resInventory) => {
              if (resInventory.userId === buildingId) {
                setNameInventory(resInventory.name);
                setTypeInventory(resInventory.type);
                setCapacityInventory(resInventory.capacity.toLocaleString());
              }
            });
          }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseAllInventories();
  });
  return (
    <div className="w-full h-48 rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        ข้อมูลอุปกรณ์
      </div>
      <div className="flex">
        <div className="w-3/12">
          <img src={solarpanel} alt="logo" className="img-center px-2" />
        </div>
        <div className="w-9/12 p-2">
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ชื่ออุปกรณ์</p>
            <p className="mr-2 xl:text-base md:text-sm text-purple-800 font-normal">
              {responseAllInventories !== 0 && nameInventory}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">ประเภทอุปกรณ์</p>
            <p className="mr-2 xl:text-base md:text-sm text-purple-800 font-normal">
              {responseAllInventories !== 0 && typeInventory}
            </p>
          </div>
          <div className="flex justify-between">
            <p className="xl:text-base md:text-sm text-gray-600 font-normal">กำลังการผลิต</p>
            <p className="mr-2 xl:text-base md:text-sm text-purple-800 font-normal">
              {capacityInventory && capacityInventory} kWp
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardWallet = (props) => {
  const [responseWallet, setResponseWallet] = useState();
  const [balance, setBalance] = useState();
  async function getAllWalletAdmin() {
    try {
      const responseAllUser = await Admin.getAllUsers();
      const responseAllWallet = await Wallet.getWalletAdmin();
      setResponseWallet(responseAllWallet.data.length);
      responseAllUser.data.forEach((resUser) => {
        if (resUser.userProfile.length > 0) {
          if (resUser.userProfile[0].name === `${props.name}`) {
            const buildingId = resUser.userId;
            responseAllWallet.data.forEach((resWallet) => {
              if (resWallet.userId === buildingId) {
                setBalance(
                  resWallet.token.toLocaleString('en-US', {
                    maximumFractionDigits: 2,
                    minimumFractionDigits: 2,
                  })
                );
              }
            });
          }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getAllWalletAdmin();
  }, []);
  return (
    <div className="w-full h-48 rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        กระเป๋าเงิน
      </div>
      <div className="flex">
        <div className="w-3/12 ml-4">
          <img src={wallet} alt="logo" className="img-center" />
        </div>
        <div className="w-9/12 p-2">
          <div className="flex justify-end">
            <p className="mr-2 xl:text-lg md:text-base text-gray-600 font-normal">
              จำนวน Token ที่มี
            </p>
          </div>
          <div className="flex justify-end items-center">
            <p className="mr-2 xl:text-4xl md:text-3xl text-purple-800 font-bold">
              {responseWallet > 0 && balance}
            </p>
            <p className="mr-2 xl:text-base md:text-sm text-gray-600 font-normal">Token</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardUsageInformation = (props) => {
  const [consumer, setConsumer] = useState();
  const [prosumer, setProsumer] = useState();

  async function getResponseDSOReconcile() {
    try {
      const responseAllUser = await Admin.getAllUsers();
      const responseMeter = await SmartMeter.getSmartMeterAdmin();
      responseAllUser.data.forEach((resUser) => {
        if (resUser.userProfile.length > 0) {
          if (resUser.userProfile[0].name === `${props.name}`) {
            const buildingId = resUser.userId;
            responseMeter.data.forEach(async (resMeter) => {
              if (resMeter.userId === buildingId) {
                const response = await getElectricity(resMeter.smartMeterNo);
                const { data, code } = response;
                if (code === '0') {
                  data.map((value) => {
                    if (value.type === 'consumer') {
                      setConsumer(value);
                    }
                    if (value.type === 'prosumer') {
                      setProsumer(value);
                    }
                  });
                } else {
                  console.log(response.message);
                }
              }
            });
          }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseDSOReconcile();
  }, []);

  return (
    <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-l-4 border-yellow-600">
        ข้อมูลการใช้ / การผลิต
      </div>
      <div className="flex flex-col m-2">
        <div className="h-6/12 mb-2">
          <div className="flex">
            <div className="flex flex-col items-between text-center xl:w-6/12 md:w-7/12">
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">
                ปริมาณพลังงานไฟฟ้านำเข้า
              </p>
              <p className="mr-2 xl:text-3xl md:text-2xl text-purple-800 font-bold">
                {!consumer ? '0' : consumer.sumEnergyKwh}
              </p>
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">kWh</p>
            </div>
            <div className="flex flex-col items-between text-center xl:w-6/12 md:w-5/12">
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">
                ปริมาณพลังงานไฟฟ้านำออก
              </p>
              <p className="mr-2 xl:text-3xl md:text-2xl text-purple-800 font-bold">
                {!prosumer ? '0' : prosumer.sumEnergyKwh}
              </p>
              <p className="mr-2 xl:text-base md:text-xs text-gray-600 font-normal">kWh</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardPowerConsumptionGraph = () => (
  <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
    <div className="flex justify-between">
      <div className="px-6 py-2 font-bold text-xl text-start my-2 text-pea-yellow">
        กราฟแสดงปริมาณการใช้ไฟฟ้า
      </div>
      <div className="px-3 py-2">
        <div className="flex">
          <p className="m-4 text-purple-800 font-bold text-xs">2 ตุลาคม 2563</p>
          <button
            type="button"
            className="rounded-md flex justify-between m-2  py-2 px-2 border border-purple-800 text-xs leading-4 font-normal text-purple-800  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-purple-800 focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
          >
            <svg
              className="w-4 mr-2"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
            </svg>
            เลือกวันที่
          </button>
          <button
            type="button"
            className="rounded-md  m-2 py-2 px-2 border border-purple-800 text-xs leading-4 font-normal text-purple-800  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-purple-800 focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
          >
            7 วันย้อนหลัง
          </button>
        </div>
      </div>
    </div>
    <div className="text-center mx-5 my-2">
      <img src={PowerConsumptionGraph} alt="logo" className="img-center" />
    </div>
    <div className="mx-10 mb-5">
      <div className="flex">
        <div className="mr-2 flex">
          <div className="w-4 h-4 bg-yellow-700 mt-1 mr-2" />
          <p className="text-pea-gray">ขาย</p>
        </div>
        <div className="ml-20 flex">
          <div className="w-4 h-4 bg-purple-800 mt-1 mr-2" />
          <p className="text-pea-gray">ซื้อ</p>
        </div>
      </div>
    </div>
  </div>
);

export default {
  CardBuildingInfo,
  CardToolInfo,
  CardWallet,
  CardUsageInformation,
  CardPowerConsumptionGraph,
};
