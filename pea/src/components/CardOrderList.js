import React, { useState, useEffect, useContext } from 'react';
import { useLocation } from 'react-router-dom';
import OutsideClickHandler from 'react-outside-click-handler';
import moment from 'moment';
import _ from 'lodash';
import 'moment/locale/th';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import * as Order from '../api/Order.script';
import * as Admin from '../api/Admin.script';
import SelectDateContext, { SelectDateConsumer } from '../store';

export const CardOrderList = () => {
  const [times, setTimes] = useState(new Date());
  const [hide, setHide] = useState(true);
  const [listOrder, setListOrder] = useState({});
  const [path, setPath] = useState();

  async function getOrderUser() {
    try {
      const response = await Order.getOrderUser();
      if (response.data) {
        setListOrder({ ...listOrder, response });
      } else {
        setListOrder({});
      }
    } catch (error) {
      console.log(error);
    }
  }

  async function selectTime(times) {
    setTimes(times);
    try {
      const newDateTime = moment(times).format('YYYY-MM-DD');
      const response = await Order.getOrderUser(newDateTime);
      if (response.data) {
        setListOrder({ ...listOrder, response });
      } else {
        setListOrder({});
      }
    } catch (error) {
      console.log(error);
    }
  }
  const location = useLocation();

  useEffect(() => {
    const currentPath = location.pathname;
    setPath(currentPath);
    getOrderUser();
  }, []);

  const getDataSell = () => {
    const data = listOrder.response.data.reverse();
    const sell = _.filter(data, { side: 'sell' });
    return sell;
  };

  const getDataBuy = () => {
    const data = listOrder.response.data.reverse();
    const buy = _.filter(data, { side: 'buy' });
    return buy;
  };

  const getDataAll = () => {
    const data = listOrder.response.data.reverse();
    if (path === '/TradingBuy') {
      return getDataBuy();
    }
    if (path === '/TradingSell') {
      return getDataSell();
    }
    return data;
  };

  return (
    <>
      <div className="w-full rounded overflow-hidden shadow bg-white ">
        <div className="flex justify-between">
          <div className="font-bold xl:text-xl md:text-lg pl-5 mt-5 mb-2 text-header-pea-gray border-l-4 border-yellow-600">
            รายการคำสั่ง วัน{moment(times).format('dddd Do MMMM YYYY')}
          </div>
          <div className="px-3 py-2">
            <div className="flex">
              <OutsideClickHandler
                onOutsideClick={() => {
                  !hide && setHide(true);
                }}
              >
                <button
                  type="button"
                  onClick={() => setHide(!hide)}
                  aria-hidden="true"
                  className="flex rounded-md justify-between items-center m-2  py-2 px-2 border border-color-pea-primary text-xs leading-4 font-normal text-pea-primary  hover:text-white hover:bg-purple-800 focus:outline-none focus:border-color-pea-primary focus:shadow-outline-purple focus:text-white focus:bg-purple-800 transition ease-in-out duration-150"
                >
                  <svg
                    className="w-4 mr-2"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
                  </svg>
                  เลือกวันที่
                </button>
              </OutsideClickHandler>
              <div />
            </div>
            <div
              className={`absolute shadow-xl ${
                hide
                  ? 'transform opacity-0 ml-64 scale-0'
                  : 'transition ease-in duration-75 transform opacity-100 scale-100 -ml-64'
              } `}
            >
              <Calendar value={times} onChange={selectTime} />
            </div>
          </div>
        </div>
        {listOrder.response ? (
          getDataAll().length > 0 ? (
            getDataAll().map((v) => {
              return (
                <div
                  key={v.orderId}
                  className={`flex mx-5 mt-2 mb-3 px-3 py-2 justify-between border-2 ${
                    v.side === 'buy' ? 'border-color-pea-primary' : 'border-color-pea-yellow'
                  }  rounded-md`}
                >
                  <div className="w-1/3 flex">
                    <p className="xl:text-base md:text-xs text-pea-gray font-normal">
                      จำนวนที่{v.side === 'buy' ? 'ซื้อ' : 'ขาย'}
                    </p>
                    <p className="xl:text-base md:text-xs text-pea-primary font-bold md:ml-2 xl:ml-4">
                      {v.committedKwh && v.committedKwh.toLocaleString().slice(0, -2)}
                    </p>
                    <p className="xl:text-base md:text-xs text-pea-gray font-normal ml-2">
                      หน่วย(kWh)
                    </p>
                  </div>
                  <div className="w-1/3 text-center flex justify-center">
                    <p className="xl:text-base md:text-xs text-pea-gray font-normal">วันที่</p>
                    <p className="xl:text-base md:text-xs text-pea-primary font-bold ml-4">
                      {moment(v.startDate).format('YYYY-MM-DD HH:mm')}น. -{' '}
                      {moment(v.endDate).format('HH:mm')}
                      น.
                    </p>
                  </div>
                  <div className="w-1/3 text-right flex justify-end">
                    <p className="xl:text-base md:text-xs text-pea-gray font-normal">หน่วยละ</p>
                    <p className="xl:text-base md:text-xs text-pea-primary font-bold ml-6">
                      {v.unitPrice.toLocaleString().slice(0, -2)}
                    </p>
                    <p className="xl:text-base md:text-xs text-pea-gray font-normal ml-2">Token</p>
                  </div>
                </div>
              );
            })
          ) : (
            <div className="flex justify-center py-5 text-xl">ไม่พบการทำรายการ</div>
          )
        ) : (
          <div className="flex justify-center py-5 text-xl">ไม่พบการทำรายการ</div>
        )}
      </div>
    </>
  );
};

export const CardOrderListAdmin = ({ name }) => {
  const [user, setUser] = useState();
  const [order, setOrder] = useState();
  const [time, setTime] = useState();
  const [values, setError] = useState(false);
  const store = useContext(SelectDateContext);

  const getUser = async () => {
    try {
      const response = await Admin.getAllUsers();
      const { data } = response;
      data.map((value) => {
        if (value.userProfile.length > 0) {
          if (value.userProfile[0].name === name) {
            const { userId } = value;
            setUser(userId);
          }
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  const getDataOrder = async () => {
    const times = store.date;
    try {
      const newDateTime = moment(times).format('YYYY-MM-DD');
      const response = await Order.getAllOrderAdmin(newDateTime);
      const { code, data } = response;
      if (code !== '404') {
        const dataFilter = _.filter(data, { userId: user });
        if (dataFilter.length !== 0) {
          setOrder(dataFilter);
        }
        setError(true);
      } else {
        setError(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getUser();
    getDataOrder();
  }, [time]);

  const orders = () => {
    if (order) {
      return order.map((v) => (
        <div
          key={v.orderId}
          className={`flex mx-5 mt-2 mb-3 px-3 py-2 justify-between border-2 ${
            v.side === 'buy' ? 'border-color-pea-primary' : 'border-color-pea-yellow'
          }  rounded-md`}
        >
          <div className="w-1/3 flex">
            <p className="xl:text-base md:text-xs text-pea-gray font-normal">
              จำนวนที่{v.side === 'buy' ? 'ซื้อ' : 'ขาย'}
            </p>
            <p className="xl:text-base md:text-xs text-pea-primary font-bold md:ml-2 xl:ml-4">
              {v.committedKwh && v.committedKwh.toLocaleString().slice(0, -2)}
            </p>
            <p className="xl:text-base md:text-xs text-pea-gray font-normal ml-2">หน่วย(kWh)</p>
          </div>
          <div className="w-1/3 text-center flex justify-center">
            <p className="xl:text-base md:text-xs text-pea-gray font-normal">วันที่</p>
            <p className="xl:text-base md:text-xs text-pea-primary font-bold ml-4">
              {moment(v.startDate).format('YYYY-MM-DD HH:mm')}น. -{' '}
              {moment(v.endDate).format('HH:mm')}
              น.
            </p>
          </div>
          <div className="w-1/3 flex justify-end">
            <p className="xl:text-base md:text-xs text-pea-gray font-normal">หน่วยละ</p>
            <p className="xl:text-base md:text-xs text-pea-primary font-bold ml-6">
              {v.unitPrice.toLocaleString().slice(0, -2)}
            </p>
            <p className="xl:text-base md:text-xs text-pea-gray font-normal ml-2">Token</p>
          </div>
        </div>
      ));
    }
    if (values) {
      return <div className="flex justify-center py-5 text-xl">ไม่พบการทำรายการ</div>;
    }
  };

  return (
    <SelectDateConsumer>
      {(store) => {
        setTime(store.date);
        return (
          <div className="w-full rounded overflow-hidden shadow bg-white mt-2">
            <div className="flex justify-between">
              <div className="font-bold xl:text-xl md:text-lg pl-5 mt-5 mb-2 text-header-pea-gray border-l-4 border-yellow-600">
                รายการคำสั่ง วัน{moment(store.date).format('ddddที่ Do MMMM YYYY')}
              </div>
            </div>
            {orders()}
          </div>
        );
      }}
    </SelectDateConsumer>
  );
};

export default { CardOrderList, CardOrderListAdmin };
