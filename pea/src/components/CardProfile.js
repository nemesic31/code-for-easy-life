import React, { useState, useEffect } from 'react';
import wallet from '../resource/wallet.png';
import solarpanel from '../resource/solar-panel.png';
import * as Inventory from '../api/Inventory.script';
import * as User from '../api/User.script';
import * as Wallet from '../api/Wallet.script';

export const CardBuildingInfo = () => {
  const [responseUserProfile, setResponseUserProfile] = useState();
  const [nameBuilding, setNameBuilding] = useState();
  const [nameIntendant, setNameIntendant] = useState();
  const [detailUser, setDetailUser] = useState();

  async function getResponseInventory() {
    try {
      const responseUser = await User.getUserBuilding();
      setResponseUserProfile(responseUser.data.length);
      if (responseUser.data.length !== 0) {
        const { name, details, contactName } = responseUser.data[0];
        setNameBuilding(name);
        setNameIntendant(contactName);
        setDetailUser(details);
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseInventory();
  }, []);

  return (
    <div className="w-full rounded overflow-hidden shadow bg-white py-4">
      <div className="flex justify-between items-center">
        <div className="font-bold xl:text-xl md:text-lg border-yellow-600 border-l-4 px-4 text-header-pea-gray">
          ข้อมูลอาคาร
        </div>
      </div>
      <div className="px-3">
        <div className="xl:text-lg md:text-base text-purple-800 font-bold ml-3 mt-3">
          อาคาร {responseUserProfile !== 0 && nameBuilding}
        </div>
        <div className="mx-1">
          <div className="flex justify-between mt-2">
            <p className="ml-2 text-gray-600 xl:text-base md:text-sm font-normal">ชื่ออาคาร</p>
            <p className="text-purple-800 text-base font-normal">
              อาคาร {responseUserProfile !== 0 && nameBuilding}
            </p>
          </div>
          <div className="flex justify-between mt-2">
            <p className="ml-2 text-gray-600 xl:text-base md:text-sm font-normal">ผู้ดูแลอาคาร</p>
            <p className="text-purple-800 xl:text-base md:text-sm font-normal">
              {responseUserProfile !== 0 && nameIntendant}
            </p>
          </div>
          <div className="flex justify-between mt-2">
            <p className="ml-2 text-gray-600 xl:text-base md:text-sm font-normal">ข้อมูลทั่วไป</p>
            <p className="text-purple-800 xl:text-base md:text-sm font-normal">
              {responseUserProfile !== 0 && detailUser}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardWallet = () => {
  const [responseWallet, setResponseWallet] = useState();
  const [pocketNo, setPocketNo] = useState();
  const [walletAddress, setWalletAddress] = useState();

  async function getResponseWallet() {
    try {
      const response = await Wallet.getWalletUser();
      setResponseWallet(response.data.length);
      if (response.data.length !== 0) {
        const { walletNo, walletAddress } = response.data[0];
        setPocketNo(walletNo);
        setWalletAddress(walletAddress);
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseWallet();
  }, []);

  return (
    <div className="w-full rounded overflow-hidden shadow bg-white py-4">
      <div className="font-bold xl:text-xl md:text-lg text-header-pea-gray px-4 border-yellow-600 border-l-4">
        กระเป๋าเงิน
      </div>
      <div className="flex w-full px-3">
        <div className="mx-2 w-3/12">
          <img alt="wallet" src={wallet} className="img-center w-full" />
        </div>
        <div className="mr-2 w-9/12 mt-3">
          <div className="flex justify-between w-full">
            <p className="xl:text-lg md:text-base font-normal text-gray-600">เลข Wallet</p>
            <p className="text-purple-800 font-normal xl:text-base md:text-sm">
              {responseWallet !== 0 && pocketNo}
            </p>
          </div>
          <div className="w-full">
            <a
              target="_blank"
              href={`https://explorer-etp.rsddev.team/address/${
                responseWallet !== 0 && walletAddress
              }/transactions`}
            >
              <p className="text-yellow-600 font-normal xl:text-base md:text-sm mt-4 truncate">
                {responseWallet !== 0 && walletAddress}
              </p>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardTool = () => {
  const [nameTool, setNameTool] = useState();
  const [capacityTool, setCapacityTool] = useState();

  async function getResponseInventory() {
    try {
      const responseTool = await Inventory.getInventory();
      const { name, capacity } = responseTool.data[0];
      setNameTool(name);
      setCapacityTool(capacity.toLocaleString());
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseInventory();
  }, []);

  return (
    <div className="w-full rounded overflow-hidden shadow bg-white py-4">
      <div className="font-bold xl:text-xl md:text-lg text-header-pea-gray px-4 border-yellow-600 border-l-4">
        อุปกรณ์
      </div>
      <div className="w-full px-3">
        <div className="mx-1 my-5">
          <div className="flex w-full my-4 items-center border-2 border-purple-800 rounded-md py-4 px-3">
            <div className="mx-4">
              <img alt="solar" className="img-center h-16" src={solarpanel} />
            </div>
            <div className="flex justify-between w-full mx-3 ">
              <div className="flex flex-col text-gray-600 font-normal xl:text-base md:text-sm">
                <p className="my-1">ประเภทอุปกรณ์</p>
                <p className="my-1">กำลังการผลิต</p>
              </div>
              <div className="flex flex-col text-purple-800 font-normal xl:text-base md:text-sm text-right">
                <p className="my-1">{nameTool && nameTool} kW</p>
                <p className="my-1">{capacityTool && capacityTool} kWp</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default {
  CardBuildingInfo,
  CardWallet,
  CardTool,
};
