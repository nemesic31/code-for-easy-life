import React, { useState } from 'react';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import auth from '../utils/auth';
import SignInSide from '../resource/image.png';
import logo from '../resource/facebook-logo.png';
import loadingPNG from '../resource/loading.png';
import logoPEA from '../resource/LOGO_PEA.jpg';
import * as User from '../api/User.script';
import { alertComingSoon } from './SweetAlert';

export const CardImage = () => (
  <div className="bg-primary h-full">
    <img src={SignInSide} alt="logo" className="h-full w-screen opacity-50" />
  </div>
);

export const FormLogin = () => {
  const { location } = window;
  const [hide, setHide] = useState(true);
  const [loading, setLoading] = useState(false);

  const validationSchema = Yup.object({
    email: Yup.string().email('รูปแบบอีเมลไม่ถูกต้อง').required('จำเป็นต้องกรอก'),
    password: Yup.string().min(3, 'too shot').max(20, '* too long').required('จำเป็นต้องกรอก'),
  });

  const { handleSubmit, handleChange, values, errors, touched, handleBlur } = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema,
    onSubmit: async (value) => {
      setLoading(true);
      const { email, password } = value;
      const response = await User.login(email, password);
      const { token, userId } = response;

      if (token !== undefined) {
        auth.setToken(token, true);
        auth.setUserInfo(userId, true);
        const responseUser = await User.getuser(response);
        const { roles } = responseUser.data;
        if (roles[0] === 'admin') {
          location.replace('/admin');
        } else {
          location.replace('/DashboardBuildingUser');
        }
      } else {
        setHide(false);
        setLoading(false);
      }
    },
  });

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="mb-4 mt-2">
          <p className={`text-sm text-center ${!hide ? 'text-red-600' : 'text-white'}`}>
            อีเมลหรือพาสเวิร์ดผิดพลาด
          </p>
          <p className="text-gray-500 font-normal text-base">
            อีเมล <span className="text-red-500">*</span>
          </p>
          <input
            className="w-full h-10 bg-gray-200 mt-1 p-1"
            name="email"
            onBlur={handleBlur}
            value={values.email}
            onChange={handleChange}
          />
          {errors.email && touched.email ? (
            <span className="text-xs text-red-500">{errors.email}</span>
          ) : (
            <span className="text-xs opacity-0">hi</span>
          )}
        </div>
        <div className="my-4">
          <p className="text-gray-500 font-normal text-base">
            รหัสผ่าน <span className="text-red-500">*</span>
          </p>
          <input
            className="w-full h-10 bg-gray-200 mt-1 p-1"
            name="password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
            type="password"
          />
          {errors.password && touched.password ? (
            <span className="text-xs text-red-500">{errors.password}</span>
          ) : (
            <span className="text-xs opacity-0">hi</span>
          )}
        </div>

        <div className="flex justify-end my-4">
          <p
            className="text-primary font-normal text-xs cursor-pointer"
            onClick={() => alertComingSoon()}
          >
            ลืมรหัสผ่าน ?
          </p>
        </div>
        <div className="my-4">
          <button
            type="submit"
            className="bg-primary hover:bg-secondary w-full h-12 rounded-md flex items-center"
          >
            <div className="absolute px-2">
              {!loading ? (
                <svg
                  className="text-white w-5"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                    clipRule="evenodd"
                  />
                </svg>
              ) : (
                <img src={loadingPNG} alt="loading" className="animate-spin w-8" />
              )}
            </div>
            <div className="w-full text-center">
              <p className="text-white xl:text-base md:text-sm font-semibold">ลงชื่อเข้าใช้</p>
            </div>
          </button>
        </div>
      </form>
      <div className="my-4 flex">
        <div className="border-b border-black w-1/3 mb-2" />
        <div className="xl:text-sm md:text-xs text-gray-600 font-normal w-1/3 text-center">
          หรือ
        </div>
        <div className="border-b border-black w-1/3 mb-2" />
      </div>
      <div className="my-4">
        <button
          onClick={() => alertComingSoon()}
          className="bg-facebook w-full h-12 rounded-md flex items-center"
          type="button"
        >
          <div className="absolute px-2">
            <img src={logo} alt="logo" className="w-6" />
          </div>
          <div className="w-full text-center">
            <p className="text-white xl:text-base md:text-sm font-semibold">
              ลงชื่อเข้าใช้ด้วย Facebook
            </p>
          </div>
        </button>
      </div>
      <div>
        <div
          className="xl:text-sm md:text-xs text-primary font-normal text-center "
          onClick={() => alertComingSoon()}
        >
          สมัครสมาชิก
        </div>
      </div>
    </div>
  );
};

export const CardRight = () => (
  <div className="w-full">
    <div className="flex justify-center">
      <img src={logoPEA} className="w-6/12 h-full" alt="logo-pea" />
    </div>
    <div className="my-3">
      <h1 className="font-bold text-primary text-center text-4xl md:text-2xl">เข้าสู่ระบบ</h1>
    </div>
    <FormLogin />
  </div>
);

export default {
  CardImage,
  FormLogin,
};
