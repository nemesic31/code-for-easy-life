import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import swal from 'sweetalert';
import * as Yup from 'yup';
import { confirmBuy } from './SweetAlert';
import * as SmartMeter from '../api/SmartMeter.script';

export const CardBuySystem = () => {
  const dateTime = new Date();
  dateTime.setDate(dateTime.getDate() + 1);
  const showDate = `${dateTime.getDate()}/${dateTime.getMonth() + 1}/${dateTime.getFullYear()}`;
  const date = `${dateTime.getFullYear()}-${dateTime.getMonth() + 1}-${dateTime.getDate()}`;
  const times = `${dateTime.getHours()}${dateTime.getMinutes()}`;
  const [limitQuantityBuy, setLimitQuantityBuy] = useState();
  async function getSmartMeter() {
    try {
      const response = await SmartMeter.getSmartMeterUser();
      const { loadMaxValueKwp } = response.data[0];
      setLimitQuantityBuy(loadMaxValueKwp);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getSmartMeter();
  }, []);

  const validationSchema = Yup.object({
    price: Yup.number()
      .typeError('* กรอกตัวเลขเท่านั้น *')
      .required('* จำเป็นต้องกรอก *')
      .moreThan(0.01, '* ขั้นต่ำ 0.01 Token *'),
    estimated: Yup.number()
      .typeError('* กรอกตัวเลขเท่านั้น *')
      .moreThan(0.0001, '* ขั้นต่ำ 0.0001 หน่วย *')
      .lessThan(limitQuantityBuy, `* จำนวนที่ซื้อได้ ${limitQuantityBuy} หน่วย *`)
      .required('* จำเป็นต้องกรอก *'),
  });

  const { handleSubmit, handleChange, values, errors, touched, handleBlur } = useFormik({
    initialValues: {
      startTimes: '',
      endTimes: '',
      price: '',
      estimated: '',
    },
    validationSchema,
    onSubmit: async (value) => {
      if (
        value.startTimes !== '' &&
        value.endTimes !== '' &&
        value.price !== '' &&
        value.estimated !== ''
      ) {
        await confirmBuy(value);
      } else {
        swal('กรอกข้อมูลไม่ครบ', {
          buttons: 'ตกลง',
        });
      }
    },
  });

  return (
    <>
      <div className="w-full rounded overflow-hidden shadow bg-white">
        <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
          ระบบซื้อ
        </div>
        <div className="px-6 flex text-xl text-primary font-bold mb-2">
          <p className="mr-1"> ช่วงเวลาที่ซื้อในวันที่</p>
          <p>{showDate}</p>
        </div>
        <div className="px-5 pb-4">
          <form onSubmit={handleSubmit}>
            <div>
              <div className="flex justify-between mb-2">
                <div className="xl:w-2/5 md:w-5/12">
                  <div>
                    <span className="rounded-md">
                      <select
                        id="startTimes"
                        name="startTimes"
                        value={values.startTimes}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className="flex justify-between w-full rounded-md border border-color-pea-primary px-4 py-2 bg-white xl:text-base md:text-sm leading-5 font-normal text-pea-primary hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150"
                      >
                        <option>เลือกเวลา</option>
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 06:00:00`}>06.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 07:00:00`}>07.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 08:00:00`}>08.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 09:00:00`}>09.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 10:00:00`}>10.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 11:00:00`}>11.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 12:00:00`}>12.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 13:00:00`}>13.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 14:00:00`}>14.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 15:00:00`}>15.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 16:00:00`}>16.00 น.</option>
                        )}
                        {(times >= '0600') & (times <= '1800') && (
                          <option value={`${date} 17:00:00`}>17.00 น.</option>
                        )}
                      </select>
                    </span>
                  </div>
                </div>
                <div className="xl:w-1/5 md:w-2/12 xl:text-base md:text-sm flex justify-center items-center">
                  ถึง
                </div>
                <div className="xl:w-2/5 md:w-5/12">
                  <div>
                    <span className="rounded-md">
                      <select
                        id="endTimes"
                        name="endTimes"
                        value={values.endTimes}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className="flex justify-between w-full rounded-md border border-color-pea-primary px-4 py-2 bg-white xl:text-base md:text-sm leading-5 font-normal text-pea-primary hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150"
                      >
                        <option>เลือกเวลา</option>
                        {values.startTimes <= `${date} 06:00:00` && (
                          <option value={`${date} 07:00:00`}>07.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 07:00:00` && (
                          <option value={`${date} 08:00:00`}>08.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 08:00:00` && (
                          <option value={`${date} 09:00:00`}>09.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 09:00:00` && (
                          <option value={`${date} 10:00:00`}>10.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 10:00:00` && (
                          <option value={`${date} 11:00:00`}>11.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 11:00:00` && (
                          <option value={`${date} 12:00:00`}>12.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 12:00:00` && (
                          <option value={`${date} 13:00:00`}>13.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 13:00:00` && (
                          <option value={`${date} 14:00:00`}>14.00 น.</option>
                        )}

                        {values.startTimes <= `${date} 14:00:00` && (
                          <option value={`${date} 15:00:00`}>15.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 15:00:00` && (
                          <option value={`${date} 16:00:00`}>16.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 16:00:00` && (
                          <option value={`${date} 17:00:00`}>17.00 น.</option>
                        )}
                        {values.startTimes <= `${date} 17:00:00` && (
                          <option value={`${date} 18:00:00`}>18.00 น.</option>
                        )}
                      </select>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="my-2">
                <p className="text-pea-gray xl:text-base md:text-sm font-bold">ราคาซื้อ</p>
                <div className="flex justify-between items-center">
                  <input
                    name="price"
                    value={values.price}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="border xl:text-base md:text-sm text-right px-2 border-color-pea-primary rounded-md w-5/6 h-10 mt-2"
                  />
                  <p className="text-pea-gray xl:text-base md:text-sm font-normal">Token</p>
                </div>
                {errors.price && touched.price ? (
                  <span className="text-xs text-red-500">{errors.price}</span>
                ) : (
                  <span className="text-xs opacity-0">hi</span>
                )}
              </div>

              <div>
                <p className="text-pea-gray xl:text-base md:text-sm font-bold">ปริมาณการซื้อ</p>
                <div className="flex justify-between items-center">
                  <input
                    name="estimated"
                    value={values.estimated}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className="border xl:text-base md:text-sm text-right px-2 border-color-pea-primary rounded-md w-5/6 h-10 mt-2"
                  />
                  <p className="text-pea-gray xl:text-base md:text-sm font-normal md:ml-4">
                    หน่วย (kWh)
                  </p>
                </div>
                {errors.estimated && touched.estimated ? (
                  <span className="text-xs text-red-500">{errors.estimated}</span>
                ) : (
                  <span className="text-xs opacity-0">hi</span>
                )}
                <div className="mt-2">
                  <p className="fon-normal text-pea-gray text-xs">
                    (ตั้งชื้อในเวลาทำการ 06:00 - 18:00 น. ซื้อได้ไม่เกิน{' '}
                    {limitQuantityBuy !== undefined &&
                      limitQuantityBuy.toLocaleString('en-US', {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2,
                      })}{' '}
                    หน่วย)
                  </p>
                </div>
              </div>
            </div>
            <div className="flex justify-center mt-4">
              <button
                type="submit"
                className="border border-purple-800 py-2 px-3 rounded-md bg-purple-800 text-white xl:text-base md:text-sm font-normal"
              >
                ทำรายการสั่งซื้อ
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default CardBuySystem;
