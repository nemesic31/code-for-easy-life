import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import GraphIncome from '../resource/GraphIncome.png';
import * as DSOReconcile from '../api/DSOReconcile.script';
import * as SmartMeter from '../api/SmartMeter.script';
import * as Wallet from '../api/Wallet.script';

export const CardGraphIncome = () => (
  <div className="w-full rounded overflow-hidden shadow bg-white hover:shadow-xl">
    <div className="px-6 py-4">
      <div className="font-bold text-xl text-start my-2 text-pea-yellow">กราฟแสดงรายได้</div>
      <div className="text-center my-5">
        <img src={GraphIncome} alt="logo" className="img-center" />
      </div>
      <div className="mt-10">
        <div className="flex">
          <div className="mr-2 flex">
            <div className="w-4 h-4 bg-yellow-700 mt-1 mr-2" />
            <p className="text-pea-gray">พลังงานที่อาคารขายกันเอง</p>
          </div>
          <div className="ml-2 flex">
            <div className="w-4 h-4 bg-purple-800 mt-1 mr-2" />
            <p className="text-pea-gray">พลังงานที่ DSO ขาย</p>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export const CardElectricSold = () => {
  const [totalSell, setTotalSell] = useState();
  const [balance, setBalance] = useState();
  const [solarQuantity, setSolarQuantity] = useState();
  const [loadQuantity, setLoadQuantity] = useState();
  async function getResponseDSOReconcile() {
    try {
      const responseMeter = await SmartMeter.getSmartMeterUser();
      const response = await DSOReconcile.getReconcileUser();
      const responseWallet = await Wallet.getWalletUser();
      setTotalSell(response.data.totalSell);
      if (responseMeter.data) {
        const { solarMaxValueKw, loadMaxValueKwp } = responseMeter.data[0];
        setSolarQuantity(
          solarMaxValueKw.toLocaleString('en-US', {
            maximumFractionDigits: 2,
            minimumFractionDigits: 2,
          })
        );
        setLoadQuantity(
          loadMaxValueKwp.toLocaleString('en-US', {
            maximumFractionDigits: 2,
            minimumFractionDigits: 2,
          })
        );
      }
      if (responseWallet.data.length > 0) {
        const { token } = responseWallet.data[0];
        setBalance(
          token.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 })
        );
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseDSOReconcile();
  }, []);
  return (
    <div className="flex">
      <div className="w-full mr-2 h-56 rounded overflow-hidden shadow bg-white hover:shadow-xl">
        <div className="flex px-3 py-1 my-4 font-bold xl:text-xl md:text-base text-start my-2 text-header-pea-gray border-l-4 border-yellow-600">
          พลังงานไฟฟ้าที่ Grid จำหน่าย
        </div>
        <div className="flex flex-col justify-end">
          <div className="text-center">
            <p className="xl:text-4xl md:text-3xl font-bold text-purple-800">
              {!totalSell ? '0' : totalSell.toLocaleString()}
            </p>
          </div>
          <div className="mt-6">
            <p className="xl:text-base md:text-sm text-pea-gray text-center">kWh</p>
          </div>
        </div>
      </div>
      <div className="w-full mx-2 h-56 rounded overflow-hidden shadow bg-white hover:shadow-xl">
        <div className="flex px-3 py-1 my-4 font-bold xl:text-xl md:text-base text-start my-2 text-header-pea-gray border-l-4 border-yellow-600">
          ปริมาณการใช้ Load รวม
        </div>
        <div className="flex flex-col justify-end">
          <div className="text-center">
            <p className="xl:text-4xl md:text-3xl font-bold text-purple-800">{loadQuantity || 0}</p>
          </div>
          <div className="mt-6">
            <p className="xl:text-base md:text-sm text-pea-gray text-center">kWh</p>
          </div>
        </div>
      </div>
      <div className="w-full mx-2 h-56 rounded overflow-hidden shadow bg-white hover:shadow-xl">
        <div className="flex px-3 py-1 my-4 font-bold xl:text-xl md:text-base text-start my-2 text-header-pea-gray border-l-4 border-yellow-600">
          ปริมาณการผลิต Solar รวม
        </div>
        <div className="flex flex-col justify-end">
          <div className="text-center">
            <p className="xl:text-4xl md:text-3xl font-bold text-purple-800">
              {solarQuantity || 0}
            </p>
          </div>
          <div className="mt-6">
            <p className="xl:text-base md:text-sm text-pea-gray text-center">kWh</p>
          </div>
        </div>
      </div>
      <div className="w-full ml-2 h-56 rounded overflow-hidden shadow bg-white hover:shadow-xl">
        <div className="flex px-3 py-1 my-4 font-bold xl:text-xl md:text-base text-start my-2 text-header-pea-gray border-l-4 border-yellow-600">
          สำหรับ DSO: ค่าธรรมเนียม (Token)
        </div>
        <div className="flex flex-col justify-end">
          <div className="text-center">
            <p className="xl:text-4xl md:text-3xl font-bold text-purple-800">{balance || 0}</p>
          </div>
          <div className="mt-6">
            <p className="xl:text-base md:text-sm text-pea-gray text-center">Token</p>
          </div>
        </div>
      </div>
    </div>
  );
};

CardElectricSold.propTypes = {
  name: PropTypes.string,
  unit: PropTypes.string,
};

CardElectricSold.defaultProps = {
  name: '',
  unit: '',
};

export default {
  CardGraphIncome,
  CardElectricSold,
};
