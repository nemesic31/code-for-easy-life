import React, { useState } from 'react';
import DropdownTime from './DropdownTime';
import data from '../utils/Time.json';
import moment from 'moment';

const OrderForm = () => {
  const [buy, setBuy] = useState(true);

  const date = new Date();

  const [selected, setSelected] = useState({
    label: 'เลือกเวลา',
    value: 'select',
  });

  return (
    <fragment>
      <div className="container mx-auto px-4 p-2">
        <div className="w-full rounded overflow-hidden shadow bg-white">
          <div className="hidden sm:block">
            <div className="border-b border-gray-200">
              <nav className="-mb-px flex space-x-8" aria-label="Tabs">
                <button
                  className="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 group inline-flex items-center p-4 border-b-2 font-medium text-sm"
                  onClick={() => setBuy(true)}
                >
                  ซื้อ
                </button>
                <button
                  className="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 group inline-flex items-center p-4 border-b-2 font-medium text-sm"
                  onClick={() => setBuy(false)}
                >
                  ขาย
                </button>
              </nav>
            </div>
          </div>

          <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-700 border-l-4">
            {`${buy ? 'คำสั่งซื้อ' : 'คำสั่งขาย'}`}
          </div>

          <div className="px-6 flex text-xl text-primary font-bold mb-2">
            <p className="mr-1">คำสั่งจะมีผลวันที่ : {moment(date).format('DD MMMM YYYY')}</p>
          </div>
          <div className="flex justify-between mb-2">
            <label className="text-pea-gray xl:text-base md:text-sm font-bold">เริ่มต้น : </label>
            <DropdownTime selected={selected} onSelectedChange={setSelected} options={data} />
            <label className=" text-pea-gray xl:text-base md:text-sm font-bold">สิ้นสุด : </label>
            <DropdownTime selected={selected} onSelectedChange={setSelected} options={data} />
          </div>
          <div className="my-2">
            <div className="flex justify-between items-center">
              <label className="text-pea-gray xl:text-base md:text-sm font-bold">
                {`${buy ? 'ราคาซื้อ :' : 'ราคาขาย :'}`}{' '}
              </label>
              <input
                name="price"
                className="border xl:text-base md:text-sm text-right px-2 border-color-pea-primary rounded-md w-5/6 h-10 mt-2"
              />
              <label className="text-pea-gray xl:text-base md:text-sm font-normal">
                Token/หน่วย
              </label>
            </div>
          </div>
          <div className="my-2">
            <div className="flex justify-between items-center">
              <label className="text-pea-gray xl:text-base md:text-sm font-bold">{`${
                buy ? 'ปริมาณไฟฟ้าที่ซื้อ :' : 'ปริมาณไฟฟ้าที่ขาย :'
              }`}</label>
              <input
                name="price"
                className="border xl:text-base md:text-sm text-right px-2 border-color-pea-primary rounded-md w-5/6 h-10 mt-2"
              />
              <label className="text-pea-gray xl:text-base md:text-sm font-normal">
                หน่วย (kWh)
              </label>
            </div>
          </div>
          <div className="flex justify-center mt-4">
            <button
              type="submit"
              className="border border-purple-800 py-2 px-3 rounded-md bg-purple-800 text-white xl:text-base md:text-sm font-normal"
            >
              {`${buy ? 'ทำรายการสั่งซื้อ' : 'ทำรายการสั่งขาย'}`}
            </button>
          </div>
        </div>
      </div>
    </fragment>
  );
};

export default OrderForm;
