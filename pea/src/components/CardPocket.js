import React, { useState, useEffect } from 'react';
import wallet from '../resource/wallet.png';
import * as Wallet from '../api/Wallet.script';

export const CardWallet = () => {
  const [responseWallet, setResponseWallet] = useState();
  const [balance, setBalance] = useState();
  async function getResponseWallet() {
    try {
      const response = await Wallet.getWalletUser();
      setResponseWallet(response.data.length);
      if (response.data.length !== 0) {
        const { token } = response.data[0];
        setBalance(
          token.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 })
        );
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseWallet();
  }, []);

  return (
    <div className="w-full h-48 rounded overflow-hidden shadow bg-white hover:shadow-xl">
      <div className="px-6 font-bold xl:text-xl md:text-lg text-start my-4 text-header-pea-gray border-yellow-600 border-l-4">
        กระเป๋าเงิน
      </div>
      <div className="flex">
        <div className="w-3/12 ml-4">
          <img src={wallet} alt="logo" className="img-center" />
        </div>
        <div className="w-9/12 p-2">
          <div className="flex justify-end">
            <p className="mr-2 xl:text-lg md:text-base text-gray-600 font-normal">
              จำนวน Token ที่ได้
            </p>
          </div>
          <div className="flex justify-end items-center">
            <p className="mr-2 xl:text-4xl md:text-3xl text-purple-800 font-bold">
              {responseWallet !== 0 && balance}
            </p>
            <p className="mr-2 xl:text-base md:text-sm text-gray-600 font-normal">Token</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CardWalletInfo = () => {
  const [responseWallet, setResponseWallet] = useState();
  const [pocketNo, setPocketNo] = useState();
  const [walletAddress, setWalletAddress] = useState();

  async function getResponseWallet() {
    try {
      const response = await Wallet.getWalletUser();
      setResponseWallet(response.data.length);
      if (response.data.length !== 0) {
        const { walletNo, walletAddress } = response.data[0];
        setPocketNo(walletNo);
        setWalletAddress(walletAddress);
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getResponseWallet();
  }, []);

  return (
    <div className="w-full rounded overflow-hidden shadow bg-white py-4">
      <div className="font-bold xl:text-xl md:text-lg text-header-pea-gray px-4 border-yellow-600 border-l-4">
        ข้อมูลกระเป๋าเงิน
      </div>
      <div className="w-full flex justify-between mt-3 px-3">
        <div className="flex flex-col ml-4 mb-3">
          <p className="text-gray-600 font-normal xl:text-lg md:text-base">เลข Wallet</p>
          <a
            target="_blank"
            href={`https://explorer-etp.rsddev.team/address/${
              responseWallet !== 0 && walletAddress
            }/transactions`}
          >
            <p className="mt-6 text-yellow-600 font-normal xl:text-base md:text-sm">
              {responseWallet !== 0 && walletAddress}
            </p>
          </a>
        </div>
        <div className="mr-2">
          <p className="text-purple-800 xl:text-base md:text-sm font-normal">
            {responseWallet !== 0 && pocketNo}
          </p>
        </div>
      </div>
    </div>
  );
};

export const CardHistory = () => (
  <div className="w-full rounded overflow-hidden shadow bg-white py-4">
    <div className="font-bold xl:text-xl md:text-lg text-header-pea-gray px-4 border-yellow-600 border-l-4">
      ประวัติเติมเงิน
    </div>
    <div className="px-6">
      <div className="flex my-2 px-4 py-4 justify-between w-full border-2 border-purple-800 rounded-md">
        <div className="flex">
          <p className="xl:text-base md:text-sm text-gray-600 font-normal">02/09/2563</p>
          <p className="xl:text-base md:text-sm text-gray-600 font-normal ml-6">
            เติมเงินเข้ากระเป๋าตัง
          </p>
        </div>
        <div className="flex">
          <p className="xl:text-base md:text-sm text-purple-800 font-bold ">5,000</p>
          <p className="xl:text-base md:text-sm text-gray-600 font-normal ml-6">Token</p>
        </div>
      </div>
      <div className="flex my-2 px-4 py-4 justify-between w-full border-2 border-purple-800 rounded-md">
        <div className="flex">
          <p className="xl:text-base md:text-sm text-gray-600 font-normal">02/09/2563</p>
          <p className="xl:text-base md:text-sm text-gray-600 font-normal ml-6">
            เติมเงินเข้ากระเป๋าตัง
          </p>
        </div>
        <div className="flex">
          <p className="xl:text-base md:text-sm text-purple-800 font-bold ">5,000</p>
          <p className="xl:text-base md:text-sm text-gray-600 font-normal ml-6">Token</p>
        </div>
      </div>
      <div className="flex my-2 px-4 py-4 justify-between w-full border-2 border-purple-800 rounded-md">
        <div className="flex">
          <p className="xl:text-base md:text-sm text-gray-600 font-normal">02/09/2563</p>
          <p className="xl:text-base md:text-sm text-gray-600 font-normal ml-6">
            เติมเงินเข้ากระเป๋าตัง
          </p>
        </div>
        <div className="flex">
          <p className="xl:text-base md:text-sm text-purple-800 font-bold ">5,000</p>
          <p className="xl:text-base md:text-sm text-gray-600 font-normal ml-6">Token</p>
        </div>
      </div>
    </div>
  </div>
);

export default {
  CardWallet,
  CardWalletInfo,
  CardHistory,
};
