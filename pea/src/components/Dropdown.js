import React, { useState } from 'react';
import OutsideClickHandler from 'react-outside-click-handler';
import { logout } from '../api/User.script';
import auth from '../utils/auth';

export const DropDownNavAdmin = () => {
  const [hideLogout, setHideLogout] = useState(true);

  const handleLogout = async () => {
    await logout();
    auth.clearAppStorage();
    window.location.href = '/';
  };

  return (
    <>
      <OutsideClickHandler
        onOutsideClick={() => {
          !hideLogout && setHideLogout(true);
        }}
      >
        <li className="mr-6 cursor-pointer">
          <div
            className="flex text-white hover:text-purple-400"
            onClick={() => {
              setHideLogout(!hideLogout);
            }}
            aria-hidden="true"
          >
            <svg
              className="w-6 h-6 mr-2"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
              />
            </svg>
            <div>Admin</div>
          </div>
        </li>
      </OutsideClickHandler>

      <div
        className={`origin-top-right z-50 absolute right-0 mt-8 mr-2 w-48 rounded-sm shadow-lg ${
          hideLogout
            ? ' transform opacity-0 scale-95 -mt-64'
            : 'transition ease-in duration-75 transform opacity-100 scale-100'
        }`}
      >
        <div className="rounded-md bg-white shadow-xs">
          <div
            className="py-1"
            role="menu"
            aria-orientation="vertical"
            aria-labelledby="options-menu"
          >
            <button
              onClick={() => handleLogout()}
              type="button"
              className="block w-full text-left px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
              role="menuitem"
            >
              ออกจากระบบ
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export const DropdownNavUser = () => {
  const [hideLogout, setHideLogout] = useState(true);

  const handleLogout = async () => {
    await logout();
    auth.clearAppStorage();
    window.location.href = '/';
  };

  return (
    <>
      <OutsideClickHandler
        onOutsideClick={() => {
          !hideLogout && setHideLogout(true);
        }}
      >
        <li className="mr-6 py-5">
          <div
            className="flex text-white hover:text-purple-400 cursor-pointer"
            onClick={() => {
              setHideLogout(!hideLogout);
            }}
            aria-hidden="true"
          >
            <svg
              className="w-6 h-6 mr-2"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
              />
            </svg>
            <div>User</div>
          </div>
        </li>
      </OutsideClickHandler>

      <div
        className={`origin-top-right z-50 absolute right-0 mt-16 mr-2 w-48 rounded-sm shadow-lg ${
          hideLogout
            ? ' transform opacity-0 scale-95 -mt-64'
            : 'transition ease-in duration-75 transform opacity-100 scale-100'
        }`}
      >
        <div className="rounded-md bg-white shadow-xs">
          <div
            className="py-1"
            role="menu"
            aria-orientation="vertical"
            aria-labelledby="options-menu"
          >
            <a
              href="/Profile"
              className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
              role="menuitem"
            >
              ข้อมูลผู้ใช้
            </a>
            <button
              onClick={() => handleLogout()}
              type="button"
              className="block w-full text-left px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
              role="menuitem"
            >
              ออกจากระบบ
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default { DropDownNavAdmin, DropdownNavUser };
