import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { AppRoute, PrivateRoute } from './AppRoute';

import BlankLayout from './layout/BlankLayout';
import MainLayout from './layout/MainLayout';
import MainLayoutUser from './layout/MainLayoutUser';
import MainLayoutDashboardAdmin from './layout/MainLayoutDashboardAdmin';
import SignIn from './container/SignInScreen';
import { DashboardAdmin } from './container/DashboardAdmin';
import PEAGRID from './container/PEAGRID';
import { DashboardBuildingAdmin } from './container/DashboardBuildingAdmin';
import { SettingAdmin } from './container/SettingAdmin';
import { DashboardBuildingUser } from './container/DashboardBuildingUser';
import { TradingBuy } from './container/TradingBuy';
import { TradingSell } from './container/TradingSell';
import { Profile } from './container/Profile';
import Pocket from './container/Pocket';
import { SummaryCustomer } from './container/SummaryCustomer';
import { LoadingScreen } from './container/LoadingScreen';

import TradingScreen from './Screens/TradingScreen';

function App() {
  return (
    <Router>
      <Switch>
        <AppRoute path="/" exact component={SignIn} layout={BlankLayout} />
        <PrivateRoute path="/admin" exact component={DashboardAdmin} layout={MainLayout} />
        <PrivateRoute path="/PEAGRID" exact component={PEAGRID} layout={MainLayoutDashboardAdmin} />
        <PrivateRoute
          path="/DashboardBuildingAdmin"
          exact
          component={DashboardBuildingAdmin}
          layout={MainLayoutDashboardAdmin}
        />
        <PrivateRoute
          path="/LoadingScreen"
          exact
          component={LoadingScreen}
          layout={MainLayoutDashboardAdmin}
        />
        <PrivateRoute path="/SettingAdmin" exact component={SettingAdmin} layout={MainLayout} />
        <PrivateRoute
          path="/DashboardBuildingUser"
          exact
          component={DashboardBuildingUser}
          layout={MainLayoutUser}
        />
        <PrivateRoute path="/TradingBuy" exact component={TradingBuy} layout={MainLayoutUser} />
        <PrivateRoute path="/TradingSell" exact component={TradingSell} layout={MainLayoutUser} />
        <PrivateRoute path="/Profile" exact component={Profile} layout={MainLayoutUser} />
        <PrivateRoute path="/Pocket" exact component={Pocket} layout={MainLayoutUser} />
        <PrivateRoute
          path="/TradingScreen"
          exact
          component={TradingScreen}
          layout={MainLayoutUser}
        />
        <PrivateRoute
          path="/SummaryCustomer"
          exact
          component={SummaryCustomer}
          layout={MainLayoutUser}
        />
      </Switch>
    </Router>
  );
}

export default App;
