import React from 'react';
import OrderForm from '../components/OrderForm';

const TradingScreen = () => {
  return (
    <div>
      <OrderForm />
    </div>
  );
};

export default TradingScreen;
