import apiconfig from './config.json';

import auth from '../utils/auth';
import request from '../utils/request';
import responseBuilder from '../utils/return';

import moment from 'moment';
import 'moment/locale/th';

export const getReconcileUser = async () => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const date = moment().format('YYYY-MM-DD');
  const requestURL = `${apiconfig.URL}/dso-reconcile/totalEnergy/${userId}?date=${date}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getReconcileAdmin = async () => {
  const token = auth.getToken();
  const date = moment().format('YYYY-MM-DD');
  const requestURL = `${apiconfig.URL}/admin/dso-reconcile/totalEnergy?date=${date}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};
