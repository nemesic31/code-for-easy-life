import apiconfig from './config.json';
import auth from '../utils/auth';
import request from '../utils/request';
import responseBuilder from '../utils/return';

export const login = async (email, password) => {
  const requestURL = `${apiconfig.URL}/users/login`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'POST',
      body: {
        email,
        password,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const logout = async () => {
  const requestURL = `${apiconfig.URL}/users/logout`;
  let result;
  try {
    result = await request(requestURL, { method: 'POST' });
    return result;
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
};

export const getuser = async (response) => {
  const { token, userId } = response;
  const requestURL = `${apiconfig.URL}/users/${userId}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getUserBuilding = async () => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const requestURL = `${apiconfig.URL}/users/${userId}/profile`;
  let result;
  try {
    result = await request(
      requestURL,
      {
        method: 'GET',
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};
