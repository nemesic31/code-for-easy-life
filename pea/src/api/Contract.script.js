import apiconfig from './config.json';
import auth from '../utils/auth';
import request from '../utils/request';
import responseBuilder from '../utils/return';

import moment from 'moment';
import 'moment/locale/th';

export const getContractUser = async (newDateTime) => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const date = moment().format('YYYY-MM-DD');
  const requestURL = `${apiconfig.URL}/users/${userId}/contracts?startDate=${
    !newDateTime ? date : newDateTime
  }&endDate=${!newDateTime ? date : newDateTime}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getContractAdmin = async ({ newDateTime, userId }) => {
  const token = auth.getToken();
  const date = moment().format('YYYY-MM-DD');
  const requestURL = `${apiconfig.URL}/admin/contracts/user/${userId && userId}?startDate=${
    !newDateTime ? date : newDateTime
  }&endDate=${!newDateTime ? date : newDateTime}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};
