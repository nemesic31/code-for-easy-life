import apiconfig from './config.json';
import auth from '../utils/auth';
import request from '../utils/request';
import responseBuilder from '../utils/return';

export const getAllUsers = async () => {
  const token = auth.getToken();
  const requestURL = `${apiconfig.URL}/admin/users`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getAllInventories = async () => {
  const token = auth.getToken();
  const requestURL = `${apiconfig.URL}/admin/inventories`;
  let result;
  try {
    result = await request(
      requestURL,
      {
        method: 'GET',
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getAllConfigs = async () => {
  const token = auth.getToken();
  const requestURL = `${apiconfig.URL}/admin/configs`;
  let result;
  try {
    result = await request(
      requestURL,
      {
        method: 'GET',
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const putConfigsBuy = async (value) => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const { buy, idBuy } = value;
  const requestURL = `${apiconfig.URL}/admin/config/${idBuy}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'PUT',
      body: {
        configId: `${idBuy}`,
        type: 'ELECTRICITY',
        name: 'DSO_BUY_PRICE',
        displayValue: 'ราคารซื้อไฟฟ้า DSO',
        value: `${buy}`,
        description: 'ราคาซื้อไฟฟ้าของ DSO เมื่อผู้ขาย ขายไฟไม่ได้ หน่วยเป็น token',
        active: true,
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const putConfigsSell = async (value) => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const { sell, idSell } = value;
  const requestURL = `${apiconfig.URL}/admin/config/${idSell}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'PUT',
      body: {
        configId: `${idSell}`,
        type: 'ELECTRICITY',
        name: 'DSO_SELL_PRICE',
        displayValue: 'ราคารขายไฟฟ้า DSO',
        value: `${sell}`,
        description: 'ราคาขายไฟฟ้าของ DSO เมื่อผู้ซื้อ ซื้อไฟไม่ได้ หน่วยเป็น token',
        active: true,
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const putConfigsFee = async (value) => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const { fee, idFee } = value;
  const requestURL = `${apiconfig.URL}/admin/config/${idFee}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'PUT',
      body: {
        configId: `${idFee}`,
        type: 'FEE',
        name: 'TRANSACTION_FEE',
        displayValue: 'ค่าธรรมเนียมการทำธุรกรรม',
        value: `${fee}`,
        description: 'ค่าธรรมเนียมการทำธุรกรรม config เป็นเปอร์เซ็นต่อการทำธุรกรรม 1 ธุรกรรม',
        active: true,
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};
