import apiconfig from './config.json';
import auth from '../utils/auth';
import request from '../utils/request';
import responseBuilder from '../utils/return';

export const getSmartMeterUser = async () => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const requestURL = `${apiconfig.URL}/smart-meter/${userId}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getSmartMeterAdmin = async () => {
  const token = auth.getToken();
  const requestURL = `${apiconfig.URL}/smart-meter`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

