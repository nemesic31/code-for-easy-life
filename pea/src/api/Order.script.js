import apiconfig from './config.json';
import auth from '../utils/auth';
import request from '../utils/request';
import responseBuilder from '../utils/return';
import * as SmartMeter from './SmartMeter.script';
import moment from 'moment';
import 'moment/locale/th';

export const postOrderBuyUser = async (value) => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const responseSmartMeterNo = await SmartMeter.getSmartMeterUser();
  const { smartMeterNo } = responseSmartMeterNo.data[0];
  const { price, startTimes, endTimes, estimated } = value;
  const requestURL = `${apiconfig.URL}/orders`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'POST',
      body: {
        userId: `${userId}`,
        smartMeterNo: `${smartMeterNo}`,
        side: 'buy',
        unitPrice: `${price}`,
        startDate: `${startTimes}`,
        endDate: `${endTimes}`,
        estimatedKwh: `${estimated}`,
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};
export const postOrderSellUser = async (value) => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const responseSmartMeterNo = await SmartMeter.getSmartMeterUser();
  const { smartMeterNo } = responseSmartMeterNo.data[0];
  const { price, startTimes, endTimes, estimated } = value;
  const requestURL = `${apiconfig.URL}/orders`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'POST',
      body: {
        userId: `${userId}`,
        smartMeterNo: `${smartMeterNo}`,
        side: 'sell',
        unitPrice: `${price}`,
        startDate: `${startTimes}`,
        endDate: `${endTimes}`,
        estimatedKwh: `${estimated}`,
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getOrderUser = async (newDateTime) => {
  const userId = auth.getUserInfo();
  const token = auth.getToken();
  const dateTimes = new Date();
  const date = moment().format('YYYY-MM-DD');
  const requestURL = `${apiconfig.URL}/users/${userId}/orders?startDate=${
    !newDateTime ? date : newDateTime
  }&endDate=${!newDateTime ? date : newDateTime}`;
  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};

export const getAllOrderAdmin = async (newDateTime) => {
  const token = auth.getToken();
  const dateTimes = new Date();
  const date = moment().format('YYYY-MM-DD');
  const requestURL = `${apiconfig.URL}/admin/orders?startDate=${
    !newDateTime ? date : newDateTime
  }&endDate=${!newDateTime ? date : newDateTime}`;

  let result;
  try {
    result = await request(requestURL, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    responseBuilder.success();
  } catch (err) {
    if (err.response) {
      return responseBuilder.fail(err.response.payload.messageError.message);
    }
    return responseBuilder.fail(err.message, 500);
  }
  return result;
};
