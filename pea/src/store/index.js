import React, { useState } from 'react';

const SelectDateContext = React.createContext();

export const SelectDateProvider = ({ children }) => {
  const [date, setDate] = useState(new Date());
  const store = {
    date,
    submitDate: (date) => setDate(date),
  };

  return <SelectDateContext.Provider value={store}>{children}</SelectDateContext.Provider>;
};

export const SelectDateConsumer = ({ children }) => (
  <SelectDateContext.Consumer>{(value) => children(value)}</SelectDateContext.Consumer>
);

export default SelectDateContext;
