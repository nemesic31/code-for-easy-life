import 'whatwg-fetch';
import auth from './auth';
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  return response.json ? response.json() : response;
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  return parseJSON(response).then((responseFormatted) => {
    const error = new Error(response.statusText);
    error.response = response;
    error.response.payload = responseFormatted;
    if (/token/.test(responseFormatted.messageError.message)) {
      auth.clearAppStorage();
      window.location.href = '/login';
    }
    throw error;
  });
}

/**
 * Format query params
 *
 * @param params
 * @returns {string}
 */
function formatQueryParams(params) {
  return Object.keys(params)
    .map((k) => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
    .join('&');
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default function request(url, options = {}, stringify = true) {
  const requestOptions = options;
  if (stringify) {
    requestOptions.headers = { 'Content-Type': 'application/json', ...requestOptions.headers };
  }

  const token = auth.getToken();
  if (token) {
    requestOptions.headers = { Authorization: `Bearer ${token}`, ...requestOptions.headers };
  }

  requestOptions.credentials = 'same-origin';

  const requestUrl =
    requestOptions && requestOptions.params
      ? `${url}?${formatQueryParams(requestOptions.params)}`
      : url;

  if (requestOptions && requestOptions.body && stringify) {
    requestOptions.body = JSON.stringify(requestOptions.body);
  }

  return fetch(requestUrl, requestOptions).then(checkStatus).then(parseJSON);
}
