module.exports = {
  paramMissing: {
    code: 404,
    result: 'Failed',
    messageError: {
      message: 'Required parameter missing',
    },
  },
  unknownError: {
    code: 1,
    message: 'Unknown error',
  },
  success: (data) => ({
    code: 200,
    result: 'Success',
    data,
    messageError: {
      message: null,
    },
  }),
  fail: (errorMsg, errorCode = 400) => ({
    code: errorCode,
    result: 'Failed',
    data: null,
    messageError: {
      message: errorMsg,
    },
  }),
  failWithData: (data, errorMsg, errorCode = 400) => ({
    code: errorCode,
    result: 'Failed',
    data,
    messageError: {
      message: errorMsg,
    },
  }),
  failMultiErrors: (data, errors, errorCode = 400) => ({
    code: errorCode,
    result: 'Failed',
    data,
    messageError: errors,
  }),
  customResponse: (data, errorMsg, responseCode = 200, successFlg = true) => {
    let resultMsg;
    if (successFlg) {
      resultMsg = 'Success';
    } else {
      resultMsg = 'Failed';
    }
    return {
      code: responseCode,
      result: resultMsg,
      data,
      messageError: {
        message: errorMsg,
      },
    };
  },
};
