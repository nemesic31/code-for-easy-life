import { v4 } from "uuid"

export const subtaskMock = {
  'todo': {
    id: 'todo',
    title: 'TO DO',
    items: [
      {
        id: v4(),
        name: 'resume',
        time: '09/06/2021 03:42:07 PM',
        assign: {
          id: 4,
          name: 'Wade Cooper',
          avatar:
            'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        },
        description: '**hello**',
        role: { name: 'BD', username: 'Business Developer' },
        estimate: { hour: '2', minute: '' },
        spent: { hour: '', minute: '' },
        pastTask: false,
        comment: [
          { id: v4(), name: 'Tor', description: 'hello2 world!!!', image: 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' },
          { id: v4(), name: 'Beam', description: 'hello3 world!!!', image: 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' }
        ]
      },
      {
        id: v4(),
        name: 'Design System',
        time: '09/06/2021 03:42:07 PM',
        assign: {
          id: 4,
          name: 'Wade Cooper',
          avatar:
            'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        },
        description: '',
        role: { name: 'BD', username: 'Business Developer' },
        estimate: { hour: '2', minute: '' },
        spent: { hour: '', minute: '' },
        pastTask: false,
        comment: [
          { id: v4(), name: 'Wade Cooper', description: 'hello world!!!', image: 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' },
          { id: v4(), name: 'Tor', description: 'hello2 world!!!', image: 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' },
          { id: v4(), name: 'Beam', description: 'hello3 world!!!', image: 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' }
        ]
      }
    ]
  },
  'doing': {
    id: 'doing',
    title: 'DOING',
    items: []
  },
  'done': {
    id: 'done',
    title: 'DONE',
    items: [
      {
        id: v4(),
        name: 'test',
        time: '09/06/2021 03:42:07 PM ',
        assign: {
          id: 4,
          name: 'Wade Cooper',
          avatar:
            'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        },
        description: '**hello**',
        role: { name: 'BD', username: 'Business Developer' },
        estimate: { hour: '2', minute: '' },
        spent: { hour: '3', minute: '' },
        pastTask: true,
        comment: [
          { id: v4(), name: 'Tor', description: 'hello2 world!!!', image: 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' },
          { id: v4(), name: 'Beam', description: 'hello3 world!!!', image: 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' }
        ]
      }
    ]
  },
  'close': {
    id: 'close',
    title: 'CLOSE',
    items: []
  }
}