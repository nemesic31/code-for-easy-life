import { getToken } from "./jwt-util"

const makeRequest = async (method, jwt, url, headers, body) => {
  const fetchHeaders = {
    'Authorization': `Bearer ${jwt}`,
    ...headers
  }
  const fetchOption = {
    method,
    headers: fetchHeaders,
    body
  };
  const result = await fetch(url, fetchOption);
  if (result.status === 200) {
    return Promise.resolve(result)
  } else {
    return Promise.reject(result)
  }
}

export const get = async (request, url, headers) => {
  const jwt = getToken(request)
  return await makeRequest('get', jwt, url, headers, undefined);
}

export const post = async (request, url, headers, body) => {
  const jwt = getToken(request)
  headers = {'Content-Type': 'application/json', ...headers}
  return await makeRequest('post', jwt, url, headers, body);
}