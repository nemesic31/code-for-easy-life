import { getToken } from "../../utilities/jwt-util";

const debug = async (request, response) => {
  const jwt = getToken(request)
  response.status(200).send({
    'cookies' : request.cookies,
    'jwt': jwt
  })
}

export default debug
