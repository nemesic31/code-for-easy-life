import { subtaskMock } from '../../data/subtaskMock.js'

const handler = async (request, response) => {
  if (request.method === 'GET') {
    const { id, idsubtask } = request.query
    const newSubtaskMock = subtaskMock[id].items.find(element => element.id === idsubtask);
    response.status(200).json(newSubtaskMock)
  } else if (request.method === 'POST') {
    const data = request.body
    const newComment = {
      id: data.id,
      name: data.name,
      image: data.image,
      description: data.description
    }
    if (data) {
      const indexSubtask = subtaskMock[data.idCard].items.findIndex(element => element.id === data.idSubtask);
      subtaskMock[data.idCard].items[indexSubtask].comment.push(newComment)
      response.status(201).json(newComment)
    }
  }
}

export default handler