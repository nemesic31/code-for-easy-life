import { subtaskMock } from '../../data/subtaskMock.js'

const handler = async (request, response) => {
  if (request.method === 'GET') {
    response.status(200).json(subtaskMock)
  } else if (request.method === 'POST') {
    const data = request.body
    const newSubtaskMock = {
      id: data.id,
      name: data.subtaskName,
      time: data.date,
      assign: data.assignee,
      description: data.description,
      role: data.role,
      estimate: data.estimate,
      spent: data.spent,
      pastTask: data.pastTask,
      comment: []
    }
    if (data) {
      if (data.pastTask === true) {
        subtaskMock.done.items.push(newSubtaskMock)
        response.status(201).json(newSubtaskMock)

      } else {
        if (data.type === 'todo') {
          subtaskMock.todo.items.push(newSubtaskMock)
          response.status(201).json(newSubtaskMock)
        } else if (data.type === 'doing') {
          subtaskMock.doing.items.push(newSubtaskMock)
          response.status(201).json(newSubtaskMock)
        }
      }
    }
  }
}

export default handler