import { subtaskMock } from '../../data/subtaskMock.js'

const handler = async (request, response) => {
  if (request.method === 'POST') {
    const data = request.body
    if (data) {
      subtaskMock.done.items = data.state.done.items
      subtaskMock.doing.items = data.state.doing.items
      subtaskMock.todo.items = data.state.todo.items
      subtaskMock.close.items = data.state.close.items
      response.status(201).json(data)
    }
  }
}

export default handler