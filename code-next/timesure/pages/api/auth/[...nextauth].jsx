import NextAuth from "next-auth"
import Providers from "next-auth/providers"
import jwt from "jsonwebtoken"
import fs from 'fs'

const options = {
  providers: [
    Providers.Google({
      clientId: '987190608271-ntqb12d8qo47gubbvrske2grjkf3p6dd.apps.googleusercontent.com',
      clientSecret: 'jReQvNelqZLk-5PkZFmjdYDg',
    }),
  ],

  session: {
    jwt: true,
  },

  callbacks: {
    session: async (session, user, sessionToken) => {
      const token = getEncodedJWT(user);
      const userDetail = await getServerSideCredential(user)
      session.user = userDetail;
      return Promise.resolve(session)
    },
    jwt: async (token, user, account, profile, isNewUser) => {
      user && (token.user = user);
      return Promise.resolve(token)
    }
  },

  jwt : {
    encode: async ({ secret, token, maxAge }) => {
      const now = Math.floor(Date.now() / 1000);
      token['iat'] = now;
      token['exp'] = now + maxAge;
      return getEncodedJWT(token);
    },
    decode: async ({ secret, token, maxAge }) => {
      const publicKey = fs.readFileSync('keys/public.pem');
      return jwt.verify(token, publicKey)
    }
  },

  events: {
    signIn: (message) => {
      
    }
  }
}

const getEncodedJWT = (token) => {
  const privateKey = fs.readFileSync('keys/key.pem');
  const encodedToken = jwt.sign(token, privateKey, { algorithm: 'RS256' });
  return encodedToken;
}

const getServerSideCredential = async (user) => {
  const jwt = getEncodedJWT(user);
  const fetchOption = {
    method: 'post',
    headers: {
      'Authorization': `Bearer ${jwt}`,
      'Content-type': 'application/json'
    },
    body: JSON.stringify(user)
  };
  const res = await fetch(`${process.env.BACKEND_URL}/auth/authenticate`, fetchOption);
  const userDetail = await res.json();
  return Promise.resolve(userDetail);
}

export default (req, res) => NextAuth(req, res, options);
