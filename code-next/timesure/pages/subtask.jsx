import MainLayout from "../components/MainLayout"
import { getSession, useSession } from 'next-auth/client'
import dynamic from 'next/dynamic'

const subtask = () => {
  const Kanban = dynamic(import('../components/kanban'))
  const [session, loading] = useSession()

  return (
    <div className="flex flex-col ">
      <div className="bg-white rounded-md flex justify-between py-4 items-center">
        <div className="ml-11 text-3xl font-medium">
          PEA - Inno Platform
        </div>
        <div className="mr-6 font-normal text-base text-ts-gray-900 flex">
          {session &&
            <div className="flex items-center mr-4">
              <div className="">
                <img src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                  className="flex-shrink-0 h-6 w-6 rounded-full" />
              </div>

              <div className="">
                <img src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                  className="flex-shrink-0 h-6 w-6 rounded-full " />
              </div>

              <div className="">
                <img src={session.user.picture} alt={`${session.user.picture}`}
                  className="flex-shrink-0 h-6 w-6 rounded-full " />
              </div>
            </div>
          }
          <p>
            +10 people
          </p>
        </div>
      </div>
      <div className="bg-white rounded-md shadow-md mt-5 py-6 w-full h-full ">
        <Kanban />
      </div>
    </div>
  )
}

export default subtask
subtask.Layout = MainLayout;
subtask.current = 'subtask'
subtask.title = 'Sub Task'
subtask.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}