import { providers, signIn, getSession } from "next-auth/client";
import {
  UserIcon,
  KeyIcon
} from '@heroicons/react/solid'

const login = ({ providers }) => {
  return (
    <div className="min-h-screen bg-gray-50 flex flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md shadow-xl">
        <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          <img
            className="mx-auto w-auto h-44"
            src="logo-verical.png"
          />
          <form className="space-y-3 mt-6" action="#" method="POST">
            <div className="mt-1 relative rounded-lg border border-gray-400 ">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <UserIcon className="h-4 w-4 text-ts-blue-900" aria-hidden="true" />
              </div>
              <input
                type="text"
                name="username"
                className="focus:ring-gray-500 focus:border-gray-500 block w-full pl-10 sm:text-sm border-gray-300 rounded-lg px-3 py-2 bg-inputbox"
                placeholder="Username"
              />
            </div>

            <div className="mt-1 relative rounded-lg border border-gray-400">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <KeyIcon className="h-4 w-4 text-ts-blue-900" aria-hidden="true" />
              </div>
              <input
                type="password"
                name="password"
                className="focus:ring-gray-500 focus:border-gray-500 block w-full pl-10 sm:text-sm border-gray-300 rounded-lg px-3 py-2 bg-inputbox"
                placeholder="Password"
              />
            </div>

            <div className="flex items-center justify-between py-3">
              <div className="flex items-center">
                <input
                  id="remember-me"
                  name="remember-me"
                  type="checkbox"
                  className="h-4 w-4 text-gray-600 focus:ring-gray-500 border-gray-300 rounded"
                />
                <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">
                  Remember me
                </label>
              </div>

              <div className="text-sm">
                <a href="#" className="font-medium text-gray-600 hover:text-gray-500">
                  Forgot password?
                </a>
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-ts-blue-900 hover:bg-ts-blue-800 focus:outline-none transition duration-150 ease-in-ou"
              >
                Login
              </button>
            </div>
            <div>
              <button
                onClick={() => signIn(providers.google.id)}
                type="button"
                className="w-full inline-flex space-x-2 justify-center items-center py-1 px-4 border border-gray-400 rounded-md bg-white hover:bg-gray-200 text-sm leading-5 font-medium text-gray-500 focus:outline-none focus:border-blue-300 transition duration-150 ease-in-ou"
              >
                <img className="h-7 w-7" src="/google_g_icon.png" />
                <div>Login with Google</div>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default login
login.title = 'Login'

login.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (session) {
    res.writeHead(302, {
      Location: '/'
    })
    res.end()
    return
  }
  return {
    session: undefined,
    providers: await providers(context)
  }
}