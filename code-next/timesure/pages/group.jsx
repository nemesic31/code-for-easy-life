import { useState, useContext } from 'react'
import MainLayout from '../components/MainLayout'
import { getSession } from 'next-auth/client'
import HeaderBox from '../components/HeaderBox'
import ButtonSort from '../components/ButtonSort'
import { classNames } from '../utilities/css-util'
import GroupCreateForm from '../components/GroupCreateForm'
import { useEffect } from 'react'

import Notification from '../components/layout/notification/Notification'
import { NotificationContext } from '../components/layout/notification/notificationContext'
import { v4 } from 'uuid'

import {
  DotsVerticalIcon,
  ChevronRightIcon,
  ChevronDownIcon,
  UsersIcon
} from '@heroicons/react/solid'
import {
  FolderAddIcon
} from '@heroicons/react/outline'
import Pagination from '../components/layout/Pagination'


const group = () => {
  const [openGroup, setOpenGroup] = useState({})
  const [showCreateForm, setShowCreateForm] = useState(false)
  const [groups, setGroups] = useState([])
  const [groupLoading, setGroupLoading] = useState(false)
  const [sortBy, setSortBy] = useState('createdDate')
  const [sortDirection, setSortDirection] = useState('desc')
  const [pageNumber, setPageNumber] = useState(1)
  const [pageSize, setPageSize] = useState(25)
  const [totalPage, setTotalPage] = useState(0)
  const { state, dispatch } = useContext(NotificationContext);

  useEffect(() => {
    loadGroups(pageSize, pageNumber, sortBy, sortDirection)
  }, [])

  const onSaveGroup = async (data) => {
    loadGroups(pageSize, pageNumber, sortBy, sortDirection)
    dispatch({
      type: "ADD_NOTIFICATION",
      payload: {
        id: v4(),
        type: 'SUCCESS',
        title: "success",
        message: "Seccessfully saved",
      },
    })
    setShowCreateForm(false)
  }

  const loadGroups = async (_pageSize, _pageNumber, _sortBy, _sortDirection) => {
    setGroupLoading(true)
    const url = `/api/group?page_size=${_pageSize}&page=${_pageNumber}&sort_by=${_sortBy}&sort_direction=${_sortDirection}`
    try {
      const result = await fetch(url, { method: 'get' })
      const data = await result.json()
      setTotalPage(data.totalPages)
      setGroups(data.content)
    } catch (error) {
      console.error(error)
    }
    setGroupLoading(false)
  }

  const toggleOpenGroup = (groupId) => {
    const _openGroup = { ...openGroup }
    if (_openGroup[groupId])
      delete _openGroup[groupId]
    else
      _openGroup[groupId] = true

    setOpenGroup(_openGroup)
  }

  const onSort = (_sortBy, _sortDirection) => {
    setSortBy(_sortBy)
    setSortDirection(_sortDirection)
    loadGroups(pageSize, pageNumber, _sortBy, _sortDirection)
  }

  const onChangePage = (_pageNumber) => {
    setPageNumber(_pageNumber)
    loadGroups(pageSize, _pageNumber, sortBy, sortDirection)
  }

  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">
      <HeaderBox title='Group' content={groups.length + ' group(s)'} />

      <div className='flex justify-end px-5 space-x-2'>
        <button className='button-default' onClick={() => setShowCreateForm(true)}>
          <FolderAddIcon className='w-5 h-5' />
          <p>New group</p>
        </button>
      </div>

      <div className="rounded-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 divide-y divide-divider">

        <div className='grid grid-cols-9 px-8 py-2'>
          <div className='col-span-6 flex items-center font-bold'>Group Name</div>
          <div className='flex items-center font-bold justify-center'>Project</div>
          <div className='flex items-center font-bold justify-center'>Member</div>
          <div className='flex justify-center'><ButtonSort sortBy={sortBy} sortDirection={sortDirection} onSort={onSort} /></div>
        </div>

        <div className='divide-y divide-divider px-8'>
          {groupLoading && (
            <div className='flex justify-center mt-2'>
              <div className='indicator'></div>
            </div>
          )}
          {groups.length === 0 && !groupLoading && (
            <>
              <div className='text-center text-sm italic text-ts-white-400 mt-2'>empty</div>
            </>
          )}
          {groups.map((item) => (
            <div className='my-2' key={item.id}>
              <div
                className={classNames(
                  openGroup[item.id] ? 'bg-ts-aquagreen-100' : '',
                  'grid grid-cols-9 px-4 py-2 rounded-t-lg'
                )}>
                <div className='col-span-6 flex items-center space-x-2'>
                  <div className='w-8 h-8 rounded-lg bg-ts-blue-200 flex items-center justify-center font-bold uppercase'><p>{item.name.substr(0, 1)}</p></div>
                  <p>{item.name}</p>
                </div>
                <div className='flex items-center justify-center'>{item.projects.length} Project(s)</div>
                <div className='flex items-center'></div>
                <div className='flex justify-between items-center'>
                  <div className='hover:bg-gray-300 rounded-lg p-1 cursor-pointer'>
                    <DotsVerticalIcon className='w-6 h-6' />
                  </div>
                  <div onClick={() => toggleOpenGroup(item.id)} className='hover:bg-gray-300 rounded-lg p-1 cursor-pointer'>
                    {!openGroup[item.id] && <ChevronRightIcon className='w-6 h-6' />}
                    {openGroup[item.id] && <ChevronDownIcon className='w-6 h-6' />}
                  </div>
                  <div></div>
                </div>
              </div>
              <div className='bg-inputbox rounded-b-lg'>
                {openGroup[item.id] && item.projects.length === 0 && (
                  <>
                    <div className='text-center text-sm italic text-ts-white-400 py-2'>empty</div>
                  </>
                )}
                {openGroup[item.id] && item.projects.map((project) => (
                  <div className='grid grid-cols-9 px-4 py-2  hover:bg-gray-200' key={project.id}>
                    <div className='col-span-6 flex items-center space-x-2 pl-14'>{project.name}</div>
                    <div className='flex items-center'></div>
                    <div className='flex items-center justify-center space-x-1'><p><UsersIcon className='h-4 w-4' /></p><p>{project.members}</p></div>
                    <div className='flex items-center'></div>
                  </div>
                ))}
              </div>

            </div>
          ))}
        </div>

        <Pagination currentPage={pageNumber} totalPage={totalPage} onChangePage={onChangePage} />
      </div>

      <GroupCreateForm open={showCreateForm} onClose={() => setShowCreateForm(false)} onSave={onSaveGroup} />
      <Notification />
    </div>
  )
}

export default group
group.Layout = MainLayout;
group.current = 'group'
group.title = 'Group'
group.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}