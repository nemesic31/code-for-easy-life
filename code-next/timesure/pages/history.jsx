import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'

const history = () => {
  return (
    <div>
      
    </div>
  )
}

export default history
history.Layout = MainLayout
history.current = 'history'
history.title = 'History'
history.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}