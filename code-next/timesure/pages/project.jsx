import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'

const project = () => {
  return (
    <div>
      
    </div>
  )
}

export default project
project.Layout = MainLayout;
project.current = 'project'
project.title = 'Project'
project.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}