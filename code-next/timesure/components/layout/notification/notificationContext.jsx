import { createContext, useReducer }  from 'react'

export const NotificationContext = createContext()

export const NotificationContextProvider = (props) => {
  const notifications = []

  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'ADD_NOTIFICATION':
        return [...state, action.payload];
      case 'DELETE_NOTIFICATION':
        return state.filter(
          (notification) => notification.id !== action.payload
        )
      default:
        return state
    }
  }, notifications)

  return (
    <NotificationContext.Provider value={{ state, dispatch }}>
      {props.children}
    </NotificationContext.Provider>
  )
}

