import {
  ChevronRightIcon,
  ChevronLeftIcon
} from "@heroicons/react/outline"
import { classNames } from "../../utilities/css-util"

const Pagination = ({ currentPage, totalPage, onChangePage }) => {
  return (
    <div className='flex justify-center items-center space-x-2 py-4 text-pagination-text select-none'>
      <a onClick={() => currentPage <= 1 ?'':onChangePage(currentPage-1)}
        className={classNames(currentPage <= 1 ? 'text-gray-300' : 'cursor-pointer hover:bg-gray-300', 'text-sm rounded-full w-8 h-8 flex justify-center items-center')}
      >
        <ChevronLeftIcon className='w-5 h-5' />
      </a>
      {Array(totalPage).fill(1).map((el, i) =>
        <a className='cursor-pointer select-none' onClick={() => onChangePage(i + 1)}>
          <div
            className={classNames((i + 1) === currentPage ? 'bg-ts-aquagreen-600 text-white' : 'hover:bg-gray-300', 'text-sm rounded-full w-8 h-8 flex justify-center items-center')}
          >{i + 1}</div>
        </a>
      )}
      <a onClick={() => currentPage >= totalPage?'':onChangePage(currentPage + 1)}
        className={classNames(currentPage >= totalPage? 'text-gray-300' : 'cursor-pointer hover:bg-gray-300', 'text-sm rounded-full w-8 h-8 flex justify-center items-center select-none')}
      >
      <ChevronRightIcon className='w-5 h-5' />
      </a>
    </div>
  )
}

export default Pagination
