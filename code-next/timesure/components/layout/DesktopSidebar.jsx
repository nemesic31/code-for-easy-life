import React from 'react'
import { classNames } from '../../utilities/css-util'
import { useSession } from "next-auth/client"

const DesktopSidebar = ({ navigation, adminNavigation }) => {
  const [session, loading] = useSession()

  return (
    <div className="hidden md:flex md:flex-shrink-0">
      <div className="flex flex-col w-64 bg-ts-blue-900">
        {/* Sidebar component, swap this element with another sidebar if you like */}
        <div className="flex-1 flex flex-col min-h-0 space-y-2 mt-4">
          <a href='/' className="flex items-center h-16 flex-shrink-0">
            <img
              className="w-auto"
              src="logo-white.png"
            />
          </a>
          <div className="flex-1 flex flex-col overflow-y-auto">
            <nav className="flex-1 px-2 py-4 space-y-1">
              {navigation.map((item) => (
                <a
                  key={item.name}
                  href={item.href}
                  className={classNames(
                    item.current ? 'bg-ts-blue-800 text-white' : 'text-gray-300 hover:bg-ts-blue-700 hover:text-white',
                    'group flex items-center px-2 py-2 text-sm font-medium rounded-md'
                  )}
                >
                  <item.icon
                    className={classNames(
                      item.current ? 'text-gray-300' : 'text-gray-400 group-hover:text-gray-300',
                      'mr-3 flex-shrink-0 h-6 w-6'
                    )}
                    aria-hidden="true"
                  />
                  {item.name}
                </a>
              ))}

              {session?.user?.admin && <>
                <div className='border-t border-gray-600' />
                {adminNavigation.map((item) => (
                  <a
                    key={item.name}
                    href={item.href}
                    className={classNames(
                      item.current ? 'bg-ts-blue-800 text-white' : 'text-gray-300 hover:bg-ts-blue-700 hover:text-white',
                      'group flex items-center px-2 py-2 text-sm font-medium rounded-md'
                    )}
                  >
                    <item.icon
                      className={classNames(
                        item.current ? 'text-gray-300' : 'text-gray-400 group-hover:text-gray-300',
                        'mr-3 flex-shrink-0 h-6 w-6'
                      )}
                      aria-hidden="true"
                    />
                    {item.name}
                  </a>
                ))}
              </>}
            </nav>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DesktopSidebar
