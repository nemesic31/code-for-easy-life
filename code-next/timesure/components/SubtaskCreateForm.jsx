import { useState, Fragment, useEffect } from 'react'
import { Dialog, Transition, Listbox, Switch } from '@headlessui/react'
import { useForm } from "react-hook-form";
import { getSession, useSession } from 'next-auth/client'
import ReactMde, { SaveImageHandler } from "react-mde";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";

import "@uiw/react-md-editor/markdown-editor.css";
import "@uiw/react-markdown-preview/markdown.css";
import { v4 } from 'uuid';

function loadSuggestions(text) {
  return new Promise((accept, reject) => {
    setTimeout(() => {
      const suggestions = [
        {
          preview: "Andre",
          value: "@andre"
        },
        {
          preview: "Angela",
          value: "@angela"
        },
        {
          preview: "David",
          value: "@david"
        },
        {
          preview: "Louise",
          value: "@louise"
        }
      ].filter(i => i.preview.toLowerCase().includes(text.toLowerCase()));
      accept(suggestions);
    }, 250);
  });
}

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});



const role = [
  { name: 'BD', username: 'Business Developer' },
  { name: 'PM', username: 'Project Manager' },
  { name: 'DEV', username: 'Developer' }
]

const people = [
  {
    id: 1,
    name: 'Assign yourself',
    avatar: ''
  },
  {
    id: 2,
    name: 'Arlene Mccoy',
    avatar:
      'https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  },
  {
    id: 3,
    name: 'Devon Webb',
    avatar:
      'https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80',
  },
  {
    id: 4,
    name: 'Wade Cooper',
    avatar:
      'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  },
]

function classNamesAssignee(...classes) {
  return classes.filter(Boolean).join(' ')
}

function classNamesRole(...classes) {
  return classes.filter(Boolean).join(' ')
}
function classNamesToggle(...classes) {
  return classes.filter(Boolean).join(' ')
}

const SubtaskCreateForm = ({ dataCard, open, onClose, onSave }) => {
  const [session, loading] = useSession()
  const [description, setDescription] = useState("");
  const [selectedTab, setSelectedTab] = useState("write");
  const date = new Date();
  const [dateSubtask, setDateSubtask] = useState()
  const [selectedRole, setSelectedRole] = useState()
  const [selectedPerson, setSelectedPerson] = useState()
  const [enabledToggle, setEnabledToggle] = useState(false)


  const {
    register,
    handleSubmit,
    watch,
    reset,
  } = useForm();

  const watchAllFields = watch();


  const handleSubmitSubtask = (value) => {
    const { subtaskName, hourEstimated, minuteEstimated, hourSpent, minuteSpent } = value
    const dataPack = {
      date: dateSubtask,
      subtaskName: subtaskName,
      description: description,
      role: selectedRole,
      assignee: selectedPerson,
      estimate: {
        hour: hourEstimated,
        minute: minuteEstimated
      },
      pastTask: enabledToggle,
      spent: {
        hour: hourSpent,
        minute: minuteSpent
      }
    }
    onSave(dataPack)
    reset()
    setSelectedPerson()
    setSelectedRole()
    setEnabledToggle(false)
    setDescription('')
  }

  useEffect(() => {
    setDateSubtask(date.toLocaleString())
  }, [date])


  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as='div' className='fixed inset-0 overflow-hidden z-10' onClose={onClose}>
        <div className='absolute inset-0 overflow-hidden'>
          <Dialog.Overlay className='absolute inset-0' />

          <div className='fixed inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16'>
            <Transition.Child
              as={Fragment}
              enter='transform transition ease-in-out duration-500 sm:duration-700'
              enterFrom='translate-x-full'
              enterTo='translate-x-0'
              leave='transform transition ease-in-out duration-500 sm:duration-700'
              leaveFrom='translate-x-0'
              leaveTo='translate-x-full'
            >
              <div className='w-screen max-w-2xl'>
                <form className='h-full flex flex-col bg-white shadow-xl overflow-y-scroll' onSubmit={handleSubmit(handleSubmitSubtask)}>
                  <div className='flex-1'>
                    {/* Header */}
                    <div className='px-4 py-6 sm:px-6 bg-ts-aquagreen-200'>
                      <div className='flex items-start justify-between space-x-3'>
                        <div className='space-y-1'>
                          <Dialog.Title className='text-lg font-medium text-gray-900 flex space-x-2 items-center'>
                            <img src="addSubtask-icon.svg" alt="addSubtask-icon" />
                            <p className="ml-6 text-lg font-normal">Create  Subtask</p>
                          </Dialog.Title>
                        </div>
                        <div className='h-7 flex items-center'>
                          <button
                            type='button'
                            onClick={onClose}
                            className="ring-0 border-transparent"
                          >
                            <img src="close-icon.svg" alt="close-icon" />
                          </button>
                        </div>
                      </div>
                    </div>

                    {/* Divider container */}
                    <div
                      className={`${enabledToggle ? 'bg-ts-blue-500' :
                        (dataCard && dataCard.title === 'DOING' ? 'bg-ts-green-400'
                          : dataCard && dataCard.title === 'DONE' ? 'bg-ts-blue-500'
                            : dataCard && dataCard.title === 'CLOSE' ? 'bg-ts-red-600'
                              : 'bg-ts-yellow-500')} text-white w-max px-4 py-1 mt-6 mx-6 rounded-md`}
                    >
                      {enabledToggle ? 'DONE' : dataCard && dataCard.title}
                    </div>
                    <div className='py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200'>
                      {/* body */}
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 '
                          >
                            Create at
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center '>
                          <p>
                            {dateSubtask}
                          </p>
                        </div>
                      </div>
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Project
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>
                            PEA - Inno Platform
                          </p>
                        </div>
                      </div>
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Task
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>
                            BD OP-41 กระบวนรวมเอกสารส่งมอบ
                          </p>
                        </div>
                      </div>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Subtask name
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <input
                            type='text' className='inputbox w-full'
                            name="subtaskName"
                            id="subtaskName"
                            {...register("subtaskName", { required: true })}
                          />
                        </div>
                      </div>



                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-description'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Description
                          </label>
                        </div>
                        <div className='sm:col-span-2'>
                          <ReactMde
                            value={description}
                            onChange={setDescription}
                            selectedTab={selectedTab}
                            onTabChange={setSelectedTab}
                            generateMarkdownPreview={markdown =>
                              Promise.resolve(converter.makeHtml(markdown))
                            }
                            loadSuggestions={loadSuggestions}
                            childProps={{
                              writeButton: {
                                tabIndex: -1
                              }
                            }}
                          />
                        </div>
                      </div>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Role
                          </label>
                        </div>
                        <div className='sm:col-span-2'>
                          <Listbox value={selectedRole} onChange={setSelectedRole}>
                            <div className="mt-1 relative">
                              <Listbox.Button className="relative w-full bg-white border border-ts-gray-600 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-0 focus:border-ts-gray-600 sm:text-sm">
                                <span className="w-full inline-flex truncate">
                                  <span className="truncate">{selectedRole ? selectedRole.name : 'Your role in this project'}</span>
                                </span>
                                <span className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                                  <img src="chevron-down.svg" alt="chevron-down" />
                                </span>
                              </Listbox.Button>

                              <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
                                <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                                  {role.map((el) => (
                                    <Listbox.Option
                                      key={el.username}
                                      className={({ active }) =>
                                        classNamesRole(
                                          active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                                          'cursor-default select-none relative py-2 pl-3 pr-9'
                                        )
                                      }
                                      value={el}
                                    >
                                      {({ selectedRole, active }) => (
                                        <>
                                          <div className="flex justify-between">
                                            <span className={classNamesRole(selectedRole ? 'font-semibold' : 'font-normal', 'truncate')}>
                                              {el.name}
                                            </span>
                                            <span className={classNamesRole(active ? 'text-ts-gray-700' : 'text-ts-gray-500', 'ml-2 truncate')}>
                                              {el.username}
                                            </span>
                                          </div>
                                        </>
                                      )}
                                    </Listbox.Option>
                                  ))}
                                </Listbox.Options>
                              </Transition>
                            </div>
                          </Listbox>
                        </div>
                      </div>



                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Assignee
                          </label>
                        </div>
                        <div className='sm:col-span-2'>
                          <Listbox value={selectedPerson} onChange={setSelectedPerson}>
                            <div className="mt-1 relative">
                              <Listbox.Button className="relative w-full bg-white border border-ts-gray-600 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-o  focus:border-ts-gray-600 sm:text-sm">
                                <span className="flex items-center">
                                  {selectedPerson && selectedPerson.avatar && <img src={selectedPerson.avatar} alt="" className="flex-shrink-0 h-6 w-6 rounded-full" />}
                                  <span className="ml-3 block truncate">{selectedPerson ? selectedPerson.name : 'Assign task to..'}</span>
                                </span>
                                <span className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                  <img src="chevron-down.svg" alt="chevron-down" />
                                </span>
                              </Listbox.Button>

                              <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
                                <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-56 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                                  {people.map((person) => (
                                    <Listbox.Option
                                      key={person.id}
                                      className={({ active }) =>
                                        classNamesAssignee(
                                          active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                                          'cursor-default select-none relative py-2 pl-3 pr-9'
                                        )
                                      }
                                      value={person}
                                    >
                                      {({ selectedPerson, active }) => (
                                        <>
                                          <div className="flex items-center">
                                            {person.avatar && <img src={person.avatar} alt={`${person.avatar}`} className="flex-shrink-0 h-6 w-6 rounded-full" />}
                                            <span className={classNamesAssignee(selectedPerson ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}>
                                              <p className={`${!person.avatar && 'underline'}`}>
                                                {person.name}
                                              </p>
                                            </span>
                                          </div>
                                        </>
                                      )}
                                    </Listbox.Option>
                                  ))}
                                </Listbox.Options>
                              </Transition>
                            </div>
                          </Listbox>
                        </div>
                      </div>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Estimated time
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <input
                            type="number"
                            name="hourEstimated"
                            id="hourEstimated"
                            className="shadow-sm text-base text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                            {...register("hourEstimated")}
                          />

                          <p className="mx-2">
                            hour(s)
                          </p>
                          <input
                            type="number"
                            name="minuteEstimated"
                            id="minuteEstimated"
                            className="shadow-sm text-base ml-4 text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                            {...register("minuteEstimated")}
                          />
                          <p className=" mx-2">
                            minute(s)
                          </p>
                        </div>
                      </div>



                      <div>
                        <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-name'
                              className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                            >
                              Past task
                            </label>
                            <p className="text-ts-gray-600 text-xs">To specify the working hours backwards.
                              This task will be recorded as done.</p>
                          </div>
                          <div className='flex sm:col-span-2 items-center'>
                            <Switch
                              checked={enabledToggle}
                              onChange={setEnabledToggle}
                              className={classNamesToggle(
                                enabledToggle ? 'bg-ts-green-400' : 'bg-gray-200',
                                'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                              )}
                            >
                              <span className="sr-only">Use setting</span>
                              <span
                                aria-hidden="true"
                                className={classNamesToggle(
                                  enabledToggle ? 'translate-x-5' : 'translate-x-0',
                                  'pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                )}
                              />
                            </Switch>
                          </div>
                        </div>

                        {enabledToggle && <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-name'
                              className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                            >
                              Time spent
                            </label>

                          </div>
                          <div className='sm:col-span-2 flex items-center'>
                            <input
                              type="number"
                              name="hourSpent"
                              id="hourSpent"
                              className="shadow-sm text-base text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                              {...register("hourSpent")}
                            />

                            <p className="mx-2">
                              hour(s)
                            </p>
                            <input
                              type="number"
                              name="minuteSpent"
                              id="minuteSpent"
                              className="shadow-sm text-base ml-4 text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                              {...register("minuteSpent")}
                            />
                            <p className=" mx-2">
                              minute(s)
                            </p>
                          </div>
                        </div>}
                      </div>


                    </div>
                  </div>
                  {/* Action buttons */}
                  <div className='flex-shrink-0 px-4 border-t border-gray-200 py-5 sm:px-6'>
                    <div className='space-x-3 flex justify-end'>
                      <button
                        type='button'
                        className='bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                        onClick={onClose}
                      >
                        Cancel
                      </button>
                      <button
                        type='submit'
                        disabled={enabledToggle ?
                          (watchAllFields.hourSpent || watchAllFields.minuteSpent &&
                            (watchAllFields.hourEstimated || watchAllFields.minuteEstimated) && selectedPerson && selectedRole && watchAllFields.subtaskName ? false : true)
                          : (watchAllFields.hourEstimated || watchAllFields.minuteEstimated) && selectedPerson && selectedRole && watchAllFields.subtaskName ? false : true}
                        className={`ml-4 text-white text-base ${enabledToggle ?
                          (watchAllFields.hourSpent || watchAllFields.minuteSpent && (watchAllFields.hourEstimated || watchAllFields.minuteEstimated) && selectedPerson && selectedRole && watchAllFields.subtaskName ? 'bg-ts-aquagreen-600' : 'bg-ts-gray-600')
                          : (watchAllFields.hourEstimated || watchAllFields.minuteEstimated) && selectedPerson && selectedRole && watchAllFields.subtaskName ? 'bg-ts-aquagreen-600' : 'bg-ts-gray-600'} 
                        px-8 py-1 rounded-md`}
                      >
                        Create
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  )
}

export default SubtaskCreateForm
