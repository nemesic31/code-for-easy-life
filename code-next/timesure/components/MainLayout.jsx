import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import {
  CalendarIcon,
  ClipboardListIcon,
  CollectionIcon,
  FolderIcon,
  ClockIcon,
  MenuAlt2Icon,
  XIcon,
  ChartSquareBarIcon,
  UserGroupIcon
} from '@heroicons/react/outline'
import DesktopSidebar from "./layout/DesktopSidebar"
import MobileSidebar from "./layout/MobileSidebar"
import UserMenu from "./layout/UserMenu"


const MainLayout = ({ children, current }) => {
  const navigation = [
    { name: 'Timesheet', href: '/', icon: CalendarIcon, current: current === 'timesheet' },
    { name: 'My Task', href: '/mytask', icon: ClipboardListIcon, current: current === 'mytask' },
    { name: 'Group', href: '/group', icon: CollectionIcon, current: current === 'group' },
    { name: 'Project', href: '/project', icon: FolderIcon, current: current === 'project' },
    { name: 'History', href: '/history', icon: ClockIcon, current: current === 'history' },
  ]
  const userNavigation = [
    { name: 'Your Profile', href: '#' },
    { name: 'Settings', href: '#' },
  ]
  const adminNavigation = [
    { name: 'Report', href: '/report', icon: ChartSquareBarIcon, current: current === 'report' },
    { name: 'Manage User', href: '/manage_user', icon: UserGroupIcon, current: current === 'manage_user' },
  ]
  const [sidebarOpen, setSidebarOpen] = useState(false)

  return (
    <div className="h-screen flex overflow-hidden bg-gray-100">
      <Transition.Root show={sidebarOpen} as={Fragment}>
        <Dialog as="div" className="fixed inset-0 flex z-40 md:hidden" onClose={setSidebarOpen}>
          <Transition.Child
            as={Fragment}
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-600 bg-opacity-75" />
          </Transition.Child>
          <Transition.Child
            as={Fragment}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
          >
            <div className="relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-ts-blue-900">
              <Transition.Child
                as={Fragment}
                enter="ease-in-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in-out duration-300"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <div className="absolute top-0 right-0 -mr-12 pt-2">
                  <button
                    type="button"
                    className="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                    onClick={() => setSidebarOpen(false)}
                  >
                    <span className="sr-only">Close sidebar</span>
                    <XIcon className="h-6 w-6 text-white" aria-hidden="true" />
                  </button>
                </div>
              </Transition.Child>
              <MobileSidebar navigation={navigation} adminNavigation={adminNavigation} />
            </div>
          </Transition.Child>
          <div className="flex-shrink-0 w-14" aria-hidden="true">
            {/* Dummy element to force sidebar to shrink to fit close icon */}
          </div>
        </Dialog>
      </Transition.Root>

      <DesktopSidebar navigation={navigation} adminNavigation={adminNavigation} />
      <div className="flex flex-col w-0 flex-1 overflow-hidden">
        <div className="relative z-10 flex-shrink-0 flex h-16 bg-white shadow">
          <button
            type="button"
            className="px-4 border-r border-gray-200 text-gray-500 md:hidden"
            onClick={() => setSidebarOpen(true)}
          >
            <span className="sr-only">Open sidebar</span>
            <MenuAlt2Icon className="h-6 w-6" aria-hidden="true" />
          </button>
          <div className="flex-1 px-4 flex justify-between">
            <div className="flex-1 flex">
              {/* Space for searchbox*/}
            </div>
            <div className="ml-4 flex items-center md:ml-6">
              <UserMenu userNavigation={userNavigation} />
            </div>
          </div>
        </div>

        <main className="flex-1 relative overflow-y-auto focus:outline-none">
          <div className="py-6">
            <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8">
              {children}
            </div>
          </div>
        </main>
      </div>

    </div>
  )
}

export default MainLayout