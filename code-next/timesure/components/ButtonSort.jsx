import { Menu } from '@headlessui/react'
import { Fragment } from 'react'
import { Transition } from '@headlessui/react'
import { classNames } from '../utilities/css-util'
import {
  SwitchVerticalIcon,
} from '@heroicons/react/solid'

const ButtonSort = ({sortBy, sortDirection, onSort}) => {
  return (
    <Menu as="div" className="ml-3 relative">
      <div>
        <Menu.Button className="order-1 inline-flex items-center p-2 border border-gray-300 shadow-sm rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none sm:order-0 sm:ml-0">
          <SwitchVerticalIcon
            className="flex-shrink-0 h-4 text-gray-400"
            aria-hidden="true"
          />
          <span className="ml-2 text-gray-700 text-sm font-medium">
            Sort by
          </span>
        </Menu.Button>
      </div>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="text-left origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
          <div className='text-xs bg-inputbox px-1 text-gray-400'>
            Project Name
          </div>
          <Menu.Item>
            {({ active }) => (
              <a
                className={classNames(active || (sortBy==='name' && sortDirection==='asc') ? 'bg-gray-200' : '', 'block px-3 text-sm text-gray-600 mx-1 mt-1 rounded-lg cursor-pointer')}
                onClick={() => onSort('name', 'asc')}
              >
                A-Z
              </a>
            )}
          </Menu.Item>
          <Menu.Item>
            {({ active }) => (
              <a
                className={classNames(active || (sortBy==='name' && sortDirection==='desc') ? 'bg-gray-200' : '', 'block px-3 text-sm text-gray-600 mx-1 mt-1 rounded-lg cursor-pointer')}
                onClick={() => onSort('name', 'desc')}
              >
                Z-A
              </a>
            )}
          </Menu.Item>
          <div className='text-xs bg-inputbox px-1 text-gray-400'>
            Date
          </div>
          <Menu.Item>
            {({ active }) => (
              <a

                className={classNames(active || (sortBy==='createdDate' && sortDirection==='desc') ? 'bg-gray-200' : '', 'block px-3 text-sm text-gray-600 mx-1 mt-1 rounded-lg cursor-pointer')}
                onClick={() => onSort('createdDate', 'desc')}
              >
                Assign Date
              </a>
            )}
          </Menu.Item>
          <Menu.Item>
            {({ active }) => (
              <a
                className={classNames(active || (sortBy==='updatedDate' && sortDirection==='desc') ? 'bg-gray-200' : '', 'block px-3 text-sm text-gray-600 mx-1 mt-1 rounded-lg cursor-pointer')}
                onClick={() => onSort('updatedDate', 'desc')}
              >
                Last Update
              </a>
            )}
          </Menu.Item>
        </Menu.Items>
      </Transition>
    </Menu>
  )
}

export default ButtonSort
