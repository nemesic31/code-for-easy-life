import React, { Fragment, useState, useEffect } from "react"
import { useForm } from "react-hook-form";
import { Listbox, Transition, Switch } from '@headlessui/react'
import { useSession } from "next-auth/client"
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd"
import SubtaskCreateForm from './SubtaskCreateForm'
import SubtaskViewForm from './SubtaskViewForm'
import _, { map } from 'lodash'
import { v4 } from "uuid"

const kanban = () => {
  const [session, loading] = useSession()
  const [showViewForm, setShowViewForm] = useState(false)
  const [showCreateForm, setShowCreateForm] = useState(false)
  const [showSubtask, setShowSubtask] = useState({ title: '', id: '' })
  const [dataSubtask, setDataSubtask] = useState()
  const [state, setState] = useState()

  const getSubtask = async () => {
    try {
      const response = await fetch('api/subtask')
      const data = await response.json()
      if (data) {
        setState(data)
      }
    } catch (error) {
      console.log(error);
    }
  }

  const handleDragEnd = async ({ destination, source }) => {
    if (!destination) {
      console.log('not droped in dropable');
      return
    }
    if (destination.index === source.index && destination.droppableId === source.droppableId) {
      console.log('droped in sameplace ');
      return
    }
    //create copyitem before removing it form state
    const itemCopy = { ...state[source.droppableId].items[source.index] }
    setState(prev => {
      prev = { ...prev }
      //remove item form array
      prev[source.droppableId].items.splice(source.index, 1)

      //adding item to new array
      prev[destination.droppableId].items.splice(destination.index, 0, itemCopy)

      return prev
    })

    const response = await fetch('/api/dndcard', {
      method: 'POST',
      body: JSON.stringify({
        state
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    const data = await response.json()
    if (data) {
      getSubtask()
    }
  }

  const addItem = async (value) => {
    try {
      const { date, subtaskName, description, role, assignee, estimate, pastTask, spent } = value
      const id = v4()
      const response = await fetch('/api/subtask', {
        method: 'POST',
        body: JSON.stringify({
          type: showSubtask.id, id, date, subtaskName, description, role, assignee, estimate, pastTask, spent
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })

      const data = await response.json()
      if (data) {
        getSubtask()
        setShowSubtask({ title: '', id: '' })
        setShowCreateForm(false)
      }

    } catch (error) {
      console.log(error);
    }
  }

  const handleCreateSubtask = (data) => {
    if (showCreateForm === false) {
      setShowCreateForm(true)
      setShowSubtask({ title: data.title, id: data.id })
    } else {
      setShowSubtask({ title: '', id: '' })
    }
  }

  const handleViewSubtaskForm = (id, index) => {
    const result = state[id].items[index]
    const idSubtask = state[id]
    const dataPack = {
      showViewForm: true,
      id: idSubtask.id,
      title: idSubtask.title,
      items:
      {
        id: result.id,
        name: result.name,
        time: result.time,
        assign: result.assign,
        description: result.description,
        role: result.role,
        estimate: result.estimate,
        spent: result.spent,
        pastTask: result.pastTask,
        comment: result.comment
      }
    }
    setDataSubtask(dataPack)
    if (showViewForm === false) {
      if (dataSubtask) {
        setShowViewForm(true)
      }
    } else {
      setDataSubtask()
      setShowViewForm(false)
    }

  }

  const addComment = async (value, comments) => {
    try {
      const response = await fetch('/api/viewsubtaskbyid', {
        method: 'POST',
        body: JSON.stringify({
          idCard: value.id,
          idSubtask: value.items.id,
          id: v4(),
          name: session.user.name,
          description: comments,
          image: session.user.picture
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
      const indexSubtask = state[value.id].items.findIndex(element => element.id === value.items.id);
      const result = state[value.id].items[indexSubtask]
      const idSubtask = state[value.id]
      const dataPack = {
        showViewForm: true,
        id: idSubtask.id,
        title: idSubtask.title,
        items:
        {
          id: result.id,
          name: result.name,
          time: result.time,
          assign: result.assign,
          description: result.description,
          role: result.role,
          estimate: result.estimate,
          spent: result.spent,
          pastTask: result.pastTask,
          comment: result.comment
        }
      }
      const data = await response.json()
      if (data) {
        getSubtask()
        setDataSubtask(dataPack)
      }

    } catch (error) {
      console.log(error);
    }
  }

  const submitEdit = async (value) => {
    try {
      const response = await fetch('/api/editsubtask', {
        method: 'POST',
        body: JSON.stringify(value),
        headers: {
          'Content-Type': 'application/json',
        },
      })
      getSubtask()

    } catch (error) {
      console.log(error);
    }
  }


  useEffect(() => {
    getSubtask()
  }, [])

  return (
    <div className="flex flex-col w-full">
      {showCreateForm && <div onClick={() => setShowCreateForm(false)} className="w-full h-screen bg-ts-blue-900 absolute -top-0 -left-0 opacity-20" />}
      <SubtaskCreateForm dataCard={showSubtask} open={showCreateForm} onClose={() => setShowCreateForm(false)} onSave={addItem} />
      {showViewForm && <div onClick={() => setShowViewForm(false)} className="w-full h-screen bg-ts-blue-900 absolute -top-0 -left-0 opacity-20" />}
      <SubtaskViewForm dataCard={dataSubtask} open={showViewForm} onClose={() => setShowViewForm(false)} onSaveComment={addComment} editData={submitEdit} />
      <div className="px-10 font-normal text-lg text-ts-gray-800">
        BD OP-41  กระบวนการรวมเอกสารส่งมอบ
      </div>
      <div className="flex w-full h-full mt-7 px-3 overflow-x-scroll">

        <DragDropContext onDragEnd={handleDragEnd} >
          {_.map(state, (data, key) => {
            return (
              <div key={key} className="w-full h-full mx-3 px-2 py-5 bg-ts-gray-100 shadow-inner border border-ts-gray-300 rounded-md">
                <div className="flex justify-between">
                  <div className="flex items-center">
                    <div className={`mx-2 px-4 py-1 rounded-md text-white flex w-max ${data.id === 'todo' ? 'bg-ts-yellow-500' : data.id === 'doing' ? 'bg-ts-green-400' : data.id === 'done' ? 'bg-ts-blue-500' : 'bg-ts-red-600'}`}>
                      {data.title}
                    </div>
                    <div className="ml-2 font-normal text-lg text-ts-white-400">
                      ({data.items.length})
                    </div>
                  </div>
                  {data.id === 'doing' || data.id === 'todo' ?
                    <button onClick={() => handleCreateSubtask(data)} className="w-7 h-7 border border-ts-gray-500 rounded-md p-2 hover:bg-gray-500">
                      <img className="w-full h-full" alt="add-icon-black" src="add-icon-black.svg" />
                    </button> : null}
                </div>
                <div className="h-height-card overflow-y-scroll">
                  <Droppable droppableId={key} >
                    {(provided) => {
                      return (
                        <div
                          ref={provided.innerRef}
                          {...provided.droppableProps}
                          className="w-96 h-height-card mt-6"
                        >
                          {data.items.map((el, index) => {
                            return (
                              <Draggable key={el.id} index={index} draggableId={el.id}>
                                {(provided, snapshot) => {
                                  return (
                                    <div
                                      className={`w-full flex flex-col justify-between h-32 rounded-lg ${snapshot.isDragging ? 'bg-ts-gray-200' : 'bg-white'} border border-ts-gray-300 my-2`}
                                      ref={provided.innerRef}
                                      {...provided.draggableProps}
                                      {...provided.dragHandleProps}
                                    >
                                      <button onClick={() => handleViewSubtaskForm(data.id, index)} className="font-normal text-left w-max underline text-base mx-2 my-2">
                                        {el.name}
                                      </button>
                                      <div className="font-normal text-xs px-3 py-3 border-t border-ts-gray-300 text-ts-gray-500 flex justify-between items-center">
                                        <p>
                                          Create at : {el.time}
                                        </p>
                                        {el.assign.avatar ? <img src={el.assign.avatar} alt={`${el.assign.avatar}`} className="flex-shrink-0 h-6 w-6 rounded-full" /> : <img src={session && session.user.picture} alt={`${session && session.user.picture}`} className="flex-shrink-0 h-6 w-6 rounded-full" />}
                                      </div>
                                    </div>
                                  )
                                }}
                              </Draggable>
                            )
                          })}
                          {provided.placeholder}
                        </div>
                      )
                    }}
                  </Droppable>
                </div>
              </div>
            )
          })}
        </DragDropContext>
      </div>
    </div>
  )
}

export default kanban