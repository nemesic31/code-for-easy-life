const HeaderBox = ({title, content}) => {
  return (
    <div className="rounded-lg bg-white py-3 px-10 shadow sm:rounded-lg border-b-4 border-ts-blue-900">
      <div className='flex justify-between items-center'>
        <div>
          <h1 className="ml-3 text-2xl font-bold leading-7 text-gray-900 sm:leading-9 sm:truncate">
          {title}
          </h1>
        </div>
        <div>
          <h1 className="ml-3 text-xl font-bold leading-7 text-gray-900 sm:leading-9 sm:truncate">
          {content}
          </h1>
        </div>
      </div>
    </div>
  )
}

export default HeaderBox
