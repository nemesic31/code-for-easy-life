import { useState, useEffect } from 'react'
import { signOut, useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import Link from 'next/link'




export default function Layout({ children }) {
  const router = useRouter()
  const newPath = router.query.name
  const { pathname } = router
  const [path, setPath] = useState()
  const [projectNavigation, setProjectNavigation] = useState()
  const [session, loading] = useSession()

  const handleClick = () => {
    router.push('/createProject')
  }

  const handleSendData = (name, id) => {
    router.push({
      pathname: `/`,
      query: {
        name: name,
        id: id
      }
    })
  }

  const getProject = async () => {
    try {
      const res = await fetch('/api/projects');
      const response = await res.json();
      if (response.projects.length > 0) {
        const { projects } = response
        
        setPath(projects[0].name)
        const getAllProjectValue = projects.map((item) => (
          {
            id: item.id,
            name: item.name,
            href: item.name,
            icon: '',
          }
        ))
        setProjectNavigation(getAllProjectValue)
      } else {
        handleClick()
        console.log('project null');
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getProject()
  }, [])

  return (
    <div className="h-screen flex overflow-hidden bg-white">
      <div className="flex flex-shrink-0">
        <div className="flex flex-col w-64">
          <div className="flex flex-col h-0 flex-1 border-r border-gray-200 bg-gray-100">
            <div className="flex-1 flex flex-col pt-5 pb-4 overflow-y-auto">
              <div className="flex justify-center">
                <button>
                  <Link href="/" className="flex items-center flex-shrink-0 px-4">
                    <img
                      className="h-8 w-auto"
                      src="https://tailwindui.com/img/logos/workflow-logo-pink-500-mark-gray-900-text.svg"
                      alt="Workflow"
                    />
                  </Link>
                </button>
              </div>
              <nav className="mt-8 flex-1" aria-label="Sidebar">
                <div className="px-2 space-y-1 w-full">
                  <div className="flex justify-center mb-4">
                    <button onClick={handleClick} className="flex items-center px-4 py-2 bg-gray-700 rounded-lg text-white">
                      <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                      </svg>
                      <p className=" ml-2">
                        Create Project
                      </p>
                    </button>
                  </div>
                  {projectNavigation && projectNavigation.map((item) => (
                    <button
                      key={item.name}
                      onClick={() => handleSendData(item.href, item.id)}
                      className={
                        `group flex items-center px-2 py-2 text-sm font-medium rounded-md w-full
                        ${!newPath && pathname === '/' ? (path === item.name ? 'bg-gray-300 text-gray-900'
                          : 'text-gray-600 hover:bg-gray-50 hover:text-gray-900')
                          : (newPath === item.name ? 'bg-gray-300 text-gray-900'
                            : 'text-gray-600 hover:bg-gray-50 hover:text-gray-900')}`
                      }
                    >
                      {item.icon ?
                        <img className="h-5 w-5 mx-2 rounded-full" src={item.icon} /> :
                        <img className="h-5 w-5 mx-2 rounded-full" src='empty-logo.svg' />
                      }
                      {item.name}
                    </button>
                  ))}
                </div>
              </nav>
            </div>

          </div>
        </div>
      </div>
      <div className="flex flex-col min-w-0 flex-1 overflow-hidden">
        <div className="flex w-full justify-end border-b-2">
          <div className="px-4 py-1.5">
            <div className="flex items-center">
              <div>
                <img className="inline-block h-9 w-9 rounded-full" src={session?.user?.image} alt="" />
              </div>
              <div className="ml-3">
                <p className="text-sm font-medium text-gray-700 group-hover:text-gray-900">{session?.user?.name}</p>
                <button type='button' onClick={signOut} className="text-xs font-medium text-gray-500 group-hover:text-gray-700">Logout</button>
              </div>
            </div>
          </div>
        </div>
        <div className="overflow-y-scroll">
          {children}
        </div>
      </div>
    </div >
  )
}
