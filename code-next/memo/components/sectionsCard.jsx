import React, { useState, useEffect } from 'react'
import { useForm } from "react-hook-form";
import { useRouter } from 'next/router'

const sectionsCard = () => {
    const router = useRouter()
    const { name, id } = router.query
    const [statusCopy, setStatusCopy] = useState({ status: false, text: '', color: '' })
    const [section, setSection] = useState()
    const [searchSection,setSearchSection] = useState();
    const [sortSection,setSortSection] = useState(false);
    const [filteredSection,setFilteredSection] = useState();
    const [project, setProject] = useState()
    const [statusCreateSection, setStatusCreateSection] = useState(false)
    const [idSectionForAddItem, setIdSectionForAddItem] = useState()
    const [err, setErr] = useState({ status: false, message: '', color: '' })
  
    const {
      register,
      handleSubmit,
      reset,
    } = useForm();
  
    const onSubmit = async (values) => {
      setErr({ status: false, message: '', color: '' })
      try {
        const res = await fetch(`/api/createsection?projectId=${project.id}&name=${values.name}&value=${values.value}`);
        const response = await res.json();
        if (typeof response === 'object') {
          getSectionById()
          setErr({ status: true, message: 'Create section complete !!!', color: 'green' })
          setTimeout(
            function () {
              setErr({ status: false, message: '', color: '' })
            }, 2500);
          reset()
        } else {
          setErr({ status: true, message: 'Create section Failed !!!', color: 'red' })
          setTimeout(
            function () {
              setErr({ status: false, message: '', color: '' })
            }, 2500);
        }
      } catch (error) {
        console.log(error);
        setErr({ status: true, message: 'Create section Failed !!!', color: 'red' })
        setTimeout(
          function () {
            setErr({ status: false, message: '', color: '' })
          }, 2500);
  
      }
    };
  
    const onSubmitSectionItem = async (values) => {
      try {
        const res = await fetch(`/api/createsectionitem?sectionId=${idSectionForAddItem.id}&name=${values.name}&value=${values.value}`);
        const response = await res.json();
        if (typeof response === 'object') {
          reset()
          handleRefresh(idSectionForAddItem.index)
        } else {
          console.log('Create section-items Failed !!!');
        }
      } catch (error) {
        console.log(error);
      }
    };
  
  
    const getSectionById = async () => {
      try {
        const resProject = await fetch('/api/projects');
        const responseProject = await resProject.json();
        const { projects } = responseProject
        const data = id ? id : projects[0].id
        const resProjectById = await fetch(`/api/projectbyid?id=${data}`);
        const responseProjectById = await resProjectById.json();
        const res = await fetch(`/api/sections?id=${data}`);
        const response = await res.json();
        if (responseProjectById.projects.length > 0) {
          setProject(responseProjectById.projects[0])
          if (response.projectId) {
            const { sections } = response
            const datavalue = {
              idProject: response.projectId,
              valueSection: sections.map((element) => ({
                idSection: element.id,
                name: element.name,
                value: element.value,
                status: true,
                createdAt: element.createdAt,
                updatedAt: element.updatedAt,
                sectionItems: []
              }
              ))
            }
            setSection(datavalue)
            setFilteredSection(datavalue.valueSection)
          } else {
            console.log('error get section');
          }
        } else {
          console.log('error get project by id');
        }
  
      } catch (error) {
        console.log(error);
      }
    }
  
  
    const handleRefresh = async (index) => {
      let newDataPack = { ...section }
      const res = await fetch(`/api/sectionitems?id=${idSectionForAddItem.id}`);
      const response = await res.json();
      if (response.sectionId) {
        const { sectionItems } = response
        newDataPack.valueSection[index].sectionItems = sectionItems
        setSection(newDataPack)
        if (searchSection) {
          let result = [];
          result = newDataPack.valueSection.filter((data) => {
          return data.name.search(searchSection) != -1;
          });
          setFilteredSection(result);
        } else {
          setFilteredSection(newDataPack.valueSection)
        }
      }
    }
  
    const handleClick = async (index) => {
      let newDataPack = { ...section }
      if (newDataPack.valueSection[index].status) {
        newDataPack.valueSection[index].status = false;
        if (newDataPack.valueSection[index].sectionItems.length === 0) {
          const { idSection } = newDataPack.valueSection[index]
          const res = await fetch(`/api/sectionitems?id=${idSection}`);
          const response = await res.json();
          if (response.sectionId) {
            const { sectionItems } = response
            if (sectionItems.length > 0) {
              newDataPack.valueSection[index].sectionItems = sectionItems
            }
          } else {
            console.log('section item not found!!!');
          }
        }
      } else {
        newDataPack.valueSection[index].status = true;
      }
      setSection(newDataPack)
      if (searchSection) {
        let result = [];
        result = newDataPack.valueSection.filter((data) => {
        return data.name.search(searchSection) != -1;
        });
        setFilteredSection(result);
      } else {
        setFilteredSection(newDataPack.valueSection)
      }
    }
  
  
    const handleCopyClipboard = (data) => {
      var textArea = document.createElement("textarea");
      textArea.value = data
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();
  
      try {
        var successful = document.execCommand('copy');
        if (successful) {
          setStatusCopy({ status: true, text: 'copy clipboard success!!', color: 'green' })
          setTimeout(
            function () {
              setStatusCopy({ status: false, text: '', color: '' })
            }, 2500);
        } else {
          setStatusCopy({ status: true, text: 'copy clipboard unsuccessful!!', color: 'yellow' })
          setTimeout(
            function () {
              setStatusCopy({ status: false, text: '', color: '' })
            }, 2500);
        }
  
      } catch (err) {
        setStatusCopy({ status: true, text: 'Oops, unable to copy !!', color: 'red' })
      }
  
      document.body.removeChild(textArea);
    }
  
    const handleClickStatusCreatesEction = () => {
      if (statusCreateSection) {
        setStatusCreateSection(false)
      } else {
        setStatusCreateSection(true)
      }
    }
  
    const handleSearch = (event) =>{
      let value = event.target.value.toLowerCase();
      setSearchSection(value)
      let result = [];
      result = section.valueSection.filter((data) => {
      return data.name.search(value) != -1;
      });
      setFilteredSection(result);
    }
  
    const handleSortSection = () =>{
      if (sortSection === true) {
        setSortSection(false)
      } else {
        setSortSection(true)
      }
    }
  
    useEffect(() => {
      getSectionById()
    }, [id])
  
    return (
      <div className="flex flex-col items-center w-full h-full">
        <div className="flex justify-end w-full mt-4 ">
          <button onClick={() => handleClickStatusCreatesEction()} className={`${statusCreateSection ? 'bg-gray-700 border-gray-700 text-white ' : 'bg-white border-gray-700 border text-gray-700'}   px-3 py-2 rounded-lg flex items-center justify-center`}>
            {statusCreateSection ? <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
            </svg> : <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
              <path fill-rule="evenodd" d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" clip-rule="evenodd" />
            </svg>}
            <p className="ml-2">
              Section
            </p>
          </button>
        </div>
        {
          err.status && <div className={`flex justify-center items-center px-2 py-2 font-semibold rounded-lg mt-5 ${err.color === 'green' ? 'bg-green-400 text-green-800' : 'bg-red-400 text-red-700'} w-2/5`}>
            {err.message}
          </div>
        }
        {
          statusCreateSection && <div className="bg-gray-200 flex flex-col w-4/6 mt-10 text-gray-700 font-bold text-lg px-8 py-4 rounded-lg">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="my-4 flex items-center">
                <div className="w-1/5">
                  <p>Section name</p>
                </div>
                <input
                  type="text"
                  name="name"
                  id="name"
                  className="shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-4/5 h-10 rounded-xl p-3 "
                  placeholder="Section name"
                  {...register("name", { required: true })}
                />
  
              </div>
              <div className="my-4 flex items-center">
                <div className="w-1/5">
                  <p>Value</p>
                </div>
                <input
                  type="text"
                  name="value"
                  id="value"
                  className="shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-4/5 h-10 rounded-xl p-3 "
                  placeholder="Value"
                  {...register("value")}
                />
  
              </div>
              <div className="flex justify-end">
                <button type="reset" className="bg-white text-gray-700 px-2 py-1 rounded-lg">
                  clear
                </button>
                <button type="submit" className="bg-gray-700 text-white ml-2 px-2 py-1 rounded-lg">
                  create
                </button>
              </div>
            </form>
          </div>
        }
        <div class="flex items-center justify-end w-full mt-4">
          <div class="max-w-lg w-full lg:max-w-xs">
              <label for="search" class="sr-only">Search</label>
              <div class="relative">
                <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                  <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
                  </svg>
                </div>
                <input id="search" name="search" onChange={(event) =>handleSearch(event)} class="block w-full pl-10 pr-3 py-2 border border-gray-300 rounded-md leading-5 bg-white shadow-sm placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:ring-1 focus:ring-blue-600 focus:border-blue-600 sm:text-sm" placeholder="Search" type="search"/>
              </div>
          </div>
          <button onClick={()=>handleSortSection()} className=" flex rounded-lg text-white bg-black ml-2 px-4 py-2">
            <p>
              Sort by date
            </p>
          <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 16V4m0 0L3 8m4-4l4 4m6 0v12m0 0l4-4m-4 4l-4-4" />
          </svg>
          </button>
        </div>
        {
          statusCopy.status && <div className={`mt-4 ${statusCopy.color === 'green' ? 'bg-green-400 text-green-800' : statusCopy.color === 'yellow' ? 'bg-yellow-200 text-yellow-500' : 'bg-red-400 text-red-700'} w-11/12 flex justify-center py-2 rounded-lg`}>
            {statusCopy.text}
          </div>
        }
        <div className="w-full mt-10">
        {filteredSection && filteredSection.length > 0 ? 
        (sortSection !== false ?
          (filteredSection.sort((a,b) =>  new Date(b.updatedAt) - new Date(a.updatedAt)).map((value,index)=>{
          return(
            <div key={value.idSection} className="mt-4">      
              <button onClick={() => handleClick(index)} className={`flex items-center w-full py-3 px-6 rounded-lg justify-between ${value.status ? 'border ' : 'bg-gray-700 text-white rounded-b-none'}`}>
                <div className="flex items-center">
                  <div className="bg-white w-10 h-10 flex justify-center items-center rounded-lg">
                    <img className="w-5" src='google-icon.svg' />
                  </div>
                  <div className="ml-4 flex">
                    <p >
                      {value.name} {value.value}
                    </p>
  
                  </div>
                </div>
                <img className='w-5' src={value.status ? 'arrow-right.svg' : 'arrow-down.svg'} />
              </button>
              {value.status === false && <div className="flex bg-gray-100 py-4 px-12 mb-4 justify-between">
                {value.sectionItems.length > 0 ? <div className="w-full">
                  <div className="mx-6 mt-3 flex justify-end items-center">
                    <button className="bg-gray-700 text-white px-4 py-2 rounded-lg">
                      edit
                    </button>
                  </div>
                  <div className="mt-4">
                    <form onSubmit={handleSubmit(onSubmitSectionItem)}>
                      <div className="flex items-center">
                        <div className="w-1/6">
                          <p>
                            Add section-items :
                          </p>
                        </div>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className=" shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Name"
                          {...register("name", { required: true })}
                        />
                        <input
                          type="text"
                          name="value"
                          id="value"
                          className="ml-2 shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Value"
                          {...register("value", { required: true })}
                        />
                        <div className="flex -ml-8 w-1/6 justify-end">
                          <button type="submit" onClick={() => setIdSectionForAddItem({ id: value.idSection, index: index })} className="bg-gray-700 text-white px-2 py-1 rounded-lg">
                            Add
                          </button>
                          <button type="reset" className="bg-white text-gray-700 ml-2 border-gray-700 border px-2 py-1 rounded-lg">
                            clear
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  {value.sectionItems.length > 0 && value.sectionItems.map((item) => (
                    <div key={item.id} className="w-full flex justify-between">
                      <div className="flex mt-3 w-full pr-6">
                        <div className="bg-gray-100 w-2/12 px-6 py-4 rounded-l-lg border border-gray-400">
                          <p>
                            {item.name}
                          </p>
                        </div>
                        <div className="bg-white flex px-6 py-4 w-full rounded-r-lg border-r border-t border-b border-gray-400">
                          <p>
                            {item.value}
                          </p>
                          <button onClick={() => handleCopyClipboard(item.value)} className=" ml-4 border border-gray-600 text-gray-600 text-sm px-2  rounded-xl">
                            copy
                          </button>
                        </div>
                      </div>
                    </div>
                  ))}
                </div> : <div className="w-full flex flex-col">
                  <div className="mt-4">
                    <form onSubmit={handleSubmit(onSubmitSectionItem)}>
                      <div className="flex items-center">
                        <div className="w-1/6">
                          <p>
                            Add section-items :
                          </p>
                        </div>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className=" shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Name"
                          {...register("name", { required: true })}
                        />
                        <input
                          type="text"
                          name="value"
                          id="value"
                          className="ml-2 shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Value"
                          {...register("value", { required: true })}
                        />
                        <div className="flex -ml-8 w-1/6 justify-end">
                          <button type="submit" onClick={() => setIdSectionForAddItem({ id: value.idSection, index: index })} className="bg-gray-700 text-white px-2 py-1 rounded-lg">
                            Add
                          </button>
                          <button type="reset" className="bg-white text-gray-700 ml-2 border-gray-700 border px-2 py-1 rounded-lg">
                            clear
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="flex justify-center w-full mt-6">
                    -- null --
                  </div>
                </div>}
              </div>}
             
            </div>
                )
        }))
        :
        (filteredSection.sort((a,b) =>  new Date(a.updatedAt) - new Date(b.updatedAt)).map((value,index)=>{
          return(
            <div key={value.idSection} className="mt-4">      
              <button onClick={() => handleClick(index)} className={`flex items-center w-full py-3 px-6 rounded-lg justify-between ${value.status ? 'border ' : 'bg-gray-700 text-white rounded-b-none'}`}>
                <div className="flex items-center">
                  <div className="bg-white w-10 h-10 flex justify-center items-center rounded-lg">
                    <img className="w-5" src='google-icon.svg' />
                  </div>
                  <div className="ml-4 flex">
                    <p >
                      {value.name} {value.value}
                    </p>
  
                  </div>
                </div>
                <img className='w-5' src={value.status ? 'arrow-right.svg' : 'arrow-down.svg'} />
              </button>
              {value.status === false && <div className="flex bg-gray-100 py-4 px-12 mb-4 justify-between">
                {value.sectionItems.length > 0 ? <div className="w-full">
                  <div className="mx-6 mt-3 flex justify-end items-center">
                    <button className="bg-gray-700 text-white px-4 py-2 rounded-lg">
                      edit
                    </button>
                  </div>
                  <div className="mt-4">
                    <form onSubmit={handleSubmit(onSubmitSectionItem)}>
                      <div className="flex items-center">
                        <div className="w-1/6">
                          <p>
                            Add section-items :
                          </p>
                        </div>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className=" shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Name"
                          {...register("name", { required: true })}
                        />
                        <input
                          type="text"
                          name="value"
                          id="value"
                          className="ml-2 shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Value"
                          {...register("value", { required: true })}
                        />
                        <div className="flex -ml-8 w-1/6 justify-end">
                          <button type="submit" onClick={() => setIdSectionForAddItem({ id: value.idSection, index: index })} className="bg-gray-700 text-white px-2 py-1 rounded-lg">
                            Add
                          </button>
                          <button type="reset" className="bg-white text-gray-700 ml-2 border-gray-700 border px-2 py-1 rounded-lg">
                            clear
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  {value.sectionItems.length > 0 && value.sectionItems.map((item) => (
                    <div key={item.id} className="w-full flex justify-between">
                      <div className="flex mt-3 w-full pr-6">
                        <div className="bg-gray-100 w-2/12 px-6 py-4 rounded-l-lg border border-gray-400">
                          
                          <p>
                            {item.name}
                          </p>
                        </div>
                        <div className="bg-white flex justify-between px-6 py-4 w-10/12 rounded-r-lg border-r border-t border-b border-gray-400">
                        <div className="flex  w-8/12 justify-start ">
                          <p className="truncate">
                            {item.value}                        
                          </p>
                        </div>
                        <div className="flex items-center  w-4/12">
                          <button onClick={() => handleCopyClipboard(item.value)} className=" ml-4 border border-gray-600 text-gray-600 text-sm px-2  rounded-xl">
                            copy
                          </button>
                        </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div> : <div className="w-full flex flex-col">
                  <div className="mt-4">
                    <form onSubmit={handleSubmit(onSubmitSectionItem)}>
                      <div className="flex items-center">
                        <div className="w-1/6">
                          <p>
                            Add section-items :
                          </p>
                        </div>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className=" shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Name"
                          {...register("name", { required: true })}
                        />
                        <input
                          type="text"
                          name="value"
                          id="value"
                          className="ml-2 shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
                          placeholder="Value"
                          {...register("value", { required: true })}
                        />
                        <div className="flex -ml-8 w-1/6 justify-end">
                          <button type="submit" onClick={() => setIdSectionForAddItem({ id: value.idSection, index: index })} className="bg-gray-700 text-white px-2 py-1 rounded-lg">
                            Add
                          </button>
                          <button type="reset" className="bg-white text-gray-700 ml-2 border-gray-700 border px-2 py-1 rounded-lg">
                            clear
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="flex justify-center w-full mt-6">
                    -- null --
                  </div>
                </div>}
              </div>}
             
            </div>
                )
        })))
        : 
        <div className="flex mt-4 justify-center w-full">
        --section null --
        </div>
      }
        </div>
      </div >
    )
}

export default sectionsCard
