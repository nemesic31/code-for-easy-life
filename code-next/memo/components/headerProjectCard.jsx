import React, { useState, useEffect } from 'react'
import { useForm } from "react-hook-form";
import { useRouter } from 'next/router'

const headerProjectCard = () => {

    const router = useRouter()
    const { name, id } = router.query
    const [project, setProject] = useState()
    const [statusEditProject, setStatusEditProject] = useState(false)

    const {
        register,
        handleSubmit,
        reset,
      } = useForm();


    const getProjectById = async () => {
        try {
          const resProject = await fetch('/api/projects');
          const responseProject = await resProject.json();
          const { projects } = responseProject
          const data = id ? id : projects[0].id
          const resProjectById = await fetch(`/api/projectbyid?id=${data}`);
          const responseProjectById = await resProjectById.json();
          if (responseProjectById.projects.length > 0) {
            setProject(responseProjectById.projects[0])
          } else {
            console.log('error get project by id');
          }
    
        } catch (error) {
          console.log(error);
        }
      }

    const onSubmitEditProject = async (values) => {
        try {
          if (values.name && values.value) {
            const { name, value } = values
            const nameProject = name !== '' ? name : project.name
            const descriptionProject = value !== '' ? value : project.text
            const res = await fetch(`/api/updateproject?id=${project.id}&name=${nameProject}&text=${descriptionProject}`);
            const response = await res.json();
            if (typeof response === 'object') {
              const resProjectById = await fetch(`/api/projectbyid?id=${project.id}`);
              const responseProjectById = await resProjectById.json();
              reset()
              if (responseProjectById.projects.length > 0) {
                setProject(responseProjectById.projects[0])
                location.reload()
              }
            } else {
              console.log('update project Failed !!!');
            }
          } else {
            setStatusEditProject(false)
          }
        } catch (error) {
          console.log(error);
        }
      };

      useEffect(() => {
        getProjectById()
      }, [id])

  return (
    <div className="flex flex-col w-full">
        {project && (statusEditProject === false ? (<div className="flex flex-col justify-start w-full px-8 mt-6">
        <div className="flex text-gray-700 font-bold text-3xl">
          <p>
            {project.name}
          </p>
          <button onClick={() => setStatusEditProject(true)} className="ml-3">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
              <path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
              <path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd" />
            </svg>
          </button>
        </div>
        <p className="font-light">
          {project.text}
        </p>
      </div>) : (<div className="flex flex-col justify-start w-full px-8 mt-6">
        <form onSubmit={handleSubmit(onSubmitEditProject)}>
          <div className="flex items-center">
            <input
              type="text"
              name="name"
              id="name"
              className=" shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
              placeholder={project.name}
              {...register("name", { required: true })}
            />
            <input
              type="text"
              name="value"
              id="value"
              className="ml-2 shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-2/6 h-10 rounded-xl p-3 "
              placeholder={project.text}
              {...register("value", { required: true })}
            />
            <div className="flex -ml-8 w-1/6 justify-end">
              <button type="submit" className="bg-gray-700 text-white px-2 py-1 rounded-lg">
                save
              </button>
              <button type="reset" className="bg-white text-gray-700 ml-2 border-gray-700 border px-2 py-1 rounded-lg">
                clear
              </button>
            </div>
          </div>
        </form>

      </div>))}
      
    </div>
  )
}

export default headerProjectCard
