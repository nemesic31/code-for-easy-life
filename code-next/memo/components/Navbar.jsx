import { classNames } from '../utilities/CSSUtility'

const Navbar = ({ navigation }) => {
  return (
    <nav className="mt-5 flex-1" aria-label="Sidebar">
      <div className="px-2 space-y-1">
        {navigation.map((item) => (
          <a
            key={item.name}
            href={`${item.href}`}
            className={classNames(
              item.current
                ? 'bg-gray-200 text-gray-900'
                : 'text-gray-600 hover:bg-gray-50 hover:text-gray-900',
              'group flex items-center px-2 py-2 text-sm font-medium rounded-md'
            )}
            aria-current={item.current ? 'page' : undefined}
          >
            {item.icon ?
              <img className="h-5 w-5 mx-2 rounded-full" src={item.icon} /> :
              <img className="h-5 w-5 mx-2 rounded-full" src='empty-logo.svg' />
            }
            {item.name}
          </a>
        ))}
      </div>
    </nav>
  )
}

export default Navbar
