import Layout from '../components/Layout'

const example = () => {
  async function callBackend() {
    const res = await fetch('/api/hello?id=11cd9016-06d5-4228-a73c-48257352e6e4');
    const text = await res.text();
  }

  return (
    <div>
      <button className='p-1 bg-gray-400 rounded-lg' onClick={callBackend}>Test call api</button>
    </div>
  )
}

export default example
example.Layout = Layout;
