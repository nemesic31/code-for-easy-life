import React, { useState, useEffect } from 'react'
import Layout from '../components/Layout'
import { getSession } from 'next-auth/client'
import dynamic from 'next/dynamic'



const index = () => {
  const Header = dynamic(import('../components/headerProjectCard'))
  const Body = dynamic(import('../components/sectionsCard'))
 
  return (
    <div className="flex flex-col p-4 items-center h-full">
      <div className="flex w-full justify-start px-8 mt-6">
      <Header/>
      </div>
      <div className="flex w-11/12 mt-4">
      <Body/>
      </div>
    </div >
  )
}

export default index
index.Layout = Layout;

index.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}