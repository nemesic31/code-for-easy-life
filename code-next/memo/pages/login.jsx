import { providers, signIn, getSession, getProviders } from "next-auth/client";

const login = ({ providers }) => {
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <img
            className="mx-auto h-12 w-auto"
            src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
            alt="Workflow"
          />
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Login to your account</h2>
        </div>
        <div>
          <button
            onClick={() => signIn(providers.google.id)}
            type="button"
            className="mt-4 w-full inline-flex space-x-2 justify-center py-2 px-4 border border-gray-300 rounded-md bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition duration-150 ease-in-ou"
          >
            <div className="flex">
              <div className="mx-2">
                <img className="h-5 w-5" src="/google-icon.svg" />
              </div>
              <div className="mx-2">Login with Google</div>
            </div>
          </button>
        </div>

      </div>
    </div>
  )
}

export default login

login.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (session) {
    res.writeHead(302, {
      Location: '/'
    })
    res.end()
    return
  }
  return {
    session: undefined,
    providers: await providers(context)
  }
}