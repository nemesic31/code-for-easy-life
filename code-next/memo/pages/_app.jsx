import 'tailwindcss/tailwind.css'
import App from "next/app";
import { Provider, getSession } from 'next-auth/client'

const MyApp = ({ Component, pageProps }) => {
  const Layout = Component.Layout || emptyLayout;

  return (
    <Provider session={pageProps.session}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  )
}

export default MyApp

const emptyLayout = ({children}) => <>{children}</>


