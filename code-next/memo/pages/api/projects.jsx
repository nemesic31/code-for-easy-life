const handler = async (request, response) => {
  let fetchOption = {
    method: 'get'
  };
  const res = await fetch(`${process.env.BACKEND_URL}/api/memo/projects`, fetchOption);
  if (res.status === 200) {
    const text = await res.json();
    response.status(200).send(text)
  } else {
    response.status(res.status).send()
  }
}

export default handler