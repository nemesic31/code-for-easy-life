const handler = async (request, response) => {
  const { sectionId, name, value } = request.query
  let fetchOption = {
    method: 'post',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify({
      sectionId,
      name,
      value
    })

  };
  const res = await fetch(`${process.env.BACKEND_URL}/api/memo/section-items`, fetchOption);
  if (res.ok) {
    const text = await res.json();
    response.status(200).send(text)
  } else {
    response.status(res.status).send()
  }
}

export default handler