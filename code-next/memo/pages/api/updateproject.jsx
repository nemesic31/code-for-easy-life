const handler = async (request, response) => {
  const { id, name, text } = request.query
  let fetchOption = {
    method: 'put',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify({
      name,
      text
    })

  };
  const res = await fetch(`${process.env.BACKEND_URL}/api/memo/projects/project/${id}`, fetchOption);
  if (res.ok) {
    const text = await res.json();
    response.status(200).send(text)
  } else {
    response.status(res.status).send()
  }
}

export default handler