const handler = async (request, response) => {
  const { id } = request.query
  let fetchOption = {
    method: 'get'
  };
  const res = await fetch(`${process.env.BACKEND_URL}/api/memo/section-items/section/${id}`, fetchOption);
  if (res.status === 200) {
    const text = await res.json();
    response.status(200).send(text)
  } else {
    response.status(res.status).send()
  }
}

export default handler