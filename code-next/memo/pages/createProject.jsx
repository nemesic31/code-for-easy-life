import React, { useState, useEffect } from 'react'
import { getSession } from 'next-auth/client'
import { useForm } from "react-hook-form";
import Layout from '../components/Layout'


const createProject = () => {

  const [err, setErr] = useState({ status: false, message: '', color: '' })

  const {
    register,
    handleSubmit,
    watch,
    reset,
  } = useForm();

  const onSubmit = async (values) => {
    setErr({ status: false, message: '', color: '' })
    try {
      const res = await fetch(`/api/createproject?name=${values.name}&text=${values.description}`);
      const response = await res.json();
      if (typeof response === 'object') {
        setErr({ status: true, message: 'Create project complete !!!', color: 'green' })
        setTimeout(
          function () {
            setErr({ status: false, message: '', color: '' })
          }, 2500);
        reset()
        location.reload()
      } else {
        setErr({ status: true, message: 'Create project Failed !!!', color: 'red' })
        setTimeout(
          function () {
            setErr({ status: false, message: '', color: '' })
          }, 2500);
      }
    } catch (error) {
      console.log(error);
      setErr({ status: true, message: 'Create project Failed !!!', color: 'red' })
      setTimeout(
        function () {
          setErr({ status: false, message: '', color: '' })
        }, 2500);

    }
  };

  const watchAllFields = watch();


  return (
    <div className="flex flex-col items-center">
      <div className="flex justify-start w-full px-8 mt-6 text-gray-700 font-bold text-3xl">
        <p>
          Create Project
        </p>
      </div>
      {err.status && <div className={`flex justify-center items-center px-2 py-2 font-semibold rounded-lg mt-5 ${err.color === 'green' ? 'bg-green-400 text-green-800' : 'bg-red-400 text-red-700'} w-2/5`}>
        {err.message}
      </div>}
      <div className="bg-gray-400 flex flex-col w-4/6 mt-10 text-white font-bold text-lg px-8 py-4 rounded-lg">

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="my-4 flex items-center">
            <div className="w-1/5">
              <p>Project name</p>
            </div>
            <input
              type="text"
              name="name"
              id="name"
              className="shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-4/5 h-10 rounded-xl p-3 "
              placeholder="Project name"
              {...register("name", { required: true })}
            />

          </div>
          <div className="my-4 flex items-center">
            <div className="w-1/5">
              <p>Description</p>
            </div>
            <input
              type="text"
              name="description"
              id="description"
              className="shadow-sm text-sm text-black block focus:border-gray-700 focus:border focus:ring-0 font-normal w-4/5 h-10 rounded-xl p-3 "
              placeholder="Description"
              {...register("description", { required: true })}
            />

          </div>
          <div className="flex justify-end">
            <button type="reset" className="bg-white text-gray-700 px-2 py-1 rounded-lg">
              clear
            </button>
            <button type="submit" className="bg-gray-700 text-white ml-2 px-2 py-1 rounded-lg">
              create
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default createProject
createProject.Layout = Layout;

createProject.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}
