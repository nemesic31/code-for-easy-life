import { isEmpty } from 'lodash';

const TOKEN_KEY = 'jwtToken';
const REFRESH_TOKEN = 'refreshToken';
const USER_REF_ID = 'userRefId';
const USER_INFO = 'userInfo';

const { stringify, parse } = JSON;

const auth = {
  /**
   * Remove an item from the used storage
   * @param  {String} key [description]
   */
  clear(key) {
    if (localStorage && localStorage.getItem(key)) {
      localStorage.removeItem(key);
    }

    if (sessionStorage && sessionStorage.getItem(key)) {
      sessionStorage.removeItem(key);
    }

    return null;
  },

  /**
   * Clear all app storage
   */
  clearAppStorage() {
    if (localStorage) {
      localStorage.clear();
    }

    if (sessionStorage) {
      sessionStorage.clear();
    }
  },

  clearToken(tokenKey = TOKEN_KEY) {
    return auth.clear(tokenKey);
  },

  clearRefreshToken(refreshTokenKey = REFRESH_TOKEN) {
    return auth.clear(refreshTokenKey);
  },

  clearUserRefId(userRefIdKey = USER_REF_ID) {
    return auth.clear(userRefIdKey);
  },

  clearUserInfo(userInfo = USER_INFO) {
    return auth.clear(userInfo);
  },

  /**
   * Returns data from storage
   * @param  {String} key Item to get from the storage
   * @return {String|Object}     Data from the storage
   */
  get(key) {
    if (localStorage && localStorage.getItem(key)) {
      return parse(localStorage.getItem(key)) || null;
    }

    if (sessionStorage && sessionStorage.getItem(key)) {
      return parse(sessionStorage.getItem(key)) || null;
    }

    return null;
  },

  getToken(tokenKey = TOKEN_KEY) {
    return auth.get(tokenKey);
  },

  getRefreshToken(refreshTokenKey = REFRESH_TOKEN) {
    return auth.get(refreshTokenKey);
  },

  getUserRefId(userRefIdKey = USER_REF_ID) {
    return auth.get(userRefIdKey);
  },

  getUserInfo(userInfo = USER_INFO) {
    return auth.get(userInfo);
  },

  /**
   * Set data in storage
   * @param {String|Object}  value    The data to store
   * @param {String}  key
   * @param {Boolean} isLocalStorage  Defines if we need to store in localStorage or sessionStorage
   */
  set(value, key, isLocalStorage) {
    if (isEmpty(value)) {
      return null;
    }

    if (isLocalStorage && localStorage) {
      return localStorage.setItem(key, stringify(value));
    }

    if (sessionStorage) {
      return sessionStorage.setItem(key, stringify(value));
    }

    return null;
  },

  setToken(value = '', isLocalStorage = false, tokenKey = TOKEN_KEY) {
    return auth.set(value, tokenKey, isLocalStorage);
  },

  setRefreshToken(value = '', isLocalStorage = false, refreshTokenKey = REFRESH_TOKEN) {
    return auth.set(value, refreshTokenKey, isLocalStorage);
  },

  setUserRefId(value = '', isLocalStorage = false, userRefIdKey = USER_REF_ID) {
    return auth.set(value, userRefIdKey, isLocalStorage);
  },

  setUserInfo(value = '', isLocalStorage = false, userInfo = USER_INFO) {
    return auth.set(value, userInfo, isLocalStorage);
  },
};

export default auth;
