import * as yup from 'yup';

export const objectSchema = yup.object().shape({
  phone: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .matches(/^\d+$/, 'ต้องเป็นตัวเลขเท่านั้น')
    .min(10, 'ต้องมีแค่ 10 ตัว'),
  email: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .email('กรุณากรอกอีเมลที่ถูกต้อง'),
  password: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .min(8, 'จำนวน 8 ตัวขึ้นไป')
    .matches(RegExp('(.*[a-z].*)'), 'มีตัวอักษรพิมพ์เล็กอย่างน้อย 1 ตัว')
    .matches(RegExp('(.*[A-Z].*)'), 'มีตัวอักษรพิมพ์ใหญ่อย่างน้อย 1 ตัว')
    .matches(RegExp('(.*\\d.*)'), 'มีตัวเลขอย่างน้อย 1 ตัว'),
  confirmPassword: yup
    .string()
    .required('กรุณากรอกรหัสยืนยัน')
    .when('password', {
      is: (password) => (password && password.length > 0 ? true : false),
      then: yup.string().oneOf([yup.ref('password')], 'รหัสผ่านไม่ตรงกัน'),
    }),
});

export const usernameSchema = yup.object({
  username: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .email('กรุณากรอกอีเมลที่ถูกต้อง'),
});
export const telSchema = yup.object({
  username: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .matches(/^\d+$/, 'ต้องเป็นตัวเลขเท่านั้น')
    .min(10, 'ต้องมีแค่ 10 ตัว')
});
export const loginSchema = yup.object({

  password: yup.string().required('จำเป็นต้องกรอก'),
});

export const phoneSchema = yup.object({
  phone: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .matches(/^\d+$/, 'ต้องเป็นตัวเลขเท่านั้น')
    .min(10, 'ต้องมี 10 ตัว'),
});
