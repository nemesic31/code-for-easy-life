import React from 'react';
import { Link } from 'react-router-dom';

import FormLogin from '../components/FormLogin';
import ButtonSocial from '../components/ButtonSocial';

const HomePage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <FormLogin />
      <div className="mt-5 relative">
        <div className="absolute inset-0 flex items-center" aria-hidden="true">
          <div className="w-full border-t border-gray-600"></div>
        </div>
        <div className="relative flex justify-center text-sm">
          <span className="px-2 bg-white text-gray-600">หรือ</span>
        </div>
      </div>
      <ButtonSocial />
      <div className="flex justify-start mt-5 text-gray-600">
        <p>ยังไม่มีบัญชีผู้ใช้ ?</p>
        <Link to={`${process.env.PUBLIC_URL}/register`}>
          <p className="font-bold underline ml-2">ลงทะเบียน</p>
        </Link>
      </div>
    </div>
  </div>
);

export default HomePage;
