import React from 'react';

import ForgetPassCard from '../components/ForgetPassCard';


const ForgetPassPage = () => (
  <div className="flex justify-center">
    <div >
      <ForgetPassCard />
    </div>
  </div>
);

export default ForgetPassPage;
