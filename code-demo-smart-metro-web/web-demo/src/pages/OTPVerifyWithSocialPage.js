import React from 'react';

import OTPVerifyWithSocialCard from '../components/OTPVerifyWithSocialCard';


const OTPVerifyWithSocialPage = () => (
  <div className="flex justify-center">
    <div >
      <OTPVerifyWithSocialCard />
    </div>
  </div>
);

export default OTPVerifyWithSocialPage;