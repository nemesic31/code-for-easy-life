import React from 'react';
import { Link } from 'react-router-dom';
import FormRegister from '../components/FormRegister';

import ButtonSocial from '../components/ButtonSocial';

const RegisterPage = () => {
  const client_id = sessionStorage.getItem("client_id")
  const scope = sessionStorage.getItem("scope")
  const client_redirect_uri = sessionStorage.getItem("redirect_uri")
  const state = sessionStorage.getItem("state")
  return (
    <div className="flex justify-center">
      <div className="w-full">
        <FormRegister />
        <div className="mt-5 relative">
          <div className="absolute inset-0 flex items-center" aria-hidden="true">
            <div className="w-full border-t border-gray-600"></div>
          </div>
          <div className="relative flex justify-center text-sm">
            <span className="px-2 bg-white text-gray-600">หรือ</span>
          </div>
        </div>
        <ButtonSocial />
        <div className="flex mt-5 text-gray-600">
          <p>มีบัญชีผู้ใช้แล้ว ?</p>
          <Link to={client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}` : `${process.env.PUBLIC_URL}/`}>
            <p className="font-bold underline ml-2">เข้าสู่ระบบ</p>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default RegisterPage;
