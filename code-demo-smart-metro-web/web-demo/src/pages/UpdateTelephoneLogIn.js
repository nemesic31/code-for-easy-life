import React from 'react';

import UpdateTelephoneLogInCard from '../components/UpdateTelephoneLogInCard';

const UpdateTelephoneLogIn = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <UpdateTelephoneLogInCard />
    </div>
  </div>
);

export default UpdateTelephoneLogIn;
