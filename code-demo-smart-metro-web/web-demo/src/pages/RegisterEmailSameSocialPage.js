import React from 'react';
import RegisterEmailSameSocialCard from '../components/RegisterEmailSameSocialCard';

const RegisterEmailSameSocialPage = () => (
  <div className="mt-8">
    <RegisterEmailSameSocialCard />
  </div>
);

export default RegisterEmailSameSocialPage;