import React from 'react';

import OTPCard from '../components/OTPCard';


const OTPPage = () => (
  <div className="flex justify-center">
    <div >
      <OTPCard />
    </div>
  </div>
);

export default OTPPage;
