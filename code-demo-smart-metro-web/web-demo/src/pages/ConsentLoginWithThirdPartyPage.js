import React from 'react';

import ConsentLoginWithThirdPartyCard from '../components/ConsentLoginWithThirdPartyCard';

const ConsentLoginWithThirdPartyPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <ConsentLoginWithThirdPartyCard />
    </div>
  </div>
);

export default ConsentLoginWithThirdPartyPage;