import React from 'react';

import ConsentLogInCard from '../components/ConsentLogInCard';

const ConsentLogInPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <ConsentLogInCard />
    </div>
  </div>
);

export default ConsentLogInPage;