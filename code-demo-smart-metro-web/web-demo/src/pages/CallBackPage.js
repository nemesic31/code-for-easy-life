import React, { useState, useEffect } from 'react';
import { receiveToken } from '../api/Social.script'
import auth from '../utils/auth';
import { withRouter } from 'react-router-dom';
import loadings from '../resource/icon/loading.png';
import apiconfig from '../api/config.json';
import { getProfile } from '../api/User.script';
import { verifyAccountThirdPartyWithSocial } from '../api/ThirdParty.script';
import { element } from 'prop-types';
import { createOtp } from '../api/Otp.script';


const CallBackPage = (props) => {
  const params = new URLSearchParams(window.location.search)
  const error = params.get('error')
  const getToken = async () => {
    const code = params.get('code')
    try {
      const client_id = sessionStorage.getItem("client_id")
      const scope = sessionStorage.getItem("scope")
      const state = sessionStorage.getItem("state")
      const client_redirect_uri = sessionStorage.getItem("redirect_uri")
      const redirect_uri = apiconfig.mea_redirect_uri
      if (client_id) {
          const inOAuthFlow = true
          const response = await receiveToken(code, inOAuthFlow);
          if (response.error_description) {
            alert(response.error_description)
          } 
          else {
              const {
                code,
                requireConsent,
                requireMobile,
                requireOtpMobile,
                userRefId } = response
              if (requireConsent === false && requireMobile === false && requireOtpMobile === false) {
                const url = `${client_redirect_uri}?code=${code}&state=${state}`
                window.location.assign(url)
              } else if (requireConsent === true) {
                const responseProfile = await getProfile(userRefId)
                if (responseProfile.mobileNumber) {
                  const phone_number = responseProfile.mobileNumber
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/consent-login-social`,
                    state: { phone_number, requireMobile, requireOtpMobile, userRefId,code },
                  };
                  props.history.push(location);
                } else if (responseProfile.mobileNumber === undefined) {
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/consent-login-social`,
                    state: { requireMobile, requireOtpMobile, userRefId,code },
                  };
                  props.history.push(location);
                }
              } else if (requireConsent === false && requireMobile === true) {
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/updateTelephone-social`,
                  state: { requireOtpMobile, userRefId,code },
                };
                props.history.push(location);
              } else if (requireConsent === false && requireMobile === false && requireOtpMobile === true) {
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/updateTelephone-social`,
                  state: { requireOtpMobile, userRefId,code },
                };
                props.history.push(location);
              } else {
                console.log('status require is wrong !!!');
              }
          }

      } else {
        const inOAuthFlow = false
        const response = await receiveToken(code,inOAuthFlow);
        if (response.errorDescription) {
          console.log(response.errorDescription);
        } else if (response.accessToken) {
          const {
            accessToken,
            refreshToken,
            requireConsent,
            requireMobile,
            requireOtpMobile,
            userRefId } = response
          auth.setToken(accessToken, true);
          auth.setRefreshToken(refreshToken, true);
          if (requireConsent === false && requireMobile === false && requireOtpMobile === false) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/profile`,
              state: { userRefId },
            };
            props.history.push(location);
          } else if (requireConsent === true) {
            const responseProfile = await getProfile(userRefId)
            if (responseProfile.mobileNumber) {
              const phone_number = responseProfile.mobileNumber
              const location = {
                pathname: `${process.env.PUBLIC_URL}/consent-login-social`,
                state: { phone_number, requireMobile, requireOtpMobile, userRefId },
              };
              props.history.push(location);
            } else if (responseProfile.mobileNumber === undefined) {
              const location = {
                pathname: `${process.env.PUBLIC_URL}/consent-login-social`,
                state: { requireMobile, requireOtpMobile, userRefId },
              };
              props.history.push(location);
            }
          } else if (requireConsent === false && requireMobile === true) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/updateTelephone-social`,
              state: { requireOtpMobile, userRefId },
            };
            props.history.push(location);
          } else if (requireConsent === false && requireMobile === false && requireOtpMobile === true) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/updateTelephone-social`,
              state: { requireOtpMobile, userRefId },
            };
            props.history.push(location);
          } else {
            console.log('status require is wrong !!!');
          }
        } else {
          console.log('something wrong !!!');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  const handleBackToApplication = async () => {
    const client_redirect_uri = sessionStorage.getItem("redirect_uri")
   window.location.assign(`${client_redirect_uri}`)
  }

  useEffect(() => {
    getToken()

  }, [])
  return (
    <div className="flex justify-center">
      {error ? 
      (<div>
        <div className="mt-14">
          <div className="flex justify-center ">
          <p className="text-2xl font-semibold">ขออภัยในความไม่สะดวก</p>
          </div>
          <p className="mt-6 text-xl ">
            ระบบไม่สามารถดำเนินการต่อได้เนื่องจากมีข้อผิด
            พลาดบางอย่าง กรุณาติดต่อกับผู้ดูแลระบบ
            และลองใหม่อีกครั้ง</p>
          <p className="mt-6 text-xl font-semibold">Error message: {error}</p>
        </div>
        <button 
        onClick={() => handleBackToApplication()}
        className="mt-6 w-full flex justify-center py-3 px-4 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none disabled:opacity-50">
        Back to application
        </button>
      </div>):
      <img src={loadings} className="animate-spin mt-20" alt="load" />}
    </div>
  );
}

export default withRouter(CallBackPage);
