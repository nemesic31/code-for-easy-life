import React from 'react';

import ConsentThirdPartWithSocialCard from '../components/ConsentThirdPartWithSocialCard';

const ConsentThirdPartWithSocialPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <ConsentThirdPartWithSocialCard />
    </div>
  </div>
);

export default ConsentThirdPartWithSocialPage;
