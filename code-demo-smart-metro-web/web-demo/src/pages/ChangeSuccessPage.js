import React from 'react';

import ChangeSuccessCard from '../components/ChangeSuccessCard';


const ChangeSuccessPage = () => (
  <div className="flex justify-center">
    <div >
      <ChangeSuccessCard />
    </div>
  </div>
);

export default ChangeSuccessPage;
