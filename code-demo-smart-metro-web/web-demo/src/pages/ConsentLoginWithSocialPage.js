import React from 'react';

import ConsentLoginWithSocialCard from '../components/ConsentLoginWithSocialCard';

const ConsentLoginWithSocialPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <ConsentLoginWithSocialCard />
    </div>
  </div>
);

export default ConsentLoginWithSocialPage;