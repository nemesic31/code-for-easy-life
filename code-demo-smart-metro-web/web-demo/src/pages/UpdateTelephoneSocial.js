import React from 'react';

import FormUpdateTelephoneSocial from '../components/FormUpdateTelephoneSocial';

const UpdateTelephoneSocial = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <FormUpdateTelephoneSocial />
    </div>
  </div>
);

export default UpdateTelephoneSocial;
