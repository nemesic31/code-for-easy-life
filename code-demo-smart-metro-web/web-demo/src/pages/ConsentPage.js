import React from 'react';

import ConditionCard from '../components/ConsentCard';

const ConsentPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <ConditionCard />
    </div>
  </div>
);

export default ConsentPage;
