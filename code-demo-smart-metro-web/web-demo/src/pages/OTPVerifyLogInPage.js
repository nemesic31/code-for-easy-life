import React from 'react';

import OTPVerifyLogInCard from '../components/OTPVerifyLogInCard';


const OTPVerifyLogInPage = () => (
  <div className="flex justify-center">
    <div >
      <OTPVerifyLogInCard />
    </div>
  </div>
);

export default OTPVerifyLogInPage;