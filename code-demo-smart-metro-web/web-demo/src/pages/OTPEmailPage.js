import React from 'react';

import OTPEmailCard from '../components/OTPEmailCard';


const OTPEmailPage = () => (
  <div className="flex justify-center">
    <div >
      <OTPEmailCard />
    </div>
  </div>
);

export default OTPEmailPage;
