import React from 'react';

import ChangePasswordForm from '../components/ChangePasswordForm';

const NewPasswordPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <ChangePasswordForm />
    </div>
  </div>
);

export default NewPasswordPage;
