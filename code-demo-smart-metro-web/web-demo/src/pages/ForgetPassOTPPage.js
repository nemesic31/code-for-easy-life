import React from 'react';

import ForgetPassOTPCard from '../components/ForgetPassOTPCard';


const ForgetPassOTPPage = () => (
  <div className="flex justify-center">
    <div >
      <ForgetPassOTPCard />
    </div>
  </div>
);

export default ForgetPassOTPPage;
