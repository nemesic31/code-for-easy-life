import React from 'react';

import CreatePasswordCard from '../components/CreatePasswordCard';

const CreatePasswordPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <CreatePasswordCard />
    </div>
  </div>
);

export default CreatePasswordPage;