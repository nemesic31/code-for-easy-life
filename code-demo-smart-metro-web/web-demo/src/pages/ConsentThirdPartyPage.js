import React from 'react';

import ConsentThirdPartyCard from '../components/ConsentThirdPartyCard';

const ConsentThirdPartyPage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <ConsentThirdPartyCard />
    </div>
  </div>
);

export default ConsentThirdPartyPage;
