import React from 'react';

import OTPVerifyEmailLogInCard from '../components/OTPVerifyEmailLogInCard';


const OTPVerifyEmailLogInPage = () => (
  <div className="flex justify-center">
    <div >
      <OTPVerifyEmailLogInCard />
    </div>
  </div>
);

export default OTPVerifyEmailLogInPage;