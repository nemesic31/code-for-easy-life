import React from 'react';

import EditTelephoneCard from '../components/EditTelephoneCard';

const EditTelephonePage = () => (
  <div className="flex justify-center">
    <div className="w-full">
      <EditTelephoneCard />
    </div>
  </div>
);

export default EditTelephonePage;
