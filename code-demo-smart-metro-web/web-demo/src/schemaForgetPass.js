import * as yup from 'yup';

export const objectSchema = yup.object().shape({
  phone: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .matches(/^\d+$/, 'หมายเลขโทรศัพท์ต้องเป็นตัวเลขเท่านั้น')
    .max(10, 'หมายเลขโทรศัพท์ต้องมีแค่ 10 ตัว'),
});
