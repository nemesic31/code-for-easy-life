import fetchApiForSocial from './FetchApiForSocial.script';
import apiConfig from './config.json';

export const reset = async (email) => {
  const response = await fetchApiForSocial('/auth/users/forgot-password/request', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({ email }),
  });
  return response.json();
};

export const verify = async (userRefId, code, ref) => {
  const response = await fetchApiForSocial('/auth/users/forgot-password/verify', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      userRefId,
      code,
      ref
    }),
  });
  return response.json();
};

export const newPassword = async (userRefId, password, confirmPassword) => {
  const response = await fetchApiForSocial(`/auth/users/${userRefId}/new-password`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      userRefId,
      password,
      confirmPassword,
    }),
  });
  return response.json();
};
