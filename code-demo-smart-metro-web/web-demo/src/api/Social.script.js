import fetchApiForSocial from './FetchApiForSocial.script';
import apiConfig from './config.json';


export const startAuthenticate = async (data, clientId, scope, redirectUri) => {
  const params = { clientId, redirectUri, scope };
  const query = Object.keys(params)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&');
  const response = await fetchApiForSocial(`/auth/social/${data}?clientId=${clientId}&redirectUri=${redirectUri}&scope=${scope}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};

export const receiveToken = async (code,inOAuthFlow) => {
  const client_id = sessionStorage.getItem("client_id")
  const client_redirect_uri = sessionStorage.getItem("redirect_uri")
  const response = await fetchApiForSocial(`/auth/users/social-register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      code,
      clientId: `${client_id ? client_id : apiConfig.mea_client_id}`,
      secret: `${client_id ? (client_id === 'auth-service' ? apiConfig.mea_client_secret : null) : apiConfig.mea_client_secret}`,
      redirectUri: `${client_redirect_uri ? client_redirect_uri : apiConfig.mea_redirect_uri}`,
      inOAuthFlow
    }),
  });
  return response.json();
};
