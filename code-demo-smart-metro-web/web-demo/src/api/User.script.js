import fetchApi from './FetchApi.script';
import fetchApiForSocial from './FetchApiForSocial.script';
import auth from '../utils/auth';
import apiConfig from './config.json';

export const login = async (username, password) => {
  const response = await fetchApiForSocial('/auth/users/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({ username, password }),
  });
  return response.json();
};

export const logout = async () => {
  const refresh_token = auth.getRefreshToken();
  const userRefId = auth.getUserRefId();
  try {
    await fetchApiForSocial(`/auth/users/${userRefId}/logout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': apiConfig.api_key
      },
      body: JSON.stringify({ refresh_token }),
    });
  } catch (error) {
    console.log(error);
  } finally {
    auth.clearToken();
    auth.clearRefreshToken();
    auth.clearUserRefId();
  }
};


export const getProfile = async (userRefId) => {
  const response = await fetchApi(`/profile/users/${userRefId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
  });
  return response.json();
};

export const register = async (username, mobileNo, password, confirmPassword) => {
  const response = await fetchApiForSocial('/auth/users/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      mobileNo,
      username,
      password,
      confirmPassword,
    }),
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};

export const createPasswordForLogin = async (userRefId, password, confirmPassword) => {
  const response = await fetchApiForSocial(`/auth/users/${userRefId}/new-password`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      userRefId,
      password,
      confirmPassword,
    }),
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};

export const checkUsername = async (email) => {
  const response = await fetchApiForSocial('/auth/users/verify',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': apiConfig.api_key
      },
      body: JSON.stringify({ email }),
    }
  );
  return response.json();
};

export const updateStatusUser = async (user_no) => {
  const response = await fetchApi('/users/updateStatus', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({ user_no }),
  });
  return response.json();
};

export const updateTelephone = async (user_ref_id, mobileNumber) => {
  const response = await fetchApi(`/profile/users/${user_ref_id}/mobile-number`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      mobileNumber,
    }),
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};
