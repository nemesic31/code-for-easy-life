import React from 'react';
import PropTypes from 'prop-types';
import logo_mea from '../resource/logo_mea.svg';

const LogoLayout = ({ children }) => (
  <div style={{ minHeight: '100vh' }}>
    <div className="max-w-sm mx-auto py-8 px-3 lg:py-24 sm:py-12">
      <div className="w-full justify-center flex">
        <img src={logo_mea} alt="logo" />
      </div>
      {children}
    </div>
  </div>
);

LogoLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default LogoLayout;
