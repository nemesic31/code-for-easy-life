import { transformToNestObject } from 'react-hook-form';

const parseErrorSchema = (error, validateAllFieldCriteria) => {
  return Array.isArray(error.inner)
    ? error.inner.reduce((previous, { path, message, type }) => {
        const previousTypes = validateAllFieldCriteria ? previous[path]?.types || [] : {};
        return {
          ...previous,
          [path]: {
            ...(previous[path] || {
              message,
              type,
              types: previousTypes,
            }),
            types: {
              ...previousTypes,
              [type]: validateAllFieldCriteria
                ? [message, ...(previousTypes[type] || [])]
                : message, // why `|| true` here ?
            },
          },
        };
      }, {})
    : {
        [error.path]: {
          message: error.message,
          type: error.type,
          types: {},
        },
      };
};

export const yupResolver = (
  schema,
  options = {
    abortEarly: false,
  }
) => async (values, _, validateAllFieldCriteria = false) => {
  try {
    return {
      values: await schema.validate(values, { ...options }),
      errors: {},
    };
  } catch (e) {
    const errors = transformToNestObject(parseErrorSchema(e, validateAllFieldCriteria));
    return {
      values: {},
      errors,
    };
  }
};
