import * as yup from 'yup';

export const objectSchema = yup.object().shape({
  password: yup
    .string()
    .required('จำเป็นต้องกรอก')
    .min(8, 'จำนวน 8 ตัวขึ้นไป')
    .matches(RegExp('(.*[a-z].*)'), 'มีตัวอักษรพิมพ์เล็กอย่างน้อย 1 ตัว')
    .matches(RegExp('(.*[A-Z].*)'), 'มีตัวอักษรพิมพ์ใหญ่อย่างน้อย 1 ตัว')
    .matches(RegExp('(.*\\d.*)'), 'มีตัวเลขอย่างน้อย 1 ตัว'),
  confirmPassword: yup
    .string()
    .required('กรุณากรอกรหัสยืนยัน')
    .when('password', {
      is: (password) => (password && password.length > 0 ? true : false),
      then: yup.string().oneOf([yup.ref('password')], 'รหัสผ่านไม่ตรงกัน'),
    }),
});
