import React, { useEffect, useState } from 'react';
import { getProfile, logout } from '../api/User.script';
import auth from '../utils/auth';
import { withRouter } from 'react-router-dom';

const Profile = (props) => {
  const authUserRefId = auth.getUserRefId();
  const token = auth.getToken();
  const [phone, setPhone] = useState();
  const [email, setEmail] = useState()


  const handleLogOut = async () => {
    await logout();
    window.location.assign(`${process.env.PUBLIC_URL}/`);
  };


  useEffect(() => {
    if(token){
      async function getUser() {
        try {
          const data = authUserRefId ? authUserRefId : props.location.state.userRefId
          const response = await getProfile(data);
          if (response.email) {
            const { email, mobileNumber } = response
            setPhone(mobileNumber);
            setEmail(email)
            if (authUserRefId === null || authUserRefId === undefined) {
              const userRefId = props.location.state.userRefId;
              auth.setUserRefId(userRefId, true);
            }
          }
        } catch (error) {
          console.log(error);
        }
      }
      getUser();
    }else{
      window.location.assign(`${process.env.PUBLIC_URL}/`)
    }
  }, [phone]);

  return (
    <div className="mt-8 w-max">
      <div className="flex justify-center">
        <span className="inline-block h-24 w-24 rounded-full overflow-hidden bg-gray-100">
          <svg className="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
            <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
          </svg>
        </span>
      </div>
      <div className="text-gray-700 text-3xl flex flex-col items-center justify-center font-semibold mt-12 w-max">
        <p>อีเมล : {email}</p>
        <p>เบอโทรศัพท์ : {phone}</p>
      </div>
      <button
        onClick={() => handleLogOut()}
        className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none mt-5"
      >
        ออกจากระบบ
      </button>
    </div>
  );
};

export default withRouter(Profile);
