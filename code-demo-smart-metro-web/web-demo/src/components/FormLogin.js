import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { withRouter } from 'react-router-dom';

import { ErrorMessage } from '@hookform/error-message';
import { usernameSchema, loginSchema } from '../schema';
import { yupResolver } from '../resolver';

import auth from '../utils/auth';

import eye from '../resource/icon/eye.svg';
import eyeOff from '../resource/icon/eye-off.svg';
import { login, checkUsername, getProfile } from '../api/User.script';
import { reset as resetPasswd } from '../api/ResetPassword.script';
import bell from '../resource/icon/bell_red.svg';
import X from '../resource/icon/x.svg';
import { verifyAccountThirdParty } from '../api/ThirdParty.script';
import { createOtp } from '../api/Otp.script';


const FormLogin = (props) => {
  const params = new URLSearchParams(window.location.search)
  const client_id = params.get('client_id')
  const redirect_uri = params.get('redirect_uri')
  const scope = params.get('scope')
  const state = params.get('state')
  const [err, setError] = useState({ show: false, message: '' });
  const [checkForm, setCheckForm] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [userCheck, setUserCheck] = useState(false);
  const [usernameLogIn, setUsernameLogIn] = useState();
  const pathName = window.location.pathname;

  const onClickShowPassword = (type) => {
    type === 'password'
      && setShowPassword(!showPassword)
  };

  const { register, errors, handleSubmit, watch, reset } = useForm({
    criteriaMode: 'all',
    reValidateMode: 'all',
    mode: 'all',
    resolver: yupResolver(!userCheck ? usernameSchema : loginSchema, { abortEarly: false }),
  });


  const onSubmit = async (values) => {
    const { username, password } = values;
    if (checkForm === false) {
      setError({ show: false, message: '' })
      if (username && !password) {
        if (username) {
          const response = await checkUsername(username)
          if (response.userRefId) {
            const { userRefId, requireMobile, requireConsent, requireOtpMobile, requireOtpEmail, socialOption, userType } = response
            const responseProfile = await getProfile(userRefId)
            if (responseProfile.mobileNumber) {
              const phone_number = responseProfile.mobileNumber
              if (userType === 'MIGRATE_USER') {
                const email = username
                if (requireConsent === false && requireMobile === false && requireOtpMobile === false && requireOtpEmail === false) {
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/create-password-login`,
                    state: { email, phone_number, userRefId },
                  };
                  props.history.push(location);
                } else if (requireConsent === true) {
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/consent-login`,
                    state: { email, phone_number, requireMobile, requireOtpMobile, userRefId, requireOtpEmail },
                  };
                  props.history.push(location);
                } else if (requireConsent === false && requireOtpEmail === true) {
                  const otp_type = 'EMAIL'
                  const responseOTP = await createOtp(userRefId, otp_type)
                  if (responseOTP.ref) {
                    const { ref } = responseOTP
                    const location = {
                      pathname: `${process.env.PUBLIC_URL}/otp-verify-email-login`,
                      state: { email, userRefId, ref, otp_type, phone_number, requireMobile, requireOtpMobile, requireOtpEmail },
                    };
                    props.history.push(location);
                  }
                } else if (requireConsent === false && requireOtpEmail === false && requireMobile === true) {
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/updateTelephone-login`,
                    state: { email, requireOtpMobile, userRefId, requireOtpEmail },
                  };
                  props.history.push(location);
                } else if (requireConsent === false && requireOtpEmail === false && requireMobile === false && requireOtpMobile === true) {

                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/updateTelephone-login`,
                    state: { email, phone_number, userRefId, requireOtpEmail },
                  };
                  props.history.push(location);
                } else {
                  console.log('status require is wrong !!!');
                }
              } else if (userType === 'SOCIAL_USER') {
                const email = username
                if (socialOption.length > 0) {
                  const socialName = socialOption[0]
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/register-email-same-social`,
                    state: { email, socialName },
                  };
                  props.history.push(location)
                }
              } else if (userType === 'NORMAL_USER') {
                setUserCheck(true)
                setUsernameLogIn(username)
              }
            } else {
              setError({ show: true, message: 'ไม่พบข้อมูลบัญชีผู้ใช้' })
            }
          } else {
            if (response.code === 'MEA0003') {
              setError({ show: true, message: 'การตรวจสอบอีเมลผิดพลาด' })
            } else {
              setError({ show: true, message: 'กรุณากรอกอีเมลที่ถูกต้อง' })
            }
          }
        } else {
          setError({ show: true, message: 'กรุณากรอกอีเมลที่ถูกต้อง' });
        }
      } else {
        console.log('error');
      }
    } else {
      if (userCheck && password) {
        const email = usernameLogIn;
        const response = await checkUsername(email)
        const { userRefId, requireMobile, requireConsent, requireOtpMobile, requireOtpEmail } = response
        const responseProfile = await getProfile(userRefId)
        const responseLogin = await login(email, password)
        if (responseLogin.accessToken) {
          const { accessToken, refreshToken } = responseLogin
          auth.setToken(accessToken, true);
          auth.setRefreshToken(refreshToken, true);
          if (responseProfile.mobileNumber) {
            const phone_number = responseProfile.mobileNumber
            if (requireConsent === false && requireMobile === false && requireOtpMobile === false && requireOtpEmail === false) {
              if (pathName === '/oauth/authorize') {
                const response = await verifyAccountThirdParty(client_id, scope, email, password, redirect_uri, state)
                if (response.errorDescription) {
                  if (response.errorDescription === "Consent Required") {
                    const consentApp = response.errorDetail.consents
                    const location = {
                      pathname: `${process.env.PUBLIC_URL}/consent-third-party`,
                      state: { consentApp, email, password },
                    };
                    props.history.push(location);
                  } else {
                    alert(response.errorDescription)
                    setError({ show: true, message: 'อีเมล หรือ รหัสผ่านไม่ถูกต้อง' });
                  }
                } else {
                  window.location.assign(`${response.redirectUrl}`)
                }
              } else {
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/profile`,
                  state: { userRefId },
                };
                props.history.push(location);
              }
            } else if (requireConsent === true) {
              const location = {
                pathname: `${process.env.PUBLIC_URL}/consent-login`,
                state: { email, phone_number, requireMobile, requireOtpMobile, userRefId, requireOtpEmail, password },
              };
              props.history.push(location);
            } else if (requireConsent === false && requireOtpEmail === true) {
              const otp_type = 'EMAIL'
              const responseOTP = await createOtp(userRefId, otp_type)
              if (responseOTP.ref) {
                const { ref } = responseOTP
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/otp-verify-email-login`,
                  state: { email, userRefId, password, ref, otp_type, phone_number, requireMobile, requireOtpMobile, requireOtpEmail },
                };
                props.history.push(location);
              }
            }
            else if (requireConsent === false && requireOtpEmail === false && requireMobile === true) {
              const location = {
                pathname: `${process.env.PUBLIC_URL}/updateTelephone-login`,
                state: { email, requireOtpMobile, userRefId, requireOtpEmail, password },
              };
              props.history.push(location);
            } else if (requireConsent === false && requireOtpEmail === false && requireMobile === false && requireOtpMobile === true) {
              const location = {
                pathname: `${process.env.PUBLIC_URL}/updateTelephone-login`,
                state: { email, requireOtpMobile, userRefId, requireOtpEmail, password },
              };
              props.history.push(location);
            }
            else {
              console.log('status require is wrong !!!');
            }
          } else {
            setError({ show: true, message: 'ไม่พบข้อมูลบัญชีผู้ใช้' })
          }
        } else {
          setError({ show: true, message: 'อีเมล หรือ รหัสผ่านไม่ถูกต้อง' });
          setCheckForm(false)
        }
      }
    }
  };

  const watchAllFields = watch();



  const onChange = (e) => {

    const { value, maxLength } = e.target;
    if (value.length > maxLength) {
      e.target.value = e.target.value.slice(0, maxLength);
    }
  };

  const onKeyPress = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }
  };

  const onForgetPassword = async () => {
    const email = usernameLogIn;
    try {
      if (email) {
        const response = await resetPasswd(email);
        if (response.errorDescription) {
          setError({
            show: true,
            message: 'ไม่พบบัญชีผู้ใช้',
          });
        } else {
          const { ref, userRefId } = response
          const location = {
            pathname: `${process.env.PUBLIC_URL}/forget-password-otp`,
            state: { ref, userRefId, email }
          };
          setError({
            show: false,
            message: '',
          });
          props.history.push(location);
        }
      } else {
        props.history.push(`${process.env.PUBLIC_URL}/forget-password`);
      }

    } catch (error) {
      console.log(error);
      setError({
        show: true,
        message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ',
      });
    }
  }

  useEffect(() => {
    const userRefId = auth.getUserRefId()
    if (userRefId !== null) {
      window.location.assign(`${process.env.PUBLIC_URL}/profile`)
    }
    if (pathName === '/oauth/authorize') {
      if (client_id && redirect_uri && scope && state) {
        sessionStorage.setItem("client_id", client_id);
        sessionStorage.setItem("redirect_uri", redirect_uri);
        sessionStorage.setItem("scope", scope);
        sessionStorage.setItem("state", state);
      }
    }
  }, [])

  return (
    <div>
      <div className="mt-8 flex flex-col items-center w-full">
        {err.show ? (
          <div className="flex w-max">
            <div className="absolute px-4 py-3">
              <img src={bell} alt="bell" />
            </div>
            <div className="flex bg-red-200 px-8 py-2 rounded-md mb-4 justify-center w-max">
              <p className="ml-2 font-normal text-rose-600">{err.message}</p>
            </div>
          </div>
        ) : null}
        <div className="flex justify-center">
          <p className="text-GrayMEA-700 font-semibold text-xl">เข้าสู่ระบบ MEA ID</p>
        </div>
        {usernameLogIn && <div className="flex flex-col items-center mt-6">
          <p className="text-GrayMEA-700 font-normal text-lg">
            บัญชีผู้ใช้
        </p>
          <p className="text-GrayMEA-700 font-normal text-lg">
            {usernameLogIn}
          </p>
        </div>}
        <form onSubmit={handleSubmit(onSubmit)} className="mt-7 w-full">
          {!userCheck && <div>
            <label className="block font-semibold text-gray-800 ">บัญชีผู้ใช้</label>
            <div className="mt-1">
              <input
                type="text"
                name="username"
                id="username"
                onClick={() => setError(false)}
                className="shadow-sm text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
                placeholder="example@example.com"
                ref={register}
              />
            </div>
            <div className="text-sm mt-2">
              <ErrorMessage
                errors={errors}
                name="username"
                render={({ messages }) =>
                  messages &&
                  Object.entries(messages).map(([type, message]) => (
                    <div className="text-red-600 text-sm flex" key={type}>
                      <img alt="x" src={X} className="mr-2" />
                      {message}
                    </div>
                  ))
                }
              />
            </div>
          </div>}
          {userCheck && <div>
            <label className="block font-semibold text-gray-800 ">รหัสผ่าน</label>
            <div className="mt-1 flex justify-end items-center">
              <input
                type={showPassword ? 'text' : 'password'}
                name="password"
                id="password"
                onClick={() => setError(false)}
                className="shadow-sm text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
                placeholder="รหัสผ่านของคุณ"
                ref={register}
              />
              {watchAllFields.password && (
                <div className="absolute mr-2">
                  <button type="button" onClick={() => onClickShowPassword('password')}>
                    <img alt="eye" src={showPassword ? eye : eyeOff} />
                  </button>
                </div>
              )}
            </div>
            <div className="text-sm mt-2">
              <ErrorMessage
                errors={errors}
                name="password"
                render={({ messages }) =>
                  messages &&
                  Object.entries(messages).map(([type, message]) => (
                    <div className="text-red-600 text-sm flex" key={type}>
                      <img alt="x" src={X} className="mr-2" />
                      {message}
                    </div>
                  ))
                }
              />
            </div>
          </div>}
          <div className="mt-6">
            {!userCheck ? <button
              onClick={() => setCheckForm(false)}
              type="submit"
              className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none"
            >
              ต่อไป
          </button>
              :
              <button
                onClick={() => setCheckForm(true)}
                type="submit"
                className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none"
              >
                เข้าสู่ระบบ
          </button>
            }
          </div>

        </form>
      </div>
      <div className="flex justify-start">
        <button className="mt-5" onClick={() => onForgetPassword()}>
          <p className="text-gray-600 font-bold underline ">ลืมรหัสผ่าน</p>
        </button>
      </div>
    </div>
  );
};

export default withRouter(FormLogin);
