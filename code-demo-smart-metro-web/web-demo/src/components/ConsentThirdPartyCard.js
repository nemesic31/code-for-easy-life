import { object } from 'prop-types';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { verifyAccountThirdParty, getConsentThirdParty,consentsThirdParty } from '../api/ThirdParty.script';


const ConsentThirdPartyCard = (props) => {
  const { consentApp, email, password } = props.location.state
  const client_id = sessionStorage.getItem("client_id")
  const scope = sessionStorage.getItem("scope")
  const redirect_uri = sessionStorage.getItem("redirect_uri")
  const state = sessionStorage.getItem("state")
  const [nameClient, setNameClient] = useState()

  const getConsent = async () => {    
    try {
      const response = await getConsentThirdParty(client_id)
      setNameClient(response.name)
    } catch (error) {
      console.log(error);
    }
  }

  const handleAcceptWithThirdParty = async () => {
   try {
     const consentAccept = true
    const response = await consentsThirdParty(client_id, scope, email, password, redirect_uri, state,consentAccept)
    console.log(response);
    if (response.errorDescription) {
      alert(response.errorDescription)
      window.location.assign(`${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&scope=${scope}&state=${state}`)
    } else {
      window.location.assign(`${response.redirectUrl}`)
    }
   } 
   catch (error) {
     console.log(error);
     alert(error)
   }

  }
  const handleDenyWithThirdParty = async () => {
    try {
      const consentAccept = false
    const response = await consentsThirdParty(client_id, scope, email, password, redirect_uri, state,consentAccept)
    if (response.errorDescription) {
      alert(response.errorDescription)
      window.location.assign(`${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&scope=${scope}&state=${state}`)
    } else {
      window.location.assign(`${response.redirectUrl}`)
    }
    } 
    catch (error) {
      console.log(error);
     alert(error)
    }

  }

  useEffect(() => {
    getConsent()
  }, []);


  return (
    <div className="w-full justify-center">
      <div className="flex justify-center mt-4">
        <div className="mt-2">
         
          <div className="flex justify-center w-max">
            <div className="h-96 w-auto overflow-y-scroll mt-4 p-4 border-GrayMEA-400 border-2">
            <div className="text-3xl flex justify-center font-semibold w-max mx-8 my-4">
          การยินยอมให้ข้อมูลกับแอปพลิเคชัน
          </div>
          <div className="mx-2 my-8 ">
            <p className="text-2xl">{nameClient ? nameClient : client_id} ต้องการข้อมูลของคุณ ดังนี้</p>
              <div
              className="text-2xl mt-4"
                dangerouslySetInnerHTML={{
                __html: consentApp
              }}></div>
              <p className="text-2xl mt-8">คุณจะอนุญาตให้แอปพลิเคชันใช้ข้อมูลเหล่านี้หรือไม่?</p>
          </div>
            </div>
          </div>

          <div className="flex flex-col justify-center items-center mt-5">
            <button
              onClick={() => handleAcceptWithThirdParty()}
              className="w-full flex justify-center py-3 px-4 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none disabled:opacity-50"
            >
              ยอมรับและดำเนินการต่อ
              </button>
            <button 
            onClick={() => handleDenyWithThirdParty()}
            className="text-OrangeMEA-500 font-semibold text-base mt-3"
            >
              ยกเลิก
              </button>
          </div>

        </div>
      </div>
    </div>
  );
};

export default withRouter(ConsentThirdPartyCard);
