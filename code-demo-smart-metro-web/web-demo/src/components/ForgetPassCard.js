import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';

import { useForm } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import { usernameSchema } from '../schema';
import { yupResolver } from '../resolver';
import { reset as resetPasswd } from '../api/ResetPassword.script';
import { checkUsername } from '../api/User.script';


import X from '../resource/icon/x.svg';
import bell from '../resource/icon/bell_red.svg';
import loadings from '../resource/icon/loading.png';

const ForgetPassCard = (props) => {
  const [loading, setLoading] = useState(false);
  const [err, setError] = useState({ show: false, message: '' });

  const { register, errors, handleSubmit } = useForm({
    criteriaMode: 'all',
    reValidateMode: 'all',
    mode: 'all',
    resolver: yupResolver(usernameSchema, { abortEarly: false }),
  });

  const onSubmit = async (values) => {
    setLoading(true);
    const { username } = values;
    try {
      const responseStatus = await checkUsername(username)
      const { socialOption, userType } = responseStatus
      if (userType === 'SOCIAL_USER') {
        const email = username
        if (socialOption.length > 0) {
          const socialName = socialOption[0]
          const location = {
            pathname: `${process.env.PUBLIC_URL}/register-email-same-social`,
            state: { email, socialName },
          };
          props.history.push(location)
        }
      } else if (userType === 'MIGRATE_USER' || userType === 'NORMAL_USER') {
        const response = await resetPasswd(username);
        if (response.errorDescription) {
          setError({
            show: true,
            message: 'ไม่พบบัญชีผู้ใช้',
          });
          setLoading(false);
        } else {
          const { ref, userRefId } = response
          const email = username
          const location = {
            pathname: `${process.env.PUBLIC_URL}/forget-password-otp`,
            state: { ref, userRefId, email }
          };
          setError({
            show: false,
            message: '',
          });
          props.history.push(location);
        }
      } else {
        setError({
          show: true,
          message: 'ไม่พบบัญชีผู้ใช้',
        });
        setLoading(false);
      }

    } catch (error) {
      console.log(error);
      setLoading(false);
      setError({
        show: true,
        message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ',
      });

    }
  };



  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)} className="mt-8 w-full">
        {loading === true && (
          <div className="opacity-100 fixed inset-0 overflow-y-auto z-50">
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
              <div
                className="opacity-100 translate-y-0 sm:scale-100 fixed inset-0 transition-opacity"
                aria-hidden="true"
              >
                <div className="absolute inset-0 bg-gray-100 opacity-75"></div>
              </div>
              <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
                &#8203;
              </span>
              <div
                className="inline-block align-bottom rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden transform transition-all sm:my-8 sm:align-middle sm:max-w-sm  sm:p-6"
                role="dialog"
                aria-modal="true"
                aria-labelledby="modal-headline"
              >
                <img src={loadings} className="animate-spin" alt="load" />
              </div>
            </div>
          </div>
        )}
        {err.show && (
          <div className="flex w-max">
            <div className="absolute px-4 py-3">
              <img src={bell} alt="bell" />
            </div>
            <div className="flex bg-red-200 px-8 py-2 rounded-md mb-4 justify-center w-max">
              <p className="ml-2 font-normal text-rose-600">{err.message}</p>
            </div>
          </div>
        )}

        <div className="text-gray-700 text-3xl flex justify-center font-semibold">ลืมรหัสผ่าน</div>
        <div className="mt-6 text-GrayMEA-500 text-lg font-light">
          กรอกอีเมลที่ใช้ลงทะเบียนของคุณ
          </div>
        <div className="mt-4 w-full">
          <div className="mt-1 w-full">
            <label className="block font-semibold text-gray-800 ">อีเมล</label>
            <input
              type="text"
              name="username"
              id="username"
              onClick={() => setError(false)}
              className="shadow-sm text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
              placeholder="example@example.com"
              ref={register}
            />
          </div>
          <div className="text-sm mt-2">
            <ErrorMessage
              errors={errors}
              name="username"
              render={({ messages }) =>
                messages &&
                Object.entries(messages).map(([type, message]) => (
                  <div className="text-red-600 text-sm flex" key={type}>
                    <img alt="x" src={X} className="mr-2" />
                    {message}
                  </div>
                ))
              }
            />
          </div>
        </div>


        <div className="mt-6">
          <button
            type="submit"
            className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none"
          >
            ดำเนินการต่อ
          </button>
        </div>
      </form>
    </div>
  );
};

export default withRouter(ForgetPassCard);
