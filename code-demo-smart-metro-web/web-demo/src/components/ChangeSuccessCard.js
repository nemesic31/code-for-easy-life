import React from 'react';

const ChangeSuccessCard = () => {
  const client_id = sessionStorage.getItem("client_id")
  const scope = sessionStorage.getItem("scope")
  const client_redirect_uri = sessionStorage.getItem("redirect_uri")
  const state = sessionStorage.getItem("state")
  return (
    <div className="w-max justify-center">
      <div className="mt-8">
        <div className="text-gray-700 text-3xl flex justify-center font-semibold">
          ตั้งรหัสผ่านใหม่สำเร็จ
        </div>

        <div className="mt-6 text-GrayMEA-500 text-lg font-light flex justify-center">
          การตั้งรหัสผ่านใหม่ของคุณสำเร็จแล้ว
        </div>
        <div className="text-GrayMEA-500 text-lg font-light flex justify-center">
          สามารถเข้าสู่ระบบด้วยรหัสผ่านใหม่ของคุณได้ทันที
        </div>
      </div>
      <div className="mt-6">
        <button
          onClick={() => window.location.assign(`${client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}` : `${process.env.PUBLIC_URL}/`}`)}
          className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none"
        >
          เข้าสู่ระบบ
        </button>
      </div>
    </div>
  );
};

export default ChangeSuccessCard;
