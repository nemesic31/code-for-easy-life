import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';

import bell from '../resource/icon/bell_red.svg';
import loadings from '../resource/icon/loading.png';

import { consents, updateConsent } from '../api/Pdpa.script';
import { reset as resetPasswd } from '../api/ResetPassword.script';
import { createOtp } from '../api/Otp.script';

const ConsentLoginWithSocialCard = (props) => {
  const code = props.location.state.code ? props.location.state.code : null
  const [loading, setLoading] = useState(false);
  const [PDPA, setPDPA] = useState();
  const [isDisable, setDisable] = useState(false);
  const [values, setValues] = useState();
  const [err, setError] = useState({ show: false, message: '' });
  const client_id = sessionStorage.getItem("client_id")
  const scope = sessionStorage.getItem("scope")
  const client_redirect_uri = sessionStorage.getItem("redirect_uri")
  const state = sessionStorage.getItem("state")
  const [checkRequire, setCheckRequire] = useState(false);

  const getPDPA = async () => {
    const { userRefId } = props.location.state
    try {
      const response = await consents();
      if (typeof response === 'string') {
        const data = JSON.parse(response);
        setPDPA(data);
        if (userRefId) {
          const defaultValues = {
            pdpa_consents: data.map((v) => ({
              version: v.version,
              type: v.type,
              name: v.name,
              detail: v.detailTH,
              checked: v.required,
            })),
          };
          setValues(defaultValues);
        } else {
          console.log('email or phone not found !!!');
        }
      } else {
        console.log('get consent failed');
      }
    } catch (error) {
      console.log(error);
    }
  };
  

  const { handleSubmit, control } = useForm({ reValidateMode: 'onChange' });

  const checkValidate = (event) => {
    const { name, checked } = event.target;
    setValue(name, checked);
    if (PDPA !== undefined) {
      const check = PDPA.map((v) => {
        if (v.name === name) {
          if (v.required) {
            if (!checked) {
              if (isDisable) {
                  setDisable(true);
                  setCheckRequire(true)
              } else {
                setDisable(true);
              }
            } else {
              if (checkRequire) {
                if (!isDisable) {
                  setDisable(false);
                }else{
                    setCheckRequire(false)
                }
              } else {
                setDisable(false);
              }
            }
          }
        }
      });
      return check;
    }
  };

  const setValue = (name, check) => {
    if (values) {
      const { pdpa_consents } = values;
      pdpa_consents.map((v) => {
        if (name === v.name) {
          v.checked = check;
        }
      });
    }
  };

  const onSubmit = async () => {
    const pdpaCheck = [];
    for (let i = 0; i < values.pdpa_consents.length; i++) {
      const element = values.pdpa_consents[i];
      if (element.checked === true) {
        pdpaCheck.push({
          version: element.version,
          type: element.type,
          name: element.name,
          detail: element.detail
        })
      }
    }

    const { phone_number, requireMobile, requireOtpMobile, userRefId } = props.location.state
    setLoading(true);
    setError({
      show: false,
      message: '',
    });
    try {
      const response = await updateConsent(userRefId, pdpaCheck)
      if (typeof response === 'string') {
        if (requireMobile === true) {
          const location = {
            pathname: `${process.env.PUBLIC_URL}/updateTelephone-social`,
            state: {
              requireOtpMobile, userRefId,code
            },
          };
          props.history.push(location);
        } else if (requireMobile === false && requireOtpMobile === true) {
          const otp_type = 'MOBILE'
          const responseOTP = await createOtp(userRefId, otp_type)
          if (responseOTP.ref) {
            const { ref } = responseOTP
            const location = {
              pathname: `${process.env.PUBLIC_URL}/otp-verify-social`,
              state: { phone_number, userRefId, ref, otp_type,code },
            };
            props.history.push(location);
          }
        } else {
          if (code) {
            const url = `${client_redirect_uri}?code=${code}&state=${state}`
            window.location.assign(url)
          } else {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/profile`,
              state: { userRefId },
            };
            props.history.push(location);
          }

        }
      } else {
        if (response.code === 'MEA0002') {
          setError({
            show: true,
            message: 'ไม่พบบัญชีผู้ใช้',
          });
        }
        else {
          setError({
            show: true,
            message: 'ไม่สามารถดำเนินการต่อได้ โปรดติดต่อผู้ให้บริการ',
          });
        }
        setLoading(false);
      }
    } catch (error) {
      console.log(error);
    }
  };


  const handleBack = () =>{
    const { phone_number, requireMobile, requireOtpMobile, userRefId } = props.location.state
    window.onpopstate = function(event) {
      if(event.state === null){
        alert("เมื่อกดกลับหน้าต่างนี้จะถูกรีเซ็ททันที");
        const location = {
          pathname: `${process.env.PUBLIC_URL}/consent-login-social`,
          state: { phone_number, requireMobile, requireOtpMobile, userRefId },
        };
        props.history.push(location);
      }
    };
  }

  useEffect(() => {
    handleBack()
    getPDPA();
  }, []);


  return (
    <div className="w-full justify-center flex flex-col items-center">
      {loading === true && (
        <div className="opacity-100 fixed inset-0 overflow-y-auto z-50">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="opacity-100 translate-y-0 sm:scale-100 fixed inset-0 transition-opacity"
              aria-hidden="true"
            >
              <div className="absolute inset-0 bg-gray-100 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
              &#8203;
            </span>
            <div
              className="inline-block align-bottom rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden transform transition-all sm:my-8 sm:align-middle sm:max-w-sm  sm:p-6"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <img src={loadings} className="animate-spin" alt="load" />
            </div>
          </div>
        </div>
      )}
      {err.show && (
        <div className="flex w-max">
          <div className="absolute px-4 py-3">
            <img src={bell} alt="bell" />
          </div>
          <div className="flex bg-red-200 px-8 py-2 rounded-md mb-4 justify-center w-max">
            <p className="ml-2 font-normal text-rose-600">{err.message}</p>
          </div>
        </div>
      )}
      <div className="flex flex-col justify-center items-center">
        <div className="text-GrayMEA-600 text-3xl mt-4 flex justify-center font-semibold w-max">
          เงื่อนไขการใช้บริการ และนโยบายความเป็นส่วนตัว
          </div>
        <div className="mt-2">
          <div className="flex justify-center w-full">
            <div className="h-96 w-96 overflow-y-scroll mt-4 p-4 border-GrayMEA-400 border-2">
              <p className="font-light">
                {PDPA !== undefined && PDPA.map((v) => (
                  <div className="flex flex-col justify-between " key={v.name}>
                    {v.detailTH && v.detailTH.split('<h4>').map((item, key) => {
                      return <span key={key} className="text-base font-normal" >{item.split('</h4>').map((items, key) => {
                        return <span key={key} className="text-base font-normal">{items.split('<p>').map((items, key) => {
                          return <span key={key} className={items === 'ข้อมูลทดสอบ'
                            ? "text-lg font-semibold" : "text-base font-normal"}>{items.split('</p>')}</span>
                        })}<br /> </span>
                      })}</span>
                    })}
                  </div>
                ))}
              </p>
            </div>
          </div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mt-2">
              {PDPA !== undefined
                ? PDPA.map((v) => (
                  <div className="flex items-center justify-between" key={v.name}>
                    <Controller
                      control={control}
                      name={v.name}
                      render={({ name }) => {
                        return (
                          <div className="flex items-center">
                            <input
                              type="checkbox"
                              name={name}
                              defaultChecked={v.required}
                              className="text-OrangeMEA-500 checked:bg-OrangeMEA-500 checked:border-transparent border-gray-300 rounded"
                              onChange={(e) => checkValidate(e)}
                            />
                            <label className="ml-2 text-GrayMEA-500 text-lg font-light">
                              {v.displayNameTH}
                            </label>
                          </div>
                        );
                      }}
                    />
                  </div>
                ))
                : null}
            </div>
            <div className="flex flex-col justify-center items-center mt-5">
              <button
                type="submit"
                className="w-full flex justify-center py-3 px-4 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none disabled:opacity-50"
                disabled={isDisable}
              >
                ยอมรับและดำเนินการต่อ
              </button>
              <Link to={client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}` : `${process.env.PUBLIC_URL}/`} className="text-OrangeMEA-500 font-semibold text-base mt-3">
                ยกเลิก
              </Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ConsentLoginWithSocialCard);
