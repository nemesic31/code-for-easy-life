import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { newPassword } from '../api/ResetPassword.script';


import { useForm } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import { objectSchema } from '../schemaChangePass';
import { yupResolver } from '../resolver';

import eye from '../resource/icon/eye.svg';
import eyeOff from '../resource/icon/eye-off.svg';
import X from '../resource/icon/x.svg';
import bell from '../resource/icon/bell_red.svg';
import loadings from '../resource/icon/loading.png';
import check from '../resource/icon/check.svg';

const ChangePasswordForm = (props) => {
  const { email, userRefId } = props.location.state;
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [err, setError] = useState({ show: false, message: '' });
  const [checkForm, setCheckForm] = useState(false);
  const [loading, setLoading] = useState(false);
  const [checkInputConfirmPassword, setCheckInputConfirmPassword] = useState();
  const [checkInputPassword, setCheckInputPassword] = useState()

  const onClickShowPassword = (type) => {
    type === 'password'
      ? setShowPassword(!showPassword)
      : setShowConfirmPassword(!showConfirmPassword);
  };

  const { register, errors, handleSubmit, watch, reset } = useForm({
    criteriaMode: 'all',
    reValidateMode: 'all',
    mode: 'all',
    resolver: yupResolver(objectSchema, { abortEarly: false }),
  });

  const onSubmit = async (values) => {
    const { password, confirmPassword } = values;
    if (checkForm === true) {
      setError({
        show: false,
        message: '',
      });
      setLoading(true);
      try {
        const response = await newPassword(userRefId, password, confirmPassword);
        if (response.errorDescription) {
          setError({
            show: true,
            message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ',
          });
          setLoading(false);
        } else {
          setLoading(false);
          window.location.assign(`${process.env.PUBLIC_URL}/change-success`)
        }
      } catch (error) {
        setLoading(false);
        setError({
          show: true,
          message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ',
        });
        setCheckForm(false)
      }
    }
  };

  const onChangePassword = async (e) => {
    setCheckInputPassword(e.target.value)
  };

  const onChangeConfirmPassword = async (e) => {
    if (checkInputPassword) {
      setCheckInputConfirmPassword(e.target.value)
    }
  };

  const watchAllFields = watch();

  useEffect(() => {
  }, [])

  return (
    <div className="w-full justify-center">
      {loading === true && (
        <div className="opacity-100 fixed inset-0 overflow-y-auto z-50">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="opacity-100 translate-y-0 sm:scale-100 fixed inset-0 transition-opacity"
              aria-hidden="true"
            >
              <div className="absolute inset-0 bg-gray-100 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
              &#8203;
            </span>
            <div
              className="inline-block align-bottom rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden transform transition-all sm:my-8 sm:align-middle sm:max-w-sm  sm:p-6"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <img src={loadings} className="animate-spin" alt="load" />
            </div>
          </div>
        </div>
      )}
      <div className="mt-8">
        {err.show && (
          <div className="flex bg-red-200 px-4 py-2 rounded-md mb-4">
            <img src={bell} alt="bell" />
            <p className="ml-2 font-normal text-rose-600">{err.message}</p>
          </div>
        )}
        <div className="text-gray-700 text-3xl flex justify-center font-semibold">
          สร้างรหัสผ่านใหม่
        </div>
        <div className="mt-6 text-GrayMEA-600 text-lg font-normal flex flex-col items-center">
          <p>
            บัญชีผู้ใช้
          </p>
          <p>
            {email && email}
          </p>
        </div>
        <div className="mt-6 text-GrayMEA-500 text-lg font-light">
          เลือกรหัสผ่านที่รัดกุมและไม่นำรหัสผ่านนี้ไปใช้ซ้ำกับบัญชีอื่นๆ
        </div>
        <form onSubmit={handleSubmit(onSubmit)} className="mt-2 w-full">
          <div className="mt-5">
            <label className="block font-semibold text-gray-800 ">รหัสผ่าน</label>
            <div className="text-GrayMEA-500 font-normal my-2">
              รหัสผ่านจะต้องมีอย่างน้อย 8 ตัวขึ้นไป ซึ่งประกอบด้วยตัว
              อักษรพิมพ์ใหญ่และพิมพ์เล็กอย่างน้อย 1 ตัวและมีตัวเลข
              อย่างน้อย 1 ตัว
            </div>
            <div className="mt-1 flex justify-end items-center">
              <input
                type={showPassword ? 'text' : 'password'}
                name="password"
                onChange={(e) => onChangePassword(e)}
                onClick={() => setError(false)}
                className="shadow-sm  text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
                placeholder="กรอกรหัสผ่านจำนวนตั้งแต่ 8 ตัวขึ้นไป"
                ref={register}
              />
              {watchAllFields.password && (
                <div className="absolute mr-2">
                  <button type="button" onClick={() => onClickShowPassword('password')}>
                    <img alt="eye" src={showPassword ? eye : eyeOff} />
                  </button>
                </div>
              )}
            </div>
            <div className="text-sm mt-2">
              <ErrorMessage
                errors={errors}
                name="password"
                render={({ messages }) =>
                  messages &&
                  Object.entries(messages).map(([type, message]) => (
                    <div className="text-red-600 text-sm " key={type}>
                      {message.length === 1 ? (
                        <div className="flex">
                          <img alt="x" src={X} className="mr-2" />
                          {message}
                        </div>
                      ) : (
                        message.map((v) => (
                          <div className="flex">
                            <img alt="x" src={X} className="mr-2" />
                            <p>{v}</p>
                          </div>
                        ))
                      )}
                    </div>
                  ))
                }
              />
            </div>
          </div>
          <div className="mt-5">
            <label className="block font-semibold text-gray-800">ยืนยันรหัสผ่าน</label>
            <div className="mt-1 flex justify-end items-center">
              <input
                type={showConfirmPassword ? 'text' : 'password'}
                onChange={(e) => onChangeConfirmPassword(e)}
                onClick={() => setError(false)}
                name="confirmPassword"
                className="shadow-sm  text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
                placeholder="กรอกรหัสผ่านอีกครั้ง"
                ref={register}
              />
              {watchAllFields.confirmPassword && (
                <div className="absolute mr-2">
                  <button type="button" onClick={() => onClickShowPassword('confirmPassword')}>
                    <img alt="eye" src={showConfirmPassword ? eye : eyeOff} />
                  </button>
                </div>
              )}
            </div>
            <div className="text-sm mt-2">
              <ErrorMessage
                errors={errors}
                name="confirmPassword"
                render={({ messages }) =>
                  messages &&
                  Object.entries(messages).map(([type, message]) => (
                    <div className="text-red-600 text-sm flex" key={type}>
                      <img alt="x" src={X} className="mr-2" />
                      {message}
                    </div>
                  ))
                }
              />
              {checkInputConfirmPassword && Object.keys(errors).length === 0 && <div className="text-green-600 text-sm flex">
                <img alt="check" src={check} className="mr-2" />
                รหัสผ่านตรงกัน
              </div>}
            </div>
          </div>
          <div className="mt-6">
            <button
              onClick={() => setCheckForm(true)}
              type="submit"
              className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none"
            >
              บันทึกรหัสผ่าน
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default withRouter(ChangePasswordForm);
