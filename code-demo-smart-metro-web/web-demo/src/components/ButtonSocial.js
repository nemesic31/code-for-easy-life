import React, { useEffect } from 'react';
import logo_google from '../resource/logo_google.svg';
import logo_facebook from '../resource/logo_facebook.svg';
import logo_apple from '../resource/logo_apple.svg';
import { withRouter } from 'react-router-dom';


import { startAuthenticate } from '../api/Social.script'
import apiconfig from '../api/config.json';


const ButtonSocial = (props) => {
  const pathName = window.location.pathname;


  const handleGoogleLogin = async () => {
    try {
      const data = 'google'
      if (pathName === '/oauth/authorize') {
        const client_id = sessionStorage.getItem("client_id")
        const scope = sessionStorage.getItem("scope")
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      } else {
        const client_id = apiconfig.mea_client_id
        const scope = apiconfig.mea_scope
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)

          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
  const handleFacebookLogin = async () => {
    try {
      const data = 'facebook'
      if (pathName === '/oauth/authorize') {
        const client_id = sessionStorage.getItem("client_id")
        const scope = sessionStorage.getItem("scope")
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      } else {
        const client_id = apiconfig.mea_client_id
        const scope = apiconfig.mea_scope
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  const handleAppleLogin = async () => {
    try {
      const data = 'apple'
      if (pathName === '/oauth/authorize') {
        const client_id = sessionStorage.getItem("client_id")
        const scope = sessionStorage.getItem("scope")
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      } else {
        const client_id = apiconfig.mea_client_id
        const scope = apiconfig.mea_scope
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
  }, [])

  return (
    <div>
      <div className="mt-6">
        <button
          type="submit"
          onClick={() => handleGoogleLogin()}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium border-gray-600 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <img src={logo_google} alt="logo_google" className="mr-3" />
          <div className="w-3/5">ดำเนินการต่อด้วย Google</div>
        </button>
      </div>
      <div className="mt-3">
        <button
          type="submit"
          onClick={() => handleFacebookLogin()}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium border-gray-600 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <img src={logo_facebook} alt="logo_facebook" className="mr-3" />

          <div className="w-3/5">ดำเนินการต่อด้วย facebook</div>
        </button>
      </div>
      <div className="mt-3">
        <button
          type="submit"
          onClick={() => handleAppleLogin()}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium border-gray-600 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <img src={logo_apple} alt="logo_apple" className="mr-3" />
          <div className="w-3/5">ดำเนินการต่อด้วย Apple</div>
        </button>
      </div>
    </div>
  );
}

export default withRouter(ButtonSocial);
