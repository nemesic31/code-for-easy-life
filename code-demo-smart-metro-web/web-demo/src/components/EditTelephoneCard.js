import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { withRouter } from 'react-router-dom';

import { ErrorMessage } from '@hookform/error-message';
import { telSchema } from '../schema';
import { yupResolver } from '../resolver';
import loadings from '../resource/icon/loading.png';


import { updateTelephone } from '../api/User.script';
import { reset as resetPasswd } from '../api/ResetPassword.script';
import bell from '../resource/icon/bell_red.svg';
import X from '../resource/icon/x.svg';
import { createOtp } from '../api/Otp.script';

const EditTelephoneCard = (props) => {
  const [loading, setLoading] = useState(false);
  const [checkForm, setCheckForm] = useState(false);
  const [err, setError] = useState({ show: false, message: '' });;



  const { register, errors, handleSubmit, setValue, formState } = useForm({
    criteriaMode: 'all',
    reValidateMode: 'all',
    mode: 'all',
    resolver: yupResolver(telSchema, { abortEarly: false }),
  });


  const onSubmit = async (values) => {
    const { userRefId, email } = props.location.state
    setError({
      show: false,
      message: '',
    });
    setLoading(true);
    try {
      const { username } = values;
      const phone = username
      const response = await updateTelephone(userRefId, phone)
      if (typeof response) {
        const otp_type = 'MOBILE'
        const responseOTP = await createOtp(userRefId, otp_type)
        
        if (responseOTP.ref) {
          const { ref } = responseOTP
          const location = {
            pathname: `${process.env.PUBLIC_URL}/otp`,
            state: { userRefId, email, phone, ref, otp_type },
          };
          props.history.push(location);
        }
      } else {
        if (response.code === 'MEA0003') {
          setError({
            show: true,
            message: 'การยืนยันล้มเหลว',
          });
          console.log(response.errorDescription);
        } else {
          setError({
            show: true,
            message: 'ไม่สามารถดำเนินการต่อได้ โปรดติดต่อผู้ให้บริการ',
          });
        }
        setLoading(false);
      }
    } catch (error) {
      
      console.log(error);
      setError({
        show: true,
        message: 'ไม่สามารถดำเนินการต่อได้ โปรดติดต่อผู้ให้บริการ',
      });
      setLoading(false);
    }

  };

  useEffect(() => {
  }, [])

  const onKeyPress = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }
  };

  const onChange = (e) => {
    const { value, maxLength } = e.target;
    if (value.length > maxLength) {
      e.target.value = e.target.value.slice(0, maxLength);
    }
  };

  return (
    <div className="mt-8 flex flex-col items-center w-full">
      {loading === true && (
        <div className="opacity-100 fixed inset-0 overflow-y-auto z-50">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="opacity-100 translate-y-0 sm:scale-100 fixed inset-0 transition-opacity"
              aria-hidden="true"
            >
              <div className="absolute inset-0 bg-gray-100 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
              &#8203;
            </span>
            <div
              className="inline-block align-bottom rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden transform transition-all sm:my-8 sm:align-middle sm:max-w-sm  sm:p-6"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <img src={loadings} className="animate-spin" alt="load" />
            </div>
          </div>
        </div>
      )}
      {err.show && (
        <div className="flex w-max">
          <div className="absolute px-4 py-3">
            <img src={bell} alt="bell" />
          </div>
          <div className="flex bg-red-200 px-8 py-2 rounded-md mb-4 justify-center w-max">
            <p className="ml-2 font-normal text-rose-600">{err.message}</p>
          </div>
        </div>
      )}
      <div className="flex justify-center">
        <p className="text-GrayMEA-700 font-semibold text-lg">ระบุหมายเลขโทรศัพท์</p>
      </div>
      <form onSubmit={handleSubmit(onSubmit)} className="mt-7 w-full">
        <div>
          <label className="block font-semibold text-gray-800 ">หมายเลขโทรศัพท์</label>
          <div className="mt-1">
            <input
              type="number"
              onChange={(e) => onChange(e)}
              onKeyPress={(event) => onKeyPress(event)}
              maxLength={10}
              name="username"
              id="username"
              onClick={() => setError({
                show: false,
                message: '',
              })}
              className="shadow-sm text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
              placeholder="หมายเลขโทรศัพท์"
              ref={register}
            />
          </div>
          <div className="text-sm mt-2">
            <ErrorMessage
              errors={errors}
              name="username"
              render={({ messages }) =>
                messages &&
                Object.entries(messages).map(([type, message]) => (
                  <div className="text-red-600 text-sm flex" key={type}>
                    <img alt="x" src={X} className="mr-2" />
                    {message}
                  </div>
                ))
              }
            />
          </div>
        </div>
        <div className="mt-6">
          <button
            onClick={() => setCheckForm(true)}
            type="submit"
            className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none"
          >
            ต่อไป
          </button>

        </div>

      </form>
    </div>
  );
};

export default withRouter(EditTelephoneCard);
