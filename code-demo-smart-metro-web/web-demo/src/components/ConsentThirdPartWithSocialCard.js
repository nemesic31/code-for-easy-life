import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { verifyAccountThirdPartyWithSocial, getConsentThirdParty } from '../api/ThirdParty.script';
import apiconfig from '../api/config.json';

const ConsentThirdPartWithSocialCard = (props) => {
  const client_id = sessionStorage.getItem("client_id")
  const scope = sessionStorage.getItem("scope")
  const client_redirect_uri = sessionStorage.getItem("redirect_uri")
  const redirect_uri = apiconfig.mea_redirect_uri
  const state = sessionStorage.getItem("state")
  const [appConsent, setAppConsent] = useState()

  const getConsent = async () => {
    try {
      const response = await getConsentThirdParty(client_id)
      setAppConsent(response.data)
    } catch (error) {
      console.log(error);
    }
  }

  const handleLoginWithThirdParty = async () => {
    const { code } = props.location.state
    try {
      if (code) {
        const url = `${client_redirect_uri}?code=${code}&state=${state}`
        window.location.assign(url)
      } else {
        console.log('error');
      }
    } catch (error) {
      console.log(error);
    }

  }

  useEffect(() => {
    getConsent()
  }, []);


  return (
    <div className="w-full justify-center">
      <div className="flex">
        <div className="mt-2">
          <div className="text-gray-700 text-3xl flex justify-center font-semibold">
            ขออนุญาตเข้าถึงข้อมูล
          </div>
          <div className="flex justify-center w-full">
            <div className="h-96 w-96 overflow-y-scroll mt-4 p-4 border-GrayMEA-400 border-2">
              <p className="font-light">
                {appConsent && appConsent.name} <br />
                {appConsent && appConsent.base_url}<br />
                <br />
                แอปพลิเคชันนี้ต้องการข้อมูลดังต่อไปนี้<br />
                {appConsent ? Object.keys(appConsent.default_client_scopes).map((e) => {
                  return (<div>- {e}</div>)
                }) : null}
                {appConsent && scope.split(' ').map((sc) => {
                  if (typeof appConsent.optional_client_scopes[sc] !== 'undefined') {
                    return <div>- {appConsent.optional_client_scopes[sc]}</div>
                  }
                })}
              </p>
            </div>
          </div>

          <div className="flex flex-col justify-center items-center mt-5">
            <button
              onClick={() => handleLoginWithThirdParty()}
              className="w-full flex justify-center py-3 px-4 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none disabled:opacity-50"
            >
              ยอมรับและดำเนินการต่อ
              </button>
            <Link to={`${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}`} className="text-OrangeMEA-500 font-semibold text-base mt-3">
              ยกเลิก
              </Link>
          </div>

        </div>
      </div>
    </div>
  );
};

export default withRouter(ConsentThirdPartWithSocialCard);
