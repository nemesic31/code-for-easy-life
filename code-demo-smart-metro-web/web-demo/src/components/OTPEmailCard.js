import React, { useState, useEffect } from 'react';
import OtpInput from 'react-otp-input';
import { withRouter } from 'react-router-dom';
import useInterval from '@use-it/interval';


import { createOtp, verifyOtp } from '../api/Otp.script';

import bell from '../resource/icon/bell.svg';
import bellRed from '../resource/icon/bell_red.svg';
import loadings from '../resource/icon/loading.png';
import mail from '../resource/icon/mail.svg';

const OTPEmailCard = (props) => {
  const { email, userRefId, phone, ref, otp_type } = props.location.state;
  const [otp, setOtp] = useState('');
  const [resend, setResend] = useState(false);
  const [times, setTimes] = useState(180);
  const [loading, setLoading] = useState(false);
  const [value, setError] = useState({ show: false, message: '' });
  const [refNew, setRefNew] = useState()

  

  const getOTP = async () => {
    try {
      const responseOTP = await createOtp(userRefId, otp_type)
      if (responseOTP.ref) {
        setError({ show: false, message: '' });
        setRefNew(responseOTP.ref)
      } else {
        setError({ show: true, message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ' });
      }
    } catch (error) {
      setError({ show: true, message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ' });
    }
  };

  const sendOTP = async (value) => {
    try {
      setLoading(true);
      const code = value
      const refs = refNew ? refNew : ref
      const response = await verifyOtp(code, refs, userRefId, otp_type)
      if (response.errorDescription) {
        setLoading(false);
        setOtp('');
        setError({ show: true, message: 'รหัสยืนยันผิดพลาด กรุณาตรวจสอบใหม่อีกครั้ง' });
      } else {
        const otp_type = 'MOBILE'
        const responseOTP = await createOtp(userRefId, otp_type)
        if (responseOTP.ref) {
          const { ref } = responseOTP
          const location = {
            pathname: `${process.env.PUBLIC_URL}/otp`,
            state: { phone, userRefId, ref, otp_type },
          };
          props.history.push(location);
        }
      }
    } catch (error) {
      setLoading(false);
      setError({ show: true, message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ' });
    }
  };

  const checkOTP = (value) => {
    sendOTP(value);
  };

  const resendOTP = (seconds) => {
    setTimes(seconds);
    setResend(true);
    setOtp('');
    getOTP();

  };

  useInterval(() => {
    if (times === 0) {
      resetInterval();
    }
    setTimes((currentCount) => currentCount - 1);
  }, 1000);

  const resetInterval = () => {
    setResend(false);
    setTimes(0);
  };

  useEffect(() => {
  }, []);

  return (
    <div className="w-max flex justify-center">
      {loading === true && (
        <div className="opacity-100 fixed inset-0 overflow-y-auto z-50">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="opacity-100 translate-y-0 sm:scale-100 fixed inset-0 transition-opacity"
              aria-hidden="true"
            >
              <div className="absolute inset-0 bg-gray-100 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
              &#8203;
            </span>
            <div
              className="inline-block align-bottom rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden transform transition-all sm:my-8 sm:align-middle sm:max-w-sm  sm:p-6"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <img src={loadings} className="animate-spin" alt="load" />
            </div>
          </div>
        </div>
      )}
      <div className="flex flex-col justify-center items-center mt-8 z-30">
        {value.show ? (
          <div className="flex bg-red-200 px-4 py-2 rounded-md mb-4">
            <img src={bellRed} alt="bell" />
            <p className="ml-2 font-normal text-rose-600">{value.message}</p>
          </div>
        ) : resend === true && (
          <div className="flex bg-blue-100 px-4 py-2 rounded-md mb-4">
            {' '}
            <img src={bell} alt="bell" />{' '}
            <p className="ml-2 text-lg font-normal text-blue-600">ตรวจสอบ OTP ของคุณ</p>
          </div>
        )
        }
        <div className="text-gray-700 text-3xl flex justify-center font-semibold">
          <img src={mail} alt="phone" className="mx-2" />
          <p>ยืนยันอีเมล</p>
        </div>
        <div className="mt-6 text-GrayMEA-500 text-lg font-light">REF {refNew ? refNew : ref}</div>
        <div className="mt-6 text-GrayMEA-500 text-lg font-light">
          กรอกรหัส OTP ที่ถูกส่งไปยังอีเมล {email && email}
        </div>
        <div className="mt-8 justify-center">
          <OtpInput
            value={otp}
            onChange={(v) => {
              setOtp(v);
              if (v.length === 6) {
                checkOTP(v);
              }
            }}
            numInputs={6}
            separator={<span className="text-white">-</span>}
            inputStyle={{
              width: '45px',
              borderColor: '#C9CED2',
              borderWidth: '1px',
              borderRadius: '0.375rem',
              color: '#F26D23',
            }}
          />
        </div>
        <div className="flex mt-5">
          <p className=" text-GrayMEA-600 text-lg font-light mr-2">
            {resend === false ? 'ไม่ได้รับรหัส 6 ตัว ?' : 'สามารถส่งได้อีกครั้งเมื่อ'}
          </p>
          {resend === false ? (
            <button
              className=" text-GrayMEA-600 text-lg font-semibold underline"
              onClick={() => resendOTP(180)}
            >
              ส่งรหัสอีกครั้ง
            </button>
          ) : (
            <p className="text-GrayMEA-600 text-lg font-semibold underline">
              {times / 60 < 10 ? `0${Math.floor(times / 60)}` : Math.floor(times / 60)} :{' '}
              {times % 60 < 10 ? `0${times % 60}` : times % 60}
            </p>
          )}
        </div>
        
      </div>
    </div>
  );
};


export default withRouter(OTPEmailCard);

