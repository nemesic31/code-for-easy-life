import React, { useEffect } from 'react';
import logo_google from '../resource/logo_google.svg';
import logo_facebook from '../resource/logo_facebook.svg';
import logo_apple from '../resource/logo_apple.svg';
import { Link, withRouter } from 'react-router-dom';
import { startAuthenticate } from '../api/Social.script'
import apiconfig from '../api/config.json';

const RegisterEmailSameSocialCard = (props) => {
  const pathName = window.location.pathname;
  const client_id = sessionStorage.getItem("client_id")
  const scope = sessionStorage.getItem("scope")
  const client_redirect_uri = sessionStorage.getItem("redirect_uri")
  const state = sessionStorage.getItem("state")
  const { email, socialName } = props.location.state

  const handleGoogleLogin = async () => {
    try {
      const data = 'google'
      if (pathName === '/oauth/authorize') {
        const client_id = sessionStorage.getItem("client_id")
        const scope = sessionStorage.getItem("scope")
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      } else {
        const client_id = apiconfig.mea_client_id
        const scope = apiconfig.mea_scope
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
  const handleFacebookLogin = async () => {
    try {
      const data = 'facebook'
      if (pathName === '/oauth/authorize') {
        const client_id = sessionStorage.getItem("client_id")
        const scope = sessionStorage.getItem("scope")
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      } else {
        const client_id = apiconfig.mea_client_id
        const scope = apiconfig.mea_scope
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  const handleAppleLogin = async () => {
    try {
      const data = 'apple'
      if (pathName === '/oauth/authorize') {
        const client_id = sessionStorage.getItem("client_id")
        const scope = sessionStorage.getItem("scope")
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      } else {
        const client_id = apiconfig.mea_client_id
        const scope = apiconfig.mea_scope
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (typeof response === 'string') {
          const datas = JSON.parse(response)
          window.location.assign(datas.redirectUrl)
        } else {
          console.log('error');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className="w-max justify-center flex flex-col items-center">
      <div className="mt-8">
        <div className="text-gray-700 text-3xl flex justify-center font-semibold">
          มีบัญชีผู้ใช้อยู่แล้ว
        </div>

        <div className="mt-6 text-GrayMEA-500 text-lg font-light flex justify-center">
          คุณได้สร้างบัญชีผู้ใช้โดยใช้ {socialName && socialName} แล้ว กรุณาเข้าสู่ระบบแทน
        </div>
        <div className="text-black text-base font-light my-16 flex justify-center">
          {email && email}
        </div>
      </div>

      {socialName === 'GOOGLE' && (<div className="mt-6 w-full">
        <button
          type="submit"
          onClick={() => handleGoogleLogin()}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium border-gray-600 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <img src={logo_google} alt="logo_google" className="mr-3" />
          <div className="w-3/5">ดำเนินการต่อด้วย Google</div>
        </button>
      </div>)}
      {socialName === 'FACEBOOK' && (<div className="mt-6 w-full">
        <button
          type="submit"
          onClick={() => handleFacebookLogin()}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium border-gray-600 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <img src={logo_facebook} alt="logo_facebook" className="mr-3" />

          <div className="w-3/5">ดำเนินการต่อด้วย facebook</div>
        </button>
      </div>)}
      {socialName === 'APPLE' && (<div className="mt-6 w-full">
        <button
          type="submit"
          onClick={() => handleAppleLogin()}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium border-gray-600 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <img src={logo_apple} alt="logo_apple" className="mr-3" />
          <div className="w-3/5">ดำเนินการต่อด้วย Apple</div>
        </button>
      </div>)}
      <Link to={client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}` : `${process.env.PUBLIC_URL}/`} className="text-OrangeMEA-500 font-semibold text-base mt-3">
        ยกเลิก
      </Link>
    </div>
  );
};

export default withRouter(RegisterEmailSameSocialCard);
