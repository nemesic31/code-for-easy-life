import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';

import bell from '../resource/icon/bell_red.svg';
import loadings from '../resource/icon/loading.png';

import { consents, updateConsent } from '../api/Pdpa.script';
import { reset as resetPasswd } from '../api/ResetPassword.script';

const ConsentLoginWithThirdPartyCard = (props) => {
  const [loading, setLoading] = useState(false);
  const [PDPA, setPDPA] = useState();
  const [user, setUser] = useState();
  const [isDisable, setDisable] = useState(false);
  const [values, setValues] = useState();
  const [value, setError] = useState(false);
  const client_id = sessionStorage.getItem("client_id")
  const scope = sessionStorage.getItem("scope")
  const client_redirect_uri = sessionStorage.getItem("redirect_uri")
  const state = sessionStorage.getItem("state")


  const handleLoadingWithOutPassword = async (data) => {
    setLoading(true);
    setTimeout(() => {
      const location = {
        pathname: `${process.env.PUBLIC_URL}/otp-verify-thirdparty`,
        state: { data },
      };
      props.history.push(location);
    }, 1500);
  };

  const handleLoadingWithOutTel = async (data) => {
    setLoading(true);
    setTimeout(() => {
      const location = {
        pathname: `${process.env.PUBLIC_URL}/add-phonenumber-thirdparty`,
        state: { data },
      };
      props.history.push(location);
    }, 1500);
  };

  const getPDPA = async () => {
    try {
      const dataUser = props.location.state.data.user
      const response = await consents();
      setPDPA(response.data);
      if (response.code === '0') {
        const { data } = response;
        dataUser.contacts.forEach(element => {
          if (element.name === 'email') {
            const defaultValues = {
              username: element.value,
              user_no: dataUser.user_no,
              pdpa_consents: data.map((v) => ({
                name: v.name,
                checked: v.properties.required,
              })),
            };
            setValues(defaultValues);
          } else {
            const defaultValues = {
              username: element.value,
              user_no: dataUser.user_no,
              pdpa_consents: data.map((v) => ({
                name: v.name,
                checked: v.properties.required,
              })),
            };
            setValues(defaultValues);
          }
        });


      } else {
        console.log('error');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const { handleSubmit, control } = useForm({ reValidateMode: 'onChange' });

  const checkValidate = (event) => {
    const { name, checked } = event.target;
    setValue(name, checked);
    if (PDPA !== undefined) {
      const check = PDPA.map((v) => {
        if (v.name === name) {
          if (v.properties.required) {
            if (!checked) {
              setDisable(true);
            } else {
              setDisable(false);
            }
          }
        }
      });
      return check;
    }
  };

  const setValue = (name, check) => {
    if (values) {
      const { pdpa_consents } = values;
      pdpa_consents.map((v) => {
        if (name === v.name) {
          v.checked = check;
        }
      });
    }
  };

  const handleCaseHasTel = async (event) => {
    const response = await resetPasswd(event);
    if (response.code === '0') {
      const dataUserPack = props.location.state.data
      const data = { ...response.data, values, dataUserPack }
      handleLoadingWithOutPassword(data);
    } else {
      setError(true);
      setLoading(false);
    }
  }

  const onSubmit = async () => {
    setLoading(true);
    try {

      const { pdpa_consents } = values
      const id = user.user_id
      const response = await updateConsent(id, pdpa_consents);
      if (response.code === '0') {
        user.contacts.forEach(e => {
          if (user.contacts.length > 1) {
            if (e.name === 'phone') {
              handleCaseHasTel(values.username)
            }
          } else {
            const { data } = props.location.state
            handleLoadingWithOutTel(data);
          }
        });
      } else {
        setError(true)
        setLoading(false);
        console.log('error');
      }
    }
    catch (error) {
      console.log(error);
      setError(true)
      setLoading(false);
    }
  };



  useEffect(() => {
    getPDPA();
    setUser(props.location.state.data.user);
  }, []);


  return (
    <div className="w-full justify-center">
      {loading === true && (
        <div className="opacity-100 fixed inset-0 overflow-y-auto z-50">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="opacity-100 translate-y-0 sm:scale-100 fixed inset-0 transition-opacity"
              aria-hidden="true"
            >
              <div className="absolute inset-0 bg-gray-100 opacity-75"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
              &#8203;
            </span>
            <div
              className="inline-block align-bottom rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden transform transition-all sm:my-8 sm:align-middle sm:max-w-sm  sm:p-6"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <img src={loadings} className="animate-spin" alt="load" />
            </div>
          </div>
        </div>
      )}
      {value && (
        <div className="flex bg-red-200 px-4 py-2 rounded-md mb-4 mt-4 w-full">
          <img src={bell} alt="bell" />
          <p className="ml-2 font-normal text-rose-600">เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ</p>
        </div>
      )}
      <div className="flex">
        <div className="mt-2">
          <div className="text-gray-700 text-3xl flex justify-center font-semibold">
            เงื่อนไขการใช้งาน
          </div>
          <div className="flex justify-center w-full">
            <div className="h-96 w-96 overflow-y-scroll mt-4 p-4 border-GrayMEA-400 border-2">
              <p className="font-light">
                {PDPA !== undefined ? PDPA.map((v) => (
                  <div className="flex flex-col justify-between" key={v.name}>
                    {v.detail && v.detail.split('<br/>').map((item, key) => {
                      return <span key={key} >{item.split('<br/>').map((items, key) => {
                        return <span key={key} className={items === 'ยอมรับการให้ข้อมูลสำหรับใช้บริการ'
                          ? "font-semibold text-xl" : items === ' คำจำกัดความ '
                            ? "font-semibold ml-1" : items === 'ข้อมูลที่เราเก็บรวบรวม'
                              ? "font-semibold ml-1" : items === 'ยอมรับข้อมูลสำหรับพัฒนาบริการ'
                                ? "font-semibold text-xl" : items === 'ยอมรับการให้ข้อมูลสำหรับการตลาด'
                                  ? "font-semibold text-xl" : "ml-3"} >{items}</span>
                      })}<br /></span>
                    })}
                  </div>
                ))
                  : null}
              </p>
            </div>
          </div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mt-2">
              {PDPA !== undefined
                ? PDPA.map((v) => (
                  <div className="flex items-center justify-between" key={v.name}>
                    <Controller
                      control={control}
                      name={v.name}
                      render={({ name }) => {
                        return (
                          <div className="flex items-center">
                            <input
                              type="checkbox"
                              name={name}
                              defaultChecked={v.properties.required}
                              className="text-OrangeMEA-500 checked:bg-OrangeMEA-500 checked:border-transparent border-gray-300 rounded"
                              onChange={(e) => checkValidate(e)}
                            />
                            <label className="ml-2 text-GrayMEA-500 text-lg font-light">
                              {v.value}
                            </label>
                          </div>
                        );
                      }}
                    />
                  </div>
                ))
                : null}
            </div>
            <div className="flex flex-col justify-center items-center mt-5">
              <button
                type="submit"
                className="w-full flex justify-center py-3 px-4 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none disabled:opacity-50"
                disabled={isDisable}
              >
                ยอมรับและดำเนินการต่อ
              </button>
              <Link to={`${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}`} className="text-OrangeMEA-500 font-semibold text-base mt-3">
                ยกเลิก
              </Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ConsentLoginWithThirdPartyCard);
