import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { withRouter } from 'react-router-dom';
import { ErrorMessage } from '@hookform/error-message';
import { objectSchema } from '../schema';
import { yupResolver } from '../resolver';
import { register as registerAccount } from '../api/User.script';
import { checkUsername } from '../api/User.script';
import auth from '../utils/auth';

import eye from '../resource/icon/eye.svg';
import eyeOff from '../resource/icon/eye-off.svg';
import X from '../resource/icon/x.svg';
import check from '../resource/icon/check.svg';
import bell from '../resource/icon/bell_red.svg';
import bluebell from '../resource/icon/bluebell.svg';


const FormRegister = (props) => {
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [checkForm, setCheckForm] = useState(false);
  const [err, setError] = useState({ show: false, message: '' });
  const [checkInputConfirmPassword, setCheckInputConfirmPassword] = useState();
  const [checkInputPassword, setCheckInputPassword] = useState()
  const [colorErr, setColorErr] = useState('red')

  const onClickShowPassword = (type) => {
    type === 'password'
      ? setShowPassword(!showPassword)
      : setShowConfirmPassword(!showConfirmPassword);
  };

  const { register, errors, handleSubmit, watch, reset } = useForm({
    criteriaMode: 'all',
    reValidateMode: 'all',
    mode: 'all',
    resolver: yupResolver(objectSchema, { abortEarly: false }),
  });

  const onSubmit = async (values) => {

    const { phone, email, password, confirmPassword } = values;
    try {
      if (checkForm === true) {
        setError({
          show: false,
          message: '',
        });
        const response = await registerAccount(email, phone, password, confirmPassword)
        if (typeof response === 'string') {
          const responseCheck = await checkUsername(email)
          if (responseCheck.userRefId) {
            const { userRefId } = responseCheck
            const location = {
              pathname: `${process.env.PUBLIC_URL}/consent`,
              state: { userRefId, email, phone },
            };
            props.history.push(location);
          } else {
            setCheckForm(false)
            setColorErr('red')
            setError({
              show: true,
              message: 'ลงทะเบียนไม่สำเร็จ กรุณาติดต่อผู้ให้บริการ',
            });
          }
        }
        else if (response.code === 'MEA1001') {
          const responseCheck = await checkUsername(email)
          if (responseCheck.userType === 'SOCIAL_USER') {
            if (responseCheck.socialOption.length > 0) {
              const socialName = responseCheck.socialOption[0]
              const location = {
                pathname: `${process.env.PUBLIC_URL}/register-email-same-social`,
                state: { email, socialName },
              };
              props.history.push(location);
            }
          } else {
            setColorErr('blue')
            setCheckForm(false)
            setError({
              show: true,
              message: 'อีเมลนี้ลงทะเบียนแล้ว กรุณาเข้าสู่ระบบ',
            });
          }
        } else {
          setCheckForm(false)
          setColorErr('red')
          setError({
            show: true,
            message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ',
          });
        }
      }
    } catch (error) {
      setCheckForm(false)
      console.log(error);
      setColorErr('red')
      setError({
        show: true,
        message: 'เกิดข้อผิดพลาด กรุณาติดต่อผู้ให้บริการ',
      });
    }

  };

  const watchAllFields = watch();

  const onChange = (e) => {
    const { value, maxLength } = e.target;
    if (value.length > maxLength) {
      e.target.value = e.target.value.slice(0, maxLength);
    }
  };

  const onKeyPress = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }
  };

  const onChangePassword = async (e) => {
    setCheckInputPassword(e.target.value)
  };

  const onChangeConfirmPassword = async (e) => {
    if (checkInputPassword) {
      setCheckInputConfirmPassword(e.target.value)
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="mt-8 w-full">
      {err.show && (
        <div className={`flex ${colorErr === 'red' ? 'bg-red-200' : 'bg-indigo-200'} px-4 py-2 rounded-md mb-4`}>
          <img src={colorErr === 'red' ? bell : bluebell} alt="bell" />
          <p className={`ml-2 font-normal ${colorErr === 'red' ? 'text-rose-600' : 'text-indigo-600'}`}>{err.message}</p>
        </div>
      )}
      <div className="flex justify-center">
        <p className="text-GrayMEA-700 font-semibold text-lg">ลงทะเบียนด้วยอีเมล</p>
      </div>
      <div className="mt-8">
        <label className="block font-semibold text-gray-800 ">หมายเลขโทรศัพท์</label>
        <div className="mt-1">
          <input
            type="number"
            onChange={(e) => onChange(e)}
            maxLength={10}
            name="phone"
            onKeyPress={(event) => onKeyPress(event)}
            onClick={() => setError(false)}
            className="shadow-sm  text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
            placeholder="หมายเลขโทรศัพท์ 10 ตัว"
            ref={register}
          />
        </div>
        <div className="text-sm mt-2">
          <ErrorMessage
            errors={errors}
            name="phone"
            render={({ messages }) =>
              messages &&
              Object.entries(messages).map(([type, message]) => (
                <div className="text-red-600 text-sm flex" key={type}>
                  <img alt="x" src={X} className="mr-2" />
                  {message}
                </div>
              ))
            }
          />
        </div>
      </div>
      <div className="mt-4">
        <label className="block font-semibold text-gray-800 ">อีเมล</label>
        <div className="mt-1">
          <input
            type="text"
            name="email"
            id="email"
            onClick={() => setError(false)}
            className="shadow-sm text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
            placeholder="example@example.com"
            ref={register}
          />
        </div>
        <div className="text-sm mt-2">
          <ErrorMessage
            errors={errors}
            name="email"
            render={({ messages }) =>
              messages &&
              Object.entries(messages).map(([type, message]) => (
                <div className="text-red-600 text-sm flex" key={type}>
                  <img alt="x" src={X} className="mr-2" />
                  {message}
                </div>
              ))
            }
          />
        </div>
      </div>
      <div className="mt-5">
        <label className="block font-semibold text-gray-800 ">รหัสผ่าน</label>
        <div className="text-GrayMEA-500 font-normal my-2">
          {checkForm === false && 'รหัสผ่านจะต้องมีอย่างน้อย 8 ตัวขึ้นไป ซึ่งประกอบด้วยตัว อักษรพิมพ์ใหญ่และพิมพ์เล็กอย่างน้อย 1 ตัวและมีตัวเลขอย่างน้อย 1 ตัว'}
        </div>
        <div className="mt-1 flex justify-end items-center">
          <input
            type={showPassword ? 'text' : 'password'}
            name="password"
            onChange={(e) => onChangePassword(e)}
            onClick={() => setError(false)}
            className="shadow-sm  text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
            placeholder="กรอกรหัสผ่านจำนวนตั้งแต่ 8 ตัวขึ้นไป"
            ref={register}
          />
          {watchAllFields.password && (
            <div className="absolute mr-2">
              <button type="button" onClick={() => onClickShowPassword('password')}>
                <img alt="eye" src={showPassword ? eye : eyeOff} />
              </button>
            </div>
          )}
        </div>
        <div className="text-sm mt-2">
          <ErrorMessage
            errors={errors}
            name="password"
            render={({ messages }) =>
              messages &&
              Object.entries(messages).map(([type, message]) => (
                <div className="text-red-600 text-sm " key={type}>
                  {message.length === 1 ? (
                    <div className="flex">
                      <img alt="x" src={X} className="mr-2" />
                      {message}
                    </div>
                  ) : (
                    message.map((v) => (
                      <div className="flex">
                        <img alt="x" src={X} className="mr-2" />
                        <p>{v}</p>
                      </div>
                    ))
                  )}
                </div>
              ))
            }
          />
        </div>
      </div>
      <div className="mt-5">
        <label className="block font-semibold text-gray-800">ยืนยันรหัสผ่าน</label>
        <div className="mt-1 flex justify-end items-center">
          <input
            type={showConfirmPassword ? 'text' : 'password'}
            onClick={() => setError(false)}
            onChange={(e) => onChangeConfirmPassword(e)}
            name="confirmPassword"
            className="shadow-sm  text-sm block w-full h-10 sm:text-sm border-gray-300 rounded-md p-3 border"
            placeholder="กรอกรหัสผ่านอีกครั้ง"
            ref={register}
          />
          {watchAllFields.confirmPassword && (
            <div className="absolute mr-2">
              <button type="button" onClick={() => onClickShowPassword('confirmPassword')}>
                <img alt="eye" src={showConfirmPassword ? eye : eyeOff} />
              </button>
            </div>
          )}
        </div>
        <div className="text-sm mt-2">
          <ErrorMessage
            errors={errors}
            name="confirmPassword"
            render={({ messages }) =>
              messages &&
              Object.entries(messages).map(([type, message]) => (
                <div className="text-red-600 text-sm flex" key={type}>
                  <img alt="x" src={X} className="mr-2" />
                  {message}
                </div>
              ))
            }
          />
          {checkInputConfirmPassword && Object.keys(errors).length === 0 && <div className="text-green-600 text-sm flex">
            <img alt="check" src={check} className="mr-2" />
                รหัสผ่านตรงกัน
              </div>}
        </div>
      </div>
      <div className="mt-6">
        <button
          onClick={() => setCheckForm(true)}
          type="submit"
          className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none"
        >
          สร้างบัญชีผู้ใช้
        </button>
      </div>
    </form>
  );
};

export default withRouter(FormRegister);
