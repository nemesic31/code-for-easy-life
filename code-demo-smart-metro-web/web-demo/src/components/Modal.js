import React from 'react';
import PropTypes from 'prop-types';

export const ModalRegisterSuccess = ({ css, onClickgLogin }) => (
  <div
    className={`${css} inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:px-6 sm:py-8`}
    role="dialog"
    aria-modal="true"
    aria-labelledby="modal-headline"
  >
    <div>
      <div className="mx-auto flex items-center justify-center h-16 w-16 rounded-full bg-green-500">
        <svg
          className="h-10 w-10 text-green-100"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          aria-hidden="true"
        >
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7" />
        </svg>
      </div>
      <div className="mt-3 text-center sm:mt-5">
        <h3 className="text-lg leading-6 font-medium text-gray-900" id="modal-headline">
          ลงทะเบียนสำเร็จ
        </h3>
      </div>
    </div>
    <div className="mt-5 sm:mt-6">
      <button
        type="button"
        onClick={() => onClickgLogin()}
        className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none mt-5"
      >
        กลับไปหน้าเข้าสู่ระบบ
      </button>
    </div>
  </div>
);

ModalRegisterSuccess.propTypes = {
  css: PropTypes.string,
};

ModalRegisterSuccess.defaultProps = {
  css: 'opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95',
};
