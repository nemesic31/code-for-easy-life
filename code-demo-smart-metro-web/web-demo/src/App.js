import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { AppRoute } from './AppRoute';

import LogoLayout from './layout/LogoLayout';

import HomePage from './pages/HomePage';
import RegisterPage from './pages/RegisterPage';
import ConsentPage from './pages/ConsentPage';
import ConsentLogInPage from './pages/ConsentLogInPage';
import ConsentLoginWithSocialPage from './pages/ConsentLoginWithSocialPage';
import ConsentLoginWithThirdPartyPage from './pages/ConsentLoginWithThirdPartyPage';
import ConsentThirdPartyWithSocialPage from './pages/ConsentThirdPartWithSocialPage';
import ConsentThirdPartyPage from './pages/ConsentThirdPartyPage';
import OTPVerifyLogInPage from './pages/OTPVerifyLogInPage';
import OTPVerifyEmailLogInPage from './pages/OTPVerifyEmailLogInPage';
import OTPPage from './pages/OTPPage';
import OTPEmailPage from './pages/OTPEmailPage';
import OTPVerifyWithSocial from './pages/OTPVerifyWithSocialPage';
import ForgetPassPage from './pages/ForgetPassPage';
import ForgetPassOTPPage from './pages/ForgetPassOTPPage';
import NewPasswordPage from './pages/NewPasswordPage';
import ChangeSuccessPage from './pages/ChangeSuccessPage';
import ProfilePage from './pages/ProfilePage';
import UpdateTelephoneSocial from './pages/UpdateTelephoneSocial';
import UpdateTelephoneLogIn from './pages/UpdateTelephoneLogIn';
import CallBackPage from './pages/CallBackPage';
import CreatePasswordPage from './pages/CreatePasswordPage';
import RegisterEmailSameSocialPage from './pages/RegisterEmailSameSocialPage';
import EditTelephonePage from './pages/EditTelephonePage';

import auth from './utils/auth';

function App() {
  const authUserRefId = auth.getUserRefId();
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>
        <AppRoute path="/" exact component={authUserRefId ? ProfilePage : HomePage} layout={LogoLayout} />
        <AppRoute path="/oauth/authorize" exact component={HomePage} layout={LogoLayout} />
        <AppRoute path="/register" exact component={RegisterPage} layout={LogoLayout} />
        <AppRoute path="/callback" exact component={CallBackPage} layout={LogoLayout} />
        <AppRoute path="/consent" exact component={ConsentPage} layout={LogoLayout} />
        <AppRoute path="/consent-login" exact component={ConsentLogInPage} layout={LogoLayout} />
        <AppRoute path="/consent-login-social" exact component={ConsentLoginWithSocialPage} layout={LogoLayout} />
        <AppRoute path="/consent-login-thirdparty" exact component={ConsentLoginWithThirdPartyPage} layout={LogoLayout} />
        <AppRoute path="/consent-third-party" exact component={ConsentThirdPartyPage} layout={LogoLayout} />
        <AppRoute path="/consent-third-party-with-social" exact component={ConsentThirdPartyWithSocialPage} layout={LogoLayout} />
        <AppRoute path="/updateTelephone-login" exact component={UpdateTelephoneLogIn} layout={LogoLayout} />
        <AppRoute path="/updateTelephone-social" exact component={UpdateTelephoneSocial} layout={LogoLayout} />
        <AppRoute path="/editTelephone" exact component={EditTelephonePage} layout={LogoLayout} />
        <AppRoute path="/otp" exact component={OTPPage} layout={LogoLayout} />
        <AppRoute path="/otp-email" exact component={OTPEmailPage} layout={LogoLayout} />
        <AppRoute path="/otp-verify-login" exact component={OTPVerifyLogInPage} layout={LogoLayout} />
        <AppRoute path="/otp-verify-email-login" exact component={OTPVerifyEmailLogInPage} layout={LogoLayout} />
        <AppRoute path="/otp-verify-social" exact component={OTPVerifyWithSocial} layout={LogoLayout} />
        <AppRoute path="/create-password-login" exact component={CreatePasswordPage} layout={LogoLayout} />
        <AppRoute path="/register-email-same-social" exact component={RegisterEmailSameSocialPage} layout={LogoLayout} />
        <AppRoute path="/profile" exact component={ProfilePage} layout={LogoLayout} />

        <AppRoute path="/forget-password" exact component={ForgetPassPage} layout={LogoLayout} />
        <AppRoute
          path="/forget-password-otp"
          exact
          component={ForgetPassOTPPage}
          layout={LogoLayout}
        />
        <AppRoute path="/new-password" exact component={NewPasswordPage} layout={LogoLayout} />
        <AppRoute path="/change-success" exact component={ChangeSuccessPage} layout={LogoLayout} />
      </Switch>
    </Router>
  );
}

export default App;
