import fetchApi from './FetchApi.script';
import fetchApiForSocial from './FetchApiForSocial.script';
import auth from '../utils/auth';
import apiConfig from './config.json';

export const login = async (username, password) => {
  const response = await fetchApiForSocial('/auth/users/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({ username, password }),
  });
  return response.json();
};

export const logout = async () => {
  const refreshToken = auth.getRefreshToken();
  try {
    await fetchApiForSocial(`/auth/users/logout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': apiConfig.api_key
      },
      body: JSON.stringify({ refreshToken }),
    });
  } catch (error) {
    console.log(error);
  } finally {
    auth.clearToken();
    auth.clearRefreshToken();
  }
};

export const register = async (username, password, confirmPassword, gender, fullName) => {
  const response = await fetchApiForSocial('/auth/users/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      username,
      password,
      confirmPassword,
      gender,
      fullName,
    }),
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};


export const checkUsername = async (email) => {
  const response = await fetchApiForSocial('/auth/users/verify',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': apiConfig.api_key
      },
      body: JSON.stringify({ email }),
    }
  );
  return response.json();
};

export const getProfile = async (token) => {
  const response = await fetchApi(`/profile/auth/users`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
  });
  return response.json();
};
