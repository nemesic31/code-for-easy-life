import fetchApiForSocial from './FetchApiForSocial.script';
import apiConfig from './config.json';

export const createOtp = async (userRefId, otp_type) => {
  const response = await fetchApiForSocial(`/auth/otp/${otp_type}/request`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({ userRefId }),
  });
  return response.json();
};

export const verifyOtp = async (code, ref, userRefId, otp_type) => {
  const response = await fetchApiForSocial(`/auth/otp/${otp_type}/verify`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({ code, ref, userRefId }),
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};
