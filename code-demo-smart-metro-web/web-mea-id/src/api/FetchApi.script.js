import auth from '../utils/auth';
import apiConfig from './config.json';


/**
 * Requests a URL, returning a promise
 *
 * @param  {string} uri       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default function fetchApi(uri, options = {}) {

  if (!options.headers) {
    options.headers = {};
  }

  const token = auth.getToken();
  if (token) {
    options.headers = { 'Authorization': `Bearer ${token}`, ...options.headers };
  }

  return fetch(`${apiConfig.URL}${uri}`, options);
}
