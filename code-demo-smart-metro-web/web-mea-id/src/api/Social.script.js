import fetchApi from './FetchApi.script';
import fetchApiForSocial from './FetchApiForSocial.script';
import apiConfig from './config.json';
import auth from '../utils/auth';


export const startAuthenticate = async (data, clientId, scope, redirectUri) => {
  const response = await fetchApiForSocial(`/auth/social/${data}?clientId=${clientId}&redirectUri=${redirectUri}&scope=${scope}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
  });

  return response.json();

};

export const receiveToken = async (code, inOAuthFlow) => {
  const client_id = auth.getClientId()
  const client_redirect_uri = auth.getRedirectUri()
  const response = await fetchApiForSocial(`/auth/users/social-register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      code,
      clientId: `${client_id ? client_id : apiConfig.mea_client_id}`,
      secret: `${client_id ? (client_id === 'auth-service' ? apiConfig.mea_client_secret : null) : apiConfig.mea_client_secret}`,
      redirectUri: `${client_redirect_uri ? client_redirect_uri : apiConfig.mea_redirect_uri}`,
      meaIdCallbackUri: apiConfig.mea_redirect_uri,
      inOAuthFlow
    }),
  });
  return response.json();
};

export const getProfileSocial = async (userRefId) => {
  const response = await fetchApi(`/profile/users/${userRefId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
  });
  return response.json();
};

export const updateProfileSocial = async (userRefId, displayName, gender) => {
  const response = await fetchApi(`/profile/users/${userRefId}/after-social-login`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      displayName,
      gender,
    }),
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};
