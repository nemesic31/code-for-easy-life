import fetchApi from './FetchApi.script';
import fetchApiForSocial from './FetchApiForSocial.script';
import apiConfig from './config.json';

export const verifyAccountThirdParty = async (clientId, scope, username, password, redirectUri, state) => {
  const response = await fetchApiForSocial(`/auth/users/client-login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      username,
      password,
      clientId,
      redirectUri,
      scope,
      state
    }),
  });
  return response.json();
};

export const consentsThirdParty = async (clientId, scope, username, password, redirectUri, state, consentAccept) => {
  const response = await fetchApiForSocial(`/auth/users/client-login?consentAccept=${consentAccept}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify({
      username,
      password,
      clientId,
      redirectUri,
      scope,
      state
    }),
  });
  return response.json();
};

export const getConsentThirdParty = async (client_id) => {
  const response = await fetchApiForSocial(`/auth/clients/${client_id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
  })
  return response.json();
};

export const verifyAccountThirdPartyWithSocial = async (client_id, scope, code, redirect_uri) => {
  const response = await fetchApi('/users/social/code', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      client_id,
      scope,
      code,
      redirect_uri
    }),
  });
  return response.json();
};

export const getToken = async (client_id, scope, username, password) => {
  const response = await fetchApi('/users/access/token', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      client_id,
      scope,
      username,
      password,
      client_secret: apiConfig.mea_client_secret,
    }),
  });
  return response.json();
};