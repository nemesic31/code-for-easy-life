import fetchApi from './FetchApi.script';
import apiConfig from './config.json';

export const consents = async () => {
  const type = 'pdpa'
  const response = await fetchApi(`/consent/consents/${type}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
  });

  return response.json();

};

export const updateConsent = async (user_ref_id, pdpaCheck) => {
  const response = await fetchApi(`/profile/users/${user_ref_id}/consents`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': apiConfig.api_key
    },
    body: JSON.stringify(pdpaCheck),
  });
  if (response.ok) {
    return response.text();
  } else {
    return response.json();
  }
};
