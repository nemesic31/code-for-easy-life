import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import auth from './utils/auth';


import LoginPage from './pages/LoginPage'
import RegisterPage from './pages/RegisterPage'
import ConsentPage from './pages/ConsentPage'
import VerifyEmailPage from './pages/VerifyEmailPage'
import VerifySuccessPage from './pages/VerifySuccessPage'
import CallBackPage from './pages/CallBackPage'
import ErrorLoginSocialPage from './pages/ErrorLoginSocialPage'
import ForgotPasswordPage from './pages/ForgotPasswordPage'
import ForgotPasswordOTPPage from './pages/ForgotPasswordOTPPage'
import ChangePasswordPage from './pages/ChangePasswordPage'
import UpdateProfilePage from './pages/UpdateProfilePage'
import MyProfilePage from './pages/MyProfilePage'
import ConsentThirdPartyPage from './pages/ConsentThirdPartyPage'

function App() {
  const params = new URLSearchParams(window.location.search)
  const meaid_idp_action = params.get('meaid-idp-action')
  const token = auth.getToken();
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>
        <Route path="/update-profile" component={UpdateProfilePage} />
        <Route path="/change-password" component={ChangePasswordPage} />
        <Route path="/forget-password-otp" component={ForgotPasswordOTPPage} />
        <Route path="/forget-password" component={ForgotPasswordPage} />
        <Route path="/error-login-social" component={ErrorLoginSocialPage} />
        <Route path="/callback" component={CallBackPage} />
        <Route path="/consent-third-party" component={ConsentThirdPartyPage} />
        <Route path="/verify-success" component={VerifySuccessPage} />
        <Route path="/verify-email" component={VerifyEmailPage} />
        <Route path="/consent" component={ConsentPage} />
        <Route path="/register" component={RegisterPage} />
        <Route path="/oauth/authorize" exact component={meaid_idp_action === 'register' ? RegisterPage : LoginPage} />
        <Route path="/profile" component={MyProfilePage} />
        <Route path="/" component={token ? MyProfilePage : LoginPage} />
      </Switch>
    </Router>
  );
}

export default App;