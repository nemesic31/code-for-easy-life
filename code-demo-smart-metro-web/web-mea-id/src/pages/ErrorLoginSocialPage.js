import React from "react"

import errorLoginSocial from '../resource/icon/errorLoginSocial.svg';

import auth from '../utils/auth';


const ErrorLoginSocialPage = () => {
  const client_id = auth.getClientId()
  const scope = auth.getScope()
  const client_redirect_uri = auth.getRedirectUri()
  const state = auth.getState()

  const handleBackToApplication = async () => {
    window.location.assign(`${client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}&meaid-idp-action=login` : `${process.env.PUBLIC_URL}/`}`)
  }
  return (
    <div className="flex flex-col items-center  xl:px-2/6">
      <div className="w-20 h-20 justify-center flex mt-32">
        <img src={errorLoginSocial} alt="errorLoginSocial" />
      </div>
      <div className="px-4">
        <div className="mt-3 flex flex-col items-center">
          <div className="flex justify-center ">
            <p className="text-2xl font-semibold">Can’t register an account.</p>
          </div>
          <p className="mt-3 text-xl px-4 text-GrayMEA-700">
            Please make sure you allow permission
          </p>
          <p className="text-xl px-4 text-GrayMEA-700">
            to use an email for registration
          </p>
        </div>
        <button
          onClick={() => handleBackToApplication()}
          className="mt-6 w-full flex justify-center py-3 px-4 border-2 hover:text-black rounded-full shadow-sm text-xl font-semibold text-white bg-black hover:bg-white hover:border-black focus:outline-none disabled:opacity-50">
          Retry
        </button>
      </div>
    </div>
  )
}

export default ErrorLoginSocialPage