import React from "react"
import ForgotPasswordOTPCard from '../components/ForgotPasswordOTPCard'

const ForgotPasswordOTPPage = () => {
  return (
    <div className="xl:px-2/6">
      <ForgotPasswordOTPCard />
    </div>
  )
}

export default ForgotPasswordOTPPage