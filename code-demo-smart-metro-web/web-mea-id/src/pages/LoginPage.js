import React from 'react'
import {
  Link
} from "react-router-dom";

import LoginCard from '../components/LoginCard';
import ButtonSocialCard from '../components/ButtonSocialCard';

const LoginPage = () => {
  return (
    <div className="flex flex-col xl:px-2/6">
      <div className="w-full">
        <LoginCard />
        <div className="mt-5 relative ">
          <div className="absolute inset-0 flex items-center px-20" aria-hidden="true">
            <div className="w-full border-t border-GrayMEA-400 "></div>
          </div>
          <div className="relative flex justify-center text-lg font-normal">
            <span className="px-2 bg-white text-gray-600">OR</span>
          </div>
        </div>
        <ButtonSocialCard />
        <div className="flex justify-start mt-5 text-lg font-normal px-5">
          <p>Don't have any account ?</p>
          <Link to={`${process.env.PUBLIC_URL}/register`}>
            <p className="text-OrangeMEA-EV  underline ml-2">SignUp</p>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default LoginPage