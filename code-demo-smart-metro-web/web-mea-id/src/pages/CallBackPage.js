import React, { useEffect } from 'react';
import { receiveToken } from '../api/Social.script'
import auth from '../utils/auth';
import { withRouter } from 'react-router-dom';

import loadings from '../resource/icon/loading.svg';
import logo_mea from '../resource/MEA_EV_Logo.svg';
import errorLoginSocial from '../resource/icon/errorLoginSocial.svg';



const CallBackPage = (props) => {
  const params = new URLSearchParams(window.location.search)
  const error = params.get('error')
  const getToken = async () => {
    const code = params.get('code')
    try {
      const client_id = auth.getClientId()
      const scope = auth.getScope()
      const client_redirect_uri = auth.getRedirectUri()
      const state = auth.getState()

      if (client_id) {
        const userType = 'SOCIAL_USER'
        const inOAuthFlow = true
        const response = await receiveToken(code, inOAuthFlow);
        if (response.error_description || response.errorDescription) {
          console.log(response.error_description ? response.error_description : response.errorDescription);
        } else {
          const {
            code,
            requireConsent,
            requireUpdateProfile,
            userRefId } = response
          if (requireUpdateProfile) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/update-profile`,
              state: { userRefId, userType, requireConsent, code },
            };
            props.history.push(location);

          }
          else if (requireConsent) {

            const location = {
              pathname: `${process.env.PUBLIC_URL}/consent`,
              state: { userRefId, userType, code, state, client_redirect_uri },
            };
            props.history.push(location);
          }
          else {
            const url = `${client_redirect_uri}?code=${code}&state=${state}`
            window.location.assign(url)
          }
        }
      }
      else {
        const userType = 'SOCIAL_USER'
        const inOAuthFlow = false
        const response = await receiveToken(code, inOAuthFlow);
        if (response.error_description || response.errorDescription) {
          console.log(response.error_description ? response.error_description : response.errorDescription);
        } else {
          const {
            accessToken,
            refreshToken,
            requireConsent,
            requireUpdateProfile,
            userRefId } = response
          auth.setToken(accessToken, true);
          auth.setRefreshToken(refreshToken, true);
          if (requireUpdateProfile) {

            const location = {
              pathname: `${process.env.PUBLIC_URL}/update-profile`,
              state: { userRefId, userType, requireConsent },
            };
            props.history.push(location);

          }
          else if (requireConsent) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/consent`,
              state: { userRefId, userType },
            };
            props.history.push(location);
          } else {
            window.location.assign(`${process.env.PUBLIC_URL}/profile`)
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  const handleBackToApplication = async () => {
    const client_redirect_uri = auth.getRedirectUri()
    window.location.assign(`${client_redirect_uri}`)
  }

  useEffect(() => {
    getToken()
  }, [])
  return (
    <div className="flex flex-col items-center">
      {error ?
        (<div className="px-4 flex flex-col items-center">
          <div className="w-20 h-20 justify-center flex mt-32">
            <img src={errorLoginSocial} alt="errorLoginSocial" />
          </div>
          <div className="mt-3 flex flex-col items-center">
            <div className="flex justify-center ">
              <p className="text-2xl font-semibold">Can’t register an account.</p>
            </div>
            <p className="mt-3 text-xl px-4 text-GrayMEA-700">
              Please make sure you allow permission
            </p>
            <p className="text-xl px-4 text-GrayMEA-700">
              to use an email for registration
            </p>
          </div>
          <button
            onClick={() => handleBackToApplication()}
            className="mt-6 w-full flex justify-center py-3 px-4 border-2 hover:text-black rounded-full shadow-sm text-xl font-semibold text-white bg-black hover:bg-white hover:border-black focus:outline-none disabled:opacity-50">
            Back to application
          </button>
        </div>) :
        <div className="flex flex-col items-center">
          <div className="w-24 h-24 mt-5 flex flex-col items-center">
            <img src={logo_mea} alt="logo" />
          </div>
            <p className="mt-1 text-OrangeMEA-EV text-2xl font-bold">Electric Vehical Station</p>
          <img src={loadings} className="animate-spin mt-20" alt="load" />
        </div>
      }
    </div>
  );
}

export default withRouter(CallBackPage);
