import React from "react"
import VerifyEmailCard from '../components/VerifyEmailCard'

const VerifyEmailPage = () => {
  return (
    <div>
      <VerifyEmailCard />
    </div>
  )
}

export default VerifyEmailPage