import React from "react"
import ForgotPasswordCard from '../components/ForgotPasswordCard'

const ForgotPasswordPage = () => {
  return (
    <div className="xl:px-2/6">
      <ForgotPasswordCard />
    </div>
  )
}

export default ForgotPasswordPage