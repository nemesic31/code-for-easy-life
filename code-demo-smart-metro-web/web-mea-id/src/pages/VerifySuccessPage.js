import React from "react"

import VerifySuccessCard from '../components/VerifySuccessCard'

const VerifySuccessPage = () => {
  return (
    <div className="xl:px-2/6">
      <VerifySuccessCard />
    </div>
  )
}

export default VerifySuccessPage