import React from 'react'
import RegisterCard from '../components/RegisterCard'


const RegisterPage = () => {
    
  return (
    <div className="xl:px-2/6">     
      <RegisterCard />
    </div>
  )
}

export default RegisterPage