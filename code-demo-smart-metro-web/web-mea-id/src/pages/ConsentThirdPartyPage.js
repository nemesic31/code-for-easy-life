import React from "react"

import ConsentThirdPartyCard from '../components/ConsentThirdPartyCard';

const ConsentThirdPartyPage = () => {
  return (
    <div className="xl:px-2/6">
      <ConsentThirdPartyCard />
    </div>
  )
}

export default ConsentThirdPartyPage