import React from "react"
import ChangePasswordCard from '../components/ChangePasswordCard'

const ChangePasswordPage = () => {
  return (
    <div className="xl:px-2/6">
      <ChangePasswordCard />
    </div>
  )
}

export default ChangePasswordPage