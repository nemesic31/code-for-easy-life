import React from "react"
import ConsentCard from "../components/ConsentCard"

const ConsentPage = () => {
  return (
    <div className="xl:px-2/6">
      <ConsentCard />
    </div>
  )
}

export default ConsentPage