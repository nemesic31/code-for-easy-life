import React from "react"
import UpdateProfileCard from '../components/UpdateProfileCard'

const UpdateProfilePage = () => {
  return (
    <div className="xl:px-2/6">
      <UpdateProfileCard />
    </div>
  )
}

export default UpdateProfilePage