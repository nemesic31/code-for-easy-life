import React from "react";
import MyProfileCard from '../components/MyProfileCard'

const MyProfilePage = () => {
  return (
    <div className="flex justify-center xl:px-2/6">
      <MyProfileCard />
    </div>
  )
}

export default MyProfilePage;