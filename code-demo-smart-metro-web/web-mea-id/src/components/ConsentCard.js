import React, { useState, useEffect } from "react"
import { withRouter } from 'react-router-dom';

import emergencyError from '../resource/icon/emergencyError.svg';

import { consents, updateConsent } from '../api/Pdpa.script';
import { createOtp } from '../api/Otp.script';
import loadings from '../resource/icon/loading.svg';



const ConsentCard = (props) => {
  const { userRefId, email } = props.location.state

  const [loading, setLoading] = useState(false);
  const [err, setError] = useState({ show: false, message: '', colorErr: '' });

  const [PDPA, setPDPA] = useState()
  const [PDPAStack, setPDPAStack] = useState(0)


  const getPDPA = async () => {
    setError({ show: false, message: '', colorErr: '' });
    try {
      const response = await consents();
      if (response.length > 0) {
        const responseConsent = {
          length: response.length,
          data: response.data.map((v) => ({
            version: v.version,
            type: v.type,
            name: v.name,
            displayName: v.displayNameEN,
            detail: v.detailEN,
            checked: v.required,
          })),
        };
        setPDPA(responseConsent)
      } else {
        setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
      }
    } catch (error) {
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }
  };

  const handleCheckConsent = async () => {
    const { data } = PDPA
    if (PDPAStack < PDPA.length - 1) {
      setPDPAStack(PDPAStack + 1)
      data[PDPAStack].checked = true
    } else if (PDPAStack === PDPA.length - 1) {
      data[PDPAStack].checked = true
      updateConsentPDPA()
    }
  }

  const handleUnCheckConsent = async () => {
    const { data } = PDPA
    if (PDPAStack < PDPA.length - 1) {
      setPDPAStack(PDPAStack + 1)
      data[PDPAStack].checked = false
    } else if (PDPAStack === PDPA.length - 1) {
      data[PDPAStack].checked = false
      updateConsentPDPA()
    }
  }

  const updateConsentPDPA = async () => {
    setLoading(true);
    setError({ show: false, message: '', colorErr: '' });
    try {
      const pdpaCheck = [];
      for (let i = 0; i < PDPA.data.length; i++) {
        const element = PDPA.data[i];
        if (element.checked === true) {
          pdpaCheck.push({
            version: element.version,
            type: element.type,
            name: element.name,
            detail: element.detail
          })
        }
      }
      const response = await updateConsent(userRefId, pdpaCheck)
      if (response.errorDescription) {
        setLoading(false);
        setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
      } else {
        const { userType, password } = props.location.state
        if (userType === 'MIGRATE_USER') {
          const otp_type = 'EMAIL'
          const responseOTP = await createOtp(userRefId, otp_type)
          if (responseOTP.errorDescription) {
            setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
          } else {
            const { ref } = responseOTP
            const location = {
              pathname: `${process.env.PUBLIC_URL}/verify-email`,
              state: { userRefId, email, ref, otp_type, userType },
            };
            props.history.push(location);
          }
        }
        else if (userType === 'SOCIAL_USER') {
          const { code, state, client_redirect_uri } = props.location.state
          if (client_redirect_uri) {
            const url = `${client_redirect_uri}?code=${code}&state=${state}`
            window.location.assign(url)
          } else {
            window.location.assign(`${process.env.PUBLIC_URL}/profile`)
          }
        }
        else {
          const otp_type = 'EMAIL'
          const responseOTP = await createOtp(userRefId, otp_type)
          if (responseOTP.errorDescription) {
            setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
          } else {
            const { ref } = responseOTP
            const location = {
              pathname: `${process.env.PUBLIC_URL}/verify-email`,
              state: userType ? { userRefId, email, ref, otp_type, userType, password } : { userRefId, email, ref, otp_type },
            };
            props.history.push(location);
          }
        }

      }
    } catch (error) {
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }
  }


  useEffect(() => {
    getPDPA()
  }, [])

  return (
    <div className="h-full">
      {loading === true &&
        <div>
          <div className="z-20 -my-10 w-full h-full flex flex-col justify-center items-center bg-GrayMEA-1000 absolute opacity-20">
            <img className="animate-spin" alt="loading_icon" src={loadings} />
          </div>
        </div>
      }
      <div className="flex flex-col justify-center w-full px-4 mt-4">
        {err.show && (
          <div className="flex w-full justify-center">
            <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
              <img alt="emergencyError" src={emergencyError} />
              <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
            </div>
          </div>
        )}

        <div >
          {PDPA &&
            <div>
              <div >
                <p className="font-bold text-3xl text-GrayMEA-1000 mt-0">
                  {PDPA.data[PDPAStack].displayName}
                </p>
                <div
                  className="text-2xl mt-4"
                  dangerouslySetInnerHTML={{
                    __html: PDPA.data[PDPAStack].detail
                  }}></div>
              </div>
            </div>
          }
          {PDPA && <div className="flex flex-col mb-4">
            {PDPA.data[PDPAStack].checked === false ? <div className="flex justify-between mt-5"><button onClick={() => handleUnCheckConsent()}
              className={`w-full h-11 flex mx-2 justify-center items-center py-2 px-4 border-2  focus:ring-black focus:ring-2 focus:border-black focus:border-2 focus:outline-none rounded-full text-2xl font-bold text-black bg-white`}
            >
              Skip
            </button>
              <button onClick={() => handleCheckConsent()}
                className={`w-full h-11 mx-2 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold text-white bg-black focus:outline-none`}
              >
                Agree
              </button></div> : <button onClick={() => handleCheckConsent()}
                className={`w-full h-11 mt-5 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold text-white bg-black focus:outline-none`}
              >
              Agree
            </button>}
          </div>}
        </div>
      </div>

    </div>
  )
}

export default withRouter(ConsentCard)