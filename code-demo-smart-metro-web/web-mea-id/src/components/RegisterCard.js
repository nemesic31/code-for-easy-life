import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom';
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import trailing from '../resource/icon/TrailingIcon.svg'
import correct from '../resource/icon/correct.svg';
import eye from '../resource/icon/eye.svg';
import eyeOff from '../resource/icon/eye-off.svg';
import emergency from '../resource/icon/emergency.svg';
import emergencyError from '../resource/icon/emergencyError.svg';


import auth from '../utils/auth';
import { checkUsername } from '../api/User.script';
import { register as registerAccount } from '../api/User.script';


const RegisterSchema = yup.object().shape({
  fullname: yup.string().required('please fill your fullname'),
  email: yup.string().required('please fill a valid email').email('please fill a valid email'),
  password: yup
    .string()
    .required('please fill password')
    .min(8, 'your password must be 8 characters')
    .matches(RegExp('(.*[a-z].*)'), 'include at least one lowercase letter')
    .matches(RegExp('(.*[A-Z].*)'), 'include at least one uppercase letter')
    .matches(RegExp('(.*\\d.*)'), 'include at least one number'),
  confirmPassword: yup
    .string()
    .required('please fill confirm password')
    .when('password', {
      is: (password) => (password && password.length > 0 ? true : false),
      then: yup.string().oneOf([yup.ref('password')], 'Please make sure password is match'),
    }),
});

const RegisterCard = (props) => {
  const params = new URLSearchParams(window.location.search)
  const client_id = params.get('client_id')
  const redirect_uri = params.get('redirect_uri')
  const scope = params.get('scope')
  const state = params.get('state')
  const meaid_idp_action = params.get('meaid-idp-action')
  const pathName = window.location.pathname;

  const [err, setError] = useState({ show: false, message: '', colorErr: '' });
  const [errEmail, setErrorEmail] = useState({ show: false, message: '' });
  const [statusEmail, setStatusEmail] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);


  const onClickShowPassword = (type) => {
    type === 'password'
      ? setShowPassword(!showPassword)
      : setShowConfirmPassword(!showConfirmPassword);
  };

  const {
    register,
    watch,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(RegisterSchema)
  });

  const onSubmit = async (values) => {
    setError({ show: false, message: ``, colorErr: '' })
    const { email, password, confirmPassword, gender, fullname } = values
    try {
      const response = await registerAccount(email, password, confirmPassword, gender, fullname)
      if (response.errorDescription) {
        if (response.code === "MEA0011") {
          setError({ show: true, message: `${response.errorDescription}`, colorErr: 'blue' })

        } else {
          setError({ show: true, message: `registration failed Please contact the service provider`, colorErr: 'red' })
        }
      } else {
        const responseCheck = await checkUsername(email)
        if (responseCheck.userRefId) {
          const { userRefId } = responseCheck
          const location = {
            pathname: `${process.env.PUBLIC_URL}/consent`,
            state: { userRefId, email },
          };
          props.history.push(location);
        } else {
          setError({ show: true, message: `registration failed Please contact the service provider`, colorErr: 'red' })
        }
      }
    } catch (error) {
      setError({ show: true, message: `registration failed Please contact the service provider`, colorErr: 'red' })
    }

  };


  const checkEmail = async (e) => {
    setErrorEmail({ show: false, message: `` })
    const emails = e.target.value
    const mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    try {

      const response = await checkUsername(emails)
      if (response.errorDescription) {
        if (emails.match(mailformat)) {
          setStatusEmail(true)
        } else {
          setStatusEmail(false)
        }
      } else {
        setStatusEmail(false)
        setErrorEmail({ show: true, message: `This email has already been registered` })
      }

    } catch (error) {
      console.log(error);
    }
  }


  const watchAllFields = watch();



  const email = register('email', { required: true })


  useEffect(() => {
    if (pathName === '/oauth/authorize') {
      if (client_id !== 'keycloak-fallback') {
        auth.setClientId(client_id, true);
        auth.setScope(scope, true);
        auth.setRedirectUri(redirect_uri, true)
        auth.setState(state, true)
        if (meaid_idp_action) {
          sessionStorage.setItem("meaid-idp-action", meaid_idp_action);
        }
      } else if (client_id === 'keycloak-fallback') {
        window.location.assign(`${process.env.PUBLIC_URL}/error-login-social`)
      }
    }
  }, [])

  return (
    <div className="flex flex-col px-5 w-full">
      {err.show && (
        <div className="flex w-full justify-center">
          <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
            <img alt="emergencyError" src={emergencyError} />
            <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
          </div>
        </div>
      )}
      <form onSubmit={handleSubmit(onSubmit)} className="mt-1">
        <div>
          <label className="block font-normal text-lg text-black ">Full name</label>
          <div className="mt-1 relative">
            <input
              type="text"
              name="fullname"
              id="fullname"
              className="shadow-sm text-2xl block focus:border-OrangeMEA-EV focus:ring-0 font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl p-3 border"
              placeholder="Full name"
              {...register("fullname", { required: true })}
            />
            {errors.fullname && <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <div className="flex flex-col">
                <img className="h-5 w-5" src={emergency} alt="emergency" />
                <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                  <svg className=" absolute left-28 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                      <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                        <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                          <g id="Group-2" transform="translate(24.000000, 0.000000)">
                            <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                  <p className="text-base font-bold text-RedMEA-primary">{errors.fullname.message}</p>
                </div>
              </div>
            </div>}
          </div>
        </div>
        <div className="mt-5">
          <label className="block font-normal text-lg text-black ">Gender</label>
          <div className="mt-1 relative">
            <div className="rounded-md shadow-sm">
              <div>
                <select
                  id="gender"
                  name="gender"
                  className="shadow-sm text-2xl block focus:border-OrangeMEA-EV focus:ring-0 font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl px-3 py-1 border"
                  {...register("gender", { required: true })}
                >
                  <option>Select gender</option>
                  <option>Male</option>
                  <option>Female</option>
                  <option>Gender-fluid</option>
                  <option>Prefer not to say</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <label className="block font-normal text-lg text-black ">Email</label>
          <div className="mt-1 relative">
            <input
              type="text"
              name="email"
              id="email"
              className="shadow-sm text-2xl block focus:border-OrangeMEA-EV focus:ring-0 font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl p-3 border"
              placeholder="example@example.com"
              onBlur={(e) => {
                email.onChange(e);
                checkEmail(e);
              }}
              ref={email.ref}
            />
            {errors.email ? <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none ">
              <div className="flex flex-col">
                <img className="h-5 w-5" src={emergency} alt="emergency" />
                <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                  <svg className=" absolute left-28 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                      <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                        <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                          <g id="Group-2" transform="translate(24.000000, 0.000000)">
                            <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                  <p className="text-base font-bold text-RedMEA-primary">{errors.email.message}</p>
                </div>
              </div>
            </div> :
              watchAllFields.email ?
                statusEmail === false ? errEmail.show ? <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                  <div className="flex flex-col">
                    <img className="h-5 w-5" src={emergency} alt="emergency" />
                    <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                      <svg className=" absolute left-28 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                        <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                          <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                            <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                              <g id="Group-2" transform="translate(24.000000, 0.000000)">
                                <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                      <p className="text-base font-bold text-RedMEA-primary">{errEmail.message}</p>
                    </div>
                  </div>
                </div> : <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                  <div className="flex flex-col">
                    <img className="h-5 w-5 animate-spin" src={trailing} alt="emergency" />
                  </div>
                </div> :
                  <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <div className="flex flex-col">
                      <img className="h-5 w-5" src={correct} alt="emergency" />
                    </div>
                  </div> : null}
          </div>
        </div>
        <div className="mt-5">
          <label className="block font-normal text-lg text-black ">Password</label>
          <div className="text-GrayMEA-600 font-normal my-2">
            Your password must be at least 8 characters, contain at least one lowercase letter, one uppercase letter, and a number.
          </div>
          <div className="mt-1 relative">
            <input
              type={showPassword ? 'text' : 'password'}
              name="password"
              id="password"
              className="shadow-sm text-lg focus:border-OrangeMEA-EV focus:ring-0 block font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl p-3 border"
              placeholder="Password"
              {...register("password", { required: true })}
            />
            {watchAllFields.password && (
              <div className="absolute inset-y-0 right-0 pr-3 flex items-center">
                <button type="button" className="focus:outline-none" onClick={() => onClickShowPassword('password')} >
                  <img alt="eye" src={showPassword ? eye : eyeOff} />
                </button>
              </div>
            )}
            {errors.password && <div className="absolute inset-y-0 right-8 pr-3 flex items-center pointer-events-none">
              <div className="flex flex-col">
                <img className="h-5 w-5" src={emergency} alt="emergency" />
                <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                  <svg className=" absolute left-44 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                      <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                        <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                          <g id="Group-2" transform="translate(24.000000, 0.000000)">
                            <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                  <p className="text-base font-bold text-RedMEA-primary">{errors.password.message}</p>
                </div>
              </div>
            </div>}
          </div>
        </div>
        <div className="mt-5">
          <label className={`block font-normal text-lg ${errors.fullname ? "text-RedMEA-primary" : "text-black"} `}>Confirm Password</label>
          <div className="mt-1 relative">
            <input
              type={showConfirmPassword ? 'text' : 'password'}
              name="confirmPassword"
              id="confirmPassword"
              className={`shadow-sm text-lg focus:border-OrangeMEA-EV focus:ring-0 block font-normal w-full h-10 ${errors.fullname ? "border-RedMEA-primary" : "border-GrayMEA-500"} placeholder-GrayMEA-500 rounded-xl p-3 border`}
              placeholder="Confirm Password"
              {...register("confirmPassword", { required: true })}
            />
            {watchAllFields.confirmPassword && (
              <div className="absolute inset-y-0 right-0 pr-3 flex items-center ">
                <button type="button" className="focus:outline-none" onClick={() => onClickShowPassword('confirmPassword')} >
                  <img alt="eye" src={showConfirmPassword ? eye : eyeOff} />
                </button>
              </div>
            )}
            {errors.confirmPassword && <div className="absolute inset-y-0 right-8 pr-3 flex items-center pointer-events-none">
              <div className="flex flex-col">
                <img className="h-5 w-5" src={emergency} alt="emergency" />
                <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                  <svg className=" absolute left-44 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                      <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                        <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                          <g id="Group-2" transform="translate(24.000000, 0.000000)">
                            <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                  <p className="text-base font-bold text-RedMEA-primary">{errors.confirmPassword.message}</p>
                </div>
              </div>
            </div>}
          </div>
        </div>

        <div className="mt-20">
          <button
            type="submit"
            disabled={watchAllFields.password && watchAllFields.confirmPassword && watchAllFields.email && watchAllFields.gender !== 'Select gender' && watchAllFields.fullname ? false : true}
            className={`w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold ${watchAllFields.password && watchAllFields.confirmPassword && watchAllFields.email && watchAllFields.gender !== 'Select gender' && watchAllFields.fullname ? "text-white bg-black" : "text-GrayMEA-500 bg-GrayMEA-300 "} focus:outline-none`}
          >
            Continue
          </button>
        </div>
      </form>
    </div>
  )
}

export default withRouter(RegisterCard)