import React from "react"
import { withRouter } from 'react-router-dom';

import success from '../resource/success.svg'

import auth from '../utils/auth';


const VerifySuccessCard = () => {
  const client_id = auth.getClientId()
  const scope = auth.getScope()
  const redirect_uri = auth.getRedirectUri()
  const state = auth.getState()

  return (
    <div>
      <div className="flex flex-col justify-center items-center w-full h-full mt-4 px-14">
        <div className="flex flex-col items-center">
          <p className="text-4xl text-black font-bold">
            Register Success
          </p>
          <p className="text-2xl text-GrayMEA-700 font-medium">
            go straight and have a safe trip :)
          </p>
        </div>
        <img className="mt-4 px-7 w-full" alt="success" src={success} />
        <button className="mt-2 bg-black focus:outline-none text-white w-full font-bold text-2xl rounded-full py-2"
          onClick={() => window.location.assign(`${client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&scope=${scope}&state=${state}&meaid-idp-action=login` : `${process.env.PUBLIC_URL}/`}`)}
        >
          Login
        </button>
      </div>
    </div>
  )
}

export default withRouter(VerifySuccessCard)