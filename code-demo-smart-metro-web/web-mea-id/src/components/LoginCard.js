import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { useForm } from "react-hook-form";

import logo_mea from '../resource/MEA_EV_Logo.svg';
import eye from '../resource/icon/eye.svg';
import eyeOff from '../resource/icon/eye-off.svg';
import emergencyError from '../resource/icon/emergencyError.svg';

import auth from '../utils/auth';

import { login, checkUsername } from '../api/User.script';
import { createOtp } from '../api/Otp.script';
import { verifyAccountThirdParty } from '../api/ThirdParty.script';
import { reset as resetPasswd } from '../api/ResetPassword.script';



const LoginCard = (props) => {
  const params = new URLSearchParams(window.location.search)
  const client_id = params.get('client_id')
  const redirect_uri = params.get('redirect_uri')
  const scope = params.get('scope')
  const state = params.get('state')
  const meaid_idp_action = params.get('meaid-idp-action')
  const pathName = window.location.pathname;

  const [err, setError] = useState({ show: false, message: '', colorErr: '' });
  const [checkForm, setCheckForm] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [userCheck, setUserCheck] = useState(false);

  const [usernameLogIn, setUsernameLogIn] = useState();
  const [userStatus, setUserStatus] = useState({});


  const onClickShowPassword = (type) => {
    type === 'password'
      && setShowPassword(!showPassword)
  };

  const {
    register,
    watch,
    handleSubmit
  } = useForm();

  const onSubmit = async (values) => {
    const { username, password } = values
    setError({ show: false, message: '', colorErr: '' });
    try {
      if (checkForm === false) {
        setError({ show: false, message: '' })
        if (username && !password) {
          if (username) {
            const response = await checkUsername(username)
            if (response.userRefId) {
              setUserStatus({ ...response })
              const { userRefId, requireConsent, requireOtpEmail, userType, socialOption } = response
              if (userType === 'MIGRATE_USER') {
                const email = username
                if (requireConsent === true) {
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/consent`,
                    state: { userRefId, email, userType },
                  };
                  props.history.push(location);
                } else if (requireOtpEmail === true) {
                  const otp_type = 'EMAIL'
                  const responseOTP = await createOtp(userRefId, otp_type)
                  if (responseOTP.errorDescription) {
                    setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
                  } else {
                    const { ref } = responseOTP
                    const location = {
                      pathname: `${process.env.PUBLIC_URL}/verify-email`,
                      state: { userRefId, email, ref, otp_type, userType },
                    };
                    props.history.push(location);
                  }
                } else {
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/change-password`,
                    state: { userRefId, email, userType }
                  };
                  props.history.push(location);
                }
              } else if (userType === 'SOCIAL_USER') {
                if (socialOption.length > 0) {
                  const socialName = socialOption[0]
                  setError({ show: true, message: `This is social acccount by ${socialName}. Please login with ${socialName} `, colorErr: 'blue' })
                }
              } else if (userType === 'NORMAL_USER') {
                setUserCheck(true)
                setUsernameLogIn(username)
              }
            } else {
              setError({ show: true, message: 'This account does not exist', colorErr: 'red' })
            }
          } else {
            setError({ show: true, message: 'This account does not exist', colorErr: 'red' });
          }
        }
      } else {
        setError({ show: false, message: '' })
        if (userCheck && password) {
          const email = usernameLogIn;
          const { userRefId, requireConsent, requireOtpEmail, userType } = userStatus
          const responseLogin = await login(email, password)
          if (responseLogin.accessToken) {
            if (pathName === '/oauth/authorize') {
              if (requireConsent === true) {
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/consent`,
                  state: { userRefId, email, userType, password },
                };
                props.history.push(location);
              } else if (requireOtpEmail === true) {
                const otp_type = 'EMAIL'
                const responseOTP = await createOtp(userRefId, otp_type)
                if (responseOTP.errorDescription) {
                  setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
                } else {
                  const { ref } = responseOTP
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/verify-email`,
                    state: { userRefId, email, ref, otp_type, userType, password },
                  };
                  props.history.push(location);
                }
              } else {
                const response = await verifyAccountThirdParty(client_id, scope, email, password, redirect_uri, state)
                if (response.errorDescription) {
                  if (response.errorDescription === "Consent Required") {
                    const consentApp = response.errorDetail.consents
                    const location = {
                      pathname: `${process.env.PUBLIC_URL}/consent-third-party`,
                      state: { consentApp, email, password },
                    };
                    props.history.push(location)
                  } else {
                    setError({ show: true, message: 'Email or Password is not correct', colorErr: 'red' });
                  }
                } else {
                  window.location.assign(`${response.redirectUrl}`)
                  console.log(response.redirectUrl);
                }
              }
            } else {
              const { accessToken, refreshToken } = responseLogin
              auth.setToken(accessToken, true);
              auth.setRefreshToken(refreshToken, true);
              if (requireConsent === true) {
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/consent`,
                  state: { userRefId, email, userType, password },
                };
                props.history.push(location);
              } else if (requireOtpEmail === true) {
                const otp_type = 'EMAIL'
                const responseOTP = await createOtp(userRefId, otp_type)
                if (responseOTP.errorDescription) {
                  setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
                } else {
                  const { ref } = responseOTP
                  const location = {
                    pathname: `${process.env.PUBLIC_URL}/verify-email`,
                    state: { userRefId, email, ref, otp_type, userType, password },
                  };
                  props.history.push(location);
                }
              } else {
                window.location.assign(`${process.env.PUBLIC_URL}/profile`)
              }
            }

          } else {
            setError({ show: true, message: 'Email or Password is not correct', colorErr: 'red' });
            setCheckForm(false)
          }
        }
      }
    } catch (error) {
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }
  };

  const watchAllFields = watch();

  const onForgetPassword = async () => {
    const email = usernameLogIn;
    try {
      if (email) {
        const response = await resetPasswd(email);
        if (response.errorDescription) {
          setError({ show: true, message: 'This account does not exist', colorErr: 'red' })
        } else {
          const { ref, userRefId } = response
          const location = {
            pathname: `${process.env.PUBLIC_URL}/forget-password-otp`,
            state: { ref, userRefId, email }
          };
          setError({
            show: false,
            message: '',
            colorErr: ''
          });
          props.history.push(location);
        }
      } else {
        window.location.assign(`${process.env.PUBLIC_URL}/forget-password`);
      }
    } catch (error) {
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }
  }

  useEffect(() => {
    if (pathName === '/oauth/authorize') {
      if (client_id !== 'keycloak-fallback') {
        auth.setClientId(client_id, true);
        auth.setScope(scope, true);
        auth.setRedirectUri(redirect_uri, true)
        auth.setState(state, true)
        if (meaid_idp_action) {
          sessionStorage.setItem("meaid-idp-action", meaid_idp_action);
        }
      } else if (client_id === 'keycloak-fallback') {
        window.location.assign(`${process.env.PUBLIC_URL}/error-login-social`)
      }
    } else {
      auth.clearToken();
      auth.clearRefreshToken();
      auth.clearClientId();
      auth.clearScope();
      auth.clearState();
      auth.clearRedirectUri();
    }
  }, [])

  return (
    <div className="flex flex-col items-center px-5">
      <div className="w-24 h-24 mt-5 flex flex-col items-center">
        <img src={logo_mea} alt="logo" />
      </div>
      <p className="mt-1 text-OrangeMEA-EV text-2xl font-bold">Electric Vehical Station</p>
      {err.show && (
        <div className="flex w-full justify-center">
          <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
            <img alt="emergencyError" src={emergencyError} />
            <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
          </div>
        </div>
      )}
      <form onSubmit={handleSubmit(onSubmit)} className={`${err.show ? 'mt-3 w-full' : 'mt-16 w-full'}`}>
        {!userCheck && <div>
          <label className="block font-normal text-lg text-black ">Email</label>
          <div className="mt-1">
            <input
              type="text"
              name="username"
              id="username"
              className="shadow-sm text-2xl block focus:border-black focus:ring-0 font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl p-3 border"
              placeholder="example@example.com"
              {...register("username", { required: true })}
            />
          </div>
        </div>}
        {userCheck && <div>
          <label className="block font-normal text-lg text-black ">Password</label>
          <div className="mt-1 flex justify-end items-center relative">
            <input
              type={showPassword ? 'text' : 'password'}
              name="password"
              id="password"
              className="shadow-sm text-lg focus:border-black focus:ring-0 block font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl p-3 border"
              placeholder="Password"
              {...register("password", { required: true })}
            />
            {watchAllFields.password && (
              <div className="absolute inset-y-0 right-0 pr-3 flex items-center">
                <button type="button" className="focus:outline-none" onClick={() => onClickShowPassword('password')} >
                  <img alt="eye" src={showPassword ? eye : eyeOff} />
                </button>
              </div>
            )}
          </div>
        </div>}
        <div className="mt-5">
          <div className="flex justify-end w-full my-1">
            <button type="button" onClick={() => onForgetPassword()}>
              <p className="text-lg font-normal text-OrangeMEA-EV">
                Forget password ?
              </p>
            </button>
          </div>
          {!userCheck ? <button
            onClick={() => setCheckForm(false)}
            type="submit"
            disabled={watchAllFields.username ? false : true}
            className={`w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold ${!watchAllFields.username ? "text-GrayMEA-500 bg-GrayMEA-300" : "text-white bg-black"} focus:outline-none`}
          >
            Next
          </button>
            :
            <button
              onClick={() => setCheckForm(true)}
              type="submit"
              disabled={watchAllFields.password ? false : true}
              className={`w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold ${!watchAllFields.password ? "text-GrayMEA-500 bg-GrayMEA-300" : "text-white bg-black"} focus:outline-none`}
            >
              Login
            </button>
          }
        </div>
      </form>
    </div>
  )
}

export default withRouter(LoginCard)