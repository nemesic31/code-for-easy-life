import React, { useState, useEffect } from "react";
import { withRouter } from 'react-router-dom'
import { useForm } from "react-hook-form";

import emergencyError from '../resource/icon/emergencyError.svg';

import { getProfileSocial, updateProfileSocial } from '../api/Social.script'
import auth from '../utils/auth';


const UpdateProfileCard = (props) => {
  const { userRefId, requireConsent, userType, code } = props.location.state
  const client_id = auth.getClientId()
  const scope = auth.getScope()
  const client_redirect_uri = auth.getRedirectUri()
  const state = auth.getState()

  const [err, setError] = useState({ show: false, message: '', colorErr: '' });
  const [profile, setProfile] = useState()

  const {
    register,
    watch,
    formState: { errors },
    handleSubmit,
  } = useForm();

  const getProfile = async () => {
    try {
      const responseProfile = await getProfileSocial(userRefId);
      if (responseProfile.error_description || responseProfile.errorDescription) {
        alert(responseProfile.error_description ? responseProfile.error_description : responseProfile.errorDescription);
        window.location.assign(`${client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${client_redirect_uri}&scope=${scope}&state=${state}&meaid-idp-action=login` : `${process.env.PUBLIC_URL}/`}`)
      } else {
        setProfile(responseProfile)
      }
    } catch (error) {
      console.log(error);
    }
  }

  const onSubmit = async (values) => {
    setError({ show: false, message: ``, colorErr: '' })
    const { gender, displayName } = values
    try {
      if (displayName) {
        const response = await updateProfileSocial(userRefId, displayName, gender)
        if (response.errorDescription) {
          setError({ show: true, message: `Update profile failed Please contact the service provider`, colorErr: 'red' })
        } else {
          if (requireConsent) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/consent`,
              state: client_redirect_uri ? { userRefId, userType, code, state, client_redirect_uri } : { userRefId, userType },
            };
            props.history.push(location);
          }
          else {
            const url = `${client_redirect_uri}?code=${code}&state=${state}`
            window.location.assign(url)
          }
        }
      } else {
        const response = await updateProfileSocial(userRefId, profile.displayName, gender)
        if (response.errorDescription) {
          setError({ show: true, message: `Update profile failed Please contact the service provider`, colorErr: 'red' })
        } else {
          if (requireConsent) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/consent`,
              state: client_redirect_uri ? { userRefId, userType, code, state, client_redirect_uri } : { userRefId, userType },
            };
            props.history.push(location);
          }
          else {
            const url = `${client_redirect_uri}?code=${code}&state=${state}`
            window.location.assign(url)
          }
        }
      }
    } catch (error) {
      setError({ show: true, message: `Update profile failed Please contact the service provider`, colorErr: 'red' })
      console.log(error);
    }

  };

  const watchAllFields = watch();

  useEffect(() => {
    getProfile()
  }, [])


  return (
    <div className="flex flex-col px-5 w-full">
      {err.show && (
        <div className="flex w-full justify-center">
          <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
            <img alt="emergencyError" src={emergencyError} />
            <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
          </div>
        </div>
      )}
      <form onSubmit={handleSubmit(onSubmit)} className="mt-1">
        {profile && <div>
          <label className="block font-normal text-lg text-black ">Full name</label>
          <div className="mt-1 relative">
            <input
              type="text"
              name="displayName"
              id="displayName"
              className={`shadow-sm text-2xl block focus:border-OrangeMEA-EV focus:ring-0 focus:placeholder-transparent font-normal w-full h-10 border-GrayMEA-500 ${profile.displayName ? 'placeholder-black' : 'placeholder-GrayMEA-500'} rounded-xl p-3 border`}
              placeholder={profile.displayName ? profile.displayName : "Full name"}
              {...register("displayName")}
            />
          </div>
        </div>}
        <div className="mt-5">
          <label className="block font-normal text-lg text-black ">Gender</label>
          <div className="mt-1 relative">
            <div className="rounded-md shadow-sm">
              <div>
                <select
                  id="gender"
                  name="gender"
                  className="shadow-sm text-2xl block focus:border-OrangeMEA-EV focus:ring-0 font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl px-3 py-1 border"
                  {...register("gender", { required: true })}
                >
                  <option>Select gender</option>
                  <option>Male</option>
                  <option>Female</option>
                  <option>Gender-fluid</option>
                  <option>Prefer not to say</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <label className="text-black font-normal text-lg">Email</label>
          <div className="bg-GrayMEA-200 px-3 py-2 mt-1 rounded-xl border-GrayMEA-500 border-2">
            <p className="text-GrayMEA-1000 font-normal text-2xl">{profile && profile.email}</p>
          </div>
        </div>
        <div className="mt-56">
          {profile && profile.displayName ?
            <button
              type="submit"
              disabled={watchAllFields.gender !== 'Select gender' ? false : true}
              className={`w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold ${watchAllFields.gender !== 'Select gender' ? "text-white bg-black" : "text-GrayMEA-500 bg-GrayMEA-300 "} focus:outline-none`}
            >
              Continue
            </button>
            :
            <button
              type="submit"
              disabled={watchAllFields.gender !== 'Select gender' && watchAllFields.displayName ? false : true}
              className={`w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold ${watchAllFields.gender !== 'Select gender' && watchAllFields.displayName ? "text-white bg-black" : "text-GrayMEA-500 bg-GrayMEA-300 "} focus:outline-none`}
            >
              Continue
            </button>
          }

        </div>
      </form>
    </div>
  )
}

export default withRouter(UpdateProfileCard);