import React, { useState, useEffect } from "react"

import { withRouter } from 'react-router-dom';

import emergencyError from '../resource/icon/emergencyError.svg';

import { getConsentThirdParty, consentsThirdParty } from '../api/ThirdParty.script';
import auth from '../utils/auth';

const ConsentThirdPartyCard = (props) => {

  const { consentApp, email, password } = props.location.state
  const client_id = auth.getClientId()
  const scope = auth.getScope()
  const redirect_uri = auth.getRedirectUri()
  const state = auth.getState()
  const [nameClient, setNameClient] = useState()

  const [err, setError] = useState({ show: false, message: '', colorErr: '' });


  const getConsent = async () => {
    try {
      const response = await getConsentThirdParty(client_id)
      setNameClient(response.name)
    } catch (error) {
      console.log(error);
    }
  }

  const handleAcceptWithThirdParty = async () => {
    try {
      setError({ show: false, message: '', colorErr: '' });
      const consentAccept = true
      const response = await consentsThirdParty(client_id, scope, email, password, redirect_uri, state, consentAccept)
      if (response.redirectUrl) {
        window.location.assign(`${response.redirectUrl}`)
      } else {
        setError({ show: true, message: 'an error occurred Please try again', colorErr: 'red' });
      }
    }
    catch (error) {
      alert(error)
    }

  }
  const handleDenyWithThirdParty = async () => {
    try {
      setError({ show: false, message: '', colorErr: '' });
      const consentAccept = false
      const response = await consentsThirdParty(client_id, scope, email, password, redirect_uri, state, consentAccept)
      if (response.redirectUrl) {
        window.location.assign(`${response.redirectUrl}`)
      } else {
        setError({ show: true, message: 'an error occurred Please try again', colorErr: 'red' });
      }
    }
    catch (error) {
      alert(error)
    }

  }

  useEffect(() => {
    getConsent()
  }, []);

  return (
    <div>
      <div className="flex flex-col justify-center w-full px-4 mt-10">
        {err.show && (
          <div className="flex w-full justify-center">
            <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
              <img alt="emergencyError" src={emergencyError} />
              <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
            </div>
          </div>
        )}
        <div >
          {consentApp &&
            <div>
              <div >
                <p className="font-bold text-3xl text-GrayMEA-1000 mt-0">
                  Consent to provide information to the application
                </p>
                <p className="text-xl">{nameClient ? nameClient : client_id} need your information as follows:</p>
                <div
                  className="text-2xl mt-4"
                  dangerouslySetInnerHTML={{
                    __html: consentApp
                  }}></div>
                <p className="text-xl mt-8">Will you allow the app to use this information?</p>
              </div>
            </div>
          }
          {consentApp && <div className="flex justify-between mt-5 ">
            <button onClick={() => handleDenyWithThirdParty()}
              className={`w-full h-11 mx-3 border-2 flex justify-center items-center py-2 px-4  focus:ring-black focus:ring-2 focus:border-black focus:border-2 focus:outline-none rounded-full text-2xl font-bold text-black bg-white`}
            >
              Deny
            </button>
            <button onClick={() => handleAcceptWithThirdParty()}
              className={`w-full h-11 mx-3 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold text-white bg-black focus:outline-none`}
            >
              Agree
            </button>
          </div>}
        </div>
      </div>

    </div>
  )
}

export default withRouter(ConsentThirdPartyCard)