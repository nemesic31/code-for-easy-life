import React, { useEffect, useState } from "react";
import { getProfile, logout } from '../api/User.script';
import auth from '../utils/auth';
import { withRouter } from 'react-router-dom';
import { checkUsername } from '../api/User.script';
import { createOtp } from '../api/Otp.script';


import loadings from '../resource/icon/loading.svg';
import logo_mea from '../resource/MEA_EV_Logo.svg';
import emergencyError from '../resource/icon/emergencyError.svg';

const MyProfileCard = (props) => {
  const token = auth.getToken();
  const [profile, setProfile] = useState();
  const [err, setError] = useState({ show: false, message: '', colorErr: '' });


  const handleLogOut = async () => {
    await logout();
    window.location.assign(`${process.env.PUBLIC_URL}/`);
  };

  const getProfileUser = async () => {
    try {
      const response = await getProfile(token);
      if (response.email) {
        const email = response.email
        const responseCheck = await checkUsername(email)
        if (responseCheck.userRefId) {
          const { userRefId, requireConsent, requireOtpEmail, userType, socialOption, requireUpdateProfile } = responseCheck

          if (requireUpdateProfile) {

            const location = {
              pathname: `${process.env.PUBLIC_URL}/update-profile`,
              state: { userRefId, userType, requireConsent },
            };
            props.history.push(location);

          }

          if (requireConsent === true) {
            const location = {
              pathname: `${process.env.PUBLIC_URL}/consent`,
              state: userType === 'SOCIAL_USER' ? { userRefId, userType } : { userRefId, email, userType },
            };
            props.history.push(location);
          } else if (requireOtpEmail === true) {
            if (userType === 'SOCIAL_USER') {
              setProfile(response)
            } else {
              const otp_type = 'EMAIL'
              const responseOTP = await createOtp(userRefId, otp_type)
              if (responseOTP.errorDescription) {
                setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
              } else {
                const { ref } = responseOTP
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/verify-email`,
                  state: { userRefId, email, ref, otp_type, userType },
                };
                props.history.push(location);
              }
            }
          } else {
            setProfile(response)
          }
        }
      } else {
        setError({ show: true, message: `Account not found`, colorErr: 'red' })
        alert('session expired')
        handleLogOut();
      }
    } catch (error) {
      console.log(error);
      alert('session expired')
      handleLogOut();
    }
  }

  useEffect(() => {
    getProfileUser();
  }, []);

  return (
    <div className="mt-8 w-max flex flex-col items-center">
      <div className="w-24 h-24 mt-5 flex flex-col items-center">
        <img src={logo_mea} alt="logo" />
      </div>
      <p className="mt-1 text-OrangeMEA-EV text-2xl font-bold">Electric Vehical Station</p>
      {err.show && (
        <div className="flex w-full justify-center">
          <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
            <img alt="emergencyError" src={emergencyError} />
            <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
          </div>
        </div>
      )}
      {profile ?
        (<div className="mt-3">
          <div className="flex justify-center">
            <span className="inline-block h-14 w-14 rounded-full overflow-hidden bg-gray-100">
              <svg className="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
                <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
              </svg>
            </span>
          </div>
          <div className="text-gray-700 text-3xl flex flex-col items-center justify-center font-semibold mt-7 w-max">
            <p>Email : {profile.email} </p>
            <p>Full Name : {profile.displayName}</p>
            <p>Gender : {profile.gender}</p>
          </div>
          <button
            onClick={() => handleLogOut()}
            className="w-full h-11 flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-2xl font-medium text-white bg-OrangeMEA-500 hover:bg-OrangeMEA-600 focus:outline-none mt-5"
          >
            Log Out
          </button>
        </div>
        ) : <div className="flex justify-center">
          <img src={loadings} className="animate-spin mt-20" alt="load" />
        </div>}
    </div>
  );
};


export default withRouter(MyProfileCard);