import React, { useState } from "react"
import { withRouter } from 'react-router-dom'
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import eye from '../resource/icon/eye.svg';
import eyeOff from '../resource/icon/eye-off.svg';
import emergency from '../resource/icon/emergency.svg';
import emergencyError from '../resource/icon/emergencyError.svg';

import { login } from '../api/User.script';
import { newPassword } from '../api/ResetPassword.script';
import { verifyAccountThirdParty } from '../api/ThirdParty.script';
import auth from '../utils/auth';



const NewPasswordSchema = yup.object().shape({
  password: yup
    .string()
    .required('please fill password')
    .min(8, 'your password must be 8 characters')
    .matches(RegExp('(.*[a-z].*)'), 'include at least one lowercase letter')
    .matches(RegExp('(.*[A-Z].*)'), 'include at least one uppercase letter')
    .matches(RegExp('(.*\\d.*)'), 'include at least one number'),
  confirmPassword: yup
    .string()
    .required('please fill confirm password')
    .when('password', {
      is: (password) => (password && password.length > 0 ? true : false),
      then: yup.string().oneOf([yup.ref('password')], 'Please make sure password is match'),
    }),
});

const ChangePasswordCard = (props) => {
  const { email, userRefId, userType } = props.location.state;
  const client_id = auth.getClientId()
  const scope = auth.getScope()
  const redirect_uri = auth.getRedirectUri()
  const state = auth.getState()

  const [err, setError] = useState({ show: false, message: '', colorErr: '' });
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const onClickShowPassword = (type) => {
    type === 'password'
      ? setShowPassword(!showPassword)
      : setShowConfirmPassword(!showConfirmPassword);
  };

  const {
    register,
    watch,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(NewPasswordSchema)
  });

  const onSubmit = async (values) => {
    setError({ show: false, message: ``, colorErr: '' })
    const { password, confirmPassword } = values;
    try {
      const response = await newPassword(userRefId, password, confirmPassword);
      if (response.errorDescription) {
        setError({ show: true, message: 'Forgot Password Request Expired', colorErr: 'red' });
      } else {
        if (userType === 'MIGRATE_USER') {
          if (client_id) {
            const response = await verifyAccountThirdParty(client_id, scope, email, password, redirect_uri, state)
            if (response.errorDescription) {
              if (response.errorDescription === "Consent Required") {
                const consentApp = response.errorDetail.consents
                const location = {
                  pathname: `${process.env.PUBLIC_URL}/consent-third-party`,
                  state: { consentApp, email, password },
                };
                props.history.push(location)
              } else {
                setError({ show: true, message: 'Email or Password is not correct', colorErr: 'red' });
              }
            } else {
              window.location.assign(`${response.redirectUrl}`)
            }
          } else {
            const responseLogin = await login(email, password)
            if (responseLogin.accessToken) {
              const { accessToken, refreshToken } = responseLogin
              auth.setToken(accessToken, true);
              auth.setRefreshToken(refreshToken, true);
              window.location.assign(`${process.env.PUBLIC_URL}/profile`)
            } else {
              console.log(response.errorDescription);
            }
          }
        } else {
          window.location.assign(`${client_id ? `${process.env.PUBLIC_URL}/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&scope=${scope}&state=${state}&meaid-idp-action=login` : `${process.env.PUBLIC_URL}/`}`)
        }
      }
    } catch (error) {
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }

  };

  const watchAllFields = watch();

  return (
    <div>
      <div className="h-full">
        <div className="px-5 mt-5">
          <div>
            <p className="text-black font-bold text-3xl">
              {userType ? 'Create Password' : 'Enter The New Password'}
            </p>
            <p className="text-GrayMEA-700 font-normal text-lg">
              {userType ? 'Your password must be 8 characters, and include at least one lowercase letter, one uppercase letter, and a number.' : 'Please set up your new password differently from the previous used password.'}</p>
          </div>
          {err.show && (
            <div className="flex w-full justify-center">
              <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
                <img alt="emergencyError" src={emergencyError} />
                <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
              </div>
            </div>
          )}
          <form onSubmit={handleSubmit(onSubmit)} className="mt-1 flex flex-col">
            <div>
              <div className={!err.show && `mt-5`}>
                <label className="block font-normal text-lg text-black ">Password</label>
                <div className="text-GrayMEA-600 font-normal my-2">
                  Your password must be 8 characters, and include at least one lowercase letter, one uppercase letter, and a number.

                </div>
                <div className="mt-1 relative">
                  <input
                    type={showPassword ? 'text' : 'password'}
                    name="password"
                    id="password"
                    className="shadow-sm text-lg focus:border-OrangeMEA-EV focus:ring-0 block font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl p-3 border"
                    placeholder="Password"
                    {...register("password", { required: true })}
                  />
                  {watchAllFields.password && (
                    <div className="absolute inset-y-0 right-0 pr-3 flex items-center">
                      <button type="button" className="focus:outline-none" onClick={() => onClickShowPassword('password')} >
                        <img alt="eye" src={showPassword ? eye : eyeOff} />
                      </button>
                    </div>
                  )}
                  {errors.password && <div className="absolute inset-y-0 right-8 pr-3 flex items-center pointer-events-none">
                    <div className="flex flex-col">
                      <img className="h-5 w-5" src={emergency} alt="emergency" />
                      <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                        <svg className=" absolute left-44 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                          <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                            <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                              <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                                <g id="Group-2" transform="translate(24.000000, 0.000000)">
                                  <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg>
                        <p className="text-base font-bold text-RedMEA-primary">{errors.password.message}</p>
                      </div>
                    </div>
                  </div>}
                </div>
              </div>
              <div className="mt-5">
                <label className={`block font-normal text-lg ${errors.fullname ? "text-RedMEA-primary" : "text-black"} `}>Confirm Password</label>
                <div className="mt-1 relative">
                  <input
                    type={showConfirmPassword ? 'text' : 'password'}
                    name="confirmPassword"
                    id="confirmPassword"
                    className={`shadow-sm text-lg focus:border-OrangeMEA-EV focus:ring-0 block font-normal w-full h-10 ${errors.fullname ? "border-RedMEA-primary" : "border-GrayMEA-500"} placeholder-GrayMEA-500 rounded-xl p-3 border`}
                    placeholder="Confirm Password"
                    {...register("confirmPassword", { required: true })}
                  />
                  {watchAllFields.confirmPassword && (
                    <div className="absolute inset-y-0 right-0 pr-3 flex items-center ">
                      <button type="button" className="focus:outline-none" onClick={() => onClickShowPassword('confirmPassword')} >
                        <img alt="eye" src={showConfirmPassword ? eye : eyeOff} />
                      </button>
                    </div>
                  )}
                  {errors.confirmPassword && <div className="absolute inset-y-0 right-8 pr-3 flex items-center pointer-events-none">
                    <div className="flex flex-col">
                      <img className="h-5 w-5" src={emergency} alt="emergency" />
                      <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                        <svg className=" absolute left-44 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                          <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                            <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                              <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                                <g id="Group-2" transform="translate(24.000000, 0.000000)">
                                  <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg>
                        <p className="text-base font-bold text-RedMEA-primary">{errors.confirmPassword.message}</p>
                      </div>
                    </div>
                  </div>}
                </div>
              </div>
              <div className={`flex justify-center ${err.show ? 'mt-48' : 'mt-60'} w-full mb-4`}>
                <button disabled={watchAllFields.password && watchAllFields.confirmPassword ? false : true} type="submit" className={`${watchAllFields.password && watchAllFields.confirmPassword ? 'bg-black text-white' : 'bg-GrayMEA-300 text-GrayMEA-500'} w-full py-2 rounded-full text-2xl font-bold focus:outline-none`}>
                  Confirm
                </button>
              </div>
            </div>
          </form>
        </div >
      </div >
    </div>
  )
}

export default withRouter(ChangePasswordCard)