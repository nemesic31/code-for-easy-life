import React from 'react';
import { withRouter } from 'react-router-dom';

import logo_google from '../resource/logo_google.svg';
import logo_facebook from '../resource/logo_facebook.svg';
import logo_apple from '../resource/logo_apple.svg';

import { startAuthenticate } from '../api/Social.script'
import apiconfig from '../api/config.json';
import auth from '../utils/auth';



const ButtonSocialCard = (props) => {
  const pathName = window.location.pathname;


  const handleSocialLogin = async (event) => {
    try {
      const data = event
      if (pathName === '/oauth/authorize') {
        const client_id = auth.getClientId()
        const scope = auth.getScope()
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (response.error_description || response.errorDescription) {
          alert(response.error_description ? response.error_description : response.errorDescription);
        } else {
          window.location.assign(response.redirectUrl)
        }
      } else {
        const client_id = apiconfig.mea_client_id
        const scope = apiconfig.mea_scope
        const redirect_uri = apiconfig.mea_redirect_uri
        const response = await startAuthenticate(data, client_id, scope, redirect_uri);
        if (response.error_description || response.errorDescription) {
          console.log(response.error_description ? response.error_description : response.errorDescription);
        } else {
          window.location.assign(response.redirectUrl)
        }
      }
    } catch (error) {
      console.log(error);
      alert(error)
    }
  }


  return (
    <div className="px-5">
      <div className="mt-4">
        <button
          type="submit"
          onClick={() => handleSocialLogin('google')}
          className=" w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold border-GrayMEA-300 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <div className="flex">
            <img src={logo_google} alt="logo_google" />
            <div className="ml-3">Continue with Google</div>
          </div>
        </button>
      </div>
      <div className="mt-3">
        <button
          type="submit"
          onClick={() => handleSocialLogin('facebook')}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold border-GrayMEA-300 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <div className="flex">

            <img src={logo_facebook} alt="logo_facebook" />
            <div className="ml-3">Continue with Facebook</div>
          </div>
        </button>
      </div>
      <div className="mt-3">
        <button
          type="submit"
          onClick={() => handleSocialLogin('apple')}
          className="w-full h-10 flex justify-center items-center py-2 px-4 border border-transparent rounded-full shadow-sm text-2xl font-bold border-GrayMEA-300 bg-white hover:border-8 hover:shadow-md focus:outline-none"
        >
          <div className="flex">

            <img src={logo_apple} alt="logo_apple" />
            <div className="ml-3">Continue with Apple</div>
          </div>
        </button>
      </div>
    </div>
  );
}

export default withRouter(ButtonSocialCard);
