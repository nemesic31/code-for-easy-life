import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import OtpInput from 'react-otp-input';
import useInterval from '@use-it/interval';

import resend_icon from '../resource/icon/resend_icon.svg';
import loadings from '../resource/icon/loading.svg';
import resendGray_icon from '../resource/icon/resendGray_icon.svg';

import { reset as resetPasswd, verify } from '../api/ResetPassword.script';


const ForgotPasswordOTPCard = (props) => {
  const { ref, userRefId, email } = props.location.state

  const [err, setError] = useState({ show: false, message: '', colorErr: '' });
  const [resend, setResend] = useState(false);
  const [loading, setLoading] = useState(false);

  const [otp, setOtp] = useState('');
  const [refNew, setRefNew] = useState()
  const [times, setTimes] = useState(180);

  const getOTP = async () => {
    try {
      const response = await resetPasswd(email);
      if (response.ref) {
        setError({ show: false, message: '', colorErr: '' });
        const { ref } = response
        setRefNew(ref)
      } else {
        setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
      }
    } catch (error) {
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }
  };

  const checkOTP = (value) => {
    sendOTP(value);
  };

  const sendOTP = async (value) => {
    setLoading(true);
    try {
      const code = value
      const lastRef = refNew ? refNew : ref
      const response = await verify(userRefId, code, lastRef);
      if (response.errorDescription) {
        setLoading(false);
        setOtp('');
        if (response.errorDescription === 'OTP Invalid') {
          setError({ show: true, message: 'Please make sure verification code is match', colorErr: 'red' });
        } else {
          setError({ show: true, message: 'error verification code Please check again', colorErr: 'red' });
        }
      } else {
        const location = {
          pathname: `${process.env.PUBLIC_URL}/change-password`,
          state: { userRefId, email }
        };
        setError({
          show: false,
          message: '',
          colorErr: ''
        });
        props.history.push(location);
      }
    } catch (error) {
      setLoading(false);
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }
  };

  const resendOTP = (seconds) => {
    setTimes(seconds);
    setResend(false);
    setOtp('');
    getOTP();

  };

  useInterval(() => {
    if (times === 0) {
      resetInterval();
    }
    setTimes((currentCount) => currentCount - 1);
  }, 1000);

  const resetInterval = () => {
    setResend(true);
    setTimes(0);
  };

  return (
    <div className="h-full">
      {loading === true &&
        <div className="z-20 -my-5 w-full h-full flex flex-col justify-center items-center bg-GrayMEA-1000 absolute opacity-20">
          <img className="animate-spin" alt="loading_icon" src={loadings} />
        </div>
      }
      <div className="px-5 mt-5">
        <div>
          <p className="text-black font-bold text-3xl">
            Verification Code
          </p>
          <p className="text-GrayMEA-700 font-normal text-lg">
            Please verify your identity to create the account, we'll send a verification code to your email address.
          </p>
        </div>
        <div className="mt-5">
          <label className="text-black font-normal text-lg">Email</label>
          <div className="bg-GrayMEA-200 px-3 py-2 mt-1 rounded-xl border-GrayMEA-500 border-2">
            <p className="text-GrayMEA-1000 font-normal text-2xl">{email && email}</p>
          </div>
        </div>
        <div className="mt-5 flex justify-center">
          <div className="relative">
            <OtpInput
              value={otp}
              onChange={(v) => {
                setOtp(v);
                setError({ show: false, message: '', colorErr: '' });
                if (v.length === 6) {
                  checkOTP(v);
                }
              }}
              numInputs={6}
              separator={<span className="text-white">-</span>}
              inputStyle={{
                width: '49px',
                height: '49px',
                borderColor: '#757575',
                borderWidth: '1px',
                borderRadius: '0.375rem',
                color: '#111111',
                fontSize: '28px'
              }}
              isInputNum={true}
            />
            {err.show &&
              <div className="absolute inset-y-0 right-0 mt-3 pr-3 flex items-center pointer-events-none">
                <div className="">
                  <div role="tooltip" className=" bg-RedMEA-Light w-max transition absolute duration-150 ease-in-out mt-2 right-0 top-9 shadow-lg p-2 rounded">
                    <svg className=" absolute left-52 -ml-2 bottom-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                      <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                        <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                          <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                            <g id="Group-2" transform="translate(24.000000, 0.000000)">
                              <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg>
                    <p className="text-base font-bold text-RedMEA-primary">{err.message}</p>
                  </div>
                </div>
              </div>
            }

          </div>
        </div>
        <div className={`flex justify-end ${err.show ? 'mt-14' : 'mt-5'}`}>
          {resend === true ?
            (<button className="flex border-GrayMEA-400 border-2 px-3 py-1 rounded-full items-center"
              onClick={() => resendOTP(180)}
            >
              <img className="ml-1 top-0 bottom-0" alt="resend_icon" src={resend_icon} />
              <p className="ml-2 font-bold text-black text-lg">
                Resend code
              </p>
            </button>) : (
              <div className="flex border-GrayMEA-400 border-2 px-3 py-1 rounded-full items-center">
                <img className="ml-1 top-0 bottom-0 animate-spin" alt="resend_icon" src={resendGray_icon} />
                <p className="ml-2 font-bold text-GrayMEA-500 text-lg">
                  Resend code ({times})
                </p>
              </div>
            )}

        </div>
      </div >
    </div >
  )
}

export default withRouter(ForgotPasswordOTPCard)