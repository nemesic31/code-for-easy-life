import React, { useState } from "react";
import { withRouter } from 'react-router-dom'
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import trailing from '../resource/icon/TrailingIcon.svg'
import emergency from '../resource/icon/emergency.svg';
import emergencyError from '../resource/icon/emergencyError.svg';
import correct from '../resource/icon/correct.svg';

import { reset as resetPasswd } from '../api/ResetPassword.script';
import { checkUsername } from '../api/User.script';

const ForgotPassSchema = yup.object().shape({
  email: yup.string().required('please fill a valid email').email('please fill a valid email'),
});

const ForgotPasswordCard = (props) => {
  const [err, setError] = useState({ show: false, message: '', colorErr: '' });
  const [errEmail, setErrorEmail] = useState({ show: false, message: '' });
  const [statusEmail, setStatusEmail] = useState(false);

  const {
    register,
    watch,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(ForgotPassSchema)
  });

  const onSubmit = async (values) => {
    const { email } = values
    try {
      const response = await checkUsername(email)
      if (response.errorDescription) {
        setError({ show: true, message: 'This account does not exist', colorErr: 'red' })
      } else {
        const { socialOption, userType } = response
        if (userType === 'SOCIAL_USER') {
          if (socialOption.length > 0) {
            const socialName = socialOption[0]
            setError({ show: true, message: `This is social acccount by ${socialName}. Please login with ${socialName} `, colorErr: 'blue' })
          }
        } else if (userType === 'MIGRATE_USER' || userType === 'NORMAL_USER') {
          const response = await resetPasswd(email);
          if (response.errorDescription) {
            setError({ show: true, message: 'This account does not exist', colorErr: 'red' })
          } else {
            const { ref, userRefId } = response
            const location = {
              pathname: `${process.env.PUBLIC_URL}/forget-password-otp`,
              state: { ref, userRefId, email }
            };
            setError({
              show: false,
              message: '',
              colorErr: ''
            });
            props.history.push(location);
          }
        }
      }
    } catch (error) {
      setError({ show: true, message: 'an error occurred Please contact the service provider', colorErr: 'red' });
    }
  }

  const checkEmail = async (e) => {
    setErrorEmail({ show: false, message: `` })
    const emails = e.target.value
    const mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    try {

      const response = await checkUsername(emails)
      if (response.errorDescription) {
        if (emails.match(mailformat)) {
          setStatusEmail(false)
          setErrorEmail({ show: true, message: `This account does not exist` })
        } else {
          setStatusEmail(false)
        }
      } else {
        setStatusEmail(true)
      }

    } catch (error) {
      console.log(error);
    }
  }

  const watchAllFields = watch();

  const email = register('email', { required: true })

  return (
    <div className="px-5 mt-5 h-full">
      <div>
        <p className="font-bold text-3xl">
          Reset password
        </p>
        <p className="font-normal text-lg text-GrayMEA-700">
          Please select which contact detail to reset your password.
        </p>
      </div>
      {err.show && (
        <div className="flex w-full justify-center">
          <div className={`flex ${err.colorErr === 'red' ? "bg-RedMEA-Light" : "bg-indigo-200"} px-4 py-2 rounded-md mb-4 w-full`}>
            <img alt="emergencyError" src={emergencyError} />
            <p className={`ml-2 font-normal ${err.colorErr === 'red' ? "text-black" : "text-indigo-600"} text-lg  `}>{err.message}</p>
          </div>
        </div>
      )}
      <div className={err.show ? `mt-1 h-full` : `mt-5 h-full`}>
        <form onSubmit={handleSubmit(onSubmit)} className="mt-1 flex flex-col">
          <div>
            <label className="block font-normal text-lg text-black ">Email</label>
            <div className="mt-1 relative">
              <input
                type="text"
                name="email"
                id="email"
                className="shadow-sm text-2xl block focus:border-OrangeMEA-EV focus:ring-0 font-normal w-full h-10 border-GrayMEA-500 placeholder-GrayMEA-500 rounded-xl p-3 border"
                placeholder="example@example.com"
                onChange={(e) => {
                  email.onChange(e);
                  checkEmail(e);
                }}
                ref={email.ref}
              />
              {errors.email ? <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none ">
                <div className="flex flex-col">
                  <img className="h-5 w-5" src={emergency} alt="emergency" />
                  <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                    <svg className=" absolute left-28 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                      <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                        <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                          <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                            <g id="Group-2" transform="translate(24.000000, 0.000000)">
                              <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg>
                    <p className="text-base font-bold text-RedMEA-primary">{errors.email.message}</p>
                  </div>
                </div>
              </div> :
                watchAllFields.email ?
                  statusEmail === false ? errEmail.show ? <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <div className="flex flex-col">
                      <img className="h-5 w-5" src={emergency} alt="emergency" />
                      <div role="tooltip" className="z-20 bg-RedMEA-Light w-max absolute transition duration-150 ease-in-out right-0 top-9 shadow-lg p-2 rounded">
                        <svg className=" absolute left-28 -ml-2 bottom-0 -top-5 h-full" width="12px" height="16px" viewBox="0 0 9 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                          <g id="Page-1" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                            <g id="Tooltips-" transform="translate(-874.000000, -1029.000000)" fill="#FFEDED">
                              <g id="Group-3-Copy-16" transform="translate(850.000000, 975.000000)">
                                <g id="Group-2" transform="translate(24.000000, 0.000000)">
                                  <polygon id="Triangle" transform="translate(4.500000, 62.000000) rotate(0.000000) translate(-4.500000, -62.000000) " points="4.5 57.5 12.5 66.5 -3.5 66.5" />
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg>
                        <p className="text-base font-bold text-RedMEA-primary">{errEmail.message}</p>
                      </div>
                    </div>
                  </div> : <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <div className="flex flex-col">
                      <img className="h-5 w-5 animate-spin" src={trailing} alt="emergency" />
                    </div>
                  </div> :
                    <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                      <div className="flex flex-col">
                        <img className="h-5 w-5" src={correct} alt="emergency" />
                      </div>
                    </div> : null}
            </div>
            <p className="font-normal text-lg text-GrayMEA-700 mt-1">
              Please enter email associate with your account, we will send link for setup a new password into your inbox.
            </p>
          </div>
          <div className="flex justify-center mt-72 w-full">
            <button disabled={statusEmail ? false : true} type="submit" className={`${statusEmail ? 'bg-black text-white' : 'bg-GrayMEA-300 text-GrayMEA-500'} w-full py-2 rounded-full text-2xl font-bold`}>
              Confirm
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default withRouter(ForgotPasswordCard);